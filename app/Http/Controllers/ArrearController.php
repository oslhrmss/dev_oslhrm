<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ArrearRequest;
use App\Model\EmpArrear;

class ArrearController extends Controller
{
    //

    public function index(Request $request)
    {
    	return view('arrears.add-arrear');
    }

    public function assignArrears(ArrearRequest $request)
    {
    	$validate =  $request->validate();
    	$response = EmpArrear::updateOrCreate(['emp_id'=>$request->emp_id,'month'=>$request->month,'year'=>\Carbon\Carbon::now()->format('Y')],[
    		'emp_id' => $request->emp_id,
    		'arrear_amount' => $request->amount,
    		'month' => $request->month,
    		'year' => \Carbon\Carbon::now()->format('Y'),
    		'reason' => $request->reason,
    	]);

    	return response()->json(['msg'=>'Arrears successfully assigned.'],200);
    }

    public function getArrears(Request $request)
    {
        $response = EmpArrear::select('e.name','e.emp_no','emp_arrears.arrear_amount','emp_arrears.month','emp_arrears.year','emp_arrears.reason')
                    ->join('employees as e','e.emp_no','=','emp_arrears.emp_id')
                    ->where(function($query){
                       if(auth()->user()->roles->first()->name != 'admin')
                       {
                        $query->where('e.company_id',\Utils::getCompanyId(auth()->user()->emp_code));
                       }
                    })
                    ->get();
        return response()->json($response,200);            
    }
}
