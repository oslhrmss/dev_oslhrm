<?php

namespace App\Http\Controllers;
use App\Model\Attendance;
use App\Model\Employee;
use App\Model\Holidays;
use App\Model\AttendanceAdjustment;

use Illuminate\Http\Request;


/**
*@author: Mustabshir
*
*
*/
class AttendanceController extends Controller
{
    //
    public $year = '';
    public $total_working_hour = 0;
    public $total_absent = 0;
    public $slate = 0;

    public function index(){
        return view('attendance.index');
    }

    public function auditAttendance(){
        return view('audit.index');
    }
    public function importView()
    {
    	return view('attendance.import');
    }

    public function companyWiseAttendance(Request $request)
    {
        return view("attendance.company-wise-attendance");
    }
    public function payrollAttendanceData(Request $request)
    {
        return view("attendance.payroll-attendance");
    }
    public function employeeAttendance(Request $request)
    {

        if($request->employee){
           $employee_id = $request->employee;
       }else{
           $employee_id = \Auth::user()->emp_code; 
       }
       $response[] = \Utils::attendanceData($request->from,$request->to,$employee_id, $attendance_log = false);
       return response()->json($response,200);
   }


   public function modifyManualAttendance(Request $request)
   {
        $rules = [
            'time_in' => 'required',
            'time_out' => 'required',
            'reason' => 'required | max:100'
        ];

        $messages = [
            'time_in.required' => 'Please enter time in value',
            'time_out.required' => 'Please enter time out value',
            'reason.required' => 'Please enter reason ',
            'reason.max' => 'Please enter reason must be 100 character long. ',
        ];


        $this->validate($request, $rules, $messages);
        if($request->atten_id)
        {
              $attendance = AttendanceAdjustment::where('atten_id', $request->atten_id)->first();  
              if($attendance){
                  $attendance->time_in = ($request->time_in != '00:00:00') ? $request->time_in : $attendance->time_in;
                  $attendance->time_out = ($request->time_out != '00:00:00') ? $request->time_out : $attendance->time_out;
                  $attendance->reason=$request->reason;
                  $attendance->adjustment_type='manual';
                  $attendance->adjusted_by= \Auth::user()->id;
                  $attendance->save();
              }else{
               $attendance =  new AttendanceAdjustment;
               $attendance->atten_id =  $request->atten_id;
               $attendance->time_in  = $request->time_in;
               $attendance->time_out = $request->time_out;
               $attendance->date     = $request->date;
               $attendance->reason   = $request->reason;
               $attendance->month   = \Carbon\Carbon::parse($request->date)->format('M');
               $attendance->adjustment_type ='manual';
               $attendance->adjusted_by= \Auth::user()->id;
               $attendance->save();

              }
           $response = $attendance;
        
        }else{

            $attendance_new =  new Attendance;
            $attendance_new->emp_id = $request->empcode;
            $attendance_new->time_in = "00:00:00";
            $attendance_new->time_out = "00:00:00";
            $attendance_new->date = $request->date;
            $attendance_new->month = \Carbon\Carbon::parse($request->date)->format('M');
            $attendance_new->save();

            // add record in attendance adjustment record
            $attendance_adj = new AttendanceAdjustment;
            $attendance_adj->atten_id =  $attendance_new->id; 
            $attendance_adj->time_in  = $request->time_in;
            $attendance_adj->time_out = $request->time_out;
            $attendance_adj->date     = $request->date;
            $attendance_adj->reason   = $request->reason;
            $attendance_adj->month   = \Carbon\Carbon::parse($request->date)->format('M');
            $attendance_adj->adjustment_type ='manual';
            $attendance_adj->adjusted_by= \Auth::user()->id;
            $attendance_adj->save();
            $response = $attendance_adj;
        } 
        
        return response()->json($response, 200);

    }


    public function getTimeInTimeOutValues(Request $request)
    {
        $values =  Attendance::where('id', $request->id)->first();
        return response()->json($values, 200);

    }

    public function companyAttendance(Request $request)
    {
        $employees = Employee::where('company_id', $request->company)->get();
        foreach($employees as $employee){
          if($employee->is_active){
            
            $response[] = \Utils::attendanceData($request->from,$request->to,$employee->emp_no, $attendance_log = false);        
          }
        }
        return response()->json($response,200);
    }

    public function getPayrollAttendanceData(Request $request)
    {

      set_time_limit(0);
      $employeeCode = Employee::select('emp_no')->where('company_id', $request->sub_company_id)->get();
      foreach ($employeeCode as $key => $value) {
        # code...
        $response = \Utils::attendanceData($request->from,$request->to,$value->emp_no, $attendance_log = true); 
      }

      return response()->json(['msg'=>'Data generated successfully for payroll.'],200);
    }

    public function modifiedAttendance(Request $request)
    {
      return view('attendance.modified-timesheet');
    }

    public function getModifiedAttendance(Request $request)
    {
      $response =  AttendanceAdjustment::search($request->all());
      return response()->json($response,200);
    }

    public function deleteModifiedAttendance(Request $request)
    {


      $response =  AttendanceAdjustment::deleteModifiedAttendance($request->id);
      return response()->json($response,200);
    //   if($response)
    //   {
    // //    Session::flash('message', "Special message goes here");
    //     session()->flash('success','Deleted Successfully.');
    //   }else{
    //     session()->flash('error','Something Went wrong');
    //   }
    //   return redirect()->back();
    }

}

