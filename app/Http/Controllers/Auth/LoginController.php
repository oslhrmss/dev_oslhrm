<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Session;
use App\Model\User;
use App\Repositories\EmployeeRepository;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;
    private $empRepo;
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    
    // protected function sendLoginResponse(Request $request)
    // {
    //   if($this->guard()->user()->is_active == 0){
    //     $this->guard()->logout();
    //     return redirect()->route('custom.password.change');
    //     }
    //     // $request->session()->regenerate();

    //     // $this->clearLoginAttempts($request);

    //     // return $this->authenticated($request, $this->guard()->user())
    //     //         ?: redirect()->intended($this->redirectPath());
    // }
    protected function authenticated(Request $request, $user)
    {
        //
            if($user->is_active != 1)
            {

              return redirect()->route('change.password');
            }
            if($this->empRepo->getEmpStatus($user->emp_code) == 0 && $user->roles->first()->name != 'admin')
            {

                $this->logout($request);
                return redirect()->back()
                    ->withErrors(['active' => 'Your account has been deactivated.']);
            }
           // return $user->emp_code;
            echo $this->birthDayMessage($user->emp_code,$request);

    }
    protected $redirectTo = '/home';
    
    protected function credentials(Request $request)
    {
            if(is_numeric($request->get('emp_code'))){
              return ['emp_code'=>$request->get('emp_code'),'password'=>$request->get('password')];
            }

            return $request->only($this->username(), 'password'); 
    }

    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            $this->username() => 'required|numeric',
            'password' => 'required|string',
        ]);
    }    
    
    public function username()
    {
        return 'emp_code';
    }

    public function defualt_password()
    {
        return 'osl123';
    }    

    public function birthDayMessage($empcode, $request)
    {

        $dob = \App\Model\Employee::select('dob','name')->where('emp_no',$empcode)->first();

        if($dob){
            if($dob->dob != ""){
                $dobs = explode("-",$dob->dob);

                if($dobs[1].'-'.$dobs[2] == date('m-d'))
                {
                    $request->session()->put('bday-message', "Dear, ".$dob->name."\n Happy Birthday from OSL !");

                }else{
                    return true;
                }
            }else{
                return true;
            }
        }
        else{
            return true;
        }
    }     
            
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(EmployeeRepository $empRepo)
    {
        $this->middleware('guest')->except('logout');
        $this->empRepo =  $empRepo;
    }
}
