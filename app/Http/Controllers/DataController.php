<?php

/**
*@author:  Mustabshir Khan <mustabshir.aziz@netsoltech.com>
*@version: v1.0
*@return : Attendance import with various condition already exist data, empty time in time out handling , adjustment handling and delete adjustment if alrady exist 
*/


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Excel;
use App\Model\Attendance;
use App\Model\Employee;
use App\Model\Role;
use App\User;
use App\Model\AttendanceAdjustment;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use App\Http\Traits\GenerateUserTrait;
use App\Utilities\Utils;
use PDF;
class DataController extends Controller
{
    //
use GenerateUserTrait;
public function import(Request $request)
{
    $path  =  storage_path().'\data.xlsx';
    
    $filedata =  Excel::load($path, function ($reader) {
    })->get();

   //$dt = new DateTime('2012-07-24 11:52:01');
 
   foreach($filedata as $key => $value)
    {
        //echo $value['d.o.j'].'<br>';
         $pieces = explode(" ", $value['d.o.j']);
        $check_date = explode(" ",$value->checktime);        
        $newdata[] = ['emp_no' => $value->employee_id, 
        'name' => $value->employee_name,
        'father_name' => $value->father_name,
        'date_of_hiring' => $pieces[0],

        ];
              
    }
    //return $newdata;

   Employee::insert($newdata);
     return 'Save';

    
}

    public function import2(Request $request)
    {
        ini_set('max_execution_time', '3600');
        $array1 = [];
        
        //$path  =  storage_path().'\CHECKINOUT.xlsx';
        $path = $request->file('atten_file')->getRealPath();
        $filedata =  Excel::load($path, function ($reader) {
        })->get();
       // return $filedata;
        foreach($filedata as $key => $value)
        {
            $time = explode(" ",$value->time); // convert time in array
            $date =  str_replace("/","-",$time[0]); // replace / with - in date
            $date = explode("-",$date); // convert date in array for formatting          
            
            // date creation
            $newDate = date('Y-m-d',strtotime($date[2].'-'.$date[1].'-'.$date[0])); // with format Year-month-day
            
            // time creation from  00:00 AM PM
            $time = $time[1].' '.$time[2];
            $finalTime = date('H:i:s',strtotime($time)); // with format 00:00:00        
            
        
            $array[] = ['userid' => $value->get('ac_no.'),'date'=>$newDate,'time'=>$finalTime];
        }
        
        Schema::create('temp', function(Blueprint $table){
            $table->integer('userid');
            $table->date('date');
            $table->time('time');
            $table->temporary();
        });

        \DB::table('temp')->insert($array);
        $data = \DB::table('temp')->select('userid as emp_id',\DB::raw('MIN(time) as time_in'),\DB::raw('MAX(time) as time_out'),'date')->groupBy('userid','date')->get();
 
         $sys_param = \DB::table('app_param')->get();
         foreach ($sys_param as $key => $value) {
                 # code...
                $params[$value->config_key] = $value->config_value;
             }    
             //return $params['late_attendance'];
         $allSubCompany = \App\Model\SubCompany::pluck('id')->toArray();    
        foreach($data as $key => $value)
        {   
            $attendance = Attendance::where('emp_id',$value->emp_id)->where('date',$value->date)->first();


            if(date('l',strtotime($value->date)) == Utils::getSubCompanyAppParamValue('half_day',Utils::getCompanyId($value->emp_id), Utils::getRegionId($value->emp_id)))
            {
             $no_time_out_adjustment_value = Utils::getSubCompanyAppParamValue('sat_time_out', Utils::getCompanyId($value->emp_id),Utils::getRegionId($value->emp_id));
            }
            else{
             $no_time_out_adjustment_value = Utils::getSubCompanyAppParamValue('no_time_out_adjustment_value', Utils::getCompanyId($value->emp_id),Utils::getRegionId($value->emp_id));
             //$no_time_out_adjustment_value = $params['no_time_out_adjustment_value'];
            }   

// assign values in time_in and tim_out variable   
            if($value->time_in == $value->time_out)
            {

                if($value->time_in <= $params['no_time_in_adjustment_value'] ){
                    $time_in = $value->time_in;
                }else{
                    $time_in = "00:00:00";
                }
                if($value->time_out > $params['no_time_in_adjustment_value'] ){
                    $time_out = $value->time_out;
                }else{
                    $time_out = "00:00:00";
                } 
            }else{
                $time_in = $value->time_in; 
                $time_out = $value->time_out; 
            }

// adding new record            
if($attendance == null){
                 $atten = new Attendance;
                 $atten->emp_id = $value->emp_id;
                 $atten->time_in = $time_in;
                 $atten->time_out = $time_out;
                 $atten->date = $value->date;
                 $atten->month = date('F',strtotime($value->date));
                 $atten->save();

// set time_in system generate entry in adjustment table   


                 if($time_in == "00:00:00" && $time_out != "00:00:00")
                 {

                    // for dubai if time in is blank

                    if(in_array(Utils::getCompanyId($value->emp_id), Utils::allParamsCompany(Utils::getAppParamsConfigKey()['no_time_in_adjustment_value'])))
                    {
                        $time_in = Utils::getSubCompanyAppParamValue('no_time_in_adjustment_value', Utils::getCompanyId($value->emp_id),Utils::getRegionId($value->emp_id));
                    }else{
                      //  $time_in = $params['no_time_in_adjustment_value'];
                        $time_in = Utils::getSubCompanyAppParamValue('no_time_in_adjustment_value',Utils::getCompanyId($value->emp_id),Utils::getRegionId($value->emp_id));
                    }



                    $att_adjustment = new AttendanceAdjustment;
                    $att_adjustment->atten_id = $atten->id;    
                    $att_adjustment->adjustment_type = 'system';    
                    $att_adjustment->time_in = $time_in;  
                    $att_adjustment->time_out = $time_out;
                    $att_adjustment->date = $value->date;
                    $att_adjustment->month =  date('F',strtotime($value->date));
                    $att_adjustment->adjusted_at =  \Carbon\Carbon::now();
                    $att_adjustment->adjusted_by =  \Auth::user()->id;
                    $att_adjustment->save();
                        
                 }
// set time_out system generate entry in adjustment table               
                 if($time_in != "00:00:00" && $time_out == "00:00:00")
                 {
                    if(in_array(Utils::getCompanyId($value->emp_id), Utils::allParamsCompany(Utils::getAppParamsConfigKey()['no_time_out_adjustment_value'])))
                    {
                        if(date('l',strtotime($value->date)) == Utils::getSubCompanyAppParamValue('half_day',Utils::getCompanyId($value->emp_id),Utils::getRegionId($value->emp_id)))
                        {

                         $no_time_out_adjustment_value = Utils::getSubCompanyAppParamValue('sat_time_out', Utils::getCompanyId($value->emp_id),Utils::getRegionId($value->emp_id));
                         $time_out = $no_time_out_adjustment_value;
                     }else{
                        //$no_time_out_adjustment_value = $params['no_time_out_adjustment_value'];
                        $time_out = Utils::getSubCompanyAppParamValue('no_time_out_adjustment_value', Utils::getCompanyId($value->emp_id),Utils::getRegionId($value->emp_id));
                    }
                        // $time_out = Utils::getSubCompanyAppParamValue('no_time_out_adjustment_value', Utils::getCompanyId($value->emp_id));

                    }else{
                        $time_out = $no_time_out_adjustment_value;
                    }

                    $att_adjustment = new AttendanceAdjustment;
                    $att_adjustment->atten_id = $atten->id;    
                    $att_adjustment->adjustment_type = 'system';    
                    $att_adjustment->time_in = $time_in;    
                    $att_adjustment->time_out = $time_out;
                    $att_adjustment->date = $value->date;
                    $att_adjustment->month =  date('F',strtotime($value->date));
                    $att_adjustment->adjusted_at =  \Carbon\Carbon::now();
                    $att_adjustment->adjusted_by =  \Auth::user()->id;
                    $att_adjustment->save();
                 }

            }
            else{
                              
// upadting record if already exist in atredance table with the corresponding date.   
                if($attendance->time_in != "00:00:00" ){
                    if($time_in < $attendance->time_in)
                    {
                        $time_in = $time_in;
                    }else{
                        $time_in = $attendance->time_in;
                    } 
                }

                if($attendance->time_out != "00:00:00")
                {
                    if($time_out > $attendance->time_out)
                    {
                        $time_out = $time_out;
                    }else{
                        $time_out = $attendance->time_out;
                    }
                }
                $attendance->emp_id = $value->emp_id;
                $attendance->date = $value->date;
                $attendance->time_in =  $time_in;
                $attendance->time_out = $time_out;
                $attendance->month = date('F',strtotime($value->date));
                $attendance->save();

// checking attendance id  and delete when time_in time_out come up in next sheet
            if($attendance->time_in != "00:00:00" && $attendance->time_out != "00:00:00")
            {  

                if(AttendanceAdjustment::where('atten_id', $attendance->id)->where('date',$attendance->date)->where('adjustment_type', 'system')->exists())
                {
                    AttendanceAdjustment::where('atten_id', $attendance->id)->where('date',$attendance->date)->delete();
                }
            }

            }
            
        }

        \DB::table('attendance')->insert($array1);

        Schema::drop('temp');
        ini_set('max_execution_time', 60);
        return redirect()->back()->with('import','Imported Succesfully');

    }


    public function bulkCredentialsGenerate()
    {
        $allEmp = Employee::all();
        $role = Role::where('name','employee')->get()->first();
        foreach($allEmp as $value)
        {
           $user = User::firstOrNew(['emp_code'=>$value->emp_no],[
            'name' => $value->name,
            'email' => !is_null($value->p_email)  ? $value->p_email : $value->emp_no.'@star-liners.com',
            'emp_code' => $value->emp_no,
            'password' => bcrypt('osl123'),
           ]);
            $user->save();
            $user->attachRole($role);
        }

        return "Credentials created succesfully!";
    }

    public function getPDF(Request $request)
    {
        $data  =  \App\Model\Payroll::select('sc.name as company','d.name as department','et.value as emptype','des.name as designation','payroll.*')
                  ->join('employees as e','e.emp_no','=','payroll.emp_id')  
                  ->join('sub_company as sc','sc.id','=','e.company_id')  
                  ->join('department as d','d.id','=','e.dept_name')  
                  ->join('emp_type as et','et.id','=','e.emp_type_id')
                  ->join('designation as des','des.id','=','e.position_title')
                  ->where('payroll.emp_id','=', auth()->user()->emp_code)
                  ->where('payroll.month',$request->month)
                  ->where('payroll.year',$request->year)
                  ->first();
        if($data){
            
        $pdf = PDF::loadView('pdf.salaryslip',['data'=>$data]);
        return $pdf->download($data->month.'-'.$data->year.'.pdf');
//        return view('pdf.employee',['data'=>$data]);
        }          
    }
}
