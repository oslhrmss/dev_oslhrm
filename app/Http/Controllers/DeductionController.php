<?php

namespace App\Http\Controllers;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use App\Model\Loan;
use App\Model\LoanLedger;
use App\Model\LoanRelaxation;
use App\Model\LoanMeta;
use App\Model\Employee;
use App\Model\LeaveDeduction;
use DB;
use Cache;
use Illuminate\Support\Facades\Log;
class DeductionController extends Controller
{
    //

    public function index(Request $request)
    {
    	return view('deductions.index');
    }

    public function employeeList($company_id)
    {
       // \DB::enableQueryLog();
      
        return  $employeeList = \DB::table('loan as l')
                        ->select('e.name','l.id','l.emp_id','l.amount','l.monthly_deduction_amount',\DB::raw('l.amount - (SELECT IFNULL(SUM(deduction_amount),0) FROM loan_ledger WHERE loan_id = l.id) as balance'),\DB::raw('MONTHNAME(l.apply_at) as month'),'l.apply_at','l.no_of_installments as installments')
                        ->join('employees as e','e.emp_no','=' ,'l.emp_id')
                        ->where('l.is_paid',1) 
                        ->where('is_active',1)
                        ->where('e.company_id',$company_id)             
                        ->orderBy('l.id','asc')
                        ->get();
       // echo print_r(\DB::getQueryLog());
                
    }

    public function loanExistsInRelaxation($loanid,$month)
    {   
        $cur_year = date('Y');
        return LoanRelaxation::where('loan_id',$loanid)->where('month',$month)->where('loan_year',$cur_year)->exists();
    }

    public function employeeListWhoseBalancNonZero(Request $request)
    {
        $array = [];                       
        $month = $request->month;
        foreach($this->employeeList($request->company_id) as $employee)
        {
            Log::info('Emp list:  ' . $this->employeeList($request->company_id));
            if($employee->balance > 0 ){          //|| $employee->balance == NULL
            $array[] = [
                'emp_no'=> $employee->emp_id,
                'name'=> $employee->name,
                'amount'=> $employee->amount,
                'monthly_deduction_amount'=> $employee->monthly_deduction_amount,
                'month'=>$month,
                'balance' => is_null($employee->balance) ? $employee->amount : $employee->balance,
                'apply_at'=> $employee->apply_at,
                'loan_id' => $employee->id,
                'is_relaxed' => $this->loanExistsInRelaxation($employee->id, $month) ? 1 : 0 , 
                'installments' => $employee->installments
            ];          
            }   
        }
        session()->put('company', $request->company_id);
        session()->put('month', $month);
        return response()->json($array,200);                

    }

    public function loanRelaxation(Request $request, $loanid)
    {
        $cur_year = date('Y');
        $relax  = LoanRelaxation::updateOrCreate(['month'=>session()->get('month'),'loan_id'=>$loanid],['loan_id'=>$loanid,'month'=>session()->get('month'),'loan_year'=>$cur_year,'relaxed_at'=>\Carbon\Carbon::now()->format('Y-m-d')]);
        return response()->json($relax,200);

    }

    public function removeLoanRelaxation(Request $request, $loanid)
    {
        $removeRelax  = LoanRelaxation::where('month',session()->get('month'))->where('loan_id',$loanid)->delete();      
        return response()->json($removeRelax,200);
    }

    public function loanDeduct(Request $request)
    {
         # code...
        foreach($this->employeeList(session()->get('company')) as $employee)
        {
            if($employee->balance > 0 || $employee->balance == NULL ){
                $balance = is_null($employee->balance) ? $employee->amount : $employee->balance; 
                if($this->loanExistsInRelaxation($employee->id,session()->get('month')))
                {
                    $deduction_amount = 0;  
                }else{
//                    $deduction_amount = (LoanLedger::where('loan_id', $employee->id)->count() == ($employee->installments - 1)) ? $balance : $employee->monthly_deduction_amount;

                    $deduction_amount = ($employee->monthly_deduction_amount >= $balance) ? $balance : $employee->monthly_deduction_amount;
                }

                // amoutn edit check
                $cur_year = date('Y');
                if(\DB::table('loan_meta')->where('loan_id', $employee->id)->where('month',session()->get('month'))->where('loan_year',$cur_year)->exists())
                {
                    $deduction_amount = \DB::table('loan_meta')->where('loan_id', $employee->id)->where('month', session()->get('month'))->where('loan_year',$cur_year)->orderBy('id','desc')->limit(1)->value('deduction_amount');
                }else{
                    $deduction_amount = $deduction_amount;
                }

                $array[]= [
                    'loan_id'=>$employee->id,
                    'deduction_amount'=> $deduction_amount,
                    'month'=>session()->get('month'),
                    'loan_year'=>$cur_year,
                    'date'=>\Carbon\Carbon::now()->format('Y-m-d'),
                    'balance'=>round($balance - $deduction_amount),
                    'installments' => $employee->installments
                    
                ];
            }
        }
        session()->forget('company');
        foreach ($array as $key => $value) {
            # code...
            $cur_year = date('Y');
            if(!LoanLedger::where('month',$value['month'])->where('loan_id',$value['loan_id'])->where('loan_year',$cur_year)->exists()){
            $ledger =  LoanLedger::create([
                    'loan_id'=>$value['loan_id'],
                    'deduction_amount'=>$value['deduction_amount'],
                    'month'=>$value['month'],
                    'loan_year'=>date('Y'),
                    'date'=>\Carbon\Carbon::now()->format('Y-m-d'),
                    'balance'=> $value['balance']
                    
                ]);
                $response = $ledger;
                $responsCode = 200;
            }else{
                $response = "Loan Deductions can not be executed more than 1 within a month.";
                $responsCode = 400;
            }
        }
        return response()->json($response,$responsCode);
    }

    public function setDeductionAmountByFinance(Request $request)
    {
        $cur_year = date('Y');
        $data = \DB::table('loan_meta')->insert(['loan_id'=>$request->id,'month'=>session()->get('month'),'loan_year'=>$cur_year,'deduction_amount'=>$request->deduction_amount]);
        return response()->json($data,200);
    }




    public function leaveDeductionView(Request $request)
    {
        return view('deductions.leave-deduction');
    }

    public function leaveDeduction(Request $request)
    {   
        // $company_id = \Utils::getCompanyId(\Auth::user()->emp_code);
        $region_id  = \Utils::getRegionId(\Auth::user()->emp_code);
        $dxb_from = \Utils::getSubCompanyAppParamValue('dxb_from_date',$request->sub_company_id,$region_id);
        $dxb_to = \Utils::getSubCompanyAppParamValue('dxb_to_date',$request->sub_company_id,$region_id);
        $ho_from = \Utils::getSubCompanyAppParamValue('ho_from_date',$request->sub_company_id,$region_id);
        $ho_to = \Utils::getSubCompanyAppParamValue('ho_to_date',$request->sub_company_id,$region_id);

        Log::info('DXB start: ' . $dxb_from . '  DXB end: ' . $dxb_to);
        Log::info('HO start: ' . $ho_from . '  HO end: ' . $ho_to);

        $month = $request->month;
        if($request->sub_company_id != 3 )
        {
            $from = date($ho_from, strtotime('last month', strtotime($month)));
            $to =  date($ho_to, strtotime($month));
        }else{
            $from = date($dxb_from, strtotime('last month',strtotime($month)));
            $to =  date($dxb_to, strtotime($month));
        }

        $employees = Employee::where('company_id', $request->sub_company_id)->where('skip_deduction',0)->where('is_active',1)->get();


        return \Utils::leaveDeduction($employees, $from, $to, $month);
    }

    public function itemDeduction(Request $request)
    {
        return view('deductions.item-deduction');
    }


    public function loanDeductionSummary(Request $request)
    {
        if($request->month != ""){
            $month = $request->month;
        }else{
            $month =  \Carbon\Carbon::now()->subMonth()->format('F');
        }

        $response =  \DB::table('sub_company as sc')
                          ->select('sc.name',\DB::raw('SUM(ll.deduction_amount) as total'))
                          ->leftjoin('employees as e','e.company_id','=','sc.id')
                          ->join('loan as l','l.emp_id','=','e.emp_no')
                          ->join('loan_ledger as ll','ll.loan_id','=','l.id')
                          ->where('ll.month',$month)
                          ->where(function($query){
                            if(auth()->user()->roles->first()->name != 'admin')
                            {
                                $query->where('sc.id', \Utils::getCompanyId(auth()->user()->emp_code));
                            }
                          })
                          ->groupBy('sc.name')
                          ->get();
        return response()->json($response, 200);                    
    }

}
