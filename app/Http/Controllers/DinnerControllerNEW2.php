<?php

namespace App\Http\Controllers;

use DummyFullModelClass;
use App\Model\Employee;
use App\Model\DinnerAllowances;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use App\Model\Attendance;
use App\Model\AttendanceAdjustment;
use DB;
use Cache;
use Validator;
use Illuminate\Support\Facades\Log;

class DinnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\lain  $lain
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dinner.dinner-allowance-request');
    }

    public function allowanceTeller()
    {
        return view('dinner.dinner-allowance-teller');
    }

    public function allowanceApprovals()
    {
        return view('approvals.allowance');
    }

    public function dinner_allowances(){
        $allowances = DinnerAllowances::select('dinner_allowances.*')->where('is_approved',1)->get();
    }

   
    public function allowanceRequest(Request $request)
    {
        //
        // $rules = ['allowance_date' => 'required',];
        // $messages = ['allowance_date.required' => 'Allowance&#39;s request date is required!',];
        // $this->validate($request);
        
        $now = date_create()->format('Y-m-d');
        $allowance_request = new DinnerAllowances;      
        $allowance_request->emp_id = \Auth::user()->emp_code;
        $allowance_request->allowance_date = $request->allowance_date;
        $allowance_request->reason = $request->reason;
        $allowance_request->created_at = $now;
        $allowance_request->updated_at = $now;

        // if($this->leaveLastDayCheck($employee_leave->from, $employee_leave->to)){
        if($allowance_request->allowance_date == Null && $allowance_request->allowance_date == ''){
            $allowance_request = "&#9888;WARNING:&nbsp; Allowance&#39;s request date is required!";
            return response()->json($allowance_request,400);
        }
        elseif($allowance_request->reason == Null && $allowance_request->reason == ''){
            $allowance_request = "&#9888;WARNING:&nbsp; Allowance&#39;s Reason is required!";
            return response()->json($allowance_request,400);
        }
        elseif ($this->AllowanceSameDateCheck(\Auth::user()->emp_code,$request->allowance_date)) {
            $allowance_request = "&#9888;WARNING:&nbsp; You have applied for dinner allowance on this date. Please choose another date!";
            return response()->json($allowance_request,400);
        }
        elseif ($this->checkAdjustmentAttendance(\Auth::user()->emp_code,$request->allowance_date)) {
            $allowance_request = "&#9888;WARNING:&nbsp; We haven't find any Adjustment. Please choose another date!";
            return response()->json($allowance_request,400);
        }
        elseif ($this->checkPunchOutTime(\Auth::user()->emp_code,$request->allowance_date)) {
            $allowance_request = "&#9888;WARNING:&nbsp; Unfortunately you are not eligible for applying dinner allowance for requested date. Please choose another date!";
            return response()->json($allowance_request,400);
        }
        elseif ($this->checkLastTimeOut(\Auth::user()->emp_code,$request->allowance_date)) {
            $allowance_request->save();
            return response()->json($allowance_request,200);
            // $allowance_request = "&#9888;WARNING:&nbsp; Unable to apply";
            // return response()->json($allowance_request,400);
        }
        else{

            $allowance_request->save();
            return response()->json($allowance_request,200);
        }
        
    }
   


    // public static function AllowanceSameDateCheck($empCode, $allowance_date)
    // {
    //     $data = DinnerAllowances::select('allowance_date')->where('emp_id',$empCode)->orderBy('id','desc')->limit(1)->first();
    //     if($data)
    //     {
    //         if($data->allowance_date >= $allowance_date)
    //         {
    //             $response = true;
    //         }
    //         else{
    //             $response = false;
    //         }
    //     }else{
    //             $response = false;
    //     }
    //     return $response;
    // }
    public static function AllowanceSameDateCheck($empCode, $allowance_date)
    {
        $data = DinnerAllowances::select('allowance_date')->where('emp_id',$empCode)->where('allowance_date',$allowance_date)->first();

            if($data >= $allowance_date)
            {
                $response = true;
            }
            else{
                $response = false;
            }
        
        return $response;
    }

    public static function checkAdjustmentAttendance($empCode, $allowance_date)
    {
        $attendance = Attendance::select('id')
                                ->where('emp_id',$empCode)
                                ->where('date',$allowance_date)
                                ->first();

        $adjust_attendance = AttendanceAdjustment::select('atten_id','time_in','time_out')
                                                ->where('atten_id',$attendance->id)
                                                ->first();

Log::info('Adjust Attendance:  ' . $adjust_attendance);


        $start = strtotime($adjust_attendance->time_in);
        $end = strtotime($adjust_attendance->time_out);

        $WorkHours = $end - $start;
        Log::info('Adjust start time:  ' . $start);
        Log::info('Adjust end time:  ' . $end);
        Log::info('AdjustDiff is:  '. $WorkHours);

        if($attendance) {
            
            if ($WorkHours <= 42300) {

                $response = true;
            }
            else{
                $response = false;
            }

        }else{
            $response = false;
        }

        return $response;

    }

    // public function attendanceinANDout(){
    //     Attendance::
    // }

    // public static function checkPunchOutTime($empCode, $allowance_date)
    // {

    //     $data = DB::table('attendance as att')->select('id','time_in','time_out','emp_id','date')->where('emp_id',$empCode)->where('date',$allowance_date)->orderBy('id','desc')->limit(1)->first();
        
        


    //     $start = strtotime($data->time_in);
    //     $end = strtotime($data->time_out);

    //     $WorkHours = $end - $start;
    //     Log::info('start time:  ' . $start);
    //     Log::info('end time:  ' . $end);
    //     Log::info('Diff is:  '. $WorkHours);

    //     if($data) {
            
    //         if ($WorkHours <= 42300) {
    //             # code...
    //             $response = true;
    //         }
    //         else{
    //             $response = false;
    //         }

    //     }else{
    //         $response = false;
    //     }

    //     return $response;

    // }

    public static function checkLastTimeOut($empCode, $allowance_date)
    {
        $data = DB::table('attendance as att')->select('time_in','time_out','emp_id','date')->where('emp_id',$empCode)->where('date',$allowance_date)->where('is_active',1)->orderBy('id','desc')->limit(1)->first();

        $lastOut = DB::table('attendance as att')->select('time_out','emp_id','date')->where('emp_id',$empCode)->where('date','<',$allowance_date)->where('is_active',1)->orderBy('id','desc')->limit(1)->first();

        $start = strtotime($data->time_in);
        $end = strtotime($data->time_out);
        $WorkHours = $end - $start;
        $FixedOutTime = strtotime('09:00 PM'); // Told by Saquib bhai to do that Not with exact EPOC time value

        $lastOutTime = strtotime($lastOut->time_out);
         // last time out from allowance date
        Log::info('  lastouttime:  ' . $lastOutTime);
       
        Log::info('Diff is:  '. $WorkHours);

        
         //  if($lastOut) {  
         //    if ($lastOutTime <= 1582059600 && $WorkHours <= 39600) { // || $WorkHours <= 42300 
         //        # code...
         //        $response = true;
         //    }
         //    else{
         //        $response = false;
         //    }

         //  }else{
         //     $response = false;
         //}

         if($lastOut) {  
            if ($lastOutTime <= $FixedOutTime & $WorkHours <= 39600) { 
                
                $response = false;
            }
            else{
                $response = true;
            }

          }else{
             $response = true;
         }  
           
            return $response;

    }

    public static function checkPunchOutTime($empCode, $allowance_date)
    {
        $data = DB::table('attendance as att')->select('time_in','time_out','emp_id','date')->where('emp_id',$empCode)->where('date',$allowance_date)->where('is_active',1)->orderBy('id','desc')->limit(1)->first();


        $start = strtotime($data->time_in);
        $end = strtotime($data->time_out);

        $WorkHours = $end - $start;
        Log::info('start time:  ' . $start);
        Log::info('end time:  ' . $end);
        Log::info('Diff is:  '. $WorkHours);

        if($data) {
            
            if ($WorkHours <= 42300) {
                # code...
                $response = true;
            }
            else{
                $response = false;
            }

        }else{
            $response = false;
        }

        return $response;

    }

    public function getAllowanceApprovals(Request $request){

        $approvals = \DB::table('dinner_allowances as da')
                      ->leftjoin('employees as e','e.emp_no','=','da.emp_id')
                      ->select('e.name as empname','e.emp_no as empcode','da.allowance_date as allowancedate','da.reason as reason','da.is_approved as status','da.id as rowid')
                      ->where('e.report_manager','=',\Auth::user()->emp_code)
                      // ->where('is_paid','=',0)
                      ->orderBy('rowid','desc')
                      ->get();    

        if($approvals){
            foreach ($approvals as $key => $value) {
                    # code...
                    $allowance[] = [  //Changed variable name from response ...... by Saquib 15-Jan-2020
                        'empname' => $value->empname,
                        'empcode' => $value->empcode,
                        'status' => $value->status,
                        'allowancedate' => $value->allowancedate,
                        'reason' => $value->reason,
                        'rowid'=>$value->rowid
                        ];   
                }

        }else{
            $allowance = [];  
        } 
        
        return response()->json($approvals,200);   

    }


    public function AllowanceApprovalAction(Request $request, $id)
    {

        $allowance = DinnerAllowances::where('id', $id)->first();
        $not_paid = 0;
        $cancel = 3;
        // $response =  $this->leaveLastDayCheck($allowance->is_approved); 
        if($allowance.$request->status == 1)
        {
            // Log::info(' In IF Status: ' . $allowance.$request->status);  
            $action = DinnerAllowances::where('id', $id)->update(['is_approved'=> $cancel]);
            $action = 'Cancelled!';
        }
         else {
                    //echo 'false';
            $action = DinnerAllowances::find($id);
            $action->update(['is_approved'=>$request->status,'approved_by'=>($request->status== 0 || $request->status== 2 || $request->status== 3) ? '' : \Auth::user()->emp_code,'is_paid'=>($request->status== 0 || $request->status== 2 || $request->status== 3) ? '' : $not_paid]);
            $action = 'Status updated Successfully!';
        }
      
        return response()->json($action,200);
    }


    public function getApprovedAllowances(Request $request){

        $approvedAllowances = \DB::table('dinner_allowances as da')
                      ->leftjoin('employees as e','e.emp_no','=','da.emp_id')
                      ->select('e.name as empname','e.emp_no as empcode','da.allowance_date as allowancedate','da.is_approved as status','da.id as rowid', 'da.is_paid as paidStatus', \DB::raw('IFNULL(u1.name,"N/A") as approveby'))
                      ->leftjoin('users as u1','u1.emp_code','=','da.approved_by')
                      ->where('is_approved','=','1')
                      ->orderBy('rowid','desc')
                      ->get();    

        if($approvedAllowances){
            foreach ($approvedAllowances as $key => $value) {
                    # code...
                    $allowance[] = [
                        'empname' => $value->empname,
                        'empcode' => $value->empcode,
                        'status' => $value->status,
                        'allowancedate' => $value->allowancedate,
                        'approveby' => $value->approveby,
                        'paidStatus' => $value->paidStatus,
                        'rowid'=>$value->rowid
                        ];   
                }

        }else{
            $allowance = [];  
        } 
        
        return response()->json($approvedAllowances,200);   

    }


    public function dinnerAllowancePaidAction(Request $request, $id)
    {
        $ispaidAction =  DinnerAllowances::where('id',$id)->first();
        if($ispaidAction->is_approved == 0 || $ispaidAction->is_approved == 2)
        {
            $response = "Something went wrong";
            return response()->json($response,400);

        }else{
            $response = DinnerAllowances::where('id', $id)->update(['is_paid'=> $request->status,'paid_at'=>\Carbon\Carbon::today()->toDateString(), 'paid_by'=>\Auth::user()->emp_code]);
            $response = 'Paid Successfully!';         
            return response()->json($response,200);
        }

    }


    public function getMyAllowanceList(Request $request){

    $data = DB::table('dinner_allowances as da')
            ->select('da.id as rowid','da.emp_id as self','da.allowance_date as ad','da.reason as reasons','da.is_approved as Status','da.is_paid as paidStatus','da.created_at as apply_at')
            ->where('da.emp_id','=',\Auth::user()->emp_code)
            ->get();
    return $data;
    }


//End Bracket
}
