<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Employee;
use App\Model\Attendance;
use App\Model\SubCompany;
use App\Model\Department;
use App\Model\Designation;
use App\Model\Education;
use App\Model\Employer;
use App\Model\Dependents;
use App\Model\Position;
use App\Model\PfSlab;
use App\Model\SubDepartment;
use App\Model\PayBackLeaves;
use App\Model\User;
use Validator;
use App\Jobs\CreateCredentials;
use App\Jobs\SendCredentialsEmailJob;
use App\Repositories\EmployeeRepository;
use App\Model\Payroll;
use Cache;
use Illuminate\Support\Facades\log;




class EmployeeController extends Controller
{
    //
    public $minYear = 1900;
    private $empRepo;
    public function __construct(EmployeeRepository $empRepo)
    {
        $this->empRepo =  $empRepo;
    }

    public function getEmployeeList()
    {
        $employees =  Employee::whereCompanyId(\Utils::getCompanyId(auth()->user()->emp_code))->all();
        return response()->json($employees,200);
    }

    public function generateSalary(Request $request)
    {
        $date =  \Carbon\Carbon::now();
        $employeeData = Employee::find($request->employee);
        $present_days = Attendance::where('emp_id',$request->employee)->where('month',$date->format('F'))->count();
        $total_days = \Utils::totalDaysInMonth($date);
        $working_days =  \Utils::workingAndOffDaysInMonth($date,$offdays = false);
        $off_days =  \Utils::workingAndOffDaysInMonth($date,$offdays = true);
        $leaves = $working_days - $present_days;
        $gross_salary = $employeeData->salary * ($present_days + $leaves + $off_days) / $total_days;

        $data = [
                    'salary'=>$employeeData->salary,
                    'present_days' => $present_days,
                    'total_days' => $total_days,
                    'working_days' => $working_days,
                    'off_days' => $off_days,
                    'leaves' => $leaves,
                    'gross_salary' => round($gross_salary)
                ];

            return response()->json($data, 200);
    }



    public function employeeList(Request $request)
    {
        return view('employee.index');
    }

    public function addEmployee(Request $request)
    {
       $yearFrom =  \Utils::yearList($this->minYear, $present = false);
       $yearTo =  \Utils::yearList($this->minYear, $present = true);
       return view('employee.add')->with('yearFrom',$yearFrom)->with('yearTo',$yearTo);
    }


    public function storeEmployee(Request $request)
    {

        $rules =  [
            'first' => 'required',
            'last' => 'required',
            'current_salary' => 'numeric',
            'eobi' => 'numeric',
            // 'p_email' => 'required|string|email|max:255|unique:employees',
        ];

        $messages = [
         'first.required' => "Please enter your first name.",
        'last.required' => "Please enter your last name.",
        'current_salary.numeric' => "Salary must be in numeric form.",
        'eobi.numeric' => "EOBI amount must be in numeric form.",
        ];
        
        $validate =  $this->validate($request, $rules, $messages);
        
        $last_emp_no =  Employee::max('emp_no');
        $employee =  new Employee;
        $employee->emp_no = $last_emp_no+1; 
        $employee->name= $request->first.' '.$request->last;
        $employee->father_name= $request->father_name;
        $employee->nationality = $request->nationality;
        $employee->gender = $request->gender;
        $employee->marital_status = $request->m_status;
        $employee->religion = $request->religion;
        $employee->dob = $request->dob;
        $employee->place_of_birth = $request->place_of_birth;
        $employee->eobi_no = $request->eobi;
        $employee->cnic_no = $request->cnic;
        $employee->cnic_place_of_issue = $request->cnic_place;
        $employee->cnic_date_of_issue = $request->cnic_date;
        $employee->cnic_date_of_expiry = $request->cnic_expiry;
        $employee->passport_no  = $request->passport_no;
        $employee->passport_place_of_issue =  $request->passport_place;
        $employee->passport_date_of_issue = $request->passport_date;
        $employee->passport_date_of_expiry = $request->passport_expiry;
        $employee->visa_no  = $request->visa_no;
        $employee->visa_place_of_issue =  $request->visa_place;
        $employee->visa_date_of_issue = $request->visa_date;
        $employee->visa_date_of_expiry = $request->visa_expiry;
        $employee->iqama_no  = $request->iqama_no;
        $employee->iqama_place_of_issue =  $request->iqama_place;
        $employee->iqama_date_of_issue = $request->iqama_date;
        $employee->iqama_date_of_expiry = $request->iqama_expiry;
        $employee->vehicle_no = $request->vehicle_no;
        $employee->vehicle_name = $request->vehicle_name;
        $employee->vehicle_model = $request->vehicle_model;
        $employee->vehicle_date_issue = $request->vehicle_date;
        $employee->liscence_no  = $request->liscence_no;
        $employee->liscence_place_of_issue =  $request->liscence_place;
        $employee->liscence_date_of_issue = $request->liscence_date;
        $employee->liscence_date_of_expiry = $request->liscence_expiry;
        $employee->position_title = $request->designation;
        $employee->dept_name = $request->department;
        $employee->sub_depart_id = $request->subdepartment;
        $employee->company_id = $request->company;
        $employee->region_id = $request->region; // region 
        $employee->emp_type_id = $request->emp_type; // region 
        $employee->permanent_at = $request->permanent_at; // region 
        $employee->date_of_hiring = $request->date_of_hiring;
        $employee->current_salary = $request->current_salary;
        $employee->report_to = $request->report_to;
        $employee->report_manager = $request->report_manager;
        $employee->position_no = $request->role;
        $employee->pf_slab_id = $request->pf_slabs;
        $employee->pf_effective_date = $request->pf_effective_date;
        $employee->eobi_amount = $request->eobi;
        $employee->fuel_amount = $request->fuel_amount;
        $employee->parking_amount = $request->parking_amount;
        $employee->skip_deduction = is_null($request->skip_deduction) ? 0 : $request->skip_deduction;
        // current contact address
        $employee->house_no = $request->house_no;
        $employee->building = $request->building;
        $employee->street_no = $request->street;
        $employee->area = $request->area;
        $employee->po_box = $request->po_box;
        $employee->city = $request->city;
        $employee->landline = $request->landline;
        $employee->mobile_no = $request->mobile_no;
        $employee->email = $request->c_email;

        // permanent contact address
        $employee->p_house = $request->p_house_no;
        $employee->p_building = $request->p_building;
        $employee->p_street = $request->p_street;
        $employee->p_area = $request->p_area;
        $employee->p_email = $request->p_email;
        $employee->p_po_box = $request->p_po_box;
        $employee->p_city = $request->p_city;
        $employee->p_landline = $request->p_landline;
        $employee->p_mobile_no = $request->p_mobile_no;
        
        // primary emergncy contact details
        $employee->primary_emergency_name = $request->p_emergency_name;
        $employee->primary_emergency_relation = $request->relationship;
        $employee->primary_emergency_address = $request->p_emergency_address;
        $employee->primary_emergency_telephone = $request->telephone;

        // secondary emergncy contact details
        $employee->secondry_emergency_name = $request->s_emergency_name;
        $employee->secondry_emergency_relation = $request->s_relationship;
        $employee->secondry_emergency_telephone = $request->s_telephone;
        $employee->secondry_emergency_address = $request->s_emergency_address;

        // primary benificary contact details
        $employee->primary_benificiary_name = $request->p_benificiary_name;
        $employee->primary_benificiary_relation = $request->p_ben_relationship;
        $employee->primary_benificiary_telephone = $request->p_ben_telephone;
        $employee->primary_benificiary_address = $request->p_ben_address;
        $employee->primary_benificiary_cnic = $request->p_ben_nic;
        $employee->primary_benificiary_email = $request->p_ben_email;

        // secondry benificiary detail
        $employee->secondry_benificiary_name = $request->s_benificiary_name;
        $employee->secondry_benificiary_relation = $request->s_ben_relationship;
        $employee->secondry_benificiary_telephone = $request->s_ben_telephone;
        $employee->secondry_benificiary_address = $request->s_ben_address;
        $employee->secondry_benificiary_cnic = $request->s_ben_nic;
        $employee->secondry_benificiary_email = $request->s_ben_email;

        if($request->hasFile('profile_pic')){
       // $dpName = $request->profile_pic->getClientOriginalName();
        $dpName =  $last_emp_no + 1 .'.'.$request->profile_pic->getClientOriginalExtension();
        $employee->profile_pic = $dpName;
        $request->profile_pic->move(public_path('images/avatar'), $dpName);
        }
       $employee->save();
       
    if(!is_null($request->degree_title)  || !is_null($request->institute) ||  !is_null($request->cgpa_grade)  || !is_null($request->passing_from)  || !is_null($request->passing_to) || !is_null($request->degree_country) || !is_null($request->degree_city) )
        {         
       for($i = 0; $i < count($request->degree_title); $i++)
       {
            $education =  new Education;
            $education->emp_id =  $employee->id;
            $education->degree_title = $request->degree_title[$i];
            $education->institute = $request->institute[$i];
            $education->cgpa_percent = $request->cgpa_grade[$i];
            $education->passing_date = $request->passing_from[$i].' - '.$request->passing_to[$i];
            $education->degree_city = $request->degree_city[$i];
            $education->degree_country = $request->degree_country[$i];
            $education->save();


       }
      }



       if(!is_null($request->last_emp_name) || !is_null($request->last_designation) ||  !is_null($request->last_emp_from) || !is_null($request->last_emp_to) || !is_null($request->last_emp_contact))
        { 
        for($i = 0; $i < count($request->last_emp_name); $i++)
         {
            $employer =  new Employer;
            $employer->emp_id =  $employee->id;
            $employer->name = $request->last_emp_name[$i];
            $employer->last_designation = $request->last_designation[$i];
            $employer->from = $request->last_emp_from[$i];
            $employer->to = $request->last_emp_to[$i];
            $employer->ref_no = $request->last_emp_contact[$i];
           
            $employer->save();


        }
     }
      if(!is_null($request->d_name) || !is_null($request->d_type) ||  !is_null($request->d_gender) || !is_null($request->d_dob)  || !is_null($request->d_cnic)  || !is_null($request->d_contact))
        {
       for($i = 0; $i < count($request->d_name); $i++)
       {
            $dependent =  new Dependents;
            $dependent->emp_id =  $employee->id;
            $dependent->type = $request->d_type[$i];
            $dependent->name = $request->d_name[$i];
            $dependent->gender = $request->d_gender[$i];
            $dependent->dob = $request->d_dob[$i];
            $dependent->cnic_bform = $request->d_nic[$i];
            $dependent->contact_no = $request->d_contact[$i];
           
           $dependent->save();
       }
    }

    $credsData = ['name' => $employee->name,'email' => $employee->p_email,'password'=>'osl123','emp_code'=>$employee->emp_no ];
    
    $creat_creds = (new CreateCredentials($credsData))->delay(\Carbon\Carbon::now()->addSeconds(60));
    dispatch($creat_creds);
    //dispatch(new SendCredentialsEmailJob($credsData,$employee->email));
    return  redirect()->route('employee')->with('flash-message','Successfully Added');
      
    }

    public function editEmployee(Request $request)
    {
        $company =  SubCompany::all();
        $department =  Department::all();
        $sub_department =  SubDepartment::all();
        $designation =  Designation::all();
        $employee =  Employee::find($request->id);
        $education = Education::where('emp_id',$request->id)->get();
        $dependent = Dependents::where('emp_id',$request->id)->get();  
        $employer = Employer::where('emp_id',$request->id)->get();  
        $yearFrom =  \Utils::yearList($this->minYear, $present = false);
        $yearTo =  \Utils::yearList($this->minYear, $present = true);
        $pf_slabs = PfSlab::all();
        $regions = \DB::table('regions')->get();
        $managers = User::select('name','emp_code as emp_no')->withRole('manager')->orderBy('name','asc')->get();
        $position = Position::all();
        $allEmps = Employee::select('name','emp_no')->orderBy('name','asc')->get();
        //        return $employee;
        $firstname = explode(" ", $employee->name);
        $first = isset($firstname[0]) ? $firstname[0] : '';
        $last = isset($firstname[1]) ? $firstname[1] : '';
        $image = !is_null($employee->profile_pic) ? $employee->profile_pic : 'blank-avatar.png';
        $empTypes = $this->empRepo->getEmpType();   
        return view('employee.edit')->with('employee',$employee)->with('first',$first)->with('last',$last)->with('company',$company)->with('department',$department)->with('designation',$designation)->with('education',$education)->with('dependent',$dependent)->with('employer',$employer)->with('image',$image)->with('yearFrom',$yearFrom)->with('yearTo',$yearTo)->with('pf_slabs',$pf_slabs)->with('managers',$managers)->with('position',$position)->with('allEmps',$allEmps)->with('regions',$regions)->with('empTypes',$empTypes)->with('sub_department',$sub_department);
        
    }

    public function editEmployees(Request $request)
    {
        $rules =  [
            'first' => 'required',
            'last' => 'required',
            'eobi' => 'numeric',
            // 'p_email' => 'required|string|email|max:255|unique:employees',
        ];

        $messages = [
           'first.required' => "Please enter your first name.",
           'last.required' => "Please enter your last name.",
           'eobi.numeric' => "EOBI Amount must be in numeric form.",
       ];


        $employee =  Employee::find($request->id);
        //return $employee;
        $employee->name= $request->first.' '.$request->last;
        $employee->father_name= $request->father_name;
        $employee->nationality = $request->nationality;
        $employee->gender = $request->gender;
        $employee->marital_status = $request->m_status;
        $employee->religion = $request->religion;
        $employee->dob = $request->dob;
        $employee->place_of_birth = $request->place_of_birth;
        $employee->eobi_no = $request->eobi;
        $employee->fuel_amount = $request->fuel_amount;
        $employee->cnic_no = $request->cnic;
        $employee->cnic_place_of_issue = $request->cnic_place;
        $employee->cnic_date_of_issue = $request->cnic_date;
        $employee->cnic_date_of_expiry = $request->cnic_expiry;
        $employee->passport_no  = $request->passport_no;
        $employee->passport_place_of_issue =  $request->passport_place;
        $employee->passport_date_of_issue = $request->passport_date;
        $employee->passport_date_of_expiry = $request->passport_expiry;
        $employee->visa_no  = $request->visa_no;
        $employee->visa_place_of_issue =  $request->visa_place;
        $employee->visa_date_of_issue = $request->visa_date;
        $employee->visa_date_of_expiry = $request->visa_expiry;
        $employee->iqama_no  = $request->iqama_no;
        $employee->iqama_place_of_issue =  $request->iqama_place;
        $employee->iqama_date_of_issue = $request->iqama_date;
        $employee->iqama_date_of_expiry = $request->iqama_expiry;
        $employee->vehicle_no = $request->vehicle_no;
        $employee->vehicle_name = $request->vehicle_name;
        $employee->vehicle_model = $request->vehicle_model;
        $employee->vehicle_date_issue = $request->vehicle_date;
        $employee->liscence_no  = $request->liscence_no;
        $employee->liscence_place_of_issue =  $request->liscence_place;
        $employee->liscence_date_of_issue = $request->liscence_date;
        $employee->liscence_date_of_expiry = $request->liscence_expiry;
        $employee->position_title = $request->designation;
        $employee->position_no = $request->role;
        $employee->dept_name = $request->department;
        $employee->sub_depart_id = $request->subdepartment;
        $employee->company_id = $request->company;
        $employee->region_id = $request->region;
        $employee->permanent_at = $request->permanent_at;
        $employee->emp_type_id = $request->emp_type;
        $employee->date_of_hiring = $request->date_of_hiring;
        $employee->parking_amount = $request->parking_amount;
        $employee->current_salary = is_null($request->current_salary) ? $employee->current_salary : $request->current_salary;
        $employee->report_to = $request->report_to;
        $employee->report_manager = is_null($request->report_manager) ? $employee->report_manager : $request->report_manager;
        $employee->pf_slab_id = is_null($request->pf_slabs) ? $employee->pf_slab_id : $request->pf_slabs;
        $employee->pf_effective_date = $request->pf_effective_date;
        $employee->eobi_amount = $request->eobi;
        $employee->skip_deduction = is_null($request->skip_deduction) ? 0 : $request->skip_deduction;     
        // current contact address
        $employee->house_no = $request->house_no;
        $employee->building = $request->building;
        $employee->street_no = $request->street;
        $employee->area = $request->area;
        $employee->po_box = $request->po_box;
        $employee->city = $request->city;
        $employee->landline = $request->landline;
        $employee->mobile_no = $request->mobile_no;
        $employee->email = $request->c_email;

        // permanent contact address
        $employee->p_house = $request->p_house_no;
        $employee->p_building = $request->p_building;
        $employee->p_street = $request->p_street;
        $employee->p_area = $request->p_area;
        $employee->p_email = $request->p_email;
        $employee->p_po_box = $request->p_po_box;
        $employee->p_city = $request->p_city;
        $employee->p_landline = $request->p_landline;
        $employee->p_mobile_no = $request->p_mobile_no;
        
        // primary emergncy contact details
        $employee->primary_emergency_name = $request->p_emergency_name;
        $employee->primary_emergency_relation = $request->relationship;
        $employee->primary_emergency_address = $request->p_emergency_address;
        $employee->primary_emergency_telephone = $request->telephone;

        // secondary emergncy contact details
        $employee->secondry_emergency_name = $request->s_emergency_name;
        $employee->secondry_emergency_relation = $request->s_relationship;
        $employee->secondry_emergency_telephone = $request->s_telephone;
        $employee->secondry_emergency_address = $request->s_emergency_address;

        // primary benificary contact details
        $employee->primary_benificiary_name = $request->p_benificiary_name;
        $employee->primary_benificiary_relation = $request->p_ben_relationship;
        $employee->primary_benificiary_telephone = $request->p_ben_telephone;
        $employee->primary_benificiary_address = $request->p_ben_address;
        $employee->primary_benificiary_cnic = $request->p_ben_nic;
        $employee->primary_benificiary_email = $request->p_ben_email;

        // secondry benificiary detail
        $employee->secondry_benificiary_name = $request->s_benificiary_name;
        $employee->secondry_benificiary_relation = $request->s_ben_relationship;
        $employee->secondry_benificiary_telephone = $request->s_ben_telephone;
        $employee->secondry_benificiary_address = $request->s_ben_address;
        $employee->secondry_benificiary_cnic = $request->s_ben_nic;
        $employee->secondry_benificiary_email = $request->s_ben_email;
        if($request->hasFile('profile_pic')){
        $dpName = $employee->emp_no.'.'.$request->profile_pic->getClientOriginalExtension();
        $employee->profile_pic = $dpName;
        $request->profile_pic->move(public_path('images/avatar'), $dpName);
        }

        $employee->save();

     if(!is_null($request->degree_title)  || !is_null($request->institute) ||  !is_null($request->cgpa_grade)  || !is_null($request->passing_from)  || !is_null($request->passing_to) || !is_null($request->degree_country) || !is_null($request->degree_city) )
        {  
       $educationid =  Education::where('emp_id',$request->id)->delete();
        
       for($i = 0; $i < count($request->degree_title); $i++)
       {

            $education =  new Education;
            $education->emp_id =  $employee->id;
            $education->degree_title = $request->degree_title[$i];
            $education->institute = $request->institute[$i];
            $education->cgpa_percent = $request->cgpa_grade[$i];
            $education->passing_date = $request->passing_from[$i].' - '.$request->passing_to[$i];
            $education->degree_city = $request->degree_city[$i];
            $education->degree_country = $request->degree_country[$i];
            $education->save();
       }
    }
      if(!is_null($request->last_emp_name) || !is_null($request->last_designation) ||  !is_null($request->last_emp_from) || !is_null($request->last_emp_to) || !is_null($request->last_emp_contact))
        {  
       $employerid =  Employer::where('emp_id',$request->id)->delete();
        
       for($i = 0; $i < count($request->last_emp_name); $i++)
       {
            $employer =  new Employer;
            $employer->emp_id =  $employee->id;
            $employer->name = $request->last_emp_name[$i];
            $employer->last_designation = $request->last_designation[$i];
            $employer->from = $request->last_emp_from[$i];
            $employer->to = $request->last_emp_to[$i];
            $employer->ref_no = $request->last_emp_contact[$i];
           
            $employer->save();


       }
      }
    if(!is_null($request->d_name) || !is_null($request->d_type) ||  !is_null($request->d_gender) || !is_null($request->d_dob)  || !is_null($request->d_cnic)  || !is_null($request->d_contact))
    { 
       $dependentid =  Dependents::where('emp_id',$request->id)->delete();
        
       for($i = 0; $i < count($request->d_name); $i++)
       {
            $dependent =  new Dependents;
            $dependent->emp_id =  $employee->id;
            $dependent->type = $request->d_type[$i];
            $dependent->name = $request->d_name[$i];
            $dependent->gender = $request->d_gender[$i];
            $dependent->dob = $request->d_dob[$i];
            $dependent->cnic_bform = $request->d_nic[$i];
            $dependent->contact_no = $request->d_contact[$i];
           
           $dependent->save();
       }
    }
        return  redirect()->route('employee')->with('flash-message','Updated Success Fully');
    }

    public function getCompany()
    {
        $company =  Cache::remember('company',60, function(){
            return SubCompany::all();
        });

        return response()->json($company, 200);

    }

    public function getDepartment()
    {
        $department = Cache::remember('department',60, function(){
           return  Department::all();
        });
        return response()->json($department, 200);

    }

    public function getDesignation()
    {
        $designation = Cache::remember('designation',60, function(){
            return Designation::all();
        });

        return response()->json($designation, 200);

    }

    public function getEmployees(Request $request)
    {

        $employee = Employee::select('employees.*','emp_type.value as emp_status','e2.name as reportmanager','designation.name as desig','sub_company.name as company','department.name as dept')
        ->leftjoin('employees as e2','e2.emp_no','=','employees.report_manager')
        ->leftjoin('designation','designation.id','=','employees.position_title')
        ->leftjoin('department','department.id','=','employees.dept_name')
        ->leftjoin('sub_company','sub_company.id','=','employees.company_id')
        ->leftjoin('emp_type','employees.emp_type_id','=','emp_type.id')
        ->where(function($query){
            if(auth()->user()->roles->first()->name != 'admin')
            {
                $query->where('employees.company_id',\Utils::getCompanyId(auth()->user()->emp_code));
            }
        })->get();


        return response()->json($employee, 200);
    }

    public function employeeDropDown(Request $request)
    {
        $emp = Employee::select('emp_no','name','is_active')
        ->where(function($query){
            if(auth()->user()->roles->first()->name != 'admin')
            {
                $query->where('company_id',\Utils::getCompanyId(auth()->user()->emp_code));
            }
        })
        ->orderBy('name','asc')
        ->get();
        return response()->json($emp,200);
    }

    public function employeeActiveInactive(Request $request, $id)
    {
      
      $employee = Employee::find($id);
      $employee->is_active = ($request->status == 'active') ? 1 : 0;
      $employee->save();
      return response()->json($employee,200);

    }

    public function managerDropDown(Request $request)
    {
	// $managers = User::select('users.name','users.emp_code as emp_no')
	// 				->join('role_user','role_user.user_id','=','users.id')
	// 				->where('role_user.role_id','=',4)
	// 				->orderBy('users.emp_code','asc')
	// 				->get();

        $managers = Cache::remember('managers',60,function(){
            return  User::select('name','emp_code as emp_no')->withRole('manager')->orderBy('name','asc')->get();
        });
        return response()->json($managers,200);
    }

    public function getPosition(Request $request)
    {
        $positions =  Cache::remember('positions', 60, function(){
            return Position::all();
        });
        return response()->json($positions, 200);

    }

    public function getPfSlabs(Request $request)
    {
        $pf_slabs = Cache::remember('pf_slabs', 60, function(){
            return  PfSlab::all();
        });
        return response()->json($pf_slabs, 200);
    } 

    public function getRegions(Request $request)
    {
        $regions = Cache::remember('regions', 60, function(){
            return  \DB::table('regions')->get();
        });
        return response()->json($regions, 200);
    } 

    public function getPayroll(Request $request)
    {
        return view('payroll.generate-payroll');
    }



    public function generatePayroll(Request $request)
    {

        $employees = Employee::where('company_id',$request->sub_company_id)->where('is_active',1)->get();
        $month = $request->month;
        $year = $request->year;
        foreach ($employees as $key => $value) {
        //     # code...
            $leave_ded =  \Utils::deductLeavesFromSalary($value->emp_no, $month);
            $eobi = \Utils::getEobi($value->emp_no);
            $itemDeduction = \Utils::getItemDeduction($value->emp_no, $month);
            $data[] = Payroll::updateOrCreate(['emp_id'=>$value->emp_no,'month'=>$month],[
                'company_id' => $value->company_id,
                'emp_name' => $value->name,
                'emp_id' => $value->emp_no,
                'gross_sal' => sprintf("%.2f",$value->current_salary),
                'leave_ded_amt' => $leave_ded,
                'pf' => \Utils::getPfValue($value->emp_no),
                'adv_sal' => \Utils::getAdvanceSalAmount($value->emp_no, $month),
                'tax' => \Utils::getTax($value->emp_no),
                'loan_ded' => \Utils::getLoanDeductionAmt($value->emp_no, $month),
                'item_deduction' => $itemDeduction,
                'eobi' => $eobi,
                'fuel_amount' => \Utils::getFuelAmount($value->emp_no),
                'parking_amount' => \Utils::getParkingAmount($value->emp_no),
                'arrear_amount' => \Utils::getArrears($value->emp_no, $month),
                'net_sal' => \Utils::payroll($value->emp_no, $month),
                'month' => $month,
                'year' => $year,
                'payroll_date' => date('Y-m-d')
            ]);
        }
        
        return response()->json($data,200);
    }


    public function getEmployeeType()
    {
        $types = Cache::remember('types',60, function(){
            return $this->empRepo->getEmpType();
        });

        return response()->json($types,200);
    }



    public function payrollHistory(Request $request)
    {
        return view('payroll.history');
    }

    public function getPayrollHistory(Request $request)
    {
        // if($request->company_id == '' || $request->month == '' || $request->year == '')
        // {
        //     $company =  $request->company_id;
        //     $month =  \Carbon\Carbon::now()->format('F');
        //     $year =  \Carbon\Carbon::now()->format('Y');
        // }else{
        //     $company =  $request->company_id;
        //     $month =  $request->month;
        //     $year =  $request->year;
        // }
        $data =  Payroll::where(function($query){
            if(auth()->user()->roles->first()->name != 'admin')
            {
                $query->where('company_id',\Utils::getCompanyId(auth()->user()->emp_code));
            }
        })->get();
       
        foreach ($data as $key => $value) {
            # code...
            $response[] = [
                'company_name' => SubCompany::where('id',$value->company_id)->value('name'),
                'emp_name' => $value->emp_name,
                'emp_id' => $value->emp_id,
                'gross_sal' => sprintf("%.2f",$value->gross_sal),
                'leave_ded_amt' => $value->leave_ded_amt,
                'pf' => $value->pf,
                'adv_sal' => $value->adv_sal,
                'tax' => $value->tax,
                'loan_ded' => $value->loan_ded,
                'item_deduction' => $value->item_deduction,
                'eobi' => $value->eobi,
                'fuel_amount' => $value->fuel_amount,
                'parking_amount' => $value->parking_amount,
                'arrear_amount' => $value->arrear_amount,
                'net_sal' => $value->net_sal,
                'month' => $value->month,
                'year' => $value->year,
                'payroll_date' => $value->payroll_date
            ];
        }
        return $response;
    }

    public function getYearlist()
    {
        return \Utils::getYearlist();
    }

    public function getExEmployees(Request $request)
    {
        return view('employee.ex-employee');
    }


    public function  payrollSummary(Request $request)
    {
        if($request->month != ""){
            $month = $request->month;
        }else{
            $month =  \Carbon\Carbon::now()->subMonth()->format('F');
        }
        $response  = \DB::table('payroll')->select('sub_company.name', \DB::raw("(SELECT IFNULL(SUM(net_sal),0) FROM payroll WHERE payroll.month = '".$month."' AND company_id= sub_company.id) as 'Total' ") )
                                         ->rightjoin('sub_company','payroll.company_id','=','sub_company.id')
                                         ->where(function($query){
                                            if(auth()->user()->roles->first()->name != 'admin')
                                            {
                                                $query->where('sub_company.id', \Utils::getCompanyId(auth()->user()->emp_code));
                                            }
                                         })
                                         ->groupBy('sub_company.id')
                                         ->get();
        return response()->json($response, 200);
    }


    public function  getSubDepartment($id)
    {
        $sub_departments = Department::find($id)->sub_department;
        return $sub_departments;
    }

}
