<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function getPfTotalValue()
    {
        return \Utils::getPfTotalValue(\Auth::user()->emp_code);
    }

    public function getReportingManager()
    {
        $response = \Utils::getReportingManager(\Auth::user()->emp_code);
        return response()->json($response,200);
    }
}
