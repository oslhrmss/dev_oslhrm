<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ItemRepository;
use App\Http\Requests\ItemStoreRequest;
use App\Model\Employee;
use App\Model\Item;
use App\Model\ItemType;
use App\Model\EmployeeItem;
use App\Model\ItemVendor;
use Illuminate\Support\Facades\Log;
use validator;
use cache;
use DB;
class ItemController extends Controller
{
    //
    private $itemRepo;
    public function __construct(ItemRepository $itemRepo)
    {
        $this->itemRepo = $itemRepo;
    }

    public function index(Request $request)
    {
        return view('item.index');
    }


    public function getItems(Request $request)
    {
        $response =  $this->itemRepo->getItems();
        return response()->json($response,200);
    }

    //For vendors start
    public function getVendors(Request $request)
    {
        $response =  $this->itemRepo->getVendors();
        return response()->json($response,200);
    }

    public function addVendor(Request $request)
    {
        return view('item.vendor');
    }

    public function Report1(Request $request) // For Report 1
    {
        return view('item.report_1');
    }

     public function Report2(Request $request) // For Report 2
    {
        return view('item.report_2');
    }

    public function storeVendor(Request $request)
    {
         $rules = [
            'vendor_name' => 'required|unique:item_vendor|string',
            'vendor_phone' => 'required',
            'contact_person' => 'required|string',
        ];
        $messages = [
            'vendor_name.required'=> 'Vendor name is required!',
            'vendor_name.string'=> 'Vendor name must be Alphabetic!',
            'vendor_phone.required'=> 'Vendor ph# is required!',
            'vendor_name.unique'=> 'The vendor name has already been taken. Please add another Vendor.',
            'contact_person.required' => 'Contact person name is required!',
            'contact_person.string' => 'Contact person name must be Alphabetic!',
        ];

        $this->validate($request,$rules,$messages);
        $item_vendor =  new ItemVendor;    
        $item_vendor->vendor_name = $request->vendor_name;
        $item_vendor->vendor_phone = $request->vendor_phone;
        $item_vendor->contact_person = $request->contact_person;
        
        $item_vendor->save();
        return response()->json($item_vendor,200);
    }
    //For vendors end


    public function addItem(Request $request)
    {
        return view('item.add');

    }
    public function storeItem(ItemStoreRequest $request)
    {
        $validate =  $request->validate();

        if ($request->deduction_amt == null && $request->monthly_deduction == null){

        $data = [
            'name'=>$request->item_name,
            'item_type_id'=>$request->type,
            'specification'=>$request->specification,
            'brand' => $request->item_brand,
            'model' => $request->item_model,
            'imei_no' => $request->imei_no,
            'purchase_date' => $request->pur_date,
            'warranty_date' => $request->war_date,
            'cost'=>$request->cost,
            'vendor_id' => $request->vendor,
            'deduction_amt'=>0,
            'monthly_deduction_amt'=>0
        ];
        
        $dat = $this->itemRepo->create($data);
        
        $itemId = $dat->id;        
        $status = 'New';
        $By = auth()->user()->emp_code;
        $status_date = $request->pur_date;
        $assign_item = EmployeeItem::create([

            'item_id' => $itemId,
            'status'=>$status,
            'status_dates'=>$status_date,
            'is_active' => 1,
            'created_by'=>$By,
            'updated_by'=>$By

        ]);

        return response()->json($data,200);
        
        }

        elseif ($request->deduction_amt != null && $request->monthly_deduction == null)
        {
            
        $error_msg = "You forgot to enter Monthly Deduction Amount!";
           echo "<script type='text/javascript'>window.alert('$error_msg');
           window.location.href='/add.item';
           </script>";
           return $error_msg;
        }

        elseif ($request->deduction_amt == null && $request->monthly_deduction == null && $request->war_date == null)
        {
            $NA = 'N/A';
            $data = [
            'name'=>$request->item_name,
            'item_type_id'=>$request->type,
            'specification'=>$request->specification,
            'brand' => $request->item_brand,
            'model' => $request->item_model,
            'imei_no' => $request->imei_no,
            'purchase_date' => $request->pur_date,
            'warranty_date' => $NA,
            'cost'=>$request->cost,
            'vendor_id' => $request->vendor,
            'deduction_amt'=>$request->deduction_amt,
            'monthly_deduction_amt'=>$request->monthly_deduction
        ];
        $dat = $this->itemRepo->create($data);

        $itemId = $dat->id;        
        $status = 'New';
        $By = auth()->user()->emp_code;
        $status_date = $request->pur_date;
        $assign_item = EmployeeItem::create([

            'item_id' => $itemId,
            'status'=>$status,
            'status_dates'=>$status_date,
            'is_active' => 1,
            'created_by'=>$By,
            'updated_by'=>$By

        ]);

        return response()->json($data,200);
        }

        else{

        $data = [
            'name'=>$request->item_name,
            'item_type_id'=>$request->type,
            'specification'=>$request->specification,
            'brand' => $request->item_brand,
            'model' => $request->item_model,
            'imei_no' => $request->imei_no,
            'purchase_date' => $request->pur_date,
            'warranty_date' => $request->war_date,
            'cost'=>$request->cost,
            'vendor_id' => $request->vendor,
            'deduction_amt'=>$request->deduction_amt,
            'monthly_deduction_amt'=>$request->monthly_deduction
        ];
        $dat = $this->itemRepo->create($data);

        $itemId = $dat->id;        
        $status = 'New';
        $By = auth()->user()->emp_code;
        $status_date = $request->pur_date;
        $assign_item = EmployeeItem::create([

            'item_id' => $itemId,
            'status'=>$status,
            'status_dates'=>$status_date,
            'is_active' => 1,
            'created_by'=>$By,
            'updated_by'=>$By

        ]);

        return response()->json($data,200);
        
        }


    }

    public function assignItemToEmp(Request $request) // Added by Mehdi 28 JAN 2020 at: 06:15 PM
    {   
        $rules = [
            'assign_date' => 'required',
        ];
        $messages = [
            'assign_date.required'=> 'Assign Date is required!',
        ];

        $this->validate($request,$rules,$messages);
        $status = 'Assigned';
        $By = auth()->user()->emp_code;
        $assign_date = date("Y-m-d");
        $status_date = date("Y-m-d");

        $update_assign_item = EmployeeItem::where('id',$request->id)->update(['is_active'=>0]); //New

        Log:info('EmployeeItem id:  ' . $request->id . ' Item id: ' .$request->itemid);


        $assign_item = EmployeeItem::create([

            'emp_id'=>$request->emp_name,
            'item_id'=>$request->itemid,
            'status'=>$status,
            'status_dates'=>$request->assign_date,
            'is_active' => 1,
            'created_by'=>$By,
            'updated_by'=>$By

        ]);
        // return response()->json($assign_item,200);
        // $change_status = DB::table('item')->where('id','=', $request->id)->update(array('status' => $status));


        return response()->json(['messages'=>'Item assigned successfully.'],200);            
        }

    public function getEmployeeAssignedItem(Request $request)
    {
        return view('item.assign-item');
    }

    public function getItemWithType(Request $request)
    {
         $data = $this->itemRepo->getItemWithType();
         return response()->json($data,200);
    }

    public function getVendorWithId(Request $request)
    {
         $data = $this->itemRepo->getVendorWithId();
         return response()->json($data,200);
    }

    public function getTypeWithId(Request $request)
    {
         $data = $this->itemRepo->getTypeWithId();
         return response()->json($data,200);
    }

    // public function assignItemToEmployee(Request $request)
    // {   
    //     $status = 'Assigned';
    //     $response = $this->itemRepo->assignItemToEmployee($request->emp_name, $request->type, $status);
    //     return response()->json(['msg'=>'Item assigned successfully.'],200);
    // }

    public function getEmployeeItem(Request $request)
    {
        $employees = Employee::where('company_id',1)->where('is_active',1)->where('skip_deduction',0)->get();
        foreach ($employees as $key => $value) {
        
        $response[] = $this->itemRepo->getEmployeeItem($value->emp_no);
        
        }
        return $response;
    }

    public function generateEmpItemLedger(Request $request)
    {
        $employees = Employee::where('company_id',$request->sub_company_id)->where('is_active',1)->get();
        $month = $request->month;
        $year = $request->year;
        foreach ($employees as $key => $value) { 
            $response = $this->itemRepo->generateEmpItemLedger($value->emp_no, $month, $year);
        }
        return response()->json(['msg'=>'Item ledger succesfully created.'],200);
    }

    public function itemDeductionSummary(Request $request)
    {
        if($request->month != ""){
            $month = $request->month;
        }else{
            $month =  \Carbon\Carbon::now()->subMonth()->format('F');
        }
        $response = $this->itemRepo->itemDeductionSummary($month);
        return response()->json($response,200);
    }

    public function itemLedger(Request $request)
    {
        return view('ledger.item_ledger');
    }

    public function getItemLedger()
    {
        $response = $this->itemRepo->itemLedger(auth()->user()->emp_code);
        if($response->isEmpty())
        {
            $response = [];
        }
        return response()->json($response,200);
    }


    public function ItemAction(Request $request, $id)
    {

        $items = EmployeeItem::where('item_id', $id)->first();
        $response =  $this->itemRepo->getItems($items->status);
        $By = auth()->user()->emp_code;
        $status_date = date("Y-m-d");
        $recover = 'Recovered';
        $snatch = 'Snatched';
        $dead = 'Dead';
        $sold = 'Sold';
        $isActive = 0;

        if($response.$request->status == 'Assigned')
        {
            // $action = Item::where('id', $id)->update(['status'=> 'Recovered']);
            // $action = 'Unable to recovered, please recover again';

            $action4 = EmployeeItem::where('item_id',$id)->update(['is_active',$isActive]); // New
            $action = DB::table('employee_item')->insert(array(
                'item_id'=>$id, 'status'=>$recover, 'created_by'=>$By, 'is_active'=>1, 'updated_by'=>$By));


        }
        elseif($response.$request->status == 'Recovered')
        {
            $action5 = EmployeeItem::where('item_id',$id)->update(['is_active',$isActive]); // New
            $action5 = DB::table('employee_item')->insert(array(
                'item_id'=>$id, 'status'=>$sold, 'created_by'=>$By, 'is_active'=>1, 'updated_by'=>$By));
        }
         else {
                    //echo 'false';
            // $action = Item::find($id);
            // $action->update(['status'=> $request->status,'Assigned'=>($request->status== 'Snatched' || $request->status== 'Dead') ? '' : \Auth::user()->emp_code]);

            $action2 = EmployeeItem::find($id); // New
            $action2 = EmployeeItem::where('item_id',$id)->update(['is_active'=>$isActive]); // New

            $action1 = Item::find($id);
            $action1 = EmployeeItem::create(['item_id'=>$id, 'is_active' => 1, 'created_by'=>$By, 'updated_by'=>$By, 'status'=>$request->status,'Assigned'=>($request->status== $snatch || $request->status== $dead) ? '' : \Auth::user()->emp_code]);

            $action = 'Success';
        }
                  // email notification                  

        return response()->json($action,200);
    }


    public function getListOfReport_1(Request $request)
    {
        $response =  $this->itemRepo->getListOfReport_1();
        return response()->json($response,200);
    }

    public function getListOfReport_2(Request $request)
    {
        $response =  $this->itemRepo->getListOfReport_2();
        return response()->json($response,200);
    }

    // public function getItemType()
    // {
    //     $itemType =   ItemType::where('is_active',1)->get(['id','name']);
    //     return $itemType;
    // }

    public function editItem(Request $request)
    {
        $item =  Item::find($request->id);
        $type = ItemType::where('id',$item->item_type_id)->get();
        $vendor = ItemVendor::where('id',$item->vendor_id)->get();

        return view('item.edit')->with('item',$item)->with('type',$type)->with('vendor',$vendor);   
    }

    public function editItems(Request $request)
    {
        $rules =  [
            'item_name' => 'required|string',
            'type' => 'required|string',
            'item_brand' => 'required|string',
            'item_model' => 'required|string',
            'imei_no' => 'required|string',
            'cost' => 'required|numeric',
            'pur_date' => 'required',
        ];

        $messages = [
           'item_name.required' => 'Item name is required!',
            'item_name.string' => 'Item name must be Alphabetic!',
            'item_brand.string' => 'Brand should be in Alphbetic!',
            'type.required' => 'Type is required!',
            'item_brand' => 'Brand is required!',
            'item_model' => 'Model is required!',
            'imei_no' => 'IMEI No. is required!',
            'cost.required' => 'Cost is required!',
            'cost.numeric' => 'Cost is numeric!',
            'pur_date.required' => 'Purchase date must be required!',
       ];

        $item =  Item::find($request->id);
        $item->name = $request->item_name;
        $item->brand = $request->item_brand;
        $item->model = $request->item_model;
        $item->imei_no = $request->imei_no;
        $item->item_type_id = $request->type;
        $item->cost = $request->cost;
        $item->vendor_id = $request->vendor;
        $item->purchase_date = $request->pur_date;
        $item->warranty_date = $request->war_date;
        $item->deduction_amt = $request->deduction_amt;
        $item->monthly_deduction_amt = $request->monthly_deduction;
        $item->save();
        return  redirect()->route('items')->with('flash-message','&#9996;  Item has been updated Successfully!');
    }

    public function addRemarks(Request $request) // Added by Mehdi 28 JAN 2020 at: 06:15 PM
    {   
        
        $status = 'Assigned';
        $remark = $request->remarks;
        $By = auth()->user()->emp_code;

        // $update_assign_item = EmployeeItem::where('item_id',$request->id)->update(['is_active'=>0]); //New

        $assign_item = EmployeeItem::where('id',$request->id)->update([

            'remarks'=>$request->remarks,
            'updated_by'=>$By

        ]);
        // return response()->json($assign_item,200);
        // $change_status = DB::table('item')->where('id','=', $request->id)->update(array('status' => $status));


        return response()->json(['messages'=>'Remarks Added Successfully.'],200);            
        }

        public function myItems(Request $request)
        {
        return view('item.my-items');
        }

        public function getMyItems(Request $request)
        {
        $response =  $this->itemRepo->getMyItems();
        return response()->json($response,200);
        }

    
}