<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\AttendanceLog;

class KPIController extends Controller
{
    //

    public function monthlyTimingReport()
    {
    	
    	$data = AttendanceLog::select(\DB::raw('SUM(late) as late,SUM(absent) as absent, SUM(f_halfday) as first_halfday, SUM(s_halfday) as second_halfday, SUM(early_going) as early_going'))->where('emp_no',\Auth::user()->emp_code)->where('month','November')->get();
        
    	$json  = json_decode($data);
    	foreach ($json[0] as $key => $value) {
    			# code...
    			$array[] = ['columne'=>ucwords($key), 'column2'=>$value];
    		}

    		return response()->json($array,200);	
 
    }
}
