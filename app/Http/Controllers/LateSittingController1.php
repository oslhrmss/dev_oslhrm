<?php

namespace App\Http\Controllers;

use DummyFullModelClass;
use App\Model\Employee;
use App\Model\DinnerAllowances;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use App\Model\Attendance;
use App\Model\AttendanceAdjustment;
use DB;
use Cache;
use Validator;
use Illuminate\Support\Facades\Log;

class LateSittingController extends Controller
{
    
    public function index()
    {
        return view('dinner.dinner-allowance-request');
    }

    public function allowanceApprovals()
    {
        return view('approvals.allowance');
    }

    public function allowanceTeller()
    {
        return view('dinner.dinner-allowance-teller');
    }

    public function dinner_allowances(){
        $allowances = DinnerAllowances::select('dinner_allowances.*')->where('is_approved',1)->get();
    }

   
    public function allowanceRequest(Request $request)
    {
        
        $now = date_create()->format('Y-m-d');
        $allowance_request = new DinnerAllowances;      
        $allowance_request->emp_id = \Auth::user()->emp_code;
        $allowance_request->allowance_date = $request->allowance_date;
        $allowance_request->reason = $request->reason;
        $allowance_request->created_at = $now;
        $allowance_request->updated_at = $now;

        $data = DB::table('attendance as att')
        ->select('id','time_in','time_out','emp_id','date')
        ->where('emp_id',\Auth::user()->emp_code)
        ->where('date',$request->allowance_date)->where('is_active',1)->orderBy('id','desc')->limit(1)->first();

        
        if($data){
            $adjust_attendance = AttendanceAdjustment::select('atten_id','time_in','time_out')
                                                 ->where('atten_id',$data->id)->where('is_active',1)
                                                 ->orderBy('id','desc')->limit(1)->first();
        }
        elseif($allowance_request->allowance_date == Null && $allowance_request->allowance_date == ''){
            $allowance_request = "&#9888;WARNING:&nbsp; Allowance&#39;s request date is required!";
            return response()->json($allowance_request,400);
        }
        elseif($allowance_request->reason == Null && $allowance_request->reason == ''){
            $allowance_request = "&#9888;WARNING:&nbsp; Allowance&#39;s Reason is required!";
            return response()->json($allowance_request,400);
        }
        elseif ($this->AllowanceSameDateCheck(\Auth::user()->emp_code,$request->allowance_date)){
            $allowance_request = "&#9888;WARNING:&nbsp; You have applied for dinner allowance on this date. Please choose another date!";
            return response()->json($allowance_request,400);
        }
        elseif ($this->checkLastTimeOut(\Auth::user()->emp_code,$request->allowance_date,$data,$data->id,$data->time_in,$data->time_out,$data->date,$adjust_attendance)){
            $allowance_request->save();
            return response()->json($allowance_request,200);
        }
        elseif ($this->checkPunchOutTime(\Auth::user()->emp_code,$request->allowance_date,$data,$data->id,$data->time_in,$data->time_out,$data->date,$adjust_attendance)){
            $allowance_request = "&#9888;WARNING:&nbsp; Unfortunately you are not eligible for applying dinner allowance for requested date. Please choose another date!";
            return response()->json($allowance_request,400);
        }
        elseif(!$data->$request->allowance_date){
            $allowance_request = "&#9888;WARNING:&nbsp; You are unable to apply Dinner Allowance on this date!";
            return response()->json($allowance_request,400);
        }
        else{

            $allowance_request->save();
            return response()->json($allowance_request,200);
        }
        
    }
 
    public function AllowanceSameDateCheck($empCode, $allowance_date)
    {
      $data = DinnerAllowances::select('allowance_date')->where('emp_id',$empCode)->where('allowance_date',$allowance_date)->first();
        
            if($data >= $allowance_date){
                $response = true;
            }
            else{
                $response = false;
            }
        
        return $response;
    }

    public function checkPunchOutTime($empCode, $allowance_date, $data, $id, $time_in, $time_out, $date, $adjust_attendance)
    {   
        $company_id = \Utils::getCompanyId(\Auth::user()->emp_code);
        $region_id  = \Utils::getRegionId(\Auth::user()->emp_code);
        $allowance_avail_duration = \Utils::getSubCompanyAppParamValue('allowance_avail_duration',$company_id,$region_id);

        

        if($adjust_attendance)
        {
            $start = strtotime($adjust_attendance->time_in);
            $end = strtotime($adjust_attendance->time_out);
        }
        else
        {
            $start = strtotime($time_in);
            $end = strtotime($time_out);
        }
        $WorkHours = $end - $start;
        
        if($data) {
            
            if ($WorkHours <= $allowance_avail_duration) {
               
                $response = true;
            }
            else{
                $response = false;
            }

        }else{
            $response = false;
        }

        return $response;

    }


    

    public function checkLastTimeOut($empCode, $allowance_date, $data, $id, $time_in, $time_out, $date, $adjust_attendance)
    {   

        $company_id = \Utils::getCompanyId(\Auth::user()->emp_code);
        $region_id  = \Utils::getRegionId(\Auth::user()->emp_code);
        $allowance_avail_time = \Utils::getSubCompanyAppParamValue('allowance_avail_time',$company_id,$region_id);
        $allowance__secondDay_avail_duration = \Utils::getSubCompanyAppParamValue('allowance_secondDay_avail_duration',$company_id,$region_id);

        

        $lastOut = DB::table('attendance as att')->select('id','time_out','emp_id','date')->where('emp_id',$empCode)->where('date','<',$allowance_date)->where('is_active',1)->orderBy('id','desc')->limit(1)->first();

        $adjust_attendance = AttendanceAdjustment::select('atten_id','time_in','time_out')
                                                 ->where('atten_id',$lastOut->id)->where('is_active',1)
                                                 ->orderBy('id','desc')->limit(1)->first();

        if($adjust_attendance)
         {
             $start = strtotime($adjust_attendance->time_in);
             $end = strtotime($adjust_attendance->time_out);
         }
         else
         {
             $start = strtotime($time_in);
             $end = strtotime($time_out);
         }

        $WorkHours = $end - $start;
        $FixedTime = strtotime($allowance_avail_time);
        $lastOutTime = strtotime($lastOut->time_out);
        
          if($lastOut) {  
            if ($lastOutTime >= $FixedTime && $WorkHours >= $allowance__secondDay_avail_duration) { 
                
                $response = true;
            }
            else{
                $response = false;
            }

          }else{
             $response = false;
         }  
           
            return $response;

    }


    public function getAllowanceApprovals(Request $request){

        $approvals = \DB::table('dinner_allowances as da')
                      ->leftjoin('employees as e','e.emp_no','=','da.emp_id')
                      ->select('e.name as empname','e.emp_no as empcode','da.allowance_date as allowancedate','da.reason as reason','da.is_approved as status','da.id as rowid')
                      ->where('e.report_manager','=',\Auth::user()->emp_code)
                      // ->where('is_paid','=',0)
                      ->orderBy('rowid','desc')
                      ->get();    

        if($approvals){
            foreach ($approvals as $key => $value) {
                    
                    $allowance[] = [ 
                        'empname' => $value->empname,
                        'empcode' => $value->empcode,
                        'status' => $value->status,
                        'allowancedate' => $value->allowancedate,
                        'reason' => $value->reason,
                        'rowid'=>$value->rowid
                        ];   
                }

        }else{
            $allowance = [];  
        } 
        
        return response()->json($approvals,200);   

    }


    public function AllowanceApprovalAction(Request $request, $id)
    {

        $allowance = DinnerAllowances::where('id', $id)->first();
        $not_paid = 0;
        $cancel = 3;
        
        if($allowance.$request->status == 1)
        {
            $action = DinnerAllowances::where('id', $id)->update(['is_approved'=> $cancel]);
            $action = 'Cancelled!';
        }
         else {

            $action = DinnerAllowances::find($id);
            $action->update(['is_approved'=>$request->status,'approved_by'=>($request->status== 0 || $request->status== 2) ? '' : \Auth::user()->emp_code,'is_paid'=>($request->status== 0 || $request->status== 2) ? '' : $not_paid]);
            $action = 'Status updated Successfully!';
        }
      
        return response()->json($action,200);
    }


    public function getApprovedAllowances(Request $request){

        $approvedAllowances = \DB::table('dinner_allowances as da')
                      ->leftjoin('employees as e','e.emp_no','=','da.emp_id')
                      ->select('e.name as empname','e.emp_no as empcode','da.allowance_date as allowancedate','da.is_approved as status','da.id as rowid', 'da.is_paid as paidStatus', \DB::raw('IFNULL(u1.name,"N/A") as approveby'))
                      ->leftjoin('users as u1','u1.emp_code','=','da.approved_by')
                      ->where('is_approved','=','1')
                      ->orderBy('rowid','desc')
                      ->get();    

        if($approvedAllowances){
            foreach ($approvedAllowances as $key => $value) {

                    $allowance[] = [
                        'empname' => $value->empname,
                        'empcode' => $value->empcode,
                        'status' => $value->status,
                        'allowancedate' => $value->allowancedate,
                        'approveby' => $value->approveby,
                        'paidStatus' => $value->paidStatus,
                        'rowid'=>$value->rowid
                        ];   
                }

        }else{
            $allowance = [];  
        } 
        
        return response()->json($approvedAllowances,200);   

    }


    public function dinnerAllowancePaidAction(Request $request, $id)
    {
        $ispaidAction =  DinnerAllowances::where('id',$id)->first();
        if($ispaidAction->is_approved == 0 || $ispaidAction->is_approved == 2)
        {
            $response = "Something went wrong";
            return response()->json($response,400);

        }else{
            $response = DinnerAllowances::where('id', $id)->update(['is_paid'=> $request->status,'paid_at'=>\Carbon\Carbon::today()->toDateString(), 'paid_by'=>\Auth::user()->emp_code]);
            $response = 'Paid Successfully!';         
            return response()->json($response,200);
        }
    }


    public function getMyAllowanceList(Request $request){

    $data = DB::table('dinner_allowances as da')
            ->select('da.id as rowid','da.emp_id as self','da.allowance_date as ad','da.reason as reasons','da.is_approved as Status','da.is_paid as paidStatus','da.created_at as apply_at')
            ->where('da.emp_id','=',\Auth::user()->emp_code)
            ->get();
    return $data;
    }


//End Bracket
}
