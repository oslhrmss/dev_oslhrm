<?php

namespace App\Http\Controllers;
use App\Model\SubCompany;
use App\Model\SubCompanyLeaveType;
use App\Model\LeaveType;
use App\Model\LeaveDeduction;
use App\Model\TempMedLeave;
use App\Model\Employee;
use App\Model\PayBackLeaves;
use App\Model\EmployeeLeave;
use App\Repositories\LeaveTypeRepository;
use App\Repositories\SubCompanyRepository;
use App\Repositories\SubCompanyLeaveTypeRepository;
use App\Notifications\TestEmails;
use App\Jobs\LeaveCancelJob;
use App\Jobs\leaveApplicationEmailJobs;
use Illuminate\Http\Request;
use Validator;
use DB;
use Illuminate\Support\Facades\Log;

class LeaveController extends Controller
{
    //
    private $subCompanyRepo;
    private $leaveTypeRepo;
    private $scltRepo;

    public function __construct(SubCompanyRepository $subCompanyRepo, LeaveTypeRepository $leaveTypeRepo, SubCompanyLeaveTypeRepository $scltRepo)
    {
        $this->subCompanyRepo = $subCompanyRepo; 
        $this->leaveTypeRepo = $leaveTypeRepo; 
        $this->scltRepo = $scltRepo; 
    }
    public function index(Request $request)
    {
        return view('leaves.index');
    }

    public function getSubCompany(Request $request)
    {
        return $this->subCompanyRepo->getAll();
    }
    public function getLeaveType(Request $request)
    {
        return $this->leaveTypeRepo->getAll();
    }

    public function allEmployeeLeaveListView(Request $request)
    {
        return view('leaves.all-employee-leave-list');
    }
    public function allEmployeeLeaveBalance(Request $request)
    {
        return view('leaves.all-employee-leave-balance');
    }

    public function assignCompanyLeave(Request $request)
    {
        $rules = [
            'sub_company_id' => 'required',
            'leave_type' => 'required',
            'no_of_leave' => 'required | numeric',
        ];
        $messages = [
            'sub_company_id.required' => 'Please Select Company.',
            'leave_type.required' => 'Please Select Leave Type.',
            'no_of_leave.required' => 'Please Enter no of leaves.',
            'no_of_leave.numeric' => 'No of leave must be in numeric.',
        ];

        $this->validate($request,$rules,$messages);
        
            $sub_company_leave_type = new SubCompanyLeaveType;
            $sub_company_leave_type->sub_company_id = $request->sub_company_id; 
            $sub_company_leave_type->leave_type_id = $request->leave_type;
            $sub_company_leave_type->no_of_leaves = $request->no_of_leave;
            $sub_company_leave_type->save(); 
           
            return response()->json($sub_company_leave_type,200);


    }

    public function getAllCompanyLeaves()
    {
        $response = $this->scltRepo->getAllCompanyLeaves();
        return response()->json($response,200);
    }

    public function getSubCompanyLeaveType(){

        $response = $this->scltRepo->getSubCompanyLeaveType(\Utils::getCompanyId(\Auth::user()->emp_code));
        return response()->json($response,200);
    }

    public function leaveBalanceView()
    {
      return view('leaves.leave-balance');  
    }

    public function leaveApplicationForm(Request $request)
    {
        return view('leaves.leave-application');
    }

    public function leaveApplication(Request $request)
    {
        
        $rules = [
            'leave_type_id' => 'required',
            'from' => 'required|date|date_format:Y-m-d',
            'to' => 'required|date|date_format:Y-m-d|after_or_equal:from',
            'reason' => 'required',
            // 'leave_year' => 'date|date_format:Y'
        ];
        $messages = [
            'leave_type_id.required'=> 'Leave type is required.',
            'from.required'=> 'Select FROM date.',
            'to.required'=> 'Select TO date.',
            'from.unique' => 'From date has already been taken.',
            'to.unique' => 'To date has already been taken.',
            'reason.required' => 'Please enter reason',
        ];

        
        $this->validate($request,$rules,$messages);
                
        $employee_leave =  new EmployeeLeave;    
        $employee_leave->emp_id = \Auth::user()->emp_code;
        $employee_leave->sub_company_leave_type_id = \Utils::getSubCompanyLeaveTypeId(\Auth::user()->emp_code,$request->leave_type_id);
        $employee_leave->from = $request->from;
        $employee_leave->to = $request->to;
        $employee_leave->reason = $request->reason;
        $employee_leave->application_date = date('Y-m-d');
        

        $from_month = date("m",strtotime($employee_leave->from));
        $cur_month = date('m');

        if ($from_month == 12 && $cur_month == 12) {

        $last_dates = array(26, 27, 28, 29, 30, 31);

        $from_date = date("d",strtotime($employee_leave->from));

        if(in_array($from_date , $last_dates)) {

           $cur_year = date('Y');
           $next_year = $cur_year+1;

           $employee_leave->leave_year = $next_year;

        }

        else{

        $employee_leave->leave_year = date('Y');

        }

        }

        else{

            $employee_leave->leave_year = date('Y');

        }
        
        // get company and regionId
        $company_id = \Utils::getCompanyId(\Auth::user()->emp_code);
        $region_id  = \Utils::getRegionId(\Auth::user()->emp_code);
        $leave_last_date = \Utils::getSubCompanyAppParamValue('leave_last_date',$company_id, $region_id);
        if($this->leaveLastDayCheck($employee_leave->from, $employee_leave->to)){
            $employee_leave = "Unable to apply leave of previous month.";
            return response()->json($employee_leave,400);
        }
        elseif(\Utils::checkPreviousLeavesApproved(\Auth::user()->emp_code))
        {
            $employee_leave = "You can't apply for leave until your previous leaves are approved.";
            return response()->json($employee_leave,400);
        }
        elseif(\Utils::getEmpTypeKey(auth()->user()->emp_code) != 'perm'){
            $employee_leave = "You can't apply, until your probation has been completed.";
            return response()->json($employee_leave, 400);   
        }
        elseif(\Utils::leaveSameDateCheck(\Auth::user()->emp_code,$request->from,$request->to)){
            $employee_leave = "Something went wrong, please use different dates.";
            return response()->json($employee_leave,400);
        }
        elseif($this->leaveTypeRepo->checkLeaveBalanceZero(auth()->user()->emp_code,$request->leave_type_id) <= 0){
            $employee_leave = "You don't have sufficient leave to apply.";
            return response()->json($employee_leave,400);
        }
        elseif( $this->leaveTypeRepo->checkLeaveLimit($request->leave_type_id, $request->from, $request->to, auth()->user()->emp_code) ){
            $employee_leave = "You can't apply leave more than leave limit.";
            return response()->json($employee_leave, 400);
        }
        elseif( ($this->checkCasualLimit($request->leave_type_id, $request->from, $request->to, auth()->user()->emp_code) && $request->leave_type_id == 1 )){
            $employee_leave = "You can't apply more than 3 Casual leaves.";
            return response()->json($employee_leave, 400);
        }

        // elseif(($this->approvalExistsInTempMedLeave($request->leave_type_id, $request->from, $request->to, auth()->user()->emp_code) && $request->leave_type_id == 3 )){
        //     $employee_leave = " &#9995; Please contact H.R!";
        //     return response()->json($employee_leave, 400);
        // }
        elseif ( ($this->approvalExistsInTempMedLeave($request->leave_type_id, $request->from, $request->to, auth()->user()->emp_code) && $request->leave_type_id == 3) ) {
            $employee_leave->save();
            $id = (\Auth::user()->emp_code);
            $remove_request_from_temp_med_leave = DB::table('temp_med_leave')->where('emp_id',$id)->where('is_approved','=',1)->delete();
            return response()->json($employee_leave, 200);
        }

        elseif( ($this->checkMedicalLeaveLimit($request->leave_type_id, $request->from, $request->to, auth()->user()->emp_code) && $request->leave_type_id == 3 )){
            $employee_leave = " &#9995; Please contact H.R!";
            return response()->json($employee_leave, 400);
        }


        // }elseif($this->leaveTypeRepo->checkLeaveBalanceZero(auth()->user()->emp_code,$request->leave_type_id) <= 2)
        // {
        //     $employee_leave = "You don't have sufficient Casual leave to apply.";
        //     return response()->json($employee_leave,400);
        // }
        // elseif($this->leaveTypeRepo->checkLeaveBalanceZero(auth()->user()->emp_code,$request->leave_type_id) <= 3)
        // {
        //     $employee_leave = "You don't have sufficient Medical leave to apply.";
        //     return response()->json($employee_leave,400);
        // }
        // }

        // elseif( $this->leaveTypeRepo->checkLeaveLimit($request->leave_type_id, $request->from, $request->to, auth()->user()->emp_code) )
        // {
        //     $employee_leave = "ALERT: You are exceeding your Annual leaves limit.";
        //     return response()->json($employee_leave, 400);
        // }
        
        // Below is for Casual

        // elseif($this->leaveTypeRepo->checkLeaveBalanceZero(auth()->user()->emp_code,$request->leave_type_id) <= 1)
        // {
        //     $employee_leave = "You don't have sufficient Casual leave to apply.";
        //     return response()->json($employee_leave,400);
        
        // }elseif( $this->leaveTypeRepo->checkLeaveLimit($request->leave_type_id, $request->from, $request->to, auth()->user()->emp_code,$request->leave_type_id) <= 1){
        //     $employee_leave = "ALERT: You are exceeding your Casual leave limit.";
        //     return response()->json($employee_leave, 400);
        // }elseif( $this->leaveTypeRepo->checkLeaveLimit($request->leave_type_id, $request->from, $request->to, auth()->user()->emp_code,$request->leave_type_id) <= 1){
        //     $employee_leave = "ALERT: Either your Casual leaves has been Zero or you are trying to apply extra leaves. Please note that you can't apply more than leave limit.";
        //     return response()->json($employee_leave, 400);
        // }
        // elseif( $this->leaveTypeRepo->checkLeaveLimit($request->leave_type_id, $request->from, $request->to, auth()->user()->emp_code,$request->leave_type_id) <= 2){
        //     $employee_leave = "ALERT: You are excedding your Casual leave limit.";
        //     return response()->json($employee_leave, 400);
        // }
        // }elseif( $this->leaveTypeRepo->Casuals_in_one_GO($request->leave_type_id, $request->from, $request->to, auth()->user()->emp_code,$request->leave_type_id) <= 1){
        //     $employee_leave = "You can not more than apply 3 Casual leaves in a row.";
        //     return response()->json($employee_leave, 400);
        // }


        else{

            $employee_leave->save();

            // email notification                  
            $email_job = (new leaveApplicationEmailJobs($employee_leave->id, auth()->user()->emp_code))->delay(\Carbon\Carbon::now()->addSeconds(30));
            dispatch($email_job);
            return response()->json($employee_leave,200);
        }
    }



    public function myLeaves()
    {
        return view('leaves.my-leaves');
    }


    public function leaveApprovals()
    {
        return view('approvals.leave');
    }

    //leave ledger view
    public function leaveLedger(Request $request)
    {
        return view('ledger.leave_ledger');
    }

// this function for managers
    // public function getLeaveApprovals(Request $request)
    // {   
    //     // if($leave_year == '0000'){
    //     //     $leave_year = date('Y');
    //     // }

    //     $approvals = \DB::table('employee_leave as empl')
    //                   ->select('e.name as empname','e.emp_no as empcode','lt.name as leavetype','empl.from','empl.to','empl.is_approved as status','empl.application_date as appliedat', 'empl.leave_year','empl.id as rowid',\DB::raw('(DATEDIFF(empl.to,empl.from)-1) as days'))
    //                   ->leftjoin('employees as e','e.emp_no','=','empl.emp_id')
    //                   ->leftjoin('sub_company_leave_type as sclt','sclt.id','=','empl.sub_company_leave_type_id') 
    //                   ->leftjoin('leave_type as lt','lt.id','=','sclt.leave_type_id')
    //                   ->where('e.report_manager',\Auth::user()->emp_code)
    //                   ->where('e.is_active',1)
    //                   ->orderBy('empl.application_date','asc')
    //                   ->get();

    //     //                if(!$data->isEmpty()){
    //     // foreach ($data as $key => $value) {
    //     //             # code...
    //     //             $response[] = [
    //     //                 'name' => $value->empname,
    //     //                 'emp_no' => $value->empcode,
    //     //                 'leavetype' => $value->leavetype,                        
    //     //                 'from' => $value->from,
    //     //                 'to' => $value->to,
    //     //                 'days' => $value->days - \Utils::sundaySandwich($value->empcode, $value->from, $value->to, $leave_year),
    //     //                 'applied_at' => $value->appliedat,
    //     //                 'status' => $value->status,
    //     //                 // 'approved_by' => $value->approved_by,
    //     //                 // 'approved_at' => $value->approved_at,
    //     //                 // 'rowid' => $value->rowid,
    //     //                 // 'leave_id' => $value->leave_id,
    //     //                 // 'emp_no' => $value->emp_no,
    //     //                 // 'reason' => $value->reason
    //     //             ];
    //     //         }
    //     // }else{
    //     //     $response = [];
    //     // }

    //     return response()->json($approvals,200);            

    // }


// this function for managers
    // public function getLeaveApprovals(Request $request)
    // {
    //     $leave_year = date('Y');
    //     $approvals = \DB::table('employee_leave as empl')
    //                   ->select('e.name as empname','e.emp_no as empcode','lt.name as leavetype','empl.from','empl.to','empl.is_approved as status','empl.application_date as appliedat','sclt.id as leave_id','empl.id as rowid',\DB::raw('(DATEDIFF(empl.to,empl.from)+1) as days'))
    //                   ->leftjoin('employees as e','e.emp_no','=','empl.emp_id')
    //                   ->leftjoin('sub_company_leave_type as sclt','sclt.id','=','empl.sub_company_leave_type_id') 
    //                   ->leftjoin('leave_type as lt','lt.id','=','sclt.leave_type_id')
    //                   ->where('e.report_manager',\Auth::user()->emp_code)
    //                   ->where('e.is_active',1)
    //                   ->orderBy('empl.application_date','asc')
    //                   ->get();


    //         if(!$approvals->isEmpty()){
    //             foreach ($approvals as $key => $value) {
    //             # code...
    //             $response[] = [
    //                 'empname' => $value->empname,
    //                 'empcode' => $value->empcode,
    //                 'leavetype' => $value->leavetype,
    //                 'from' => $value->from,
    //                 'to' => $value->to,
    //                 'days' => $value->days - \Utils::sundaySandwich($value->empcode, $value->from, $value->to, $value->leave_id),
    //                 'appliedat'=> $value->appliedat,
    //                 'status' => $value->status
    //                 ];
    //             }
    //     }else{
    //         $response = [];
    //     }
    //     return $response;
        
                                       
    //    // return response()->json($approvals,200);            

    // }
    // public function getLeaveApprovalss(Request $request){

    //     $response = $this->leaveTypeRepo->getLeaveApprovalss(\Auth::user()->emp_code);
    //     return $response;
    // }

    public function getLeaveApprovals(Request $request){

        $approvals = \DB::table('employee_leave as empl')
                      ->select('e.name as empname','e.emp_no as empcode','lt.name as leavetype','empl.from','empl.to','empl.is_approved as status','empl.application_date as appliedat','sclt.id as leave_id','empl.id as rowid',\DB::raw('(DATEDIFF(empl.to,empl.from)+1) as days'))
                      ->leftjoin('employees as e','e.emp_no','=','empl.emp_id')
                      ->leftjoin('sub_company_leave_type as sclt','sclt.id','=','empl.sub_company_leave_type_id') 
                      ->leftjoin('leave_type as lt','lt.id','=','sclt.leave_type_id')
                      ->where('e.report_manager',\Auth::user()->emp_code)
                      ->where('e.is_active',1)
                      ->orderBy('empl.application_date','asc')
                      ->get();    
                    
        //{"empname":"Hammad ul hasan","empcode":68,"leavetype":"Annual Leaves","from":"2019-01-07","to":"2019-01-08","status":1,"appliedat":"2019-01-01","leave_id":2,"rowid":538,"days":2}    
            //{"empname":"Hammad ul hasan","empcode":68,"leavetype":"Annual Leaves","from":"2019-01-07","to":"2019-01-08","status":1,"appliedat":"2019-01-01","leave_id":2,"rowid":538,"days":2}
        if($approvals){
            foreach ($approvals as $key => $value) {
                    # code...
                    $approvals_without_holidays[] = [   //Changed variable name from response ...... by Saquib 15-Jan-2020
                        'empname' => $value->empname,
                        'empcode' => $value->empcode,
                        'leavetype' => $value->leavetype,
                        'from' => $value->from,
                        'to' => $value->to,
                        'status' => $value->status,
                        'appliedat' => $value->appliedat,
                        'leave_id'=> $value->leave_id,
                        'rowid'=>$value->rowid,
                        'days' => $value->days - \Utils::sundaySandwich($value->empcode, $value->from, $value->to, $value->leave_id)   
                    ];
                    
                }
                
        }else{
            $approvals_without_holidays = [];
            
        } 
        
            
            return response()->json($approvals_without_holidays,200);   

    }

    public function leaveLastDayCheck($from, $to)
    {
        $company_id = \Utils::getCompanyId(\Auth::user()->emp_code);
        $region_id  = \Utils::getRegionId(\Auth::user()->emp_code);
        $leave_last_date = \Utils::getSubCompanyAppParamValue('leave_last_date',$company_id, $region_id);
        if ( $leave_last_date < \Carbon\Carbon::now()->format('d'))
        {
            $days = \Utils::getDaysAndDatesBetweenTwoDates($from, $to);
            for($i=0; $i < count($days) ; $i++)
            {
                if($days[$i] <= date('Y-m-'.$leave_last_date))
                {
                    $leave_last_date = $days[$i];
                    $response = true;
                    break;
                } else {
                    $response =  false;
                }
            }

        } else {
            $response =  false;
        }

        return $response;
    }

    public function checkCasualLimit($leave_id, $from, $to, $empCode){ // Edited by Mehdi 27 Jan 2020
        
        $company_id = \Utils::getCompanyId(\Auth::user()->emp_code);
        $region_id  = \Utils::getRegionId(\Auth::user()->emp_code);
        $casual_leave_limit = \Utils::getSubCompanyAppParamValue('casual_leave_limit',$company_id, $region_id);
        $holidays = \Utils::sundaySandwich($empCode, $from, $to, $leave_id);
        $appliedDays = \Utils::getDaysBetweenTwoDates($from , $to) - $holidays; // remove +1 in dates

            if( $appliedDays <= $casual_leave_limit ){

                return false;
             }

            else{

                return true;
             }
    }

    public function checkMedicalLeaveLimit($leave_id, $from, $to, $empCode){ // Edited by Mehdi 27 Jan 2020
        
        $company_id = \Utils::getCompanyId(\Auth::user()->emp_code);
        $region_id  = \Utils::getRegionId(\Auth::user()->emp_code);
        $medical_leave_limit = \Utils::getSubCompanyAppParamValue('medical_leave_limit',$company_id, $region_id);
        $holidays = \Utils::sundaySandwich($empCode, $from, $to, $leave_id);
        $appliedDays = \Utils::getDaysBetweenTwoDates($from , $to) - $holidays; // remove +1 in dates
        // $medleave = TempMedLeave::where('emp_id',$empCode)->where('is_approved',1)->exists();
            
            if( $appliedDays <= $medical_leave_limit){

                return false;
             }

            else{

                return true;
             }
    }

    public function approvalExistsInTempMedLeave($leave_id, $from, $to, $empCode)
    {   
        $company_id = \Utils::getCompanyId(\Auth::user()->emp_code);
        $region_id  = \Utils::getRegionId(\Auth::user()->emp_code);
        $medical_leave_limit = \Utils::getSubCompanyAppParamValue('medical_leave_limit',$company_id, $region_id);
        $holidays = \Utils::sundaySandwich($empCode, $from, $to, $leave_id);
        $appliedDays = \Utils::getDaysBetweenTwoDates($from , $to) - $holidays;

        $medleave = TempMedLeave::where('emp_id',$empCode)->where('is_approved',1)->exists();

        if($medleave == 1){
            return true;
        }

        else{
            return false;
        }


        Log::info('med leave:  ' . $medleave . '  emp code: ' . $empCode);      
    }
   /////

    public function leaveApprovalAction(Request $request, $id)
    {

        $leave_days = EmployeeLeave::where('id', $id)->first();
        $response =  $this->leaveLastDayCheck($leave_days->from, $leave_days->to);   
        if($response)
        {
            $action = EmployeeLeave::where('id', $id)->update(['is_approved'=> 2]);
            $action = 'Unable to approve, please apply again';
            
        } else {
                    //echo 'false';
            $action = EmployeeLeave::find($id);
            $action->update(['is_approved'=> $request->status,'approved_at'=>($request->status== 0 || $request->status== 2) ? '' : \Carbon\Carbon::today()->toDateString(),'approved_by'=>($request->status== 0 || $request->status== 2) ? '' : \Auth::user()->emp_code]);
            $action = 'Success';
        }
                  // email notification                  

        return response()->json($action,200);
    }

    public function employeeAllLeaves(Request $request)
    {
        $response = $this->leaveTypeRepo->employeeAllLeaves(\Auth::user()->emp_code, \Utils::getCompanyId(\Auth::user()->emp_code));
        return $response;
    }

    public function editLeaveDuration(Request $request)
    {
        $rules = [
            'reason' => 'required',
            'from' => 'required|date|date_format:Y-m-d',
            'to' => 'required|date|date_format:Y-m-d|after_or_equal:from',
        ];
        $messages = [
            'reason.required'=> 'Reason must be entered.',
            'from.required'=> 'Select FROM date.',
            'to.required'=> 'Select TO date.',
            'from.unique' => 'From date has already been taken.',
            'to.unique' => 'To date has already been taken.',
        ];
        $this->validate($request,$rules,$messages);

        if(\Utils::leaveSameDateCheck(\Auth::user()->emp_code,$request->from,$request->to))
        {
            $leave =  "Something went wrong, please use different dates.";
            return response()->json($leave,400);

        }elseif($this->leaveTypeRepo->checkLeaveLimit($request->leave_id, $request->from, $request->to, auth()->user()->emp_code))
        {
            $leave =  "You can't apply leave more than leave limit.";
            return response()->json($leave,400);            
        }else if($this->leaveLastDayCheck($request->from, $request->to)){
            $leave =  "Unable to apply leave of previous month.";
            return response()->json($leave,400);
        }

        elseif( ($this->checkCasualLimit($request->leave_id, $request->from, $request->to, auth()->user()->emp_code) && $request->leave_id == 1 )){
            $employee_leave = "You can't apply more than 3 Casual leaves.";
            return response()->json($employee_leave, 400);
        }

        elseif( ($this->checkMedicalLeaveLimit($request->leave_id, $request->from, $request->to, auth()->user()->emp_code) && $request->leave_id == 3 )){
            $leave = " &#9995; Please contact H.R!";
            return response()->json($leave, 400);
        }

        else{
        $leave = EmployeeLeave::where('id', $request->id)->update(['from'=>$request->from,'to'=>$request->to,'reason'=>$request->reason, 'is_approved'=>0]);
        return response()->json($leave,200);            
        }

    }

    public function employeeLeaveLedger(Request $request)
    {
        $leave_ledger_detail =  LeaveDeduction::select('e.name as name','e.emp_no as emp_code',\DB::raw('SUM(deducted_leave) as total_deduction'),'leave_deduction.date as date','leave_deduction.month as month')
            ->join('employees as e','e.emp_no','=','leave_deduction.emp_id')
            ->where('leave_deduction.emp_id', \Auth::user()->emp_code)
            ->where('leave_year', '=', '2020')
            ->groupBy('leave_deduction.month')
            ->get();

        return response()->json($leave_ledger_detail,200);    
    }


    public function editSubCompanyNoOfLeaves(Request $request)
    {
       $rules = [
        'no_of_leaves' => 'required|numeric',
        ];
        $messages = [
        'no_of_leaves.required'=> 'Please enter no of leaves.',
        'no_of_leaves.numeric'=> 'No of leaves must be in numeric form.',
        ];
        $this->validate($request,$rules,$messages);

        return response($this->scltRepo->editSubCompanyNoOfLeaves($request->id,$request->no_of_leaves),200);
    }


    public function allEmployeeLeaveList()
    {
        $response = $this->leaveTypeRepo->allEmployeeLeaveList();
        return $response;   
    }


     public function getLeaveBalance(Request $request)
    {
        $empCode = \Auth::user()->emp_code; 
        $response = $this->leaveTypeRepo->getLeaveBalance($empCode);
        return response()->json($response, 200);
    }

    public function getLeaveBalanceH(Request $request)
    {
        $empCode = \Auth::user()->emp_code; 
        $response = $this->leaveTypeRepo->getLeaveBalanceH($empCode);
        return response()->json($response, 200);
    }


    public function getAllEmployeeLeaveBalance(Request $request)
    {
        $employees = Employee::where('company_id', $request->sub_company_id)->where('is_active','=',1)->get(['emp_no','name']);
        $leave_year = $request->leave_year;
        foreach ($employees as  $value) {
            # code...
            $data[] = $this->leaveTypeRepo->getLeaveBalance($value->emp_no,$leave_year);
        }

        return response()->json($data,200);
    }


    public function payBackLeaves(Request $request)
    {
        return view('leaves.payback-leave');
    }

    public function generatePayBackLeave(Request $request)
    {

        $rules = [
            'employee' => 'required',
            'date' => 'required',
        ];
        $messages = [
            'employee.required'=> 'Please select employee.',
            'date.required'=> 'Please select leave date.',
        ];
        $this->validate($request,$rules,$messages);  
      //$month = $request->month;

      // $emp_id = $request->employee;
      // $paid_at = \Carbon\Carbon::now()->format('Y-m-d');
      // $month = \Carbon\Carbon::parse($request->date)->format('F');
      // $leave_year = \Carbon\Carbon::parse($request->date)->format('Y');
      // $payBack = count(explode(" ",$request->date));
      //       if($payBack){
      //       $data = PayBackLeaves::updateOrCreate(['emp_id'=>$emp_id,'leave_date'=> $request->date],
      //           [
      //               'emp_id' => $emp_id,
      //               'no_of_leaves' => $payBack,
      //               'paid_at' => $paid_at,
      //               'leave_date' => $request->date,
      //               'paid_by' => \Auth::user()->id,
      //               'month'=> $month,
      //               'leave_year' => $leave_year,
      //           ]);
      //       }

        $emp_id = $request->employee;
      $paid_at = \Carbon\Carbon::now()->format('Y-m-d');
      $month = \Carbon\Carbon::parse($request->date)->format('F');
      $leave_year = \Carbon\Carbon::parse($request->date)->format('Y');
      $payBack = count(explode(" ",$request->date));


        $from_month = date("m",strtotime($request->date));
        $cur_month = date('m');

        if ($from_month == 12) {

        $payback_last_dates = array(26, 27, 28, 29, 30, 31);

        $from_date = date("d",strtotime($request->date));

        if(in_array($from_date , $payback_last_dates)) {

           $cur_year = date('Y');
           $next_year = $cur_year;

           $leave_year = $next_year;

        }

        else{

        $leave_year = date('Y')-1;

        }

        }

        else{

            $leave_year = date('Y');

        }
            if($payBack){
            $data = PayBackLeaves::updateOrCreate(['emp_id'=>$emp_id,'leave_date'=> $request->date],
                [
                    'emp_id' => $emp_id,
                    'no_of_leaves' => $payBack,
                    'paid_at' => $paid_at,
                    'leave_date' => $request->date,
                    'paid_by' => \Auth::user()->id,
                    'month'=> $month,
                    'leave_year' => $leave_year,
                ]);
            }
        
        return response()->json(['msg'=>'Payback leave generated successfully.'],200);
    }


    public function employeePayBackLeaves(Request $request)
    {
        $response =  \Utils::employeePayBackLeaves(\Auth::user()->emp_code, true);
        return $response;
    }

    public function payBackLeavesList(Request $request)
    {
        return view('leaves.payback_leave_list');
    }

   public function getPayBackLeavesList(Request $request)
   {
       $response = $this->leaveTypeRepo->getPayBackLeaveList();
       return response()->json($response,200);
   }

   public function  getLeaveIdByName()
   {
       return \Utils::getLeaveIdByName()['payback_leaves'];
   }
   public function checkLeaveBalanceZero()
   {
    $response = $this->leaveTypeRepo->checkLeaveBalanceZero(auth()->user()->emp_code,$leaveId = 2);
    return $response;
   }

    public function editPayBackLeave(Request $request)
    {
        $response =  $this->leaveTypeRepo->editPayBackLeave($request->id,$request->from);
        return $response;
    } 

    public function deletePayBackLeave(Request $request)
    {
        $response =  PayBackLeaves::deletePaybackLeave($request->id);
        return response()->json($response,200);
    }

    public function leaveCancel($id)
    {
        $response = EmployeeLeave::find($id);
        $email_job = (new LeaveCancelJob($response))->delay(\Carbon\Carbon::now()->addSeconds(30));
        dispatch($email_job);
        $response->delete();
        return response()->json($response,200);
    }

    public function TempLeaveApprovals()
    {
        return view('approvals.temp-medical-leave-approval');
    }

    public function approveMedLeave(Request $request) // Added by Mehdi 28 JAN 2020 at: 06:15 PM
    {   
        
        $assign_item = TempMedLeave::create([
            'emp_id'=>$request->emp_id,
        ]);
        // return response()->json($assign_item,200);
        // $change_status = DB::table('item')->where('id','=', $request->id)->update(array('status' => $status));
        return response()->json(['messages'=>'Medical leave has been successfully approved from H.R!'],200);
    }

    
}


//SELECT leave_type.name, leave_ledger.`deducted_leave` FROM sub_company_leave_type INNER JOIN leave_ledger ON leave_ledger.`leave_type_id` = sub_company_leave_type.`leave_type_id` INNER JOIN leave_Type ON leave_type.id =  sub_company_leave_type.`leave_type_id`


// //SELECT GROUP_CONCAT(leave_Type.name,' : ',leave_ledger.deducted_leave) 
// FROM sub_company_leave_type 
// INNER JOIN leave_ledger ON leave_ledger.`leave_type_id` = sub_company_leave_type.`leave_type_id`
// INNER JOIN leave_type ON leave_type.id = sub_Company_leave_Type.`leave_type_id`
// WHERE leave_ledger.month = 'DECEMBER' AND leave_ledger.emp_id = 284
// GROUP BY sub_company_leave_type.id