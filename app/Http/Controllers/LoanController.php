<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Loan;
use App\Model\AdvancedSalary;
use Utitlies\Utils;
use App\Model\Role;
use App\Model\LoanLedger;
use App\Model\Employee;
use Illuminate\Support\Facades\Log;
class LoanController extends Controller
{
    //


    public function getLoanApplicationForm(Request $request)
    {
    	return view('loan.loan-application');
    }
    public function getLoanApprovalView(Request $request)
    {
        return view('approvals.loan');
    }
    public function getAppParams()
    {
        $sys_param = \DB::table('app_param')->get();
        foreach ($sys_param as $key => $value) {
                # code...
        $params[$value->config_key] = $value->config_value;
        }
        return $params;
    }
    public function getLoans(Request $request)
    {

    	$loans = Loan::select('employees.name as EmpName','loan.amount','loan.monthly_deduction_amount as monthlyamount','loan.id as loan_id','loan.no_of_installments as installments','loan.apply_at',\DB::raw('IFNULL(u1.name,"N/A") as approvedby'),'loan.is_approved as status','loan.reason as reason','loan.is_paid as paid',\DB::raw('IFNULL(loan.paid_at,"N/A") as paid_at'),\DB::raw('IFNULL(u2.name,"N/A") as paid_by'))
    		->join('employees','employees.emp_no','=','loan.emp_id')
            ->leftjoin('users as u1','u1.emp_code','=','loan.approved_by')
            ->leftjoin('users as u2','u2.emp_code','=','loan.paid_by')
    		->where('loan.emp_id',\Auth::user()->emp_code)
            ->orderBy('loan.id','desc')
    		->get();

    	return response()->json($loans, 200);	
    }

    public function loanApplication(Request $request)
    {

       $params = $this->getAppParams();
       
    	$rules = [
            'amount'=>'required|numeric',
            'no_of_installments'=>'required|numeric|min:1|max:24',
            'reason'=>'required|max:500'

        ];
        $messages = ['amount.required'=>'Please enter amount.','amount.numeric'=>'Amount must be numeric value.','required.no_of_installments'=>'Pleas enter no of installments.','no_of_installments.numeric'=>'Installments must be numeric value.','no_of_installments.max'=>'Installments can not be more than 2 year.'
        ];

    $this->validate($request, $rules, $messages);

    	$loan  = new Loan;
    	$loan->emp_id =  \Auth::user()->emp_code;
        $loan->amount  = $request->amount;
        $loan->monthly_deduction_amount = round($request->amount/ $request->no_of_installments);
        $loan->deduction_type  = 'system';
//        $loan->monthly_deduction_amount = $request->amount - ($request->amount/ $request->no_of_installments) * $request->no_of_installments + ($request->amount/ $request->no_of_installments);    
        // deduction amount value formula.
        // final installment vallue = loan_amt- instalmentvalue * no of installment + instllmentvalue
    	
        $loan->no_of_installments  = $request->no_of_installments;
    	$loan->apply_at  = date('Y-m-d');	
    	$loan->reason  = $request->reason;

        if(\Utils::checkInstallmentLevelWise(\Auth::user()->emp_code, $request->no_of_installments))
        {
           $loan= "Installments can not be more than 12.";
           return response()->json($loan, 400);
        }elseif(\Utils::getEmployeeDurationInCompany(Employee::where('emp_no',\Auth::user()->emp_code)->value('date_of_hiring')) < 12 ){
            $loan = "You can't apply, until your 1 year employment has been completed.";
            return response()->json($loan, 400);   
        }

        elseif(\Utils::lastLoanPaymentDuration(\Auth::user()->emp_code) < 12){ // Solved
            $loan = "You can't apply, before 1 year of previous loan.";
            Log::info("Emp Id: " . \Auth::user()->emp_code . " loan last payment duration: " .\Utils::lastLoanPaymentDuration(\Auth::user()->emp_code) );
            return response()->json($loan, 400);   
        }

        elseif( $this->previousLoanClearCheck(\Auth::user()->emp_code) )
        {
            Log::info("Check prev. loan:  " . $this->previousLoanClearCheck(\Auth::user()->emp_code));
        	$loan = "You can't apply, until your previous balance is clear.";
            return response()->json($loan, 400);
        }elseif(\Utils::loanAmountGradeWsie(\Auth::user()->emp_code, $request->amount)){
            $loan = "You can't apply, your amount is out of limit";
            return response()->json($loan, 400);
        }else{
            $loan->save();
            return response()->json($loan, 200);
        }


    	

    	

    }

    public function getLoanApprovals(Request $request)
    {
        $roles =  \DB::table('role_user')->join('roles','roles.id','=','role_user.role_id')->where('role_user.user_id',\Auth::user()->id)->pluck('roles.name')->toArray();
        if(in_array('hr-manager', $roles) || in_array('finance', $roles))
        {
        	        // $approval = \DB::table('employees as e1')
                 //    ->select('e1.name as empname','e1.emp_no as empno','l.apply_at as applied_at','l.monthly_deduction_amount as monthlyamount','l.no_of_installments as installments','l.amount as loan_amount','l.reason as reason','l.is_approved as status','l.is_paid as paid_status','l.id as rowid')
                 //    ->join('employees as e2','e1.report_manager','=','e2.emp_no')
                 //    ->join('loan as l','l.emp_id','=','e1.emp_no')
                 //    ->where('e2.emp_no',\Auth::user()->emp_code)
                 //    ->orderBy('l.id','desc')
                 //    ->get();
                $approval = \DB::table('loan as l')
                    ->select('e.name as empname','e.emp_no as empno','l.monthly_deduction_amount as monthlyamount','l.no_of_installments as installments','l.amount as loan_amount','l.apply_at as applied_at','l.reason as reason','l.is_approved as status','l.is_paid as paid_status','l.id as rowid',\DB::raw('IFNULL(u1.name,"N/A") as approvedby'))
                    ->join('employees as e','e.emp_no','=','l.emp_id')
                    ->leftjoin('users as u1','u1.emp_code','=','l.approved_by')
                    ->orderBy('l.id','desc')
                    ->where(function($query){
                        if(auth()->user()->roles->first()->name != 'admin')
                        {
                            $query->where('e.company_id',\Utils::getCompanyId(auth()->user()->emp_code));
                        }
                    })->get();
        }

         for($i =0; $i < count($approval); $i++)
         {
        	if(in_array('hr-manager', $roles)){
		        $approval[$i]->is_manager = 1;
        	}else{
        		$approval[$i]->is_manager = 0;
        	}
        	if(in_array('finance', $roles)){

		    	$approval[$i]->is_finance = 1;   
        	}else{
        		$approval[$i]->is_finance = 0;
        	}

         }

        return response()->json($approval, 200);            
    }

    public function loanApprovalAction(Request $request, $id)
    {
        $isapproveAction =  Loan::where('id',$id)->first();
        if($isapproveAction->is_paid == 1 )
        {
            $response = "Something went wrong";
            return response()->json($response,400);

        }else{

            $response = Loan::where('id', $id)->update(['is_approved'=> $request->status,'approved_at'=>\Carbon\Carbon::today()->toDateString(),'approved_by'=>\Auth::user()->emp_code]);
            return response()->json($response,200);
        }
    }

    public function loanPaidAction(Request $request, $id)
    {
        $ispaidAction =  Loan::where('id',$id)->first();
        if($ispaidAction->is_approved == 0 || $ispaidAction->is_approved == 2)
        {
            $response = "Something went wrong";
            return response()->json($response,400);

        }else{
            $response = Loan::where('id', $id)->update(['is_paid'=> $request->status,'paid_at'=>\Carbon\Carbon::today()->toDateString(),'paid_by'=>\Auth::user()->emp_code]);         
            return response()->json($response,200);
        }

//        $action = Loan::where('id', $id)->update(['is_paid'=> $request->status,'paid_at'=>\Carbon\Carbon::today()->toDateString(),'paid_by'=>\Auth::user()->emp_code]);
    }

    public function editLoanAmount(Request $request)
    {
        $params = $this->getAppParams();
    	$rules = ['amount'=>'required | numeric' ,'no_of_installments'=>'required|numeric|min:1|max:24'];
    	$messages = ['amount.required'=>'Please enter amount.','amount.numeric'=>'Amount must be numeric value.','required.no_of_installments'=>'Pleas enter no of installments.','no_of_installments.numeric'=>'Installments must be numeric value.','no_of_installments.max'=>'Installments can not be more than 2 years.'
        ];
    	$this->validate($request,$rules,$messages);
//    	$response = Loan::where('id',$request->id)->update(['amount'=>$request->amount]);
    	$response = Loan::find($request->id);
    	$response->amount = $request->amount; 
    	$response->monthly_deduction_amount = $request->amount/$request->no_of_installments;
        $response->no_of_installments = $request->no_of_installments;
        $response->deduction_type = 'system';
     
        if(\Utils::checkInstallmentLevelWise(\Auth::user()->emp_code, $request->no_of_installments))
        {
           $response= "Installments can not be more than 12.";
           return response()->json($response, 400);
        }elseif(\Utils::getEmployeeDurationInCompany(Employee::where('emp_no',\Auth::user()->emp_code)->value('date_of_hiring')) < 12 ){
            $response = "You can't apply, until your 1 year employment has been completed.";
            return response()->json($response, 400);   
        }elseif(\Utils::loanAmountGradeWsie(\Auth::user()->emp_code, $request->amount)){
            $response = "You can't apply, your amount is out of limit";
            return response()->json($response, 400);
        }
        else{
       	$response->save(); 
    	return response()->json($response,200);
        }
    }

    public function loanLedger(Request $request)
    {   
        return view('ledger.loan_ledger');
    }
    /*********Advance salary **********************/

    public function advanceSalaryForm(Request $request)
    {
        return view('loan.advanced-salary');
    }

    public function getAdvSalApprovalView(Request $request)
    {
        return view('approvals.advance-salary');
    }
	
	public function getAdvSalList(Request $request)
    {
        return view('loan.adv-salary-list');
    }
    public function getLoanList(Request $request)
    {
        return view('loan.loan-list');
    }

	// applyign advance salary
    public function advanceSalary(Request $request)
    {
        $params = $this->getAppParams();
        $rules = [
            
            'amount'=>'required|numeric|integer|min:1',
            'reason'=>'required|max:500',

        ];
        $this->validate($request, $rules);
        $adv_salary = new AdvancedSalary;
        $adv_salary->emp_id = \Auth::user()->emp_code;
        $adv_salary->amount = $request->amount;
        $adv_salary->apply_at = \Carbon\Carbon::today()->toDateString();
        // $adv_salary->approved_by = \Auth::user()->id;
        $adv_salary->reason = $request->reason;
        $percent_of_salary =  \Utils::getSubCompanyAppParamValue('percent_of_salary', \Utils::getCompanyId(\Auth::user()->emp_code),\Utils::getRegionId(\Auth::user()->emp_code));
        // \DB::enableQueryLog();
        // AdvancedSalary::where('emp_id',\Auth::user()->emp_code)->where(\DB::raw('MONTH(apply_at)'), \Carbon\Carbon::now()->month)->count();
        // echo print_r(\DB::getQueryLog());

        if($request->special_perm == 1)
        {
           if(AdvancedSalary::where('emp_id',\Auth::user()->emp_code)->where('is_approved',0)->where('is_paid',0)->orderBy('id','desc')->limit(1)->exists()){
           $adv_salary= "Sorry, you cannot request for adv. salary until your previous is paid or approved.";
           return response()->json($adv_salary, 400);
           } else {
            $adv_salary->is_special = $request->special_perm;
           $adv_salary->save();
           return response()->json($adv_salary, 200);

           }
        } 
        elseif(\Carbon\Carbon::today()->toDateString() > date('Y-m').'-25')
        {
           $adv_salary= "Sorry, you cannot request for adv. salary after 25 of month";
           return response()->json($adv_salary, 400);
        }elseif(AdvancedSalary::where('emp_id',\Auth::user()->emp_code)->where(\DB::raw('YEAR(apply_at)'), \Carbon\Carbon::now()->year)->where(\DB::raw('MONTH(apply_at)'), \Carbon\Carbon::now()->month)->count() > 0){
           $adv_salary= "Sorry, you cannot request for adv. salary more than one within the month";
           return response()->json($adv_salary, 400);
        }elseif(\Utils::getEmployeeDurationInCompany(Employee::where('emp_no',\Auth::user()->emp_code)->value('date_of_hiring')) <= 3 ){
            $adv_salary = "You can't apply, until your probation has been completed.";
            return response()->json($adv_salary, 400);   
        }elseif(\Utils::getThreeMonthGap(\Auth::user()->emp_code, $advance_salary = true)){
            $adv_salary = "You can't apply, until 3 month completed of previous advance.";
            return response()->json($adv_salary, 400);
        }elseif(\Utils::percentOfSalary($percent_of_salary,Employee::where('emp_no',\Auth::user()->emp_code)->value('current_salary'),$request->amount)){
            $adv_salary = "You can't apply, more than ".$percent_of_salary." amount of your salary.";
            return response()->json($adv_salary, 400);
        }else{
           $adv_salary->save();
           return response()->json($adv_salary, 200);          
 
        }
        
        
    }

    public function getAdvSalary(Request $request)
    {
        $adv_sal = AdvancedSalary::select('employees.name as EmpName','advanced_salary.amount','advanced_salary.apply_at',\DB::raw('(employees.current_salary - advanced_salary.amount) as balance'),\DB::raw('MONTHNAME(advanced_salary.apply_at) as month'),'advanced_salary.is_approved as status',\DB::raw('IFNULL(u1.name,"N/A") as approvedby'),'advanced_salary.reason','advanced_salary.is_paid',\DB::raw('IFNULL(advanced_salary.paid_at, "N/A") as paid_at'),\DB::raw('IFNULL(u2.name, "N/A") as paid_by'),'advanced_salary.id as rowid')
            ->join('employees','employees.emp_no','=','advanced_salary.emp_id')
            ->leftjoin('users as u1','u1.emp_code','=','advanced_salary.approved_by')
            ->leftjoin('users as u2','u2.emp_code','=','advanced_salary.paid_by')
            ->where('advanced_salary.emp_id','=',\Auth::user()->emp_code)
            ->orderBy('advanced_salary.id','desc')
            ->get();

        return response()->json($adv_sal, 200);   
    }


    public function getAdvSalApprovals(Request $request)
    {
        $roles =  \DB::table('role_user')->join('roles','roles.id','=','role_user.role_id')->where('role_user.user_id',\Auth::user()->id)->pluck('roles.name')->toArray();
        if(in_array('hr-manager', $roles) || in_array('finance', $roles)){
        $approval = \DB::table('advanced_salary as advs')
                    ->select('e.name as empname','e.current_salary as actual_salary','e.emp_no as empno','advs.amount as adv_amount','advs.apply_at as applied_at','advs.reason as reason','advs.is_approved as status','advs.is_paid as paid_status','advs.id as rowid',\DB::raw('IFNULL(u1.name,"N/A") as approvedby'))
                    ->join('employees as e','e.emp_no','=','advs.emp_id')
                    ->leftjoin('users as u1','u1.emp_code','=','advs.approved_by')
                    ->where(function($query){
                        if(auth()->user()->roles->first()->name != 'admin')
                        {
                            $query->where('e.company_id',\Utils::getCompanyId(auth()->user()->emp_code));
                        }
                    })
                    ->orderBy('advs.id','asc')
                    ->get();
    	}
     

 
    	
    

    	for($i =0; $i < count($approval); $i++)
    	{
        	if(in_array('hr-manager', $roles)){
		        $approval[$i]->is_manager = 1;
        	}else{
        		$approval[$i]->is_manager = 0;
        	}
        	if(in_array('finance', $roles)){

		    	$approval[$i]->is_finance = 1;   
        	}else{
        		$approval[$i]->is_finance = 0;
        	}
    		
    	}

    	return response()->json($approval, 200);			
    }

    public function advSalApprovalAction(Request $request, $id)
    {
    	$isapproveAction =  AdvancedSalary::where('id',$id)->first();
    	if($isapproveAction->is_paid == 1 )
    	{
    		$response = "Something went wrong";
	        return response()->json($response,400);

    	}else{

        $response = AdvancedSalary::where('id', $id)->update(['is_approved'=> $request->status,'approved_at'=>\Carbon\Carbon::today()->toDateString(),'approved_by'=>\Auth::user()->emp_code]);
        return response()->json($response,200);
    	}
    }
    public function advSalPaidAction(Request $request, $id)
    {
    	$ispaidAction =  AdvancedSalary::where('id',$id)->first();
    	if($ispaidAction->is_approved == 0 || $ispaidAction->is_approved == 2)
    	{
    		$response = "Something went wrong";
	        return response()->json($response,400);

    	}else{
			$response = AdvancedSalary::where('id', $id)->update(['is_paid'=> $request->status,'paid_at'=>\Carbon\Carbon::today()->toDateString(),'paid_by'=>\Auth::user()->emp_code]);    		
        	return response()->json($response,200);
    	}

//        $action = AdvancedSalary::where('id', $id)->update(['is_paid'=> $request->status,'paid_at'=>\Carbon\Carbon::today()->toDateString(),'paid_by'=>\Auth::user()->emp_code]);
    }

    public function editAdvSalAmount(Request $request)
    {
        $params = $this->getAppParams();
    	$rules = ['amount'=>'required | numeric |integer | min:1'];
    	$messages = ['amount.required'=>'Amount must be enter','amount.numeric'=>'Amount must be numeric value'];
    	$this->validate($request,$rules,$messages);
        $percent_of_salary =  \Utils::getSubCompanyAppParamValue('percent_of_salary', \Utils::getCompanyId(\Auth::user()->emp_code),\Utils::getRegionId(\Auth::user()->emp_code));

        if(\Utils::percentOfSalary($percent_of_salary,Employee::where('emp_no',\Auth::user()->emp_code)->value('current_salary'),$request->amount)){
            
            $response = "You can't apply, more than ".$percent_of_salary." amount of your salary.";
            return response()->json($response, 400);
        }else{            
           $response = AdvancedSalary::where('id',$request->id)->update(['amount'=>$request->amount]);
           return response()->json($response,200);
       }
    }
    
    public function getAdvSalaryList(Request $request)
    {
    	$adv_sal_list = \DB::table('advanced_salary as advs')
    					->select('e.name as empname','e.emp_no as emp_code','advs.amount','advs.apply_at',\DB::raw('MONTHNAME(advs.apply_at) as month'), 'advs.is_approved as status','advs.is_paid as paid','advs.reason as reason',\DB::raw('IFNULL(u1.name, "N/A") as approved_by'),\DB::raw('IFNULL(u2.name, "N/A") as paid_by'))
    					->join('employees as e','e.emp_no','=','advs.emp_id')
    					->leftjoin('users as u1','u1.emp_code','=','advs.approved_by')
                        ->leftjoin('users as u2','u2.emp_code','=','advs.paid_by')
                        ->where(function($query){
                            if(auth()->user()->roles->first()->name != 'admin')
                            {
                                $query->where('e.company_id',\Utils::getCompanyId(auth()->user()->emp_code));
                            }
                        })
                        ->get();
    	return response()->json($adv_sal_list,200);				
    }


    public function getLoansList(Request $request)
    {
        // loanlist
        $loan_list = \DB::table('loan as l')
                        ->select('e.name as empname','e.emp_no as emp_code','l.amount','l.apply_at',\DB::raw('MONTHNAME(l.apply_at) as month'),'l.no_of_installments as installments','l.is_approved as status','l.is_paid as paid','l.reason as reason')
                        ->join('employees as e','e.emp_no','=','l.emp_id')
                        ->where(function($query){
                            if(auth()->user()->roles->first()->name != 'admin')
                            {
                                $query->where('e.company_id',\Utils::getCompanyId(auth()->user()->emp_code));
                            }
                        })
                        ->get();
        return response()->json($loan_list,200);             
    }

    public function previousLoanClearCheck($empCode) // edited where is approved = 1
    {
    	// first it will check employee exists 
        if(Loan::where('emp_id',$empCode)->where('is_approved', '!=', 2)->exists()){
        $loan_id = Loan::where('emp_id',$empCode)->orderBy('id','desc')->limit(1)->value('id');    
        $balance = LoanLedger::select('loan_ledger.balance')->join('loan','loan.id','=','loan_ledger.loan_id')->where('loan.id',$loan_id)->orderBy('loan_ledger.id','desc')->limit(1)->first(); // first
        if($balance){
            $response  = ($balance->balance > 0) ? true : false;
        }else{
            $response = true;
        }
        }else{
            $response = false;
        }
        return $response;
    }


    public function employeeLoanLedger(Request $request)
    {

       $ledger =  LoanLedger::select('e.name','l.amount as amount','loan_ledger.date','l.id as loan_id','l.monthly_deduction_amount', 'loan_ledger.deduction_amount as deduction',\DB::raw('(l.amount - loan_ledger.balance) as paid_amount'),'loan_ledger.month as month','loan_ledger.balance')
            ->join('loan as l','l.id','=','loan_ledger.loan_id')
            ->leftjoin('employees as e','e.emp_no','=','l.emp_id')
            ->where('l.emp_id', \Auth::user()->emp_code)
            ->orderBy('loan_ledger.id','asc')            
            ->get();

        return response()->json($ledger, 200);
    }


    public function loanSetOff(Request $request)
    {
        return view('loan.loan-set-off');
    }


    public function getLoanSetOffHistory(Request $request)

    {
        $rules = ['emp_id'=>'required | numeric'];
        $messages = [
            'emp_id.required'=>'Employee code must be entered.',
            'emp_id.numeric'=>'Employee code must be numeric.',
        ];

        $this->validate($request, $rules, $messages);
        $history =  \DB::table('loan as l')
        ->select('l.id','e.name','e.emp_no','l.monthly_deduction_amount','l.amount',\DB::raw('IFNULL(ll.deduction_amount,l.monthly_deduction_amount) as deduction_amount'),\DB::raw('IFNULL(ll.balance,l.amount) as balance'),'l.apply_at','ll.month')
        ->leftjoin('loan_ledger as ll','ll.loan_id','=','l.id')
        ->join('employees as e','e.emp_no','=','l.emp_id')
        ->where('l.emp_id', $request->emp_id)
        ->orderBy('ll.id','desc')
        ->first();

        if($history)
        {
            session()->put('balance',$history->balance);
            session()->put('m_ded_amt',$history->monthly_deduction_amount);
            return response()->json($history,200);            
            
        }else{
           return response()->json(['msg'=>'No record found.'],400);  
       }            
   }


    public function setSetOffLoanAmount(Request $request)
    {
        $rules = ['amount'=>'required | numeric'];
        $messages = [
            'amount.required'=>'Amount must be entered.',
            'amount.numeric'=>'Amount must be numeric.',
        ];

        $this->validate($request, $rules, $messages);
        
        if(session()->get('balance') <= 0){
            return response()->json(["msg"=>"Select user loan has been completed."],400);
        }elseif(session()->get('balance') <  $request->amount){
            return response()->json(["msg"=>"Amount can't be greater than remaining balance."],400);
        }elseif(session()->get('m_ded_amt') <= session()->get('balance') && session()->get('m_ded_amt') > $request->amount){
            // this condition will if dedcution amount <=  balacne hai and request amoutn dedcution amount se chota hai 
            return response()->json(["msg"=>"Amount can't be less than monthly dedcution amount. [Mon. Deduction Amount: ".session()->get('m_ded_amt')."]"],400);
        }
       $amount = LoanLedger::updateOrCreate(
            ['loan_id'=>$request->loan_id,
            'deduction_amount' => $request->amount,
            'deduction_type' => 'manual',
            'balance' => session()->get('balance') - $request->amount,
            'month'=>\Carbon\Carbon::now()->format('F'),
            'loan_year'=>date('Y'),
            'date'=>\Carbon\Carbon::now()->format('Y-m-d'),
        ]);

        session()->forget('balance');
        session()->forget('m_ded_amt');
        return response()->json($amount,200);
    }

}
