<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\LoyalityAward;
use App\Http\Requests\AwardRequest;

class LoyalityAwardController extends Controller
{
    //


    public function index(LoyalityAward $award)
    {
//    	$awards =  $award->all();
        $awards =  $award->whereHas('employee', function($q){
            if(auth()->user()->roles->first()->name != 'admin')
            {
            $q->where('company_id',\Utils::getCompanyId(auth()->user()->emp_code));
            }
        })->get();
    	return view('awards.index')->with(compact('awards'));
    }

    public function addAwards(AwardRequest $request)
    {
    	$validate =  $request->validate();
    	$data = [
    		'emp_no'=>$request->emp_no,
    		'type'=>$request->type,
    		'amount'=>$request->amount,
    		'year'=>\Carbon\Carbon::now()->year,
    		'date'=>\Carbon\Carbon::now()->format('Y-m-d'),
    	];
	   if(LoyalityAward::create($data)){
	   	return redirect()->back()->with('success', 'Awarded Successfully.');

	   }
	   return redirect('/');
    }

    public function edit(Request $request, $id, LoyalityAward $award)
    {

    	$awards =  LoyalityAward::whereEmpNo($request->id)->whereYear($request->year)->first();
    	// dd($awards);
    	return view('awards.edit')->with('awards',$awards);
    }


}
