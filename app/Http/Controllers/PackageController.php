<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\EmployeePackageDetail as Package;
class PackageController extends Controller
{
    //


    public function index()
    {
    	return view('package.index');
    }

    public function getPackageDetail()
    {
    	$package =  Package::join('employees','employee_package_detail.emp_id','=','employees.emp_no')->where('employees.company_id',\Utils::getCompanyId(auth()->user()->emp_code))->get();
    	$result = $package->map(function($value, $key){
    		$array['emp_id']  = $value->employee['emp_no'];
    		$array['pf_amount']  = $value->pf_slab['amount'];
    		$array['pf_id']  = $value->pf_slab['id'];
    		$array['emp_name']  = $value->employee['name'];
    		$array['gross_salary']  = $value->gross_salary;
    		$array['eobi_amount']  = $value->eobi_amount ?: '0.00';
    		$array['fuel_amount']  = $value->fuel_amount ?: '0.00';
    		$array['parking_amount']  = $value->parking_amount ?: '0.00';
    		$array['year']  = $value->year;
    		return $array;
    	});
    	$sort =  $result->sortByDesc('emp_name');
    	return response()->json($result,200);
    }

    public function generatePackage()
    {
      $employees =  \App\Model\Employee::where('is_active',1)->get();
      $employees->map(function($value, $key){
      //$package = new App\Model\EmployeePackageDetail();
          Package::updateOrCreate(['emp_id'=>$value->emp_no, 'year'=>\Carbon\Carbon::now()->format('Y-m-d')],
            [
                'emp_id' => $value->emp_no,
                'gross_salary' => $value->current_salary,
                'fuel_amount' => $value->fuel_amount,
                'parking_amount' => $value->parking_amount,
                'eobi_amount' => $value->eobi_amount,
                'pf_slab_id' => $value->pf_slab_id,
                'year' => \Carbon\Carbon::now()->year,

            ]
        );
      // $package->emp_id = $value->emp_no;
      // $package->gross_salary = $value->current_salary;
      // $package->fuel_amount = $value->fuel_amount;
      // $package->parking_amount = $value->parking_amount;
      // $package->eobi_amount = $value->eobi_amount;
      // $package->pf_slab_id = $value->pf_slab_id;
      // $package->year = \Carbon\Carbon::now()->year;
      // $package->save();  
      });
      return redirect()->back()->with('success', 'Record has been successfully created.');
    }


}
