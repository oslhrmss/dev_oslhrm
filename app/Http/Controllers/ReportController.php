<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\LeaveTypeRepository;
use App\Http\Traits\ReportTrait;
class ReportController extends Controller
{
    //
	use ReportTrait;


	public function leaveDeductionReport(Request $request)
	{
		return view('report.leave-deduction');
	}

	public function getLeaveDeductionReport(Request $request)
	{
		$response =  $this->leaveDeductionReports();
		return response()->json($response,200);
	}


	public function loanDeductionReport(Request $request)
	{
		return view('report.loan-deduction');
	}

	public function getLoanDeductionReport()
	{

		$response =  $this->loanDeductionReports();
		return response()->json($response,200);
	}
	public function itemDeductionReport(Request $request)
	{
		return view('report.item-deduction');
	}

	public function getItemDeductionReport()
	{
		$response =  $this->itemDeductionReports();
		return response()->json($response,200);
	}


}
