<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Traits\GenerateUserTrait;
use Validator;
class UserController extends Controller
{
    //
    use GenerateUserTrait;

    public function index()
    {

    }

    public function changePassword(Request $request)
    {
    	$rules = [
    		'password' => 'required|string|confirmed|min:6',
    	];
    	$messages = [
    		'password.required' => 'Please enter password.',
    		'password.min' => 'Password must be atleast 6 character.'

    	];

    	$this->validate($request, $rules, $messages);
    	//return $password = $request->password;
    	$this->customPasswordChange($request->password);
    	return redirect()->to('/')->with('password-changed','You password has been successfully changed.');
    }


    public function defaultPassword(Request $request)
    {
        return view('security.reset-default-password');
    }

    public function resetDefaultPasswordPost(Request $request)
    {
        $response =  $this->resetDefaultPassword($request->emp_code);
        return response()->json($response,200);
    }

    public function adminResetDefaultPasswordPost(Request $request)
    {
        //return $request->code;
        $response =  $this->adminResetDefaultPassword($request->code);
        return response()->json($response,200);
    }
    
}
