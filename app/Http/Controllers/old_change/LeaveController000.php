<?php

namespace App\Http\Controllers;
use App\Model\SubCompany;
use App\Model\SubCompanyLeaveType;
use App\Model\LeaveType;
use App\Model\LeaveDeduction;
use App\Model\Employee;
use App\Model\PayBackLeaves;
use App\Model\EmployeeLeave;
use App\Repositories\LeaveTypeRepository;
use App\Repositories\SubCompanyRepository;
use App\Repositories\SubCompanyLeaveTypeRepository;
use App\Notifications\TestEmails;
use App\Jobs\LeaveCancelJob;
use App\Jobs\leaveApplicationEmailJobs;
use Illuminate\Http\Request;
use Validator;

class LeaveController extends Controller
{
    //
    private $subCompanyRepo;
    private $leaveTypeRepo;
    private $scltRepo;

    public function __construct(SubCompanyRepository $subCompanyRepo, LeaveTypeRepository $leaveTypeRepo, SubCompanyLeaveTypeRepository $scltRepo)
    {
        $this->subCompanyRepo = $subCompanyRepo; 
        $this->leaveTypeRepo = $leaveTypeRepo; 
        $this->scltRepo = $scltRepo; 
    }
    public function index(Request $request)
    {
        return view('leaves.index');
    }

    public function getSubCompany(Request $request)
    {
        return $this->subCompanyRepo->getAll();
    }
    public function getLeaveType(Request $request)
    {
        return $this->leaveTypeRepo->getAll();
    }

    public function allEmployeeLeaveListView(Request $request)
    {
        return view('leaves.all-employee-leave-list');
    }
    public function allEmployeeLeaveBalance(Request $request)
    {
        return view('leaves.all-employee-leave-balance');
    }

    public function assignCompanyLeave(Request $request)
    {
        $rules = [
            'sub_company_id' => 'required',
            'leave_type' => 'required',
            'no_of_leave' => 'required | numeric',
        ];
        $messages = [
            'sub_company_id.required' => 'Please Select Company.',
            'leave_type.required' => 'Please Select Leave Type.',
            'no_of_leave.required' => 'Please Enter no of leaves.',
            'no_of_leave.numeric' => 'No of leave must be in numeric.',
        ];

        $this->validate($request,$rules,$messages);
        
            $sub_company_leave_type = new SubCompanyLeaveType;
            $sub_company_leave_type->sub_company_id = $request->sub_company_id; 
            $sub_company_leave_type->leave_type_id = $request->leave_type;
            $sub_company_leave_type->no_of_leaves = $request->no_of_leave;
            $sub_company_leave_type->save(); 
           
            return response()->json($sub_company_leave_type,200);


    }

    public function getAllCompanyLeaves()
    {
        $response = $this->scltRepo->getAllCompanyLeaves();
        return response()->json($response,200);
    }

    public function getSubCompanyLeaveType(){

        $response = $this->scltRepo->getSubCompanyLeaveType(\Utils::getCompanyId(\Auth::user()->emp_code));
        return response()->json($response,200);
    }

    public function leaveBalanceView()
    {
      return view('leaves.leave-balance');  
    }

    public function getLeaveBalance(Request $request)
    {
        $empCode = \Auth::user()->emp_code; 
        $response = $this->leaveTypeRepo->getLeaveBalance($empCode);
        return response()->json($response, 200);
    }

    public function leaveApplicationForm(Request $request)
    {
        return view('leaves.leave-application');
    }

    public function leaveApplication(Request $request)
    {
        
        $rules = [
            'leave_type_id' => 'required',
            'from' => 'required|date|date_format:Y-m-d',
            'to' => 'required|date|date_format:Y-m-d|after_or_equal:from',
            'reason' => 'required'
        ];
        $messages = [
            'leave_type_id.required'=> 'Leave type is required.',
            'from.required'=> 'Select FROM date.',
            'to.required'=> 'Select TO date.',
            'from.unique' => 'From date has already been taken.',
            'to.unique' => 'To date has already been taken.',
            'reason.required' => 'Please enter reason',
        ];

        
        $this->validate($request,$rules,$messages);
                
        $employee_leave =  new EmployeeLeave;    
        $employee_leave->emp_id = \Auth::user()->emp_code;
        $employee_leave->sub_company_leave_type_id = \Utils::getSubCompanyLeaveTypeId(\Auth::user()->emp_code,$request->leave_type_id);
        $employee_leave->from = $request->from;
        $employee_leave->to = $request->to;
        $employee_leave->reason = $request->reason;
        $employee_leave->application_date = date('Y-m-d');
        
        // get company and regionId
        $company_id = \Utils::getCompanyId(\Auth::user()->emp_code);
        $region_id  = \Utils::getRegionId(\Auth::user()->emp_code);
        $leave_last_date = \Utils::getSubCompanyAppParamValue('leave_last_date',$company_id, $region_id);
        if($this->leaveLastDayCheck($employee_leave->from, $employee_leave->to)){
            $employee_leave = "Unable to apply leave of previous month.";
            return response()->json($employee_leave,400);
        }elseif(\Utils::checkPreviousLeavesApproved(\Auth::user()->emp_code))
        {
            $employee_leave = "You can't apply for leave until your previous leaves are approved.";
            return response()->json($employee_leave,400);

        }elseif(\Utils::getEmpTypeKey(auth()->user()->emp_code) != 'perm'){
            $employee_leave = "You can't apply, until your probation has been completed.";
            return response()->json($employee_leave, 400);   
        }
        elseif(\Utils::leaveSameDateCheck(\Auth::user()->emp_code,$request->from,$request->to)){
            $employee_leave = "Something went wrong, please use different dates.";
            return response()->json($employee_leave,400);


        }elseif($this->leaveTypeRepo->checkLeaveBalanceZero(auth()->user()->emp_code,$request->leave_type_id) <= 0)
        {
            $employee_leave = "You don't have sufficient leave to apply.";
            return response()->json($employee_leave,400);
        }elseif( $this->leaveTypeRepo->checkLeaveLimit($request->leave_type_id, $request->from, $request->to, auth()->user()->emp_code) ){
            $employee_leave = "You can't apply leave more than leave limit.";
            return response()->json($employee_leave, 400);
        }
        else{

            $employee_leave->save();

            // email notification                  
            $email_job = (new leaveApplicationEmailJobs($employee_leave->id, auth()->user()->emp_code))->delay(\Carbon\Carbon::now()->addSeconds(30));
            dispatch($email_job);
            return response()->json($employee_leave,200);
        }
    }



    public function myLeaves()
    {
        return view('leaves.my-leaves');
    }


    public function leaveApprovals()
    {
        return view('approvals.leave');
    }

    //leave ledger view
    public function leaveLedger(Request $request)
    {
        return view('ledger.leave_ledger');
    }

// this function for managers
    public function getLeaveApprovals(Request $request)
    {
        $approvals = \DB::table('employee_leave as empl')
                      ->select('e.name as empname','e.emp_no as empcode','lt.name as leavetype','empl.from','empl.to','empl.is_approved as status','empl.application_date as appliedat','empl.id as rowid',\DB::raw('(DATEDIFF(empl.to,empl.from)+1) as days'))
                      ->leftjoin('employees as e','e.emp_no','=','empl.emp_id')
                      ->leftjoin('sub_company_leave_type as sclt','sclt.id','=','empl.sub_company_leave_type_id') 
                      ->leftjoin('leave_type as lt','lt.id','=','sclt.leave_type_id')
                      ->where('e.report_manager',\Auth::user()->emp_code)
                      ->where('e.is_active',1)
                      ->orderBy('empl.application_date','asc')
                      ->get();                 
        return response()->json($approvals,200);            

    }

    public function leaveLastDayCheck($from, $to)
    {
        $company_id = \Utils::getCompanyId(\Auth::user()->emp_code);
        $region_id  = \Utils::getRegionId(\Auth::user()->emp_code);
        $leave_last_date = \Utils::getSubCompanyAppParamValue('leave_last_date',$company_id, $region_id);
        if ( $leave_last_date < \Carbon\Carbon::now()->format('d'))
        {
            $days = \Utils::getDaysAndDatesBetweenTwoDates($from, $to);
            for($i=0; $i < count($days) ; $i++)
            {
                if($days[$i] <= date('Y-m-'.$leave_last_date))
                {
                    $leave_last_date = $days[$i];
                    $response = true;
                    break;
                } else {
                    $response =  false;
                }
            }

        } else {
            $response =  false;
        }

        return $response;
    }

    public function leaveApprovalAction(Request $request, $id)
    {

        $leave_days = EmployeeLeave::where('id', $id)->first();
         $response =  $this->leaveLastDayCheck($leave_days->from, $leave_days->to);   
        if($response)
        {
            $action = EmployeeLeave::where('id', $id)->update(['is_approved'=> 2]);
            $action = 'Unable to approve, please apply again';
            
        } else {
                    //echo 'false';
            $action = EmployeeLeave::find($id);
            $action->update(['is_approved'=> $request->status,'approved_at'=>($request->status== 0 || $request->status== 2) ? '' : \Carbon\Carbon::today()->toDateString(),'approved_by'=>($request->status== 0 || $request->status== 2) ? '' : \Auth::user()->emp_code]);
            $action = 'Success';
        }
                  // email notification                  

        return response()->json($action,200);
    }

    public function employeeAllLeaves(Request $request)
    {
        $response = $this->leaveTypeRepo->employeeAllLeaves(\Auth::user()->emp_code, \Utils::getCompanyId(\Auth::user()->emp_code));
        return $response;
    }

    public function editLeaveDuration(Request $request)
    {
        $rules = [
            'reason' => 'required',
            'from' => 'required|date|date_format:Y-m-d',
            'to' => 'required|date|date_format:Y-m-d|after_or_equal:from',
        ];
        $messages = [
            'reason.required'=> 'Reason must be entered.',
            'from.required'=> 'Select FROM date.',
            'to.required'=> 'Select TO date.',
            'from.unique' => 'From date has already been taken.',
            'to.unique' => 'To date has already been taken.',
        ];
        $this->validate($request,$rules,$messages);

        if(\Utils::leaveSameDateCheck(\Auth::user()->emp_code,$request->from,$request->to))
        {
            $leave =  "Something went wrong, please use different dates.";
            return response()->json($leave,400);

        }elseif($this->leaveTypeRepo->checkLeaveLimit($request->leave_id, $request->from, $request->to, auth()->user()->emp_code))
        {
            $leave =  "You can't apply leave more than leave limit.";
            return response()->json($leave,400);            
        }else if($this->leaveLastDayCheck($request->from, $request->to)){
            $leave =  "Unable to apply leave of previous month.";
            return response()->json($leave,400);
        }else{
        $leave = EmployeeLeave::where('id', $request->id)->update(['from'=>$request->from,'to'=>$request->to,'reason'=>$request->reason, 'is_approved'=>0]);
        return response()->json($leave,200);            
        }

    }

    public function employeeLeaveLedger(Request $request)
    {
        $leave_ledger_detail =  LeaveDeduction::select('e.name as name','e.emp_no as emp_code',\DB::raw('SUM(deducted_leave) as total_deduction'),'leave_deduction.date as date','leave_deduction.month as month')
            ->join('employees as e','e.emp_no','=','leave_deduction.emp_id')
            ->where('leave_deduction.emp_id', \Auth::user()->emp_code)
            ->groupBy('leave_deduction.month')
            ->get();

        return response()->json($leave_ledger_detail,200);    
    }


    public function editSubCompanyNoOfLeaves(Request $request)
    {
       $rules = [
        'no_of_leaves' => 'required|numeric',
        ];
        $messages = [
        'no_of_leaves.required'=> 'Please enter no of leaves.',
        'no_of_leaves.numeric'=> 'No of leaves must be in numeric form.',
        ];
        $this->validate($request,$rules,$messages);

        return response($this->scltRepo->editSubCompanyNoOfLeaves($request->id,$request->no_of_leaves),200);
    }


    public function allEmployeeLeaveList()
    {
        $response = $this->leaveTypeRepo->allEmployeeLeaveList();
        return $response;   
    }

    public function getAllEmployeeLeaveBalance(Request $request)
    {
        $employees = Employee::where('company_id', $request->sub_company_id)->get(['emp_no','name']);
        foreach ($employees as  $value) {
            # code...
            $data[] = $this->leaveTypeRepo->getLeaveBalance($value->emp_no);
        }

        return response()->json($data,200);
    }


    public function payBackLeaves(Request $request)
    {
        return view('leaves.payback-leave');
    }

    public function generatePayBackLeave(Request $request)
    {

        $rules = [
            'employee' => 'required',
            'date' => 'required',
        ];
        $messages = [
            'employee.required'=> 'Please select employee.',
            'date.required'=> 'Please select leave date.',
        ];
        $this->validate($request,$rules,$messages);  
      //$month = $request->month;

      $emp_id = $request->employee;
      $paid_at = \Carbon\Carbon::now()->format('Y-m-d');
      $month = \Carbon\Carbon::parse($request->date)->format('F');  
      $payBack = count(explode(" ",$request->date));
            if($payBack){
            $data = PayBackLeaves::updateOrCreate(['emp_id'=>$emp_id,'leave_date'=> $request->date],
                [
                    'emp_id' => $emp_id,
                    'no_of_leaves' => $payBack,
                    'paid_at' => $paid_at,
                    'leave_date' => $request->date,
                    'paid_by' => \Auth::user()->id,
                    'month'=> $month,
                ]);
            }
        //}
        return response()->json(['msg'=>'Payback leave generated successfully.'],200);
    }


    public function employeePayBackLeaves(Request $request)
    {
        $response =  \Utils::employeePayBackLeaves(\Auth::user()->emp_code, true);
        return $response;
    }

    public function payBackLeavesList(Request $request)
    {
        return view('leaves.payback_leave_list');
    }

   public function getPayBackLeavesList(Request $request)
   {
       $response = $this->leaveTypeRepo->getPayBackLeaveList();
       return response()->json($response,200);
   }

   public function  getLeaveIdByName()
   {
       return \Utils::getLeaveIdByName()['payback_leaves'];
   }
   public function checkLeaveBalanceZero()
   {
    $response = $this->leaveTypeRepo->checkLeaveBalanceZero(auth()->user()->emp_code,$leaveId = 2);
    return $response;
   }

    public function editPayBackLeave(Request $request)
    {
        $response =  $this->leaveTypeRepo->editPayBackLeave($request->id,$request->from);
        return $response;
    } 

    public function deletePayBackLeave(Request $request)
    {
        $response =  PayBackLeaves::deletePaybackLeave($request->id);
        return response()->json($response,200);
    }

    public function leaveCancel($id)
    {
        $response = EmployeeLeave::find($id);
        $email_job = (new LeaveCancelJob($response))->delay(\Carbon\Carbon::now()->addSeconds(30));
        dispatch($email_job);
        $response->delete();
        return response()->json($response,200);
    }
}


//SELECT leave_type.name, leave_ledger.`deducted_leave` FROM sub_company_leave_type INNER JOIN leave_ledger ON leave_ledger.`leave_type_id` = sub_company_leave_type.`leave_type_id` INNER JOIN leave_Type ON leave_type.id =  sub_company_leave_type.`leave_type_id`


// //SELECT GROUP_CONCAT(leave_Type.name,' : ',leave_ledger.deducted_leave) 
// FROM sub_company_leave_type 
// INNER JOIN leave_ledger ON leave_ledger.`leave_type_id` = sub_company_leave_type.`leave_type_id`
// INNER JOIN leave_type ON leave_type.id = sub_Company_leave_Type.`leave_type_id`
// WHERE leave_ledger.month = 'DECEMBER' AND leave_ledger.emp_id = 284
// GROUP BY sub_company_leave_type.id