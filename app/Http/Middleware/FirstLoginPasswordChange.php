<?php

namespace App\Http\Middleware;

use Closure;
use App\Model\User;
use Auth;

class FirstLoginPasswordChange
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

            if(\Auth::user()->is_active == 0)
            {
              return 'change passowrd';
            }
        return $next($request);
    }
}
