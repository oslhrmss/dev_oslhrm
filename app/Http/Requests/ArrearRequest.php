<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArrearRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'amount' => 'required | numeric'
        ];
    }

    public function messages()
    {
        return [
            'amount.required' => 'Amount is required!',
            'amount.numeric' => 'Amount must be numeric!',
        ];
    }   
}
