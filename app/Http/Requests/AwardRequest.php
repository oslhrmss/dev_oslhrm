<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AwardRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'emp_no' => 'required|numeric',
            'type' => 'required|numeric',
            'amount' => 'required|numeric',           
        ];
    }


    public function messages()
    {
        return [
            'emp_no.required' => 'Emp No is required',
            'type.required' => 'Type is required!',
            'amount.required' => 'Amount is required!',
            'emp_no.numeric' => 'Emp No is numeric',
            'type.numeric' => 'Type is numeric!',
            'amount.numeric' => 'Amount is numeric!',
        ];
    }
}
