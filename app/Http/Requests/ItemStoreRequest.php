<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ItemStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'item_name' => 'required|string',
            'type' => 'required|string',
            'item_brand' => 'required|string',
            'item_model' => 'required|string',
            'imei_no' => 'required|string',
            'cost' => 'required|numeric',
            'pur_date' => 'required',
            // 'war_date' => 'date',
            // 'item_name' => 'required|string',
            // 'type' => 'required|string',
            // 'cost' => 'required|numeric',
            // 'deduction_amt' => 'required|numeric',
            // 'monthly_deduction' => 'required|numeric',
            // 'amount' => 'required|numeric'
            
        ];
    }


    public function messages()
    {
        return [
            'item_name.required' => 'Item name is required!',
            'item_name.string' => 'Item name must be Alphabetic!',
            'item_brand.string' => 'Brand should be in Alphbetic!',
            'type.required' => 'Type is required!',
            'item_brand' => 'Brand is required!',
            'item_model' => 'Model is required!',
            'imei_no' => 'IMEI No. is required!',
            'cost.required' => 'Cost is required!',
            'cost.numeric' => 'Cost is numeric!',
            'pur_date.required' => 'Purchase date must be required!',
            // 'pur_date.date' => 'Purchase date must be in correct format!',
            // 'item_name.required' => 'Item name is required!',
            // 'type.required' => 'Type is required!',
            // 'cost.required' => 'Cost is required!',
            // 'deduction_amt.required' => 'Deduction amount is required!',
            // 'monthly_deduction.required' => 'Monthly deduction  is required!',
            // 'cost.numeric' => 'Cost is numeric!',
            // 'deduction_amt.numeric' => 'Deduction amount is numeric!',
            // 'monthly_deduction.numeric' => 'Monthly deduction  is numeric!',
        ];
    }
}
