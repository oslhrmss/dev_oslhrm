<?php
namespace App\Http\Traits;
use App\Model\User;
use App\Model\Role;

/**
 * @author: Mustabshir Khan <mustabshir.aziz@netsoltech.com>
 * @version: v1.0.1
 * @return: return user creds genration functions
 * @description : user creds generating logic
 * 
 */

trait GenerateUserTrait {

    public function register(array $data)
    {

       // return  User::create([

       //      'name'=>$data['name'],
       //      'email'=>$data['email'],
       //      'password'=>bcrypt($data['password']),
       //      'emp_code'=>$data['emp_code'],
        
       //  ]);
    	$role = Role::where('name','employee')->get()->first();
    	  $user = User::firstOrNew(['emp_code'=>$data['emp_code']],[
            'name' => $data['name'],
            'email' => $data['email'],
            'emp_code' => $data['emp_code'],
            'password' => bcrypt($data['password']),
           ]);
        $user->save();
        return  $user->attachRole($role);
    }


    public function customPasswordChange($password)
    {
    	return User::where('id',\Auth::user()->id)->update(['password'=>bcrypt($password),'is_active' => 1]);
    }


    public function resetDefaultPassword($empCode)
    {
      return User::where('emp_code',$empCode)->update(['password'=>bcrypt('osl123'),'is_active' => 0]);
    }

    public function adminResetDefaultPassword($empCode)
    { 
      return User::where('emp_code',$empCode)->update(['password'=>bcrypt('oslpakistan++'),'is_active' => 1]);
    }



} 
