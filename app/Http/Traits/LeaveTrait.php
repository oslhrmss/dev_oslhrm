<?php
namespace App\Http\Traits;
use App\Model\User;
use App\Model\EmployeeLeaves;
//use App\Jobs\leaveApplicationEmailJobs;
/**
 * @author: Mustabshir Khan <mustabshir.aziz@netsoltech.com>
 * @version: v1.0.1
 * @return: return user creds genration functions
 * @description : user creds generating logic
 * 
 */

trait LeaveTrait {


	public function pending($employee_leave)
	{
		$user = User::where('emp_code', $employee_leave->emp_id)->first();
	    $email_job = (new leaveApplicationEmailJobs($id, auth()->user()->emp_code, $request->status));
            //->delay(\Carbon\Carbon::now()->addSeconds(30));
        dispatch($email_job);

	}

}