<?php
namespace App\Http\Traits;
use App\LeaveType;
use App\Loan;
use App\Model\LeaveDeduction;
use App\Model\LoanLedger;
use App\Model\SubCompany;
use App\Model\Employee;


/**
 * @author: Mustabshir Khan <mustabshir.aziz@netsoltech.com>
 * @version: v1.0.1
 * @return: return report logics
 * @description : 
 * 
 */

trait ReportTrait {

	public function loanDeductionReports()
	{
		$response = LoanLedger::select('e.name','e.emp_no','loan.amount','loan.monthly_deduction_amount','loan_ledger.deduction_amount','loan_ledger.date','loan_ledger.balance','loan_ledger.deduction_type','loan_ledger.month')
		->join('loan','loan.id','=','loan_ledger.loan_id')
		->join('employees as e','e.emp_no','=','loan.emp_id')
		->orderBy('e.name','asc')
		->orderBy('loan_ledger.id','asc')
		->where(function($query){
            if(auth()->user()->roles->first()->name != 'admin')
            {
                $query->where('e.company_id',\Utils::getCompanyId(auth()->user()->emp_code));
            }
        })
		->get();        
		return $response;            
	}

	public function leaveDeductionReports()
	{
		$response  = \DB::table('leave_deduction as ld')
		->select('e.name','e.emp_no','sc.name as company','lt.name as leavename','ld.leave_type_id','ld.deducted_leave','ld.date','ld.month')
		->leftjoin('leave_type as lt','lt.id','=','ld.leave_type_id')
		->join('sub_company as sc','sc.id','=','ld.company_id')
		->join('employees as e','e.emp_no','=','ld.emp_id')
		->where(function($query){
            if(auth()->user()->roles->first()->name != 'admin')
            {
                $query->where('e.company_id',\Utils::getCompanyId(auth()->user()->emp_code));
            }
        })
		->get();

		return $response;					

	}


	public function itemDeductionReports()
	{
		$response  = \DB::table('emp_item_ledger as etl')
		->select('e.name','e.emp_no','i.name as item','etl.deduction_amt','etl.date','etl.month')
		->join('employee_item as et','et.id','=','etl.emp_item_id')
		->join('item as i','i.id','=','et.item_id')
		->join('employees as e','e.emp_no','=','et.emp_id')
		->where(function($query){
            if(auth()->user()->roles->first()->name != 'admin')
            {
                $query->where('e.company_id',\Utils::getCompanyId(auth()->user()->emp_code));
            }
        })
		->get();

		return $response;					

	}


}