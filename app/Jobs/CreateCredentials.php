<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Http\Traits\GenerateUserTrait;
class CreateCredentials implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels,GenerateUserTrait;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $array;
    protected $payload;
    public function __construct(array $array)
    {
        //
        $this->array  = $array;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        try{
            $this->register($this->array);
            \Log::info('User added ');
        } catch(Exception $e){
            \Log::error("Job Error ". $e->getMessage());
        }
    }
}
