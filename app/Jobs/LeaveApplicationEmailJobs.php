<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Notifications\TestEmails;


class LeaveApplicationEmailJobs implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $emp_obj;
    private $request_id;
    private $status;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($request_id, $emp_obj, $status = 0)
    {
        //
        $this->emp_obj = $emp_obj;
        $this->request_id = $request_id;
        $this->status = $status;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user =  \App\Model\User::where('emp_code', $this->emp_obj)->first();
        logger('Leave Submission email start');
        logger('Request ID: '.$this->request_id.'  Emp No:'.$this->emp_obj);
        $this->leaveSubmissionEmail($user);
        logger('Leave Submission email ends');
        logger('Leave Approval email ends');
        $this->leaveApprovalEmailToManager($user);
        logger('Leave Submission email ends');
       

    }

    public function leaveSubmissionEmail($employee)
    {
        $employee->notify(new TestEmails('Leave Submitted','Your Leave Request #'.$this->request_id.' has been forwarded for approval. To view the details of the request please Log on to OSL HRM.'));
    }

    public function leaveApprovalEmailToManager($user)
    {
            if(\Utils::getManagerEmail($user->emp_code))
            {
                \Utils::getManagerEmail($user->emp_code)->notify(new TestEmails('Leave Approval Request','You have new Leave Approval Request #'.$this->request_id.' from '.$user->name.'('.$user->emp_code.'). To view the details of the request please Log on to '.config('app.name').'.'));   
            }
    }

    public function leaveStatusEmail($user)
    {
         if($this->status == 1)
         {
            $user->notify(new TestEmails('Leave Approved','Your Leave Request #'.$this->request_id.' has been approved. To view the details of the request please Log on to OSL HRM.'));  
         }elseif($this->status == 2)
         {
            $user->notify(new TestEmails('Leave Rejected','Your Leave Request #'.$this->request_id.' has been rejected. To view the details of the request please Log on to OSL HRM.'));
         }   
    }        
}
