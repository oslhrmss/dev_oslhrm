<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Model\User;
use App\Model\EmployeeLeave;
use App\Notifications\TestEmails;

class LeaveEmailStatusJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $employee_leave;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($employee_leave)
    {
        //
        $this->employee_leave = $employee_leave;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $this->leaveStatusEmail($this->employee_leave);
    }

    public function leaveStatusEmail($employee_leave)
    {
        $user = User::where('emp_code', $employee_leave->emp_id)->first();
         if($employee_leave->is_approved == EmployeeLeave::APPROVED)
         {

            $user->notify(new TestEmails('Leave Approved','Your Leave Request #'.$employee_leave->id.' has been approved. To view the details of the request please Log on to OSL HRM.'));  
            logger('Approved email sents');
         }elseif($employee_leave->is_approved == EmployeeLeave::REJECTED)
         {
            $user->notify(new TestEmails('Leave Rejected','Your Leave Request #'.$employee_leave->id.' has been rejected. To view the details of the request please Log on to OSL HRM.'));
            logger('Rejected email sents');
         } elseif ($employee_leave->is_approved == EmployeeLeave::PENDING)
         {
            $user->notify(new TestEmails('Leave Pending','Your Leave Request #'.$employee_leave->id.' status has been changed. To view the details of the request please Log on to OSL HRM.'));
            logger('Pending email sents');
         } elseif ($employee_leave->is_approved == EmployeeLeave::CANCEL)
         {
            $user->notify(new TestEmails('Leave Cancelled','Your Leave Request #'.$employee_leave->id.'  has been Cancelled. To view the details of the request please Log on to OSL HRM.'));
            logger('Cancel email sents');
         }  
    } 
}
