<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    //
    protected $table = 'attendance';
    protected $fillable = ['emp_id','time_in','time_out','date','month'];

    public function attendance_adjustment()
    {
        return $this->hasOne('App\Model\AttendanceAdjustment');
    }

}
