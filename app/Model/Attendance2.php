<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Attendance2 extends Model
{
    //
    protected $table = 'temp_atten';
    protected $fillable = ['userid','date','time'];

}
