<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AttendanceAdjustment extends Model
{
    //
    protected $table = 'attendance_adjustment';


    public function attendance()
    {
        return $this->belongsTo('App\Model\Attendance');
    }


    public static function search($options)
    {
//    	dd($options['date']);
    	$result = AttendanceAdjustment::select('e.emp_no','e.name','attendance_adjustment.id','attendance_adjustment.time_in','attendance_adjustment.time_out','attendance_adjustment.date','attendance_adjustment.month','attendance_adjustment.reason')
    		->join('attendance as a','a.id','=','attendance_adjustment.atten_id')
    		->join('employees as e','e.emp_no','=','a.emp_id')
    		->where('e.emp_no',$options['employee'])
    		->where(function($query) use ($options){
    			if(isset($options['date']) && $options['date'] != ''){
    			return $query->where('attendance_adjustment.date',$options['date']);
    				
    			}
//    			->where('e.emp_no',$options['employee']);
    		})
    		//->where('attendance_adjustment.date',$options['date'])
    		->get();
    	return $result;	
    }

    public static function deleteModifiedAttendance($id)
    {

    	return AttendanceAdjustment::find($id)->delete();
    }
}
