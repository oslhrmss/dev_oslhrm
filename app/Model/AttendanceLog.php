<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AttendanceLog extends Model
{
    //
    protected $table = 'attendance_log';
    protected $fillable = ['attendance_id','emp_no','emp_name','date','time_in','time_out','late','f_halfday','s_halfday','early_going','hours','absent','day','m_time_in','m_time_out','month'];
}
