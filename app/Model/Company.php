<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    //
    protected $table = 'company';
    protected $fillable = ['name','is_active'];

    public function sub_company()
    {
     return  $this->hasMany('App\Model\SubCompany');
    }

}


