<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    //
    protected $table = 'department';
    protected $fillable = ['name'];


    public function  sub_department()
    {
        return $this->hasMany(SubDepartment::class,'depart_id');
    }
}
