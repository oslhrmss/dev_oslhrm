<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class DinnerAllowances extends Model
{
    //
   protected $table = 'dinner_allowances';
   protected $fillable = ['emp_id','allowance_date','is_approved','is_paid','approved_by','created_at','updated_at'];
}
