<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Education extends Model
{
    //

   protected $table = 'education';
   protected $fillable = ['emp_id','degree_title','cgpa_percent','passing_date','degree_city','degree_country'];
}
