<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EmpArrear extends Model
{
    //
    protected $table = 'emp_arrears';
    protected $fillable =  ['emp_id','arrear_amount','month','year','reason'];

}
