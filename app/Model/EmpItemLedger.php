<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EmpItemLedger extends Model
{
    //
    protected $table = 'emp_item_ledger';
    protected $fillable = ['emp_item_id','deduction_amt','month','year','date'];

    public function item()
    {
        return $this->belongsToMany(EmployeeItem::class);
    }
}
