<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    //
    protected $table = 'employees';

    public function item()
    {
        return $this->belongsToMany(Item::class, 'employee_item','emp_id');
    }

    public function employee_package_detail()
    {
    	return $this->hasMany('App\Model\EmployeePackageDetail','emp_id','emp_no');
    }

    public function loyality_award()
    {
        return $this->hasMany('App\Model\LoyalityAward','loyality_award','emp_no','emp_no');
    }

}
