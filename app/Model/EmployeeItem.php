<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EmployeeItem extends Model
{
    //
    protected $table = 'employee_item';
    protected $fillable = ['emp_id','item_id','status','status_dates','remarks','is_active','created_by','updated_by'];

    
    public function item()
    {
        return $this->belongsToMany(Item::class);
    }
}
