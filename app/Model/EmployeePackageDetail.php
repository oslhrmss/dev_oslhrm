<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EmployeePackageDetail extends Model
{
    //
    protected $table =  'employee_package_detail';

    protected $fillable = ['emp_id','gross_salary','fuel_amount','parking_amount','eobi_amount','pf_slab_id','year'];


    public function pf_slab()
    {
    	return $this->belongsTo('App\Model\PfSlab');
    }


    public function employee()
    {
    	return $this->belongsTo('App\Model\Employee','emp_id','emp_no');
    }
}
