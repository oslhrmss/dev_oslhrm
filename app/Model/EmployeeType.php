<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EmployeeType extends Model
{
    //
    protected $table = 'emp_type';
    protected $fillable = ['key','value','is_active'];

}
