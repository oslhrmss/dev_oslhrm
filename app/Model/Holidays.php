<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Holidays extends Model
{
    //
protected $table = 'holidays';


	public function sub_company()
	{
		return $this->belongsTo('App\Model\SubCompany');	
	}

}
