<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    //

    protected $table = "item";
    protected $fillable = ['name','item_type_id','specification','brand','model','imei_no','purchase_date','warranty_date','cost','vendor_id','deduction_amt','monthly_deduction_amt'];

    
    public function item_type()
    {
        return $this->belongsTo(ItemType::class);
    }

    public function employee()
    {
        return $this->belongsToMany(Employee::class);
    }

    public function item_vendor()
    {
        return $this->belongsTo(ItemVendor::class);
    }
}