<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ItemStatus extends Model
{
    //

    protected $table = "item_status";
    protected $fillable = ['item_id','item_type', 'emp_id', 'status', 'purchase_date', 'assigned_date', 'recovered_date', 'snatched_date', 'dead_date'];

}
