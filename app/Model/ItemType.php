<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ItemType extends Model
{
    //

    protected $table = "item_type";


    public function item()
    {
    	return $this->hasMany(Item::class);
    }
}
