<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ItemVendor extends Model
{
    //

    protected $table = "item_vendor";
    protected $fillable = ['vendor_name','contact_person','vendor_phone'];

    public function item()
    {
        return $this->hasMany(Item::class);
    }
}
