<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LeaveDeduction extends Model
{
    //
    protected $table = 'leave_deduction';

    protected $fillable = ['company_id','leave_type_id','emp_id','deducted_leave','date','month','leave_year'];
}
