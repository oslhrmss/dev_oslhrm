<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LeaveLedger extends Model
{
    //
    protected $table = 'leave_ledger';

    protected $fillable = ['company_id','leave_type_id','emp_id','deducted_leave','date','month'];
}
