<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LoanLedger extends Model
{
    //
    protected $table = 'loan_ledger'; 

    protected $fillable = ['loan_id','deduction_amount','date','month','loan_year','balance'];
}
