<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LoanRelaxation extends Model
{
    //

    protected $table = "loan_relaxation";
    public $timestamps = false;
    protected $fillable = ['loan_id','month','loan_year','relaxed_at'];
    //protected $hidden = ['created_at','updated_at'];
}
