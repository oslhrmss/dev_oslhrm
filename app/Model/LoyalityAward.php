<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LoyalityAward extends Model
{
    //

    protected $table =  'loyality_award';

    protected $fillable  =  ['emp_no','year','date','type','amount'];

    public function employee()
    {
    	return $this->belongsTo('App\Model\Employee','emp_no','emp_no');
    }
}
