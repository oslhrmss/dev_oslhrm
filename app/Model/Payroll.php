<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Payroll extends Model
{
    //
    protected $table = 'payroll';
    protected $fillable = ['company_id','emp_id','emp_name','gross_sal','leave_ded_amt','pf','adv_sal','tax','loan_ded','item_deduction','eobi','arrear_amount','fuel_amount','parking_amount','net_sal','month','year','payroll_date'];
}
