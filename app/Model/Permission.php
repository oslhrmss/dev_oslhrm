<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Zizaco\Entrust\EntrustRole;

class Permission extends Model
{
    //
    protected $table = 'permissions';
    protected $fillable = ['name','display_name','description'];
}
