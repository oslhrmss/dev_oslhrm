<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Zizaco\Entrust\EntrustRole;

class PermissionRole extends Model
{
    //
    protected $table = 'permission_role';
    protected $fillable = ['permission_id','role_id'];
}
