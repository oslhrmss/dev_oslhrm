<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PfSlab extends Model
{
    //

    protected $table = "pf_slabs";

    public function employee_package_detail()
    {
    	return $this->hasMany('App\Model\EmployeePackageDetail');
    }
}
