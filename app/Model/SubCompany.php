<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SubCompany extends Model
{
    //
    protected $table = 'sub_company';
    protected $fillable = ['company_id','name','created_at','update_at'];

    public function company()
    {
        return $this->belognsTo('App\Model\Company');
    }

    public function holidays()
    {
        return $this->hasMany('App\Model\Holidays');
    }
}
