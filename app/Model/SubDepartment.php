<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SubDepartment extends Model
{
    //
    protected $table = 'sub_department';
    protected $fillable = ['depart_id','name'];

    public function  department()
    {
        return $this->belongsTo(Department::class);
    }
}
