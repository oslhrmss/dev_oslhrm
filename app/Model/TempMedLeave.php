<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class TempMedLeave extends Model
{
    //
   protected $table = 'temp_med_leave';
   protected $fillable = ['emp_id','is_approved','created_at','updated_at'];
}
