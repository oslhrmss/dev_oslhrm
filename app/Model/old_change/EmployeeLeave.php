<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Observers\LeaveObserver;

class EmployeeLeave extends Model
{
    //
    protected $table = 'employee_leave';

    protected $fillable  =  ['emp_id','sub_company_leave_type_id','from','to','leave_year','application_date','is_approved','approved_by','reason','approved_at','is_active','created_at','updated_at'];

    const PENDING = 0;
    const APPROVED = 1;
    const REJECTED = 2;
    const CANCEL = 3;

}