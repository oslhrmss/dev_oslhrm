<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PayBackLeaves extends Model
{
    //

    protected $table =  'payback_leaves';
    protected $fillable = ['emp_id','no_of_leaves','paid_at','month','leave_year','leave_date','paid_by'];

    public static function deletePaybackLeave($id)
    {

    	return PayBackLeaves::find($id)->delete();
    }
}
