<?php
namespace App\Observers;

use App\Jobs\LeaveEmailStatusJob;
use App\Model\EmployeeLeave;

 class LeaveObserver
 {
 	public function updated(EmployeeLeave $employee_leave)
 	{
 		if($employee_leave->isDirty('is_approved'))
 		{

 			$email_job = (new LeaveEmailStatusJob($employee_leave));
            //->delay(\Carbon\Carbon::now()->addSeconds(30));
       		dispatch($email_job);

 		}

 	}



 }