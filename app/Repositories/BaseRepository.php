<?php

/**
	* @author : Mustsabhir Khan <mustabshir.aziz@netsoltech.com>
	* @version: v1.0.0	
	* @comments: In base repository in which all basic functions of application, which will be override in all further repository or controller.  
	*/


namespace App\Repositories;



interface BaseRepository
{
	function getAll();
 
	function getById($id);
 
	function create(array $attributes);
 
	function update($id, array $attributes);
 
	function delete($id);
}