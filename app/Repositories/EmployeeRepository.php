<?php
namespace App\Repositories;
use App\Model\Employee;
use App\Model\EmployeeType;

class EmployeeRepository implements BaseRepository
{

	/**
	* @author : Mustsabhir Khan <mustabshir.aziz@netsoltech.com>
	* @version: v2.0.0	
	* @comments: This is employee repository,here all bussiness process are being 				processed.
	*/

	private $empModel;
	public function __construct(Employee $empModel)
	{
		$this->empModel = $empModel;
	}

	public function getAll()
	{
		return $this->empModel->all();
	}

	public function getById($id)
	{
		return $this->empModel->find($id);
	}
 
	public function create(array $attributes)
	{
		return $this->empModel->create($attributes);
	}
 
	public function update($id, array $attributes)
	{
		return $this->empModel->find($id)->update($attributes);
	}
 
	public function delete($id)
	{
		return $this->empModel->find($id)->delete();
	}

	public function getEmpType()
	{
		$empType =   EmployeeType::where('is_active',1)->get(['id','key','value']);
		//return response()->json($empType,200);
		return $empType;
	}

    public  function  getEmpStatus($empCode)
    {
        $status = Employee::where('emp_no', $empCode)->value('is_active');
        return $status;
    }


	

}




