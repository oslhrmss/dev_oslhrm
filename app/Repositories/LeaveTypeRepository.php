<?php
namespace App\Repositories;
use App\Model\LeaveType;
use App\Model\EmployeeLeave;
use App\Model\PayBackLeaves;
use App\Model\LeaveDeduction;
use App\Model\SubCompany;
use App\Model\Employee;
use Illuminate\Support\Facades\Log;


class LeaveTypeRepository implements BaseRepository
{

    /**
    * @author : Mustsabhir Khan <mustabshir.aziz@netsoltech.com>
    * @version: v2.0.0
    * @comments: This is leave repository,here all bussiness process are being        processed.
    */

    private $model;
    public function __construct(LeaveType $model)
    {
        $this->model = $model;
    }


    public function getAll(){
        return $this->model->all();
    }

    public function getById($id){
        return $this->model->find($id);
    }

    public function create(array $attributes){
        return $this->model->create($attributes);
    }

    public function update($id, array $attributes){
        return $this->model->find($id)->update($attributes);
    }

    public function delete($id){

        return $this->model->find($id)->delete();
    }

    public function employeeAllLeaves($empCode, $company)
    {

        $data = \DB::table('employee_leave as el')
                ->select('e.name','lt.name as leavetype','el.application_date as date',\DB::raw('MONTHNAME(el.application_date) AS month'),'el.from','el.to',\DB::raw('(DATEDIFF(el.to,el.from) +1) AS days'),'el.is_approved as status','u1.name as approved_by','el.approved_at','el.id as rowid','sclt.id as leave_id','e.emp_no','el.reason')
                ->join('sub_company_leave_type as sclt','sclt.id','=','el.sub_company_leave_type_id')
                ->join('leave_type as lt','lt.id','=','sclt.leave_type_id')
                ->join('employees as e','e.emp_no','=','el.emp_id')
                ->leftjoin('users as u1','u1.emp_code','=','el.approved_by')
                ->where('el.emp_id',$empCode)
                ->where('sclt.sub_company_id', $company)
                ->orderBy('el.id','desc')
                ->get();

        //echo print_r(\DB::getQueryLog());
        if(!$data->isEmpty()){
        foreach ($data as $key => $value) {
                    # code...
                    $response[] = [
                        'name' => $value->name,
                        'leavetype' => $value->leavetype,
                        'date' => $value->date,
                        'from' => $value->from,
                        'to' => $value->to,
                        'days' => $value->days - \Utils::sundaySandwich($value->emp_no, $value->from, $value->to, $value->leave_id),
                        'month' => $value->month,
                        'status' => $value->status,
                        'approved_by' => $value->approved_by,
                        'approved_at' => $value->approved_at,
                        'rowid' => $value->rowid,
                        'leave_id' => $value->leave_id,
                        'emp_no' => $value->emp_no,
                        'reason' => $value->reason
                    ];
                }
        }else{
            $response = [];
        }

        return $response;


    }


    public function allEmployeeLeaveList($leave_year = false)
    {
        $data = \DB::table('employee_leave as el')
                ->select('e.name','e.emp_no','lt.name as leavetype','el.from','el.to','el.leave_year',\DB::raw('(DATEDIFF(el.to,el.from) +1) AS days'),'el.application_date as applied_at','el.is_approved as status','el.approved_at','u.name as approved_by','sclt.id as leave_id')
                ->join('employees as e','e.emp_no','=','el.emp_id')
                ->join('sub_company_leave_type as sclt','sclt.id','=','el.sub_company_leave_type_id')
                ->join('leave_type as lt','lt.id','=', 'sclt.leave_type_id')
                ->leftjoin('users as u','u.emp_code','=','el.approved_by')
                ->orderBy('el.is_approved','asc')
                ->where(function($query){
                    if(auth()->user()->roles->first()->name != 'admin')
                    {
                        $query->where('e.company_id',\Utils::getCompanyId(auth()->user()->emp_code));
                    }
                })
                ->get();
                if(!$data->isEmpty()){
        foreach ($data as $key => $value) {
                    # code...
                    $response[] = [
                        'name' => $value->name,
                        'leavetype' => $value->leavetype,
                        'applied_at' => $value->applied_at,
                        'emp_no' => $value->emp_no,
                        'from' => $value->from,
                        'to' => $value->to,
                        'days' => $value->days - \Utils::sundaySandwich($value->emp_no, $value->from, $value->to, $value->leave_id,$leave_year),
                        'status' => $value->status,
                        'approved_by' => $value->approved_by,
                        'approved_at' => $value->approved_at,
                        // 'rowid' => $value->rowid,
                        // 'leave_id' => $value->leave_id,
                        // 'emp_no' => $value->emp_no,
                        // 'reason' => $value->reason
                    ];
                }
        }else{
            $response = [];
        }

        return $response;        
        //return $data;
    }

    public function getAllEmployeeLeaveBalance($empCode,$company)
    {   
        $cur_year = date('Y');
         $data = \DB::table('leave_type as lt')
                          ->select('lt.name as leavetype',\DB::raw('(SELECT name FROM employees WHERE emp_no = '.$empCode.') as emp_name'), \DB::raw('(SELECT emp_no FROM employees WHERE emp_no = '.$empCode.') as emp_no'),'sclt.no_of_leaves as total_leaves',\DB::raw('IFNULL(sclt.`no_of_leaves` - (SELECT SUM(DATEDIFF(employee_leave.to,employee_leave.from)+1) FROM employee_leave WHERE emp_id = '.$empCode.' AND yearr = '.$cur_year.'  AND sub_company_leave_type_id = lt.id AND is_approved = 1 ),sclt.`no_of_leaves`) - ( (SELECT IFNULL(SUM(deducted_leave),0) FROM leave_deduction WHERE emp_id = '.$empCode.' AND yearr = '.$cur_year.' AND leave_type_id = lt.id) ) AS balance'), \DB::raw('IFNULL((SELECT SUM(DATEDIFF(employee_leave.to,employee_leave.from)+1) FROM employee_leave WHERE emp_id = '.$empCode.' AND yearr = '.$cur_year.' AND sub_company_leave_type_id = lt.id AND is_approved =1),0) AS availed'))
                          ->join('sub_company_leave_type as sclt','sclt.id','=','lt.id')
                          ->where('sclt.sub_company_id',$company)
                          ->get();
    }



    public function getPayBackLeaveList()
    {
        $response =  PayBackLeaves::select('employees.emp_no','employees.name','payback_leaves.no_of_leaves','payback_leaves.paid_at','payback_leaves.month',\DB::raw('YEAR(payback_leaves.created_at) as year'),'payback_leaves.leave_date','users.name as paid_by','payback_leaves.id')
                     ->join('employees','employees.emp_no','=','payback_leaves.emp_id')
                     ->leftjoin('users','users.id','=','payback_leaves.paid_by')
                     ->orderBy('payback_leaves.id','desc')
                     ->where(function($query){
                        if(auth()->user()->roles->first()->name != 'admin')
                        {
                            $query->where('employees.company_id',\Utils::getCompanyId(auth()->user()->emp_code));
                        }
                    })
                    ->get();

        return $response;
    }


    public function getLeaveDeductionReport()
    {
        $response  = \DB::table('leave_deduction as ld')
                            ->select('e.name','e.emp_no','sc.name as company','lt.name as leavename','ld.leave_type_id','ld.deducted_leave','ld.date','ld.month')
                            ->leftjoin('leave_type as lt','lt.id','=','ld.leave_type_id')
                            ->join('sub_company as sc','sc.id','=','ld.company_id')
                            ->join('employees as e','e.emp_no','=','ld.emp_id')
                            ->get();

        return $response;

    }

    ////
        public function getLeaveBalanceH($empCode,$leave_id = false)
    {


        // get employee permanent year for checking either  perm year is current year or else
        $permanent_year =  Employee::where('emp_no', $empCode)->value(\DB::raw('YEAR(date_of_hiring)'));
        // $leave_year =  EmployeeLeave::where('emp_id', $empCode)->pluck('yearr')->last();
        // $cur_year = date('Y');
        $cur_year = date('Y');

        $emp_name = Employee::where('emp_no', $empCode)->value('name');
        if(!$leave_id){
            $data = \DB::table('leave_type as lt')
            ->select('lt.name as leavetype','lt.id as leave_id','sclt.no_of_leaves as total_leaves',\DB::raw('(SELECT SUM(DATEDIFF(employee_leave.to,employee_leave.from)+1) FROM employee_leave WHERE emp_id = '.$empCode.' AND leave_year='.$cur_year.' AND sub_company_leave_type_id = sclt.id AND is_approved = 1 ) - ( (SELECT IFNULL(SUM(deducted_leave),0) FROM leave_deduction WHERE emp_id = '.$empCode.' AND leave_year='.$cur_year.' AND leave_type_id = lt.id ) ) AS balance'), \DB::raw('IFNULL((SELECT SUM(DATEDIFF(employee_leave.to,employee_leave.from)+1) FROM employee_leave WHERE emp_id = '.$empCode.' AND leave_year='.$cur_year.' AND sub_company_leave_type_id = sclt.id AND is_approved =1),0) AS availed'), \DB::raw('(SELECT SUM(deducted_leave) FROM leave_deduction WHERE emp_id = '.$empCode.' AND leave_year='.$cur_year.' AND leave_type_id = lt.id) as deducted_leave'))
            ->join('sub_company_leave_type as sclt','sclt.leave_type_id','=','lt.id')
            ->where('sclt.sub_company_id',Employee::where('emp_no',$empCode)->value('company_id'))
            ->get();

        } else {
            $sub_company_leave_type_id = \Utils::getSubCompanyLeaveTypeId($empCode, $leave_id); 
            $data = \DB::table('leave_type as lt')
            ->select('lt.name as leavetype','lt.id as leave_id','sclt.no_of_leaves as total_leaves',\DB::raw('(SELECT SUM(DATEDIFF(employee_leave.to,employee_leave.from)+1) FROM employee_leave WHERE emp_id = '.$empCode.' AND leave_year='.$cur_year.' AND sub_company_leave_type_id = sclt.id AND is_approved = 1 ) - ( (SELECT IFNULL(SUM(deducted_leave),0) FROM leave_deduction WHERE emp_id = '.$empCode.' AND leave_year='.$cur_year.' AND leave_type_id = '.$leave_id.' ) ) AS balance'), \DB::raw('IFNULL((SELECT SUM(DATEDIFF(employee_leave.to,employee_leave.from)+1) FROM employee_leave WHERE emp_id = '.$empCode.' AND leave_year='.$cur_year.' AND sub_company_leave_type_id = '.$sub_company_leave_type_id.' AND is_approved =1 ),0) AS availed'), \DB::raw('(SELECT SUM(deducted_leave) FROM leave_deduction WHERE emp_id = '.$empCode.' AND leave_year='.$cur_year.' AND leave_type_id = lt.id) as deducted_leave'))
            ->join('sub_company_leave_type as sclt','sclt.leave_type_id','=','lt.id')
            ->where('sclt.leave_type_id','=',$leave_id)
            ->where('sclt.sub_company_id',Employee::where('emp_no',$empCode)->value('company_id'))
            ->get();
            

        }

            if(\Utils::getEmpTypeKey($empCode) == 'perm')
                          {
                            //$sandwich = 0;
                          foreach ($data as $key => $value){
                              # code...
                            $sandwich = \Utils::getToNFromOfLeaves($empCode, $value->leave_id);
                            if(count($sandwich))    
                            {

                               //  foreach ($getToNFrom as $k => $v) {
                               //      # code...
                               //     $sandwich = \Utils::sundaySandwich($empCode,$v->from,$v->to,$value->leave_id) ;
                               // }
                            $availed =  $value->availed - array_sum($sandwich);
//                          $balance = $value->balance + array_sum($sandwich);
                                         
                            } else {
                            
//                                $balance = $value->balance;
                                $availed = $value->availed;
                            
                            }

                            // get permanent date of employee
                             $perm_date = explode("-", Employee::where('emp_no',$empCode)->value('date_of_hiring'));
                             
                             if($permanent_year == \Carbon\Carbon::now()->format('Y'))
                             {
                             // check permanent year is current year and cut off will be assign on date of permanent
                                
                                if($perm_date[2] > 15)
                                {
                                    $cut_off_point = 1;
                                }else{
                                    $cut_off_point = 0;
                                }
                                $new_perm_date =  $perm_date[0].'-'.$perm_date[1].'-'.$perm_date[2];
                             // first of year for leave distribution on monthly basis
                                $first_date_of_the_year =  \Carbon\Carbon::now()->format('Y-01-01');
                             // no of months
                                $month_diff = \Utils::getNoOfMonths($first_date_of_the_year,$new_perm_date);
                                $total_leaves = $value->total_leaves - round( ($value->total_leaves / 12) * ($month_diff  + $cut_off_point));
                                

                            }else{
                                $total_leaves = $value->total_leaves;
                            }

                             if($value->leave_id  ==  \Utils::getLeaveIdByName()['payback_leaves']){
                                 $total_leaves = \Utils::employeePayBackLeaves($empCode,false);
                               //  $availed = 500;
                                 // $value->deducted_leave =  \Utils::employeePayBackDeductedLeaves($empCode);
                                 $value->deducted_leave = $value->deducted_leave;
                             }

                             $response[] = [
                                'total_leaves'=>$total_leaves,
                                'balance'=>($total_leaves - $availed) - $value->deducted_leave,
                                'availed'=>$availed,
                                'deducted_leave' => $value->deducted_leave,
                                'leavetype'=>$value->leavetype,
                                'emp_no' => $empCode,
                                'emp_name'=> $emp_name,
                                
                            ];

                        }
                        } else {
                            foreach ($data as $key => $value) {
                                $response[] = [
                                'total_leaves'=> $total_leaves = 0,
                                'balance'=> $balance = 0,
                                'availed'=> $availed = 0,
                                'deducted_leave' => $deducted_leave = 0,
                                'leavetype'=> $value->leavetype,
                                'emp_no' =>   $empCode,
                                'emp_name'=>  $emp_name,
                                
                            ];

                            }
                        }

        return $response;

    }
    ////


    public function getLeaveBalance($empCode,$leave_year,$leave_id = false)
    {


        // get employee permanent year for checking either  perm year is current year or else
        $permanent_year =  Employee::where('emp_no', $empCode)->value(\DB::raw('YEAR(date_of_hiring)'));
        // $leave_year =  EmployeeLeave::where('emp_id', $empCode)->pluck('yearr')->last();
        // $cur_year = date('Y');
        $cur_year = $leave_year;

        $emp_name = Employee::where('emp_no', $empCode)->value('name');
        if(!$leave_id){
            $data = \DB::table('leave_type as lt')
            ->select('lt.name as leavetype','lt.id as leave_id','sclt.no_of_leaves as total_leaves',\DB::raw('(SELECT SUM(DATEDIFF(employee_leave.to,employee_leave.from)+1) FROM employee_leave WHERE emp_id = '.$empCode.' AND leave_year ='.$cur_year.' AND sub_company_leave_type_id = sclt.id AND is_approved = 1 ) - ( (SELECT IFNULL(SUM(deducted_leave),0) FROM leave_deduction WHERE emp_id = '.$empCode.' AND leave_year ='.$cur_year.' AND leave_type_id = lt.id ) ) AS balance'), \DB::raw('IFNULL((SELECT SUM(DATEDIFF(employee_leave.to,employee_leave.from)+1) FROM employee_leave WHERE emp_id = '.$empCode.' AND leave_year ='.$cur_year.' AND sub_company_leave_type_id = sclt.id AND is_approved =1),0) AS availed'), \DB::raw('(SELECT SUM(deducted_leave) FROM leave_deduction WHERE emp_id = '.$empCode.' AND leave_year ='.$cur_year.' AND leave_type_id = lt.id) as deducted_leave'))
            ->join('sub_company_leave_type as sclt','sclt.leave_type_id','=','lt.id')
            ->where('sclt.sub_company_id',Employee::where('emp_no',$empCode)->value('company_id'))
            ->get();

        } else {
            $sub_company_leave_type_id = \Utils::getSubCompanyLeaveTypeId($empCode, $leave_id); 
            $data = \DB::table('leave_type as lt')
            ->select('lt.name as leavetype','lt.id as leave_id','sclt.no_of_leaves as total_leaves',\DB::raw('(SELECT SUM(DATEDIFF(employee_leave.to,employee_leave.from)+1) FROM employee_leave WHERE emp_id = '.$empCode.' AND leave_year ='.$cur_year.' AND sub_company_leave_type_id = sclt.id AND is_approved = 1 ) - ( (SELECT IFNULL(SUM(deducted_leave),0) FROM leave_deduction WHERE emp_id = '.$empCode.' AND leave_year ='.$cur_year.' AND leave_type_id = '.$leave_id.' ) ) AS balance'), \DB::raw('IFNULL((SELECT SUM(DATEDIFF(employee_leave.to,employee_leave.from)+1) FROM employee_leave WHERE emp_id = '.$empCode.' AND leave_year ='.$cur_year.' AND sub_company_leave_type_id = '.$sub_company_leave_type_id.' AND is_approved =1 ),0) AS availed'), \DB::raw('(SELECT SUM(deducted_leave) FROM leave_deduction WHERE emp_id = '.$empCode.' AND leave_year ='.$cur_year.' AND leave_type_id = lt.id) as deducted_leave'))
            ->join('sub_company_leave_type as sclt','sclt.leave_type_id','=','lt.id')
            ->where('sclt.leave_type_id','=',$leave_id)
            ->where('sclt.sub_company_id',Employee::where('emp_no',$empCode)->value('company_id'))
            ->get();
            

        }

            if(\Utils::getEmpTypeKey($empCode) == 'perm')
                          {
                            //$sandwich = 0;
                          foreach ($data as $key => $value){
                              # code...
                            $sandwich = \Utils::getToNFromOfLeavesHistory($empCode, $value->leave_id,$leave_year);
                            if(count($sandwich))    
                            {

                               //  foreach ($getToNFrom as $k => $v) {
                               //      # code...
                               //     $sandwich = \Utils::sundaySandwich($empCode,$v->from,$v->to,$value->leave_id) ;
                               // }
                            $availed =  $value->availed - array_sum($sandwich);
//                          $balance = $value->balance + array_sum($sandwich);
                                         
                            } else {
                            
//                                $balance = $value->balance;
                                $availed = $value->availed;
                            
                            }

                            // get permanent date of employee
                             $perm_date = explode("-", Employee::where('emp_no',$empCode)->value('date_of_hiring'));
                             //Changed by Mehdi: 11 Jan 2020 - Time: 03:03PM
                             
                              
                             if($permanent_year == $cur_year) 
                                // \Carbon\Carbon::now()->format('Y')
                            
                             {
                             // check permanent year is current year and cut off will be assign on date of permanent
                                if($perm_date[2] > 15)
                                {
                                    $cut_off_point = 1;
                                }else{
                                    $cut_off_point = 0;
                                }
                                $new_perm_date =  $perm_date[0].'-'.$perm_date[1].'-'.$perm_date[2];
                             // first of year for leave distribution on monthly basis
                                $first_date_of_the_year =  \Carbon\Carbon::now()->format('Y-01-01');
                             // no of months
                                //Changed by Mehdi: 11 Jan 2020 - Time: 03:03PM
                                $month_diff = \Utils::getNoOfMonthss($first_date_of_the_year,$new_perm_date,$leave_year);
                                $total_leaves = $value->total_leaves - round( ($value->total_leaves / 12) * ($month_diff  + $cut_off_point));

                            }
                            //else If (($permanent_year > $cur_year)
                            //{$total_leaves=0};
                            else{
                                $total_leaves = $value->total_leaves;
                            }

                             if($value->leave_id  ==  \Utils::getLeaveIdByName()['payback_leaves']){
                                 $total_leaves = \Utils::employeePayBackLeavesHistory($empCode,false,$leave_year);
                               //  $availed = 500;
                                 // $value->deducted_leave =  \Utils::employeePayBackDeductedLeaves($empCode);
                                 $value->deducted_leave = $value->deducted_leave;
                             }

                             $response[] = [
                                'total_leaves'=>$total_leaves,
                                'balance'=>($total_leaves - $availed) - $value->deducted_leave,
                                'availed'=>$availed,
                                'deducted_leave' => $value->deducted_leave,
                                'leavetype'=>$value->leavetype,
                                'emp_no' => $empCode,
                                'emp_name'=> $emp_name,
                                'leave_year'=> $cur_year
                            ];

                        }
                        } else {
                            foreach ($data as $key => $value) {
                                $response[] = [
                                'total_leaves'=> $total_leaves = 0,
                                'balance'=> $balance = 0,
                                'availed'=> $availed = 0,
                                'deducted_leave' => $deducted_leave = 0,
                                'leavetype'=> $value->leavetype,
                                'emp_no' =>   $empCode,
                                'emp_name'=>  $emp_name,
                                'leave_year'=> $cur_year
                            ];

                            }
                        }

        return $response;

    }

    ///////////////

    public function getLeaveBalanceD($empCode,$leave_id = false)
    {


        // get employee permanent year for checking either  perm year is current year or else
        $permanent_year =  Employee::where('emp_no', $empCode)->value(\DB::raw('YEAR(date_of_hiring)'));
        // $leave_year =  EmployeeLeave::where('emp_id', $empCode)->pluck('yearr')->last();
        // $cur_year = date('Y');
        

        $emp_name = Employee::where('emp_no', $empCode)->value('name');
        if(!$leave_id){
            $data = \DB::table('leave_type as lt')
            ->select('lt.name as leavetype','lt.id as leave_id','sclt.no_of_leaves as total_leaves',\DB::raw('(SELECT SUM(DATEDIFF(employee_leave.to,employee_leave.from)+1) FROM employee_leave WHERE emp_id = '.$empCode.' AND sub_company_leave_type_id = sclt.id AND is_approved = 1 ) - ( (SELECT IFNULL(SUM(deducted_leave),0) FROM leave_deduction WHERE emp_id = '.$empCode.' AND leave_type_id = lt.id ) ) AS balance'), \DB::raw('IFNULL((SELECT SUM(DATEDIFF(employee_leave.to,employee_leave.from)+1) FROM employee_leave WHERE emp_id = '.$empCode.' AND sub_company_leave_type_id = sclt.id AND is_approved =1),0) AS availed'), \DB::raw('(SELECT SUM(deducted_leave) FROM leave_deduction WHERE emp_id = '.$empCode.' AND leave_type_id = lt.id) as deducted_leave'))
            ->join('sub_company_leave_type as sclt','sclt.leave_type_id','=','lt.id')
            ->where('sclt.sub_company_id',Employee::where('emp_no',$empCode)->value('company_id'))
            ->get();

        } else {
            $sub_company_leave_type_id = \Utils::getSubCompanyLeaveTypeId($empCode, $leave_id); 
            $data = \DB::table('leave_type as lt')
            ->select('lt.name as leavetype','lt.id as leave_id','sclt.no_of_leaves as total_leaves',\DB::raw('(SELECT SUM(DATEDIFF(employee_leave.to,employee_leave.from)+1) FROM employee_leave WHERE emp_id = '.$empCode.' AND sub_company_leave_type_id = sclt.id AND is_approved = 1 ) - ( (SELECT IFNULL(SUM(deducted_leave),0) FROM leave_deduction WHERE emp_id = '.$empCode.' AND leave_type_id = '.$leave_id.' ) ) AS balance'), \DB::raw('IFNULL((SELECT SUM(DATEDIFF(employee_leave.to,employee_leave.from)+1) FROM employee_leave WHERE emp_id = '.$empCode.' AND sub_company_leave_type_id = '.$sub_company_leave_type_id.' AND is_approved =1 ),0) AS availed'), \DB::raw('(SELECT SUM(deducted_leave) FROM leave_deduction WHERE emp_id = '.$empCode.' AND leave_type_id = lt.id) as deducted_leave'))
            ->join('sub_company_leave_type as sclt','sclt.leave_type_id','=','lt.id')
            ->where('sclt.leave_type_id','=',$leave_id)
            ->where('sclt.sub_company_id',Employee::where('emp_no',$empCode)->value('company_id'))
            ->get();
            

        }

            if(\Utils::getEmpTypeKey($empCode) == 'perm')
                          {
                            //$sandwich = 0;
                          foreach ($data as $key => $value){
                              # code...
                            $sandwich = \Utils::getToNFromOfLeaves($empCode, $value->leave_id);
                            if(count($sandwich))    
                            {

                               //  foreach ($getToNFrom as $k => $v) {
                               //      # code...
                               //     $sandwich = \Utils::sundaySandwich($empCode,$v->from,$v->to,$value->leave_id) ;
                               // }
                            $availed =  $value->availed - array_sum($sandwich);
//                          $balance = $value->balance + array_sum($sandwich);
                                         
                            } else {
                            
//                                $balance = $value->balance;
                                $availed = $value->availed;
                            
                            }

                            // get permanent date of employee
                             $perm_date = explode("-", Employee::where('emp_no',$empCode)->value('date_of_hiring'));
                             //Changed by Mehdi: 11 Jan 2020 - Time: 03:03PM
                             
                              
                             if($permanent_year == \Carbon\Carbon::now()->format('Y')) 
                                // \Carbon\Carbon::now()->format('Y')
                            
                             {
                             // check permanent year is current year and cut off will be assign on date of permanent
                                if($perm_date[2] > 15)
                                {
                                    $cut_off_point = 1;
                                }else{
                                    $cut_off_point = 0;
                                }
                                $new_perm_date =  $perm_date[0].'-'.$perm_date[1].'-'.$perm_date[2];
                             // first of year for leave distribution on monthly basis
                                $first_date_of_the_year =  \Carbon\Carbon::now()->format('Y-01-01');
                             // no of months
                                //Changed by Mehdi: 11 Jan 2020 - Time: 03:03PM
                                $month_diff = \Utils::getNoOfMonths($first_date_of_the_year,$new_perm_date);
                                $total_leaves = $value->total_leaves - round( ($value->total_leaves / 12) * ($month_diff  + $cut_off_point));

                            }
                            //else If (($permanent_year > $cur_year)
                            //{$total_leaves=0};
                            else{
                                $total_leaves = $value->total_leaves;
                            }

                             if($value->leave_id  ==  \Utils::getLeaveIdByName()['payback_leaves']){
                                 $total_leaves = \Utils::employeePayBackLeaves($empCode,false);
                               //  $availed = 500;
                                 // $value->deducted_leave =  \Utils::employeePayBackDeductedLeaves($empCode);
                                 $value->deducted_leave = $value->deducted_leave;
                             }

                             $response[] = [
                                'total_leaves'=>$total_leaves,
                                'balance'=>($total_leaves - $availed) - $value->deducted_leave,
                                'availed'=>$availed,
                                'deducted_leave' => $value->deducted_leave,
                                'leavetype'=>$value->leavetype,
                                'emp_no' => $empCode,
                                'emp_name'=> $emp_name,
                                
                            ];

                        }
                        } else {
                            foreach ($data as $key => $value) {
                                $response[] = [
                                'total_leaves'=> $total_leaves = 0,
                                'balance'=> $balance = 0,
                                'availed'=> $availed = 0,
                                'deducted_leave' => $deducted_leave = 0,
                                'leavetype'=> $value->leavetype,
                                'emp_no' =>   $empCode,
                                'emp_name'=>  $emp_name,
                                
                            ];

                            }
                        }

        return $response;

    }


  public function checkLeaveBalanceZero($empCode, $leaveId)
  {
    // $payback_leaves =  \Utils::employeePayBackLeaves($empCode, false);
    // $data = \DB::table('leave_type as lt')
    //                       ->select('lt.name as leavetype','lt.id as leave_id','sclt.no_of_leaves as total_leaves',\DB::raw('(SELECT SUM(DATEDIFF(employee_leave.to,employee_leave.from)+1) FROM employee_leave WHERE emp_id = '.$empCode.' AND sub_company_leave_type_id = sclt.id AND is_approved = 1 AND YEAR(application_date) = '.\Carbon\Carbon::now()->format('Y').' ) - ( (SELECT IFNULL(SUM(deducted_leave),0) FROM leave_deduction WHERE emp_id = '.$empCode.' AND leave_type_id = lt.id) ) AS balance'), \DB::raw('IFNULL((SELECT SUM(DATEDIFF(employee_leave.to,employee_leave.from)+1) FROM employee_leave WHERE emp_id = '.$empCode.' AND sub_company_leave_type_id = sclt.id AND is_approved =1 AND YEAR(application_date) = '.\Carbon\Carbon::now()->format('Y').'),0) AS availed'), \DB::raw('(SELECT SUM(deducted_leave) FROM leave_deduction WHERE emp_id = '.$empCode.' AND leave_type_id = lt.id) as deducted_leave'))
    //                       ->join('sub_company_leave_type as sclt','sclt.leave_type_id','=','lt.id')
    //                       ->where('sclt.sub_company_id',Employee::where('emp_no',$empCode)->value('company_id'))
    //                       ->where('lt.id',$leaveId)
    //                       ->get();
    //   foreach ($data as $key => $value) {
    //                     # code...
    //     if($value->leave_id == \Utils::getLeaveIdByName()['payback_leaves'] )
    //     {
    //       $value->total_leaves = $payback_leaves;
    //     }
    //     $response = $value->total_leaves - $value->availed - $value->deducted_leave;
                       
    //   }                 
    
          $payback_leaves =  \Utils::employeePayBackLeaves($empCode, false);
          $response = $this->getLeaveBalanceH($empCode, $leaveId);
          if($leaveId == \Utils::getLeaveIdByName()['payback_leaves'] )
          {
              $balance =  $payback_leaves;
          }else{
            $balance = $response[0]['balance'] ;
        }
        return $balance;

  }

    public function checkLeaveLimit($leave_id, $from, $to, $empCode)
    {
        $sunday = \Utils::sundaySandwich($empCode, $from, $to, $leave_id);
        //$leavecount = \Utils::getSubCompanyLeaveCount($leave_id);       
        $leavebalance = $this->getLeaveBalanceH($empCode,$leave_id);       
        $appliedDays = \Utils::getDaysBetweenTwoDates($from , $to) - $sunday; // remove +1 form dates
         if( $appliedDays <=  $leavebalance[0]['balance'])
         {
            return false;
         }else{
            return true;
         }   
    }

    //  public function Casuals_in_one_GO($leave_id = 1, $from, $to, $empCode)
    // {
    //     $sunday = \Utils::sundaySandwich($empCode, $from, $to, $leave_id);
    //     //$leavecount = \Utils::getSubCompanyLeaveCount($leave_id);       
    //     $leavebalance = $this->getLeaveBalance($empCode,$leave_id);       
    //     $appliedDays = (\Utils::getDaysBetweenTwoDates($from , $to)+1) - $sunday;
    //      if( $appliedDays >  $leavebalance[3]['balance'])
    //      {
    //         return false;
    //      }else{
    //         return true;
    //      }   
    // }


    public function editPayBackLeave($id, $leave_date)
    {
        $response =  PayBackLeaves::where('id',$id)->update(['leave_date' => $leave_date,'month'=>\Carbon\Carbon::now()->parse($leave_date)->format('F'), 'paid_at'=>\Carbon\Carbon::now()->format('Y-m-d'), 'paid_by'=> \Auth::user()->id]);
        return $response;
    }

    // public function deletePayBackLeave($id, $leave_date)
    // {
    //     $response =  PayBackLeaves::where('id',$id)->delete();
    //     return $response;
    // } 
}