<?php
namespace App\Repositories;
use App\Model\SubCompany;
use App\Model\LeaveType;
use App\Model\SubCompanyLeaveType;

class SubCompanyLeaveTypeRepository implements BaseRepository
{

	/**
	* @author : Mustsabhir Khan <mustabshir.aziz@netsoltech.com>
	* @version: v2.0.0	
	* @comments: This is leave repository,here all bussiness process are being 				processed.
	*/

	private $sb_model, $lt_model, $sclt_model;
	public function __construct(SubCompany $sb_model, LeaveType $lt_model,SubCompanyLeaveType $sclt_model)
	{
		$this->sb_model = $sb_model;
		$this->lt_model = $lt_model;
		$this->sclt_model = $sclt_model;
	}


	public function getAll(){
		return $this->model->all();
	}

	function getById($id){
		return $this->model->find($id);
	}
 
	function create(array $attributes){
		return $this->model->create($attributes);
	}
 
	function update($id, array $attributes){
		return $this->model->find($id)->update($attributes);
	}
 
	function delete($id){
		return $this->model->find($id)->delete();
	}

	function getAllCompanyLeaves()
	{
		$data = $this->sclt_model->select('sub_company.name as company_name','leave_type.name as leave_name','sub_company_leave_type.no_of_leaves')
		->join('sub_company','sub_company.id','=','sub_company_leave_type.sub_company_id')
		->join('leave_type','leave_type.id','=','sub_company_leave_type.leave_type_id')
		->where(function($query){
			if(auth()->user()->roles->first()->name != 'admin')
			{
				$query->where('sub_company_id',\Utils::getCompanyId(auth()->user()->emp_code));
			}
		})
		->get();
		
		return $data;
	}


		// get subComany Leave type Repo

	function getSubCompanyLeaveType($id)
	{
		$data = $this->sclt_model->select('leave_type.name as name','sub_company_leave_type.leave_type_id as id')
		->join('sub_company','sub_company.id','=','sub_company_leave_type.sub_company_id')
		->join('leave_type','leave_type.id','=','sub_company_leave_type.leave_type_id')
		->where('sub_company_leave_type.sub_company_id',$id)
		->get();

		return $data;
	}


	public function editSubCompanyNoOfLeaves($id,$no_of_leaves)
	{
		$data = $this->sclt_model->where('id',$id)->update(['no_of_leaves'=>$no_of_leaves]);
		return $data;
	}	

	

}