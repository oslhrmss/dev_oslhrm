<?php
namespace App\Repositories;
use App\Model\SubCompany;

class SubCompanyRepository implements BaseRepository
{

	/**
	* @author : Mustsabhir Khan <mustabshir.aziz@netsoltech.com>
	* @version: v2.0.0	
	* @comments: This is leave repository,here all bussiness process are being 				processed.
	*/

	private $model;
	public function __construct(SubCompany $model)
	{
		$this->model = $model;
	}


	public function getAll(){
		return $this->model->where(function($query){
            if(auth()->user()->roles->first()->name != 'admin')
            {
                $query->where('id',\Utils::getCompanyId(auth()->user()->emp_code));
            }
        })->get();
	}

	function getById($id){
		return $this->model->find($id);
	}
 
	function create(array $attributes){
		return $this->model->create($attributes);
	}
 
	function update($id, array $attributes){
		return $this->model->find($id)->update($attributes);
	}
 
	function delete($id){
		return $this->model->find($id)->delete();
	}



	

	

}