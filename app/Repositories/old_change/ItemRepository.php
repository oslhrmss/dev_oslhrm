<?php
namespace App\Repositories;
use App\Model\Item;
use App\Model\ItemType;
use App\Model\Employee;
use App\Model\EmployeeItem;
use App\Model\EmpItemLedger;
use DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class ItemRepository implements BaseRepository
{
	public static $deduct_amount = 0;
    public static $sum_of_deductions = 0;
    public static $balance = 0;

    private $itemModel;
	public function __construct(Item $itemModel)
	{
		$this->itemModel = $itemModel;
	}


	public function getAll()
	{
		return $this->itemModel->all();
	}

	public function getById($id)
	{
		return $this->itemModel->find($id);
	}
 
	public function create(array $attributes)
	{
		return $this->itemModel->create($attributes);
	}
 
	public function update($id, array $attributes)
	{
		return $this->itemModel->find($id)->update($attributes);
	}
 
	public function delete($id)
	{
		return $this->itemModel->find($id)->delete();
	}

	public function getItems()
	{
		$data = DB::table('item') // 'employees.name as empname' 
				->join('employee_item', 'employee_item.item_id', '=', 'item.id')
				->join('item_type', 'item_type.id', '=', 'item.item_type_id')
				->join('item_vendor', 'item.vendor_id', '=', 'item_vendor.id')
				->leftjoin('employees', 'employees.emp_no', '=', 'employee_item.emp_id')

				->select('item.id as item_id','item.name as name','item.deduction_amt as deductamt','item.monthly_deduction_amt as m_deduct_amt','item.brand as brand','item.model as model','item.imei_no as imei_no','item.cost as cost','item.purchase_date as purchase_date','item.warranty_date as warranty_date', 'item_vendor.vendor_name as vn','employees.name as empname', 'item_type.name as in', 'employee_item.status as eis', 'employee_item.status_dates as eisd', 'employee_item.id as employee_item_id')
				
				->whereNotIn('employee_item.status',['Sold'])
				->where('employee_item.is_active','=',1)
				->get();
		return $data;

	}

	

	public function getVendors()
	{
		// $data =  Item::with('item_type')->get();
		// return $data;
		$data = DB::table('item_vendor as iv')
				->select('iv.vendor_name', 'iv.contact_person', 'iv.vendor_phone')
				->get();
		return $data;

	}



	public function getItemWithType()
	{	
		$data =  \DB::table('item as i')
					->select('it.name as type','i.name as item_name','i.id as item_id', 'i.status as item_status')
					->join('item_type as it','it.id','=','i.item_type_id')
					->whereNotIn('status', ['Assigned', 'Snatched', 'Dead'])
					->get();
		return $data;

	}

	public function getVendorWithId()
	{
		$data =  \DB::table('item_vendor as v')
					->select('v.id', 'v.vendor_name', 'v.vendor_phone')
					->get();
		return $data;

	}

	public function getTypeWithId()
	{
		$data =  \DB::table('item_type as it')
					->select('it.id', 'it.name')
					->get();
		return $data;

	}


	// public function assignItemToEmployee($empCode, $type, $status)
	// {
	// 	foreach ($type as $key => $value) {
	// 		// # code...
	// 		// $response = EmployeeItem::create(['emp_id'=>$empCode,'item_id'=>$value]);
	// 		$response = EmployeeItem::create(['emp_id'=>$empCode,'item_id'=>$value]);

	// 		$change_status = DB::table('item')->where('id','=', [$value])->update(array(

 // 				'status' => $status,
 // 				));
	// 	}
	// 	return $response;
	// }

	// public function getEmployeesItemDedBalZero($empCode)
 //    {
	// 	$balance =  \DB::table('item as i')
	// 					->join('employee_item as ei','ei.item_id','=','i.id')
	// 					->where('ei.emp_id',$empCode)
	// 					->where('ei.is_active',1)
	// 					->value(\DB::raw("i.deduction_amt - (SELECT IFNULL(SUM(deduction_amt),0) FROM emp_item_ledger WHERE emp_item_id = ei.id) as balance"));	
	// 	return  $balance;
 //    }


	public function getEmployeeItem($empCode)
	{
		// $balance =  $this->getEmployeesItemDedBalZero($empCode);
		if(EmployeeItem::where('emp_id',$empCode)->where('is_active',1)->exists()) { 
		$employeeItem =  EmployeeItem::where('emp_id',$empCode)->where('is_active',1)->get(); // Self Edited ->where('is_active',1)
			foreach ($employeeItem as $key => $value) {
			# code...
			$balance =  DB::table('item as i') // Self Added
						->where('id', $value['item_id'])
						->select(DB::raw("i.deduction_amt - (SELECT IFNULL(SUM(deduction_amt),0) FROM emp_item_ledger WHERE emp_item_id = $value->id) as balance"))->first();
	
			Log::info('Balance is:  ' . $balance->balance . '  item ID: ' . $value->item_id);
			// $m_ded_amt = DB::table('item')->where('id', $value->item_id)->select('monthly_deduction_amt')->first();
			// $ded_amt = DB::table('item')->where('id', $value->item_id)->select('deduction_amt')->first();
			$m_ded_amt = DB::table('item')->where('id', $value['item_id'])->select('monthly_deduction_amt')->first();
			$ded_amt = DB::table('item')->where('id', $value['item_id'])->select('deduction_amt')->first();
			
			Log::info('Monthly ded. Amt. : ' . $m_ded_amt->monthly_deduction_amt);
			Log::info('Deduction Amt. : ' . $ded_amt->deduction_amt);		
			$array[] = [
				'emp_item_id'=> $value->id,
				'item_id'=> $value->item_id,
				'm_ded_amt'=> $m_ded_amt->monthly_deduction_amt,
				'ded_amt'=> $ded_amt->deduction_amt,
				'balance'=> $balance->balance
			];
			
			} // foreach close
			
		} else {
			$array = [];
		}

		return $array;
	}

	public function generateEmpItemLedger($empCode, $month, $year)
	{
		if(!empty($this->getEmployeeItem($empCode))){
		$employeeItems = $this->getEmployeeItem($empCode);
		// Log::info('EmpItems are:  ' . $this->getEmployeeItem($empCode));
		foreach ($employeeItems as $key => $value) {
				# code...

			if($value['balance']  > 0){ // balance > 0
			$data[] = EmpItemLedger::updateOrcreate(['emp_item_id'=>$value['emp_item_id'],'month'=>$month, 'year'=>$year, 'date'=>\Carbon\Carbon::now()->format('Y-m-d')],
					[

					'emp_item_id'=>$value['emp_item_id'],
					'deduction_amt'=>$value['balance'] < $value['m_ded_amt'] ? $value['balance'] : $value['m_ded_amt']
	// when balance would be less than monthly ded amount ded amnt will balance
					// 'balance'=>(round($value['balance'] < $value['m_ded_amt'])) ? round($value['balance'] - $value['balance']) : round($value['balance'] - $value['m_ded_amt']), 
					// JUST above commit out CHANGE FOR BALANCE REMOVAL
					// 'month'=>$month,
					// 'year' => $year,					
					// 'date'=>\Carbon\Carbon::now()->format('Y-m-d')
					]);

				}
				else{
				$data = [];
			}

			} //foreach end


			}else{
				$data = [];
			}

			return $data;
	}

    public function itemDeductionSummary($month)
    {
        $response  = \DB::table('sub_company as sc')
                          ->select('sc.name',\DB::raw('SUM(etl.deduction_amt) as total'))
                          ->leftjoin('employees as e','e.company_id','=','sc.id')
                          ->leftjoin('employee_item as et','et.emp_id','=','e.emp_no')
                          ->leftjoin('emp_item_ledger as etl','etl.emp_item_id','=','et.id')
                          ->where('etl.month',$month)
                          ->where(function($query){
                          	if(auth()->user()->roles->first()->name != 'admin')
                          	{
                          		$query->where('sc.id', \Utils::getCompanyId(auth()->user()->emp_code));
                          	}
                          })
                          ->groupBy('sc.name')
                          ->get();
        return $response;
    }

    

    public function itemLedger($empCode) //'emp_item_ledger.balance as balance',
    {
    	$data =  EmpItemLedger::select('e.emp_no as emp_no','e.name as emp_name','i.name as item_name','emp_item_ledger.deduction_amt  as deduction_amt','emp_item_ledger.month as month','emp_item_ledger.date as date')
    		->join('employee_item as ei','ei.id','=','emp_item_ledger.emp_item_id')
    		->join('item as i','i.id','=','ei.item_id')
    		->join('employees as e','e.emp_no','=','ei.emp_id')
    		->where('e.emp_no',$empCode)
    		->get();

    	return $data;
    }

    public function getListOfReport_1()
    {
        $data = DB::table('item')
                ->join('item_type', 'item.item_type_id', '=', 'item_type.id')
                ->join('item_vendor', 'item.vendor_id', '=', 'item_vendor.id')
                ->leftjoin('employee_item', 'employee_item.item_id', '=', 'item.id')
                ->leftjoin('employees', 'employees.emp_no', '=', 'employee_item.emp_id')
                ->select('item.*', 'item_vendor.vendor_name as vn','employees.name as empname', 'item_type.name as in', 'employee_item.status as eis', 'employee_item.status_dates as eisd')
                ->whereNotIn('employee_item.status',['Assigned', 'Snatched', 'Dead','Sold'])
                ->where('employee_item.is_active','=',1)
                ->get();
        return $data;
    }

    public function getListOfReport_2()
    {
        $data = DB::table('item')
                ->join('item_type', 'item.item_type_id', '=', 'item_type.id')
                ->join('item_vendor', 'item.vendor_id', '=', 'item_vendor.id')
                ->leftjoin('employee_item', 'employee_item.item_id', '=', 'item.id')
                ->leftjoin('employees', 'employees.emp_no', '=', 'employee_item.emp_id')
                ->select('item.*', 'item_vendor.vendor_name as vn','employees.name as empname', 'item_type.name as in', 'employee_item.status as eis', 'employee_item.status_dates as eisd','employee_item.remarks as eir')
                ->get();
        return $data;
    }


    public function getMyItems()
	{
		$data = DB::table('item as i') // 'employees.name as empname' 
				->join('employee_item as ei', 'ei.item_id', '=', 'i.id')
				->join('item_type', 'i.item_type_id', '=', 'item_type.id')
				->select('i.id as item_id','i.name as name','i.brand as brand','i.model as model','i.imei_no as ime','i.deduction_amt as da','i.monthly_deduction_amt as mda','ei.emp_id as eii','ei.status as status', 'item_type.name as type')
				->where('ei.emp_id','=',\Auth::user()->emp_code)
				->where('ei.is_active','=',1)
				->get();
		return $data;

	}


}
