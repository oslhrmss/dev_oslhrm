<?php
/**
 * @author : Mustabshir khan
 * @version: v1.10.1
 * @param  :  date
 * @return : total days, working days, off days, and all date related values
 * 
 */

namespace App\Utilities;
use App\Model\Holidays;
use App\Model\Attendance;
use App\Model\AttendanceAdjustment;
use App\Model\Employee;
use App\Model\SubCompany;

class Utils
{
    public static $total_working_hour = 0;
    public static $total_absent ;
    public static $slate = 0;


    // get no of days in a month by date
    public static function totalDaysInMonth($date)
    {
        $month =  $date->format('n');
        $days = cal_days_in_month(CAL_GREGORIAN, $month, $date->format('Y'));
        return $days;
    }

    // get working/off days in a month
    public static function workingAndOffDaysInMonth($date, $offdays = true)
    {
        $month =  $date->format('n');
        $days = cal_days_in_month(CAL_GREGORIAN, $month, $date->format('Y'));
        for($i = 1; $i <= $days; $i++ )
        {
            $newDate = date('Y').'/'.$month.'/'.$i;
            $get_day = date('l', strtotime($newDate));
            if($offdays == true)
            {
                if($get_day == 'Sunday')
                {
                $_days[] = $i;
                }    
            }else{
            if($get_day != 'Sunday')
                {
                $_days[] = $i;
                }
            }
        }
        $result = count($_days);
        return $result;
    }
    
    // get no of days in a month by month name
    public static function noOfDaysByMonthName($month,$year)
    {
        $no_of_month = date('m',strtotime($month)); //get month number
        if($year != ''){
        $no_of_days = cal_days_in_month(CAL_GREGORIAN,$no_of_month,$year);
        }
        else{
        $no_of_days = date('t', strtotime($month));
        }
        return $no_of_days;
    }

    // get list of year in with present if true from 1900 to 2018
    public static function yearList($min ,$present = false)
    {   
        $currentYear =  \Carbon\Carbon::now();
        $min = 1900;
        $max = $currentYear->year;
     
        for($i=  $min; $i <= $max; $i++)
        {
            $years[] = $min++;

        }
        // 
        if($present == true)
        {
            $years[] =  'Present';

        }
        return array_reverse($years,true);
    }
    public static function hours($time1,$time2, $mtime1, $mtime2)
    {
        if($time1 == "00:00:00")
        {
            $time_1 = $mtime1;
        }
        else{
            $time_1 =$time1;
        }

        if($time2 == "00:00:00")
        {
            $time_2 = $mtime2;
        }
        else{
            $time_2 =$time2;
        }

        if($mtime1 == "--:--:--" && $mtime2 == "--:--:--")
        {
            $time_1 = $time1;
            $time_2 = $time2;
        }

        $time1 = strtotime($time_1);
        $time2 = strtotime($time_2);
        $difference = round(abs($time2 - $time1) / 3600,2);
        return $difference;
    
    }
    public static function getDaysAndDatesBetweenTwoDates($from,$to)
    {
    $from_date =  strtotime($from);
    $to_date =  strtotime($to);

    for ($date = $from_date; $date <= $to_date; $date+=86400) {

    $arr[]=  date("Y-m-d", $date);
    }
    return $arr;
    }


    public static function getDaysBetweenTwoDates($from, $to)
    {
        $from = date_create("2017-04-15");
        $to = date_create("2017-05-18");
        
        //difference between two dates
        $diff = date_diff($from,$to);
        
        //count days
        return $diff->format("%a"); 

    }

    public static function QueryLog($query)
    {
        \DB::enableQueryLog($query);
        
        return print_r(\DB::getQueryLog());
    }

    public static function getAppParams()
    {
        $sys_param = \DB::table('app_param')->get();
        foreach ($sys_param as $key => $value) {
                # code...
            $params[$value->config_key] = $value->config_value;
        }
        return $params;
    }

        public static function getAppParamsConfigKey()
    {
        $sys_param = \DB::table('app_param')->get();
        foreach ($sys_param as $key => $value) {
                # code...
            $params[$value->config_key] = $value->config_key;
        }
        return $params;
    }
    public static function getCompanyId($empCode)
    {

        $companyid = Employee::select('employees.company_id')
        ->join('sub_company','sub_company.id','=','employees.company_id')
        ->where('employees.emp_no','=',$empCode)
        ->first();
       return  $companyid = $companyid->company_id;
    }

    public static function allParamsCompany($config_key)
    {
        
        $allParamsCompany = \DB::table('app_param')->where('config_key',$config_key)->pluck('company_id')->toArray();
        return $allParamsCompany;

    }
    public static function attendanceData($from, $to, $empCode)
    {
        $dayArray = self::getDaysAndDatesBetweenTwoDates($from,$to);
        $params = self::getAppParams();
        $paramsConfigKey = self::getAppParamsConfigKey();
        $holidays = Holidays::where('sub_company_id',self::getCompanyId($empCode))->pluck('date')->toArray();
        $company = self::getCompanyId($empCode);
        $allSubCompany = SubCompany::pluck('id')->toArray();

    for($i=0; $i < count($dayArray); $i++)
    {
            // \DB::enableQueryLog();
      $value  = Attendance::select('attendance.id as attendid','employees.emp_no as empcode','employees.name as empname','attendance.date as date','attendance.time_in as timein','attendance.time_out as timeout','attendance_adjustment.time_in as mtimein','attendance_adjustment.time_out as mtimeout','attendance_adjustment.reason as m_reason','attendance_adjustment.adjustment_type as atype','users.name as adjustedby')
      ->join('employees','employees.emp_no','=','attendance.emp_id')
      ->leftjoin('attendance_adjustment','attendance.id','=','attendance_adjustment.atten_id')
      ->leftjoin('users','attendance_adjustment.adjusted_by','=','users.id')
      ->where('attendance.emp_id','=',$empCode)
      ->where('attendance.date',$dayArray[$i])
      ->first();
            // echo print_r(\DB::getQueryLog());
      if($value){
        $empcode = $value->empcode;
        $empname = $value->empname;
        $atten_id =  $value->attendid;
            $reason =  $value->m_reason;            // hours
            $hours = self::hours($value->timein, $value->timeout,$value->mtimein, $value->mtimeout);
            self::$total_working_hour += $hours;
            // late logics

            if($params['late_attendance'] <= $value->timein && $params['halfday_first_half'] > $value->timein){
                $late = 1;
            }else{
                $late = 0;
            }


// do not touch this condition if            
// case : when user time_out at 9 or after in previous day then late will be show after 10:01
            $previous_date = date('Y-m-d',strtotime('-1 day',strtotime($dayArray[$i])));

            $last_day_time = Attendance::select('time_out')->where('emp_id',$empCode)->where('date',$previous_date)->first();

            if($last_day_time){

                if($last_day_time->time_out >= $params['second_late_hour_provision'])
                {
                    if($value->timein < $params['special_late']  )
                    {
                        $late = 0;
                        self::$slate = 0;
                    }else{
                        $late = 1;
                        self::$slate = 1;
                    }

                }
            // $adj_time = AttendanceAdjustment::select('time_in','time_out')
            // ->where('atten_id',$value->attendid)->first();
            // if($adj_time)
            // {
            //    if($last_day_time->time_out >= $params['second_late_hour_provision'] )
            //    {
            //     if($adj_time->time_in < $params['special_late']  )
            //     {
            //         echo $adj_time->time_in;
            //          $late =0;
            //         // $this->slate = 0;
            //     }else{
            //         // $late = "no";
            //         // $this->slate = 1;
            //         echo "No";
            //     }
                
            //   }      
            // }

            }
            // else{
            //     $slate = 0;
            // }

            // first half day logics
            if($params['halfday_first_half'] <= $value->timein)
            {
                $firsthalf_halfday =1;

            }else{
                $firsthalf_halfday =0;
            }

            // modified time  first half dat logic
            if($params['halfday_first_half'] <= $value->mtimein)
            {
                $firsthalf_halfday =1;
            }

            // absent mark logic
            if($value->timein == '00:00:00' && $value->timeout == '00:00:00')
            {
                if(AttendanceAdjustment::where('atten_id',$value->attendid)->exists()){
                    $time_in = '00:00:00';
                    $time_out = '00:00:00';

                }else{
                    $time_in = 'ABSENT';
                    $time_out = 'ABSENT';
                    
                }

            }else{
                $time_in = $value->timein;
                $time_out = $value->timeout;
                //   $absent_count = 0;
            }
            
            // secondhalf halfday logic
            if($value->timeout <= $params['halfday_second_half'] && $value->timeout > $params['halfday_first_half'])
            {
                $secondhalf_halfday =1;
            }else{
                $secondhalf_halfday =0;
            }
            //modified secondhalf halfday logic            
            if($value->mtimeout <= $params['halfday_second_half'] && $value->mtimeout > $params['halfday_first_half'])
            {
                $secondhalf_halfday =1;
            }           
            // early going logic
            if($value->timeout < $params['early_going'] && $value->timeout > $params['halfday_second_half'])
            {
                $early_going =1;
            }else{
                $early_going =0;
            }
            // modified early going logic            
            if($value->mtimeout < $params['early_going'] && $value->mtimeout > $params['halfday_second_half'])
            {
                $early_going =1;
            }   

            if($late && $firsthalf_halfday)
            {
                $late = 0;
            }


   // modified time values
            $mtimein=is_null($value->mtimein) ? '--:--:--' : $value->mtimein;                
            $mtimeout=is_null($value->mtimeout) ? '--:--:--' : $value->mtimeout;                
            $atype=is_null($value->atype) ? 'N/A' : $value->atype;                
            $adjustedby=is_null($value->adjustedby) ? 'N/A' : $value->adjustedby;                
            $adjustment_time = AttendanceAdjustment::select('time_in','time_out')
            ->where('atten_id',$value->attendid)->first();
            if($adjustment_time)
            {
                if($params['late_attendance'] <= $adjustment_time->time_in && $params['halfday_first_half'] > $adjustment_time->time_in )
                {
                    // yhn pe hum modified time ko last time_out jo 21:00:00  ya use bara tha usko compare kr wa rhe hain
                    if($last_day_time)
                    {
                       if($last_day_time->time_out >= $params['second_late_hour_provision'] )
                       {
                        if($adjustment_time->time_in < $params['special_late']  )
                        {
                            //echo $adj_time->time_in;
                            $late =0;

                        }else{
                            $late = 1;
                        }

                    }else{
                        $late = 1; // ye condition jb chalegi jb last time out 9 bje se km hga or modified mein late ayega.
                    }
                }else{

                    $late = 1;
                    $firsthalf_halfday =0;
                    $secondhalf_halfday =0;
                    $early_going =0;  
                }
            }else{
                $late = 0;

            }
            if($params['halfday_first_half'] <= $adjustment_time->time_in)
            {
                $firsthalf_halfday =1;
            }else{
                $firsthalf_halfday =0;
            }
            if($adjustment_time->time_out <= $params['halfday_second_half'] && $adjustment_time->time_out > $params['halfday_first_half'])
            {
                $secondhalf_halfday =1;
            }else{
                $secondhalf_halfday =0;
            }
            if($adjustment_time->time_out < $params['early_going'] && $adjustment_time->time_out > $params['halfday_second_half'])
            {
                $early_going =1;
            }else{
                $early_going =0;
            } 

 // agr late koi employee pichly din 9:00:00 pm pe time_out kr k gya or agly din first half day k bd aya toh wo a/c to condifiton late bhi hgya or half day me bhi agya
// is condition mein wo cheez solve hai jis mein employee half day pe aya or modification time lesst 11:31 tha or pichla time 21:00:00 tha.

            if($late && $firsthalf_halfday )
            {
                $late = 0;
            }
            

        }
        // if(date('l',strtotime($dayArray[$i])) == $params['pk_sunday_off'])
        // {
        //  $late = 0;
        //     $firsthalf_halfday = 0;
        //     $secondhalf_halfday = 0;
        //     $early_going =0;   
        // }

        // if($companyid == 3)
        // {
        //     if(date('l',strtotime($dayArray[$i])) == $params['uae_friday_off'])
        //     {
        //        $late = 0;
        //        $firsthalf_halfday = 0;
        //        $secondhalf_halfday = 0;
        //        $early_going =0;   
        //    }

        // }


    }else{
        $empcode = Employee::where('emp_no',$empCode)->value('emp_no');
        $empname = Employee::where('emp_no',$empCode)->value('name');
            // holidays check 

        if(date('l',strtotime($dayArray[$i])) == $params['pk_sunday_off'])
        {
            $time_in = "<strong>OFF</strong>";
            $time_out = "<strong>OFF</strong>";    
        }else{
            $time_in = "00:00:00";
            $time_out = "00:00:00";
            
        }
        if(in_array(date('Y-m-d',strtotime($dayArray[$i])),$holidays))
        {
            $time_in = "<strong>OFF</strong>";
            $time_out = "<strong>OFF</strong>";
        }

        if(!in_array(date('Y-m-d',strtotime($dayArray[$i])),$holidays) && (date('l',strtotime($dayArray[$i])) != 'Sunday' && date('l',strtotime($dayArray[$i])) != 'Saturday' || date('l',strtotime($dayArray[$i])) == 'Saturday'))
        {
           $time_in = 'ABSENT';
           $time_out = 'ABSENT';
           $absent_count = 1;
       } 
       else{
           $absent_count = 0;
       }


       // dubai off days check
       if($company == 3)
       {
        if(date('l',strtotime($dayArray[$i])) == $params['uae_friday_off'])
        {

            $time_in = "<strong>OFF</strong>";
            $time_out = "<strong>OFF</strong>";    

        }
        if((date('l',strtotime($dayArray[$i])) != $params['uae_friday_off']))
        {
       
           $time_in = 'ABSENT';
           $time_out = 'ABSENT';
           $absent_count = 1;
       
       }else{
       
        $absent_count = 0;
       
            } 
        if(in_array(date('Y-m-d',strtotime($dayArray[$i])),$holidays))
        {
            $time_in = "<strong>OFF</strong>";
            $time_out = "<strong>OFF</strong>";
        }    
       }

       if(in_array($company, $allSubCompany))
       {
        if(in_array(date('Y-m-d',strtotime($dayArray[$i])),$holidays))
        {
            $time_in = "<strong>OFF</strong>";
            $time_out = "<strong>OFF</strong>";
        }  
       }   


            self::$total_absent += $absent_count;
            $firsthalf_halfday = 0;
            $secondhalf_halfday = 0;
            $early_going = 0;
            $late = 0;
            $hours = 0;
            $mtimein="--:--:--";                
            $mtimeout="--:--:--";                
            $atype='N/A';                
            $adjustedby='N/A';   
            // $this->slate = 0;
            $atten_id = 0; // this is attendance id
            $reason = '';
        }

         // late, halfday, second halfday will be zero ifemployee come on off saturday or any off day
       if(in_array($company, $allSubCompany))
       {

        if(in_array(date('Y-m-d',strtotime($dayArray[$i])),$holidays))
        {
            $late = 0;
            $firsthalf_halfday = 0;
            $secondhalf_halfday = 0;
            $early_going =0;
        }
       
       }


        // if($company == 1){       
        //     if(date('l',strtotime($dayArray[$i])) == $params['pk_sunday_off']){
        //     $late = 0;
        //     $firsthalf_halfday = 0;
        //     $secondhalf_halfday = 0;
        //     $early_going =0;   
        // }
        // }elseif($company == 3)
        // {
        //     if(date('l',strtotime($dayArray[$i])) == $params['uae_friday_off']){
        //         $late = 0;
        //         $firsthalf_halfday = 0;
        //         $secondhalf_halfday = 0;
        //         $early_going =0;   
        //     }

        // }

        if(in_array($company, $allSubCompany))
        {
//            return self::allParamsCompany($paramsConfigKey['uae_friday_off']);
            if(in_array($company, self::allParamsCompany($paramsConfigKey['uae_friday_off'])))
            {
                if(date('l',strtotime($dayArray[$i])) == $params['uae_friday_off']){
                $late = 0;
                $firsthalf_halfday = 0;
                $secondhalf_halfday = 0;
                $early_going =0;
                }
            }
            if(in_array($company, self::allParamsCompany($paramsConfigKey['pk_sunday_off'])))
            {
                if(date('l',strtotime($dayArray[$i])) == $params['pk_sunday_off']){
                $late = 0;
                $firsthalf_halfday = 0;
                $secondhalf_halfday = 0;
                $early_going =0;
                }
            }
        }
        // if(in_array($company, $allSubCompany) && in_array($params['pk_sunday_off'], $allParams))
        // {
        //     dd('yes');
        // //    if(date('l',strtotime($dayArray[$i])) == $params['pk_sunday_off']){
        // //     $late = 0;
        // //     $firsthalf_halfday = 0;
        // //     $secondhalf_halfday = 0;
        // //     $early_going =0;   
        // // }   
        // }


        $array[] = [
            'id' => $atten_id,
            'empcode' =>$empcode,
            'empname' => $empname,
            'date' => $dayArray[$i],
            'time_in'=> $time_in,
            'time_out' => $time_out,
            'late' =>$late,
            'f_halfday' => $firsthalf_halfday,
            's_halfday' => $secondhalf_halfday,
            'early_going' => $early_going,
            'hours' => $hours,
            'day'=>date('l',strtotime($dayArray[$i])),
            'mtimein' => $mtimein,
            'mtimeout' => $mtimeout,
            'atype' => $atype,
            'adjustedby' => $adjustedby,
            'total_hours' =>self::$total_working_hour,
            'total_absent' => self::$total_absent,
            's_late' => self::$slate,
            'role_id' => \Auth::user()->roles->first()->id,
            'reason' => $reason
        ];
    }

         return  $array;

    }

}