<?php
/**
 * @author : Mustabshir khan
 * @version: v1.10.1
 * @param  :  date
 * @return : total days, working days, off days, and all date related values
 * 
 */

namespace App\Utilities;
use App\Model\Holidays;
use App\Model\Attendance;
use App\Model\AttendanceAdjustment;
use App\Model\AttendanceLog;
use App\Model\Employee;
use App\Model\SubCompany;
use App\Model\SubCompanyAppParam;
use App\Model\AdvancedSalary;
use App\Model\Loan;
use App\Model\LoanLedger;
use App\Model\AppParam;
use App\Model\Position;
use App\Model\EmployeeLeave;
use App\Model\LeaveDeduction;
use App\Model\RegionAppParam;
use App\Model\EmployeeType;
use App\Model\EmployeeItem;
use App\Model\EmpItemLedger;
use App\Model\PayBackLeaves;
use App\Model\SubCompanyLeaveType;
use App\Model\EmpArrear;
use App\Model\LoyalityAward;
use Carbon\Carbon;


class Utils
{
    public static $total_working_hour = 0;
    public static $total_absent ;
    public static $slate = 0;
    public static  $first_date_of_the_year;


    // get no of days in a month by date
    public static function totalDaysInMonth($date)
    {
        $month =  $date->format('n');
        $days = cal_days_in_month(CAL_GREGORIAN, $month, $date->format('Y'));
        return $days;
    }

    // get working/off days in a month
    public static function workingAndOffDaysInMonth($date, $offdays = true)
    {
        $month =  $date->format('n');
        $days = cal_days_in_month(CAL_GREGORIAN, $month, $date->format('Y'));
        for($i = 1; $i <= $days; $i++ )
        {
            $newDate = date('Y').'/'.$month.'/'.$i;
            $get_day = date('l', strtotime($newDate));
            if($offdays == true)
            {
                if($get_day == 'Sunday')
                {
                $_days[] = $i;
                }    
            }else{
            if($get_day != 'Sunday')
                {
                $_days[] = $i;
                }
            }
        }
        $result = count($_days);
        return $result;
    }
    
    // get no of days in a month by month name
    public static function noOfDaysByMonthName($month,$year)
    {
        $no_of_month = date('m',strtotime($month)); //get month number
        if($year != ''){
        $no_of_days = cal_days_in_month(CAL_GREGORIAN,$no_of_month,$year);
        }
        else{
        $no_of_days = date('t', strtotime($month));
        }
        return $no_of_days;
    }

    // get number of months from tow date coordinates
    public static  function  getNoOfMonths($from, $to)
    {
        $date1 = $from;
        $date2 = $to;

        $ts1 = strtotime($date1);
        $ts2 = strtotime($date2);

        $year1 = date('Y', $ts1);
        $year2 = date('Y', $ts2);

        $month1 = date('m', $ts1);
        $month2 = date('m', $ts2);

        return  $diff = abs((($year2 - $year1) * 12) + ($month2 - $month1));
    }

    // get list of year. in with present if true from 1900 to 2018
    public static function yearList($min ,$present = false)
    {   
        $currentYear =  \Carbon\Carbon::now();
        $min = 1900;
        $max = $currentYear->year;
     
        for($i=  $min; $i <= $max; $i++)
        {
            $years[] = $min++;

        }
        // 
        if($present == true)
        {
            $years[] =  'Present';

        }
        return array_reverse($years,true);
    }
    public static function hours($time1,$time2, $mtime1, $mtime2)
    {

        if($time1 == "00:00:00" || $mtime1 != "")
        {
            $time_1 = $mtime1;
        }else{
            $time_1 =$time1;   
        }

        if($time2 == "00:00:00" || $mtime2 != "")
        {
            $time_2 = $mtime2;
        }else{
            $time_2 =$time2;
        }

        if($mtime1 == "--:--:--" && $mtime2 == "--:--:--")
        {
            $time_1 = $time1;
            $time_2 = $time2;
        }

        $time1 = strtotime($time_1);
        $time2 = strtotime($time_2);
        $difference = round(abs($time2 - $time1) / 3600,2);
        return $difference;
    
    }
    public static function getDaysAndDatesBetweenTwoDates($from,$to)
    {
    $from_date =  strtotime($from);
    $to_date =  strtotime($to);

    for ($date = $from_date; $date <= $to_date; $date+=86400) {

    $arr[]=  date("Y-m-d", $date);
    }
    return $arr;
    }


    public static function getDaysBetweenTwoDates($from, $to)
    {
        $from = date_create($from);
        $to = date_create($to);
        
        //difference between two dates
        $diff = date_diff($from,$to);
        
        //count days
        return $diff->format("%a"); 

    }

    public static function QueryLog($query)
    {
        \DB::enableQueryLog($query);
        
        return print_r(\DB::getQueryLog());
    }

    public static function getAppParams()
    {
        $sys_param = \DB::table('app_param')->get();
        foreach ($sys_param as $key => $value) {
                # code...
            $params[$value->config_key] = $value->config_value;
        }
        return $params;
    }

        public static function getAppParamsConfigKey()
    {
        $sys_param = \DB::table('sub_company_app_param')->get();
        foreach ($sys_param as $key => $value) {
                # code...
            $params[$value->config_key] = $value->config_key;
        }
        return $params;
    }
    public static function getCompanyId($empCode)
    {

        $companyid = Employee::select('employees.company_id')
        ->join('sub_company','sub_company.id','=','employees.company_id')
        ->where('employees.emp_no','=',$empCode)
        ->first();
        return  $companyid = $companyid->company_id;

    }


    public static function getRegionId($empCode)
    {

        $regionid = Employee::select('employees.region_id')
        ->join('regions','regions.id','=','employees.region_id')
        ->where('employees.emp_no','=',$empCode)
        ->first();
        return  $regionid = is_null($regionid) ? false : $regionid->region_id;
    }

    public static function allParamsCompany($config_key)
    {
        
        $allParamsCompany = \DB::table('sub_company_app_param')->where('config_key',$config_key)->pluck('sub_company_id')->toArray();
        return $allParamsCompany;

    }
    public static function getSubCompanyAppParamValue($config_key, $company_id, $region_id)
    {

        //\DB::enableQueryLog();
        $existanceCheck = SubCompanyAppParam::where('sub_company_id', $company_id)->where('config_key',$config_key)->exists();
        $appParamExists = AppParam::where('config_key', $config_key)->exists();
        $regionAppParamExists = RegionAppParam::where('region_id',$region_id)->where('config_key', $config_key)->exists();
       if($existanceCheck)
       {
        if($regionAppParamExists)
        {
            $allSubCompanyAppParam = RegionAppParam::where('region_id',$region_id)->where('config_key', $config_key)->get();

        }else{
            
        $allSubCompanyAppParam = SubCompanyAppParam::where('sub_company_id', $company_id)->where('config_key',$config_key)->get();
        }
       
       }elseif($regionAppParamExists)
       {
          $allSubCompanyAppParam = RegionAppParam::where('region_id',$region_id)->where('config_key', $config_key)->get();

       }
       elseif($appParamExists)
       {
        // $allSubCompanyAppParam =\DB::table('app_param')->where('config_key', $config)->get();
         $allSubCompanyAppParam =AppParam::where('config_key', $config_key)->get();
       }else{
            $allSubCompanyAppParam[0] = true;
       }
         return $allSubCompanyAppParam[0]['config_value'];
       //echo print_r(\DB::getQueryLog());
//        return $allSubCompanyAppParam[0]['config_value'];
    }


    public static function getRegionAppParamValue($config_key, $region_id)
    {
        $existanceCheck = \DB::table('region_app_param')->where('region_id', $region_id)->where('config_key',$config_key)->exists();
        if($existanceCheck)
        {
           $allRegionAppParam = \DB::table('region_app_param')->where('region_id', $region_id)->where('config_key',$config_key)->get();
       }

       return $allRegionAppParam[0]['config_value'];
   }


    public static function getAppliedCasualLeaveEmployee($empCode)
    {

        if(EmployeeLeave::where('emp_id', $empCode)->where('sub_company_leave_type_id',self::getSubCompanyLeaveTypeId($empCode, 1))->where('is_approved',1)->exists())
        {
            $leaves  = EmployeeLeave::select('from','to')->where('is_approved',1)->where('emp_id',$empCode)->where('sub_company_leave_type_id',self::getSubCompanyLeaveTypeId($empCode,1))->get();
        }else{
             return $array= [];
        }
      
        foreach ($leaves as $key => $value) {
            # code...
           $leaveArray[] = self::getDaysAndDatesBetweenTwoDates($value->from, $value->to);
        }
        for($i =0; $i < count($leaveArray); $i++ )
        {
           for($j=0; $j < count($leaveArray[$i]); $j++)
           {
                $array[]  = $leaveArray[$i][$j];
                
           }
        }

        return $array;

    
    }
    public static function getAppliedAnnualLeaveEmployee($empCode)
    {
       if(EmployeeLeave::where('emp_id', $empCode)->where('sub_company_leave_type_id',self::getSubCompanyLeaveTypeId($empCode, 2))->where('is_approved',1)->exists())
        {
        $leaves  = EmployeeLeave::select('from','to')->where('is_approved',1)->where('emp_id',$empCode)->where('sub_company_leave_type_id',self::getSubCompanyLeaveTypeId($empCode, 2))->get();
        }else{
        return $array = [];
        }
        foreach ($leaves as $key => $value) {
            # code...
           $leaveArray[] = self::getDaysAndDatesBetweenTwoDates($value->from, $value->to);
        }
        for($i =0; $i < count($leaveArray); $i++ )
        {
           for($j=0; $j < count($leaveArray[$i]); $j++)
           {
                $array[]  = $leaveArray[$i][$j];
                
           }
        }

        return $array;

    }

    public static function getAppliedSickLeaveEmployee($empCode)
    {
      if(EmployeeLeave::where('emp_id', $empCode)->where('sub_company_leave_type_id',self::getSubCompanyLeaveTypeId($empCode, 3))->where('is_approved',1)->exists())
        {
        $leaves  = EmployeeLeave::select('from','to')->where('is_approved',1)->where('emp_id',$empCode)->where('sub_company_leave_type_id',self::getSubCompanyLeaveTypeId($empCode, 3))->get();
        }else{
           return  $array = [];
        }

        foreach ($leaves as $key => $value) {
            # code...
           $leaveArray[] = self::getDaysAndDatesBetweenTwoDates($value->from, $value->to);
        }
        for($i =0; $i < count($leaveArray); $i++ )
        {
           for($j=0; $j < count($leaveArray[$i]); $j++)
           {
                $array[]  = $leaveArray[$i][$j];
                
           }
        }

        return $array;
    
    }

    public static function getAppliedPaybackLeaveEmployee($empCode)
    {
        $leaveids = self::getLeaveIdByName();
        if(EmployeeLeave::where('emp_id', $empCode)->where('sub_company_leave_type_id',self::getSubCompanyLeaveTypeId($empCode, $leaveids['payback_leaves']))->where('is_approved',1)->exists())
        {
        $leaves  = EmployeeLeave::select('from','to')->where('is_approved',1)->where('emp_id',$empCode)->where('sub_company_leave_type_id',self::getSubCompanyLeaveTypeId($empCode, $leaveids['payback_leaves']))->get();
        }else{
           return  $array = [];
        }

        foreach ($leaves as $key => $value) {
            # code...
           $leaveArray[] = self::getDaysAndDatesBetweenTwoDates($value->from, $value->to);
        }
        for($i =0; $i < count($leaveArray); $i++ )
        {
           for($j=0; $j < count($leaveArray[$i]); $j++)
           {
                $array[]  = $leaveArray[$i][$j];
                
           }
        }

        return $array;
    
    }
    public static function attendanceData($from, $to, $empCode, $attendance_log = false)
    {
        $dayArray = self::getDaysAndDatesBetweenTwoDates($from,$to);
        $params = self::getAppParams();
        $paramsConfigKey = self::getAppParamsConfigKey();
        $holidays = Holidays::where('sub_company_id',self::getCompanyId($empCode))->pluck('date')->toArray();
        $company = self::getCompanyId($empCode);
        $region = self::getRegionId($empCode);
        $allSubCompany = SubCompany::pluck('id')->toArray();

        $roles =  \DB::table('role_user')->join('roles','roles.id','=','role_user.role_id')->where('role_user.user_id',\Auth::user()->id)->pluck('roles.name')->toArray();
        $is_audit = in_array('audit', $roles) ? 1 : 0;
        $is_admin = in_array('admin', $roles) ? 1 : 0;

        $casual_leave_array = self::getAppliedCasualLeaveEmployee($empCode);
        $annual_leave_array = self::getAppliedAnnualLeaveEmployee($empCode);
        $sick_leave_array = self::getAppliedSickLeaveEmployee($empCode);
        $payback_leave_array = self::getAppliedPaybackLeaveEmployee($empCode);

       //return self::getSubCompanyAppParamValue('late_attendance',$company);
        //return $subCompanyAppParam[0]['config_value'];

    for($i=0; $i < count($dayArray); $i++)
    {

      $value  = Attendance::select('attendance.id as attendid','employees.emp_no as empcode','employees.name as empname','attendance.date as date','attendance.time_in as timein','attendance.time_out as timeout','attendance_adjustment.time_in as mtimein','attendance_adjustment.time_out as mtimeout','attendance_adjustment.reason as m_reason','attendance_adjustment.adjustment_type as atype','users.name as adjustedby')
      ->join('employees','employees.emp_no','=','attendance.emp_id')
      ->leftjoin('attendance_adjustment','attendance.id','=','attendance_adjustment.atten_id')
      ->leftjoin('users','attendance_adjustment.adjusted_by','=','users.id')
      ->where('attendance.emp_id','=',$empCode)
      ->where('attendance.date',$dayArray[$i])
      ->first();

      if($value){
        $empcode = $value->empcode;
        $empname = $value->empname;
        $atten_id =  $value->attendid;
            $reason =  $value->m_reason;            // hours
            $hours = self::hours($value->timein, $value->timeout,$value->mtimein, $value->mtimeout);
            self::$total_working_hour += $hours;
            // late logics

            if(self::getSubCompanyAppParamValue('late_attendance',$company,$region) <= $value->timein && self::getSubCompanyAppParamValue('halfday_first_half',$company, $region) > $value->timein){
                $late = 1;
            }else{
                $late = 0;
            }


// do not touch this condition if            
// case : when user time_out at 9 or after in previous day then late will be show after 10:01
            $previous_date = date('Y-m-d',strtotime('-1 day',strtotime($dayArray[$i])));

            $last_day_time = Attendance::select('time_out')->where('emp_id',$empCode)->where('date',$previous_date)->first();

            if($last_day_time){

                if($last_day_time->time_out >= self::getSubCompanyAppParamValue('second_late_hour_provision',$company, $region))
                {
                    if($value->timein < self::getSubCompanyAppParamValue('special_late',$company, $region)  )
                    {
                        $late = 0;
                        self::$slate = 0;
                    }

                }

            }
            // first half day logics
            if(self::getSubCompanyAppParamValue('halfday_first_half',$company, $region) <= $value->timein)
            {
                $firsthalf_halfday =1;

            }else{
                $firsthalf_halfday =0;
            }

            // modified time  first half dat logic
            if(self::getSubCompanyAppParamValue('halfday_first_half',$company, $region) <= $value->mtimein)
            {
                $firsthalf_halfday =1;
            }

            // absent mark logic
            if($value->timein == '00:00:00' && $value->timeout == '00:00:00')
            {
                if(AttendanceAdjustment::where('atten_id',$value->attendid)->exists()){
                    $time_in = '00:00:00';
                    $time_out = '00:00:00';

                }else{
                    $time_in = 'ABSENT';
                    $time_out = 'ABSENT';
                    
                }

            }else{
                $time_in = $value->timein;
                $time_out = $value->timeout;
                //   $absent_count = 0;
            }
            
            // secondhalf halfday logic
            if($value->timeout <= self::getSubCompanyAppParamValue('halfday_second_half',$company, $region) && $value->timeout > self::getSubCompanyAppParamValue('halfday_first_half',$company, $region))
            {
                $secondhalf_halfday =1;
            }else{
                $secondhalf_halfday =0;
            }
            //modified secondhalf halfday logic            
            if($value->mtimeout <= self::getSubCompanyAppParamValue('halfday_second_half',$company, $region) && $value->mtimeout > self::getSubCompanyAppParamValue('halfday_first_half',$company, $region))
            {
                $secondhalf_halfday =1;
            }           
            // early going logic
            if($value->timeout < self::getSubCompanyAppParamValue('early_going',$company, $region) && $value->timeout > self::getSubCompanyAppParamValue('halfday_second_half',$company, $region))
            {
                $early_going =1;
            }else{
                $early_going =0;
            }
            // modified early going logic            
            if($value->mtimeout < self::getSubCompanyAppParamValue('early_going',$company, $region) && $value->mtimeout > self::getSubCompanyAppParamValue('halfday_second_half',$company, $region))
            {
                $early_going =1;
            }   

            if($late && $firsthalf_halfday)
            {
                $late = 0;
            }

// get pervious date modified time

            $mlast_day_time = AttendanceAdjustment::select('attendance_adjustment.time_out','attendance_adjustment.time_in')
            ->join('attendance','attendance.id','=','attendance_adjustment.atten_id')
            ->join('employees','employees.emp_no','=','attendance.emp_id')
            ->where('employees.emp_no',$empCode)
            ->where('attendance_adjustment.date',$previous_date)
            ->first();

   // modified time values
            $mtimein=is_null($value->mtimein) ? '--:--:--' : $value->mtimein;                
            $mtimeout=is_null($value->mtimeout) ? '--:--:--' : $value->mtimeout;                
            $atype=is_null($value->atype) ? 'N/A' : $value->atype;                
            $adjustedby=is_null($value->adjustedby) ? 'N/A' : $value->adjustedby;                
            $adjustment_time = AttendanceAdjustment::select('time_in','time_out')
            ->where('atten_id',$value->attendid)->first();
            if($adjustment_time)
            {
                if(self::getSubCompanyAppParamValue('late_attendance',$company, $region) <= $adjustment_time->time_in && self::getSubCompanyAppParamValue('halfday_first_half',$company, $region) > $adjustment_time->time_in )
                {
                    // yhn pe hum modified time ko last time_out jo 21:00:00  ya use bara tha usko compare kr wa rhe hain
                    if($last_day_time)
                    {
                       if($last_day_time->time_out >= self::getSubCompanyAppParamValue('second_late_hour_provision',$company,$region) )
                       {
                        if($adjustment_time->time_in < self::getSubCompanyAppParamValue('special_late',$company, $region)  )
                        {
                            //echo $adj_time->time_in;
                            $late =0;

                        }else{
                            $late = 1;
                        }

                    }else{
                        $late = 1; // ye condition jb chalegi jb last time out 9 bje se km hga or modified mein late ayega.
                    }
                }else{

                    $late = 1;
                    $firsthalf_halfday =0;
                    $secondhalf_halfday =0;
                    $early_going =0;  
                }


            // previous modfied time values
                

                if($mlast_day_time)
                {
                 if($mlast_day_time->time_out >= self::getSubCompanyAppParamValue('second_late_hour_provision',$company, $region) )
                 {
                    if($adjustment_time->time_in < self::getSubCompanyAppParamValue('special_late',$company, $region) )
                    {
                            //echo $adj_time->time_in;
                        $late =0;

                    }else{
                        $late = 1;
                    }

                }else{
                        $late = 1; // ye condition jb chalegi jb last time out 9 bje se km hga or modified mein late ayega.
                    }

                }
            }else{
                $late = 0;

            }
            if(self::getSubCompanyAppParamValue('halfday_first_half',$company, $region) <= $adjustment_time->time_in)
            {
                $firsthalf_halfday =1;
            }else{
                $firsthalf_halfday =0;
            }
            if($adjustment_time->time_out <= self::getSubCompanyAppParamValue('halfday_second_half',$company, $region) && $adjustment_time->time_out > self::getSubCompanyAppParamValue('halfday_first_half',$company, $region))
            {
                $secondhalf_halfday =1;
            }else{
                $secondhalf_halfday =0;
            }
            if($adjustment_time->time_out < self::getSubCompanyAppParamValue('early_going',$company, $region) && $adjustment_time->time_out > self::getSubCompanyAppParamValue('halfday_second_half',$company, $region))
            {
                $early_going =1;
            }else{
                $early_going =0;
            } 

 // agr late koi employee pichly din 9:00:00 pm pe time_out kr k gya or agly din first half day k bd aya toh wo a/c to condifiton late bhi hgya or half day me bhi agya
// is condition mein wo cheez solve hai jis mein employee half day pe aya or modification time lesst 11:31 tha or pichla time 21:00:00 tha.

            if($late && $firsthalf_halfday )
            {
                $late = 0;
            }
            

        }
//ye  condition jb chlegi jb last day k modified time out mein 9 pm ya 9pm se bari value hogi
        if($mlast_day_time)
        {
            if($mlast_day_time->time_out >= self::getSubCompanyAppParamValue('second_late_hour_provision',$company, $region))
            {
                if($value->timein < self::getSubCompanyAppParamValue('special_late',$company, $region) )
                {
                            //echo $adj_time->time_in;
                    $late =0;

                }
            }
        }
        // if(date('l',strtotime($dayArray[$i])) == $params['pk_sunday_off'])
        // {
        //  $late = 0;
        //     $firsthalf_halfday = 0;
        //     $secondhalf_halfday = 0;
        //     $early_going =0;   
        // }

        // if($companyid == 3)
        // {
        //     if(date('l',strtotime($dayArray[$i])) == $params['uae_friday_off'])
        //     {
        //        $late = 0;
        //        $firsthalf_halfday = 0;
        //        $secondhalf_halfday = 0;
        //        $early_going =0;   
        //    }

        // }

    }else{
//          dump('ssdas');
        $empcode = Employee::where('emp_no',$empCode)->value('emp_no');
        $empname = Employee::where('emp_no',$empCode)->value('name');
            // holidays check 
        // off working days check 







        if(in_array($company, $allSubCompany)){

        if(date('l',strtotime($dayArray[$i])) == self::getSubCompanyAppParamValue('weekday_off',$company, $region))
        {
            $time_in = "<strong>OFF</strong>";
            $time_out = "<strong>OFF</strong>";    
        }
        // elseif(date('l',strtotime($dayArray[$i])) == self::getSubCompanyAppParamValue('weekday_off',$company))
        // {
        //     $time_in = "<strong>OFF</strong>";
        //     $time_out = "<strong>OFF</strong>";    
        // }
        else{
            $time_in = "00:00:00";
            $time_out = "00:00:00";
            
        }
        }

        if(in_array(date('Y-m-d',strtotime($dayArray[$i])),$holidays))
        {
            $time_in = "<strong>OFF</strong>";
            $time_out = "<strong>OFF</strong>";
        }

        if(!in_array(date('Y-m-d',strtotime($dayArray[$i])),$holidays) && (date('l',strtotime($dayArray[$i])) !=  self::getSubCompanyAppParamValue('weekday_off',$company, $region)) && (date('l',strtotime($dayArray[$i])) != 'Saturday' || date('l',strtotime($dayArray[$i])) == 'Saturday'))
        {
           $time_in = 'ABSENT';
           $time_out = 'ABSENT';
           $absent_count = 1;
       } 
       else{
           $absent_count = 0;
       }
       // leave check
       if(EmployeeLeave::where('emp_id',$empcode)->exists()){
       //dump($dayArray[$i]);
       if(in_array(date('Y-m-d',strtotime($dayArray[$i])), $casual_leave_array)  && date('l', strtotime($dayArray[$i])) != self::getSubCompanyAppParamValue('weekday_off',$company, $region) && !in_array(date('Y-m-d',strtotime($dayArray[$i])),$holidays))
       {
           $time_in =  'Casual Leave';
           $time_out = 'Casual Leave';
           $absent_count = 0;
       }elseif(in_array(date('Y-m-d',strtotime($dayArray[$i])), $annual_leave_array)  && date('l', strtotime($dayArray[$i])) != self::getSubCompanyAppParamValue('weekday_off',$company, $region) && !in_array(date('Y-m-d',strtotime($dayArray[$i])),$holidays)){
            $time_in =  'Annual Leave';
           $time_out = 'Annual Leave';
           $absent_count = 0;
       }elseif(in_array(date('Y-m-d',strtotime($dayArray[$i])), $sick_leave_array) && date('l', strtotime($dayArray[$i]))  != self::getSubCompanyAppParamValue('weekday_off',$company, $region) && !in_array(date('Y-m-d',strtotime($dayArray[$i])),$holidays)){
           //dd('asdas');
           $time_in =  'Sick Leave';
           $time_out = 'Sick Leave';
           $absent_count = 0;
       }elseif(in_array(date('Y-m-d',strtotime($dayArray[$i])), $payback_leave_array) && date('l', strtotime($dayArray[$i]))  != self::getSubCompanyAppParamValue('weekday_off',$company, $region) && !in_array(date('Y-m-d',strtotime($dayArray[$i])),$holidays)){
            $time_in =  'Payback Leave';
           $time_out = 'Payback Leave';
           $absent_count = 0;
       }

    }

       if(in_array($company, $allSubCompany))
       {
        if(in_array(date('Y-m-d',strtotime($dayArray[$i])),$holidays))
        {
            $time_in = "<strong>OFF</strong>";
            $time_out = "<strong>OFF</strong>";
        }  
       }   


            self::$total_absent += $absent_count;
            $firsthalf_halfday = 0;
            $secondhalf_halfday = 0;
            $early_going = 0;
            $late = 0;
            $hours = 0;
            $mtimein="--:--:--";                
            $mtimeout="--:--:--";                
            $atype='N/A';                
            $adjustedby='N/A';
            
            // $this->slate = 0;
            $atten_id = 0; // this is attendance id
            $reason = '';
        }

         // late, halfday, second halfday will be zero ifemployee come on off saturday or any off day
       if(in_array($company, $allSubCompany))
       {

        if(in_array(date('Y-m-d',strtotime($dayArray[$i])),$holidays))
        {
            $late = 0;
            $firsthalf_halfday = 0;
            $secondhalf_halfday = 0;
            $early_going =0;
        }
       
       }


        // if($company == 1){       
        //     if(date('l',strtotime($dayArray[$i])) == $params['pk_sunday_off']){
        //     $late = 0;
        //     $firsthalf_halfday = 0;
        //     $secondhalf_halfday = 0;
        //     $early_going =0;   
        // }
        // }elseif($company == 3)
        // {
        //     if(date('l',strtotime($dayArray[$i])) == $params['uae_friday_off']){
        //         $late = 0;
        //         $firsthalf_halfday = 0;
        //         $secondhalf_halfday = 0;
        //         $early_going =0;   
        //     }

        // }

        if(in_array($company, $allSubCompany))
        {
            // if(in_array($company, self::allParamsCompany($paramsConfigKey['weekday_off'])))
            // {
                if(date('l',strtotime($dayArray[$i])) == self::getSubCompanyAppParamValue('weekday_off',$company, $region)){
                $late = 0;
                $firsthalf_halfday = 0;
                $secondhalf_halfday = 0;
                $early_going =0;
                }
            
            // }

            if(date('l',strtotime($dayArray[$i])) == self::getSubCompanyAppParamValue('half_day',$company, $region))
            {
                if($time_out >= self::getSubCompanyAppParamValue('sat_time_out',$company, $region))
                {
                    $secondhalf_halfday = 0;
                    $early_going =0;
                 }
            // yhn wo condition handle hori k jb timeout blank ayega modified timeout set hoga.     
            if($time_out <= self::getSubCompanyAppParamValue('sat_time_out',$company, $region))
            {
                 if($mtimeout >= self::getSubCompanyAppParamValue('sat_time_out',$company, $region))
                 {
                    $secondhalf_halfday = 0;
                    $early_going =0;
                }else{                 
                    $secondhalf_halfday = 0;
                    $early_going =1;
                }
            }
        // yhn pe absent ko handle kya gy hai cz ye absent pe bhi early going show krwa rha tha bgahir is condition k
                if($time_out <= self::getSubCompanyAppParamValue('sat_time_out',$company, $region) && $mtimeout <= self::getSubCompanyAppParamValue('sat_time_out',$company, $region)){
                     $secondhalf_halfday = 0;
                    $early_going =1;
                }
            }

        }

        // ye condition jb chalegi jb absent hoga or leave apply ki hui hogi
        if($time_in == "ABSENT" && $time_out = "ABSENT") {
            // leave check
            if (EmployeeLeave::where('emp_id', $empcode)->exists()) {
                //dump($dayArray[$i]);
                if (in_array(date('Y-m-d', strtotime($dayArray[$i])), $casual_leave_array) && date('l', strtotime($dayArray[$i])) != self::getSubCompanyAppParamValue('weekday_off', $company, $region) && !in_array(date('Y-m-d', strtotime($dayArray[$i])), $holidays)) {
                    $time_in = 'Casual Leave';
                    $time_out = 'Casual Leave';
                    $absent_count = 0;
                } elseif (in_array(date('Y-m-d', strtotime($dayArray[$i])), $annual_leave_array) && date('l', strtotime($dayArray[$i])) != self::getSubCompanyAppParamValue('weekday_off', $company, $region) && !in_array(date('Y-m-d', strtotime($dayArray[$i])), $holidays)) {
                    $time_in = 'Annual Leave';
                    $time_out = 'Annual Leave';
                    $absent_count = 0;
                } elseif (in_array(date('Y-m-d', strtotime($dayArray[$i])), $sick_leave_array) && date('l', strtotime($dayArray[$i])) != self::getSubCompanyAppParamValue('weekday_off', $company, $region) && !in_array(date('Y-m-d', strtotime($dayArray[$i])), $holidays)) {
                    //dd('asdas');
                    $time_in = 'Sick Leave';
                    $time_out = 'Sick Leave';
                    $absent_count = 0;
                } elseif (in_array(date('Y-m-d', strtotime($dayArray[$i])), $payback_leave_array) && date('l', strtotime($dayArray[$i])) != self::getSubCompanyAppParamValue('weekday_off', $company, $region) && !in_array(date('Y-m-d', strtotime($dayArray[$i])), $holidays)) {
                    $time_in = 'Payback Leave';
                    $time_out = 'Payback Leave';
                    $absent_count = 0;
                }

            }else{
                $time_out = "ABSENT";
                $time_in = "ABSENT";
            }
        }


        $array[] = [
            'id' => $atten_id,
            'empcode' =>$empcode,
            'empname' => $empname,
            'date' => $dayArray[$i],
            'time_in'=> $time_in,
            'time_out' => $time_out,
            'late' =>$late,
            'f_halfday' => $firsthalf_halfday,
            's_halfday' => $secondhalf_halfday,
            'early_going' => $early_going,
            'hours' => $hours,
            'day'=>date('l',strtotime($dayArray[$i])),
            'mtimein' => $mtimein,
            'mtimeout' => $mtimeout,
            'atype' => $atype,
            'adjustedby' => $adjustedby,
            'total_hours' =>self::$total_working_hour,
            'total_absent' => self::$total_absent,
            's_late' => self::$slate,
            'role_id' => \Auth::user()->roles->first()->id,
            //'absent_count' =>$absent_count,
            'reason' => $reason,
            'is_audit' => $is_audit,
            'is_admin' => $is_admin,

        ];
    }
    if($attendance_log ==  true){
        $is_active =  Employee::where('emp_no', $empCode)->value('is_active');    
        foreach($array as $key => $value)
        {
            if($is_active){
            AttendanceLog::updateOrCreate(['emp_no'=>$value['empcode'],'date'=>$value['date']],[
                'attendance_id'=>$value['id'],
                'emp_no'=>$value['empcode'],
                'emp_name'=>$value['empname'],
                'date'=>$value['date'],
                'time_in'=>$value['time_in'],
                'time_out'=>$value['time_out'],
                'late'=>$value['late'],
                'absent'=>($value['time_in'] == 'ABSENT' && $value['time_out'] == 'ABSENT') ? 1 : 0,
                'f_halfday'=>$value['f_halfday'],
                's_halfday'=>$value['s_halfday'],
                'early_going'=>$value['early_going'],
                'hours'=>$value['hours'],
                'day'=>$value['day'],
                'm_time_in'=>$value['mtimein'],
                'm_time_out'=>$value['mtimeout'],
                'month'=>date('F',strtotime($value['date'])),
            ]);
        }
        }
        }  // attendance log ends

        // if ramzan is enable
        if(self::getAppParams()['is_ramzan'])
        {
            $array = [];
            $attendances = AttendanceLog::select("attendance_adjustment.adjustment_type","attendance_adjustment.time_in as mtimein","attendance_adjustment.time_out as mtimeout","attendance_log.*","users.name")
                // ->leftjoin('attendance','attendance.id','=','attendance_log.attendance_id')
                ->leftjoin('attendance', function($join){
                    $join->on('attendance.date','=','attendance_log.date')
                         ->on('attendance.emp_id','=','attendance_log.emp_no');
                })
                ->leftjoin('attendance_adjustment','attendance_adjustment.atten_id','=','attendance.id')
                ->leftjoin('users','users.id','=','attendance_adjustment.adjusted_by')
                ->where('attendance_log.emp_no',$empCode)
                ->whereBetween("attendance_log.date",[$from, $to])
                ->orderBy('attendance_log.date','asc')
                ->get();
            foreach ($attendances  as $value)
            {
                $total_absent_array[] = $value->absent;
                self::$total_absent = array_sum($total_absent_array);
                self::$total_working_hour += $value->hours;
                if(Attendance::where('emp_id', $value->emp_no)->where('date', $value->date)->exists()){
                    $actual_time_in_out =    Attendance::where('emp_id', $value->emp_no)->where('date', $value->date)->first();

                        $time_in  = $actual_time_in_out->time_in;
                        $time_out  = $actual_time_in_out->time_out;


                }else{
                    $time_in = $value->time_in;
                    $time_out = $value->time_out;
                    $mtime_in  = $value->m_time_in ;
                    $mtime_out  = $value->m_time_out;
                }


                $array[] = [
                    'id' => $value->attendance_id,
                    'empcode' =>$value->emp_no,
                    'empname' => $value->emp_name,
                    'date' => $value->date,
                    'time_in' =>   $time_in,
                    'time_out' =>  $time_out,
                    'late' =>$value->late,
                    'f_halfday' => $value->f_halfday,
                    's_halfday' => $value->s_halfday,
                    'early_going' => $value->early_going,
                    'hours' =>self::hours($time_in, $time_out,$value->mtimein ?:$value->m_time_in,$value->mtimeout ?:$value->m_time_out),
                    'day'=>$value->day,
                    'mtimein' => $value->mtimein ?:$value->m_time_in,
                    'mtimeout' => $value->mtimeout ?:$value->m_time_out,
                    'atype' => $value->adjustment_type ?: 'N/A',
                    'adjustedby' => $value->name ?: 'N/A',
                    'total_hours' =>self::$total_working_hour,
                    'total_absent' => self::$total_absent,
                    //'s_late' => self::$slate,
                    'role_id' => \Auth::user()->roles->first()->id,
                    //'absent_count' =>$absent_count,
                    'reason' => 'Reason',
                    'is_audit' => 1,
                    'is_admin' => 1,

                ];
            }
        }

       return $array;

    }

    // this is also use for adv salary duration
    public static function getEmployeeDurationInCompany($hiringDate)
    {

         if(!is_null($hiringDate)){
            $date1 = $hiringDate;
            $date2 = \Carbon\Carbon::now()->format('Y-m-d');

            $ts1 = strtotime($date1);
            $ts2 = strtotime($date2);

            $year1 = date('Y', $ts1);
            $year2 = date('Y', $ts2);

            $month1 = date('m', $ts1);
            $month2 = date('m', $ts2);

            $diff = (($year2 - $year1) * 12) + ($month2 - $month1);
            Log::info('diff in if: ' . $diff);
        } else {
            $diff = 0;
            Log::info('diff in else: ' . $diff);
        }
        return $diff;
    }


    // attendance for ramzan 
    public function attendanceLog()
    {
        
    }    

    public static function getThreeMonthGap($empCode,$advance_sal = false)
    {
        if($advance_sal == true)
        {
          $lastPaidDate  = AdvancedSalary::where('emp_id', $empCode)->orderBy('id','desc')->limit(1)->value('paid_at');
          
        }else{
          $lastPaidDate = LoanLedger::select('loan_ledger.date')->join('loan','loan.id','=','loan_ledger.loan_id')
                          ->join('employees','employees.emp_no','=','loan.emp_id')
                          ->where('employees.emp_no',$empCode)
                          ->where('loan_ledger.balance','=',0)
                          ->orderBy('loan_ledger.loan_id','desc')
                          ->first();
          $lastPaidDate = is_null($lastPaidDate) ? true : $lastPaidDate->date;                             
        }
        if(self::getDaysBetweenTwoDates($lastPaidDate, \Carbon\Carbon::now()->format('Y-m-d')) >= 90 ) {
            $response =  false;
        } elseif(is_null($lastPaidDate)) {
            $response =  false;
        } else {
            $response = true;
        }
        return $response;
    }

    function dateDiffInDays($date1, $date2)  
    { 
    // Calulating the difference in timestamps 
    $diff = strtotime($date2) - strtotime($date1); 
      
    // 1 day = 24 hours 
    // 24 * 60 * 60 = 86400 seconds 
    return abs(round($diff / 86400)); 
    }    
    public static function percentOfSalary($percent_of_salary, $salary, $adv_amount)
    {
        $percentAmount = $salary / 100 * $percent_of_salary;
        $response = ($adv_amount > $percentAmount) ? true : false;
        return $response; 
    } 

    public static function loanAmountGradeWsie($empCode, $loanAmount)
    {
       
        $datas = Employee::select('current_salary','position_no')->where('emp_no',$empCode)->first();
        $salary = $datas->current_salary;
        $level = $datas->position_no;
        $levelArray = Position::pluck('value')->toArray();
        $data = self::getAppParams()['category_one'];
        $json = json_decode($data,true);
        $data1 = self::getAppParams()['category_two'];
        $json1 = json_decode($data1,true);
        $data2 = self::getAppParams()['category_three'];
        $json2 = json_decode($data2,true);

        if($level == $json['level'])
        {
            $multipliedValue =  $json['limit'] * $salary;
            $installment = $json['installment'];
        }
        if($level == $json1['level'])
        {
            $multipliedValue =  $json1['limit'] * $salary;
            $installment = $json1['installment'];

        }
        if($level == $json2['level'])
        {
            $multipliedValue =  $json2['limit'] * $salary;
            $installment = $json2['installment'];

        }

        $response =  ($multipliedValue >= $loanAmount) ? false : true;
        return $response;
    }

    public static function checkInstallmentLevelWise($empCode, $installments)
    {
        // this functuon will leve 
        $level= Employee::where('emp_no',$empCode)->value('position_no');
        $data2 = self::getAppParams()['category_three'];
        $json2 = json_decode($data2,true);

        if($level == 'L3')
        {
            $levelInstallment =  $json2['installment'];
        }else{
            return false;
        }

        if($installments <= $levelInstallment)
        {
            $response = false;
        }else{
            $response = true;
        }

        return $response;
    }


    public static function checkPreviousLeavesApproved($empCode)
    {
        $response = EmployeeLeave::where('emp_id',$empCode)->where('is_approved',0)->orderBy('id','desc')->limit(1)->exists();
        return $response;
    } 

    public static function leaveSameDateCheck($empCode, $from, $to)
    {
        $data = EmployeeLeave::select('from','to')->where('emp_id',$empCode)->where('is_approved',1)->orderBy('id','desc')->limit(1)->first();
        if($data)
        {
            if($data->from <= $from &&  $data->to >= $to)
            {
                $response = true;
            }
            else{
                $response = false;
            }
        }else{
                $response = false;
        }
        return $response;
    } 


    public static function leaveEmptyCheck($empCode, $leavetype_id, $days)
    {

       $leaveBalance = EmployeeLeave::select(\DB::raw('sub_company_leave_type.no_of_leaves - SUM(DATEDIFF(employee_leave.to, employee_leave.from)+1) as balance'), 'sub_company_leave_type.no_of_leaves as totalleaves')
                        ->join('sub_company_leave_type','sub_company_leave_type.leave_type_id','=','employee_leave.sub_company_leave_type_id')
                        ->where('employee_leave.emp_id',$empCode)
                        ->where('employee_leave.sub_company_leave_type_id',$leavetype_id)
                        ->first();

        if($leaveBalance->balance)
        {
            $response = $leaveBalance->balance <= $days ? true : false;
        }elseif($leaveBalance->totalleaves  < $days)
        {
            $response = true;
        }
        else{
            $response = false;
        }
       return $response;
    }


    public static function sundaySandwich($empID, $from, $to, $leave_id, $apply = false)
    {
        $sandwichCount = 0;
        $holidayCount = 0;
        $company = self::getCompanyId($empID);
        $holidays = Holidays::where('sub_company_id',self::getCompanyId($empID))->pluck('date')->toArray();
        $sandwich =  self::getSubCompanyAppParamValue('sandwich',$company, self::getRegionId($empID));
        
        if(!$sandwich)
        {            
            $dayArray = self::getDaysAndDatesBetweenTwoDates($from,$to);
            
            for($i = 0; $i < count($dayArray); $i++)
            {
                if(EmployeeLeave::where('emp_id',$empID)->where('from', $from)->where('to', $to)->where('sub_company_leave_type_id', $leave_id)->exists() && !$apply){
                    if(date('l',strtotime($dayArray[$i])) == 'Sunday'){
                        $array[] = ($sandwichCount) + 1;
                    }else{
                        $array[] = 0;
                    }
                    if(\DB::table('holidays')->where('sub_company_id', $company)->where('date', $dayArray[$i])->exists())
                    {
                        $array[] = ($holidayCount) + 1;
                    }else{
                        $array[] = 0;
                    }
                    $response = array_sum($array);
                }else{
                    if(date('l',strtotime($dayArray[$i])) == 'Sunday'){
                        $array[] = ($sandwichCount) + 1;
                    }else{
                        $array[] = 0;
                    }
                    if(\DB::table('holidays')->where('sub_company_id', $company)->where('date', $dayArray[$i])->exists())
                    {
                        $array[] = ($holidayCount) + 1;
                    }else{
                        $array[] = 0;
                    }
                    $response = array_sum($array);
                }

            }
             // dd($response);   
             
           
        }else{
            $response = 0;
        }
        return $response;
    }
    
    public static function holidaySandwich($empID, $from, $to, $leave_id)
    {
        $holidayCount = 1;
       
        $company = self::getCompanyId($empID);
        $holidays = Holidays::where('sub_company_id',self::getCompanyId($empID))->pluck('date')->toArray();
        $sandwich =  self::getSubCompanyAppParamValue('sandwich',$company);
        if(!$sandwich)
        {
            
            $dayArray = self::getDaysAndDatesBetweenTwoDates($from,$to);
            
            for($i = 0; $i < count($dayArray); $i++)
            {
                if(EmployeeLeave::where('emp_id',$empID)->where('from', $from)->where('to', $to)->where('sub_company_leave_type_id', $leave_id)->exists()){
                    if(\DB::table('holidays')->where('sub_company_id', $company)->where('date', $dayArray[$i])->exists()){
                        $array[] = $holidayCount++;
                    }else{
                        $array[] = 0;
                    }
                    $response = array_sum($array);
                }else{
                    $response = 0;
                }

            }
                   
             
           
        }else{
            $response = 0;
        }

        return $response;
    }

    public static function getToNFromOfLeaves($empCode, $leave_id)
    {
 if(EmployeeLeave::select('from','to')->where('sub_company_leave_type_id', $leave_id)->where('emp_id',$empCode)->where('is_approved',1)->where(\DB::raw('YEAR(application_date)'),\Carbon\Carbon::now()->format('Y'))->exists()){

        $getToNFrom = EmployeeLeave::select('from','to')->where('sub_company_leave_type_id', $leave_id)->where('emp_id',$empCode)->where('is_approved',1)->where(\DB::raw('YEAR(application_date)'),\Carbon\Carbon::now()->format('Y'))->get();
            //return $getToNFrom;
        foreach ($getToNFrom as $k => $v) {
                                    # code...
                                   $sandwich[] = \Utils::sundaySandwich($empCode,$v->from,$v->to,$leave_id) ;
                               }
                               return $sandwich;
        }else{
            return $sandwich = [];
        }
    }


    public static function getAdvanceSalAmount($empCode, $month)
    {
        $advance_sal = AdvancedSalary::where('emp_id',$empCode)->where(\DB::raw('MONTHNAME(apply_at)'),$month)->where('is_approved', 1)->value('amount');
        $response = is_null($advance_sal) ? 0 : $advance_sal;
        return $response;
    }

    public static function getPfValue($empCode)
    {
        $pf = \DB::table('pf_slabs as pf')
                        ->join('employees as e','e.pf_slab_id','=','pf.id')
                        ->where('e.emp_no',$empCode)
                        ->value('pf.amount');
        return $response = is_null($pf) ? 0 : $pf;                
    }

    public static function getSalary($empCode)
    {
        $sal =  Employee::where('emp_no',$empCode)->value('current_salary');
        return $response = is_null($sal) ? 0 : $sal;
    }

    public static function getTax($empCode)
    {

        $tax =  \DB::table('employee_tax_slab')
                ->join('employees','employees.emp_no','=','employee_tax_slab.emp_id')
                ->join('tax_slabs','tax_slabs.id','=','employee_tax_slab.tax_slab_id')
                ->where('employee_tax_slab.emp_id',$empCode)
                ->first();
    if($tax){
        $annual_gross_sal = self::getSalary($empCode) * 12;  // gross salary
        $basic_salary = (($annual_gross_sal / 100) * 66);
        $annual_medical = (($basic_salary/100) * 10);
        $final_annual_sal = $annual_gross_sal - $annual_medical;   // final annual salary exempted medical
        if($tax->to === 0)
        {
               $final_tax = $final_annual_sal  - $tax->from; 
               $final_tax = round(((($final_annual_sal / 100) * $tax->percentage) + $tax->amount) / 12);
  
        } else {
  
            if( $tax->from <= $final_annual_sal && $final_annual_sal <= $tax->to)
            {
                $final_tax = ($final_annual_sal  - $tax->from) / 100 *$tax->percentage ;
                $final_tax =  round(($final_tax + $tax->amount) / 12);

            }
        }

        } else {
            $final_tax = 0;
        }

        return $response = is_null($final_tax) ? 0 : $final_tax;
    }

    public static function getEobi($empCode)
    {
        $eobi =  \DB::table('employees')->where('emp_no',$empCode)->where('is_active',1)->value('eobi_amount');
        return $response = is_null($eobi) ? 0 : $eobi;
    }

    public static function getParkingAmount($empCode)
    {
        $parking_amount =  \DB::table('employees')->where('emp_no',$empCode)->where('is_active',1)->value('parking_amount');
        return $response = is_null($parking_amount) ? 0 : $parking_amount;
    }

    public static function getFuelAmount($empCode)
    {
        $fuel_amount =  \DB::table('employees')->where('emp_no',$empCode)->where('is_active',1)->value('fuel_amount');
        return $response = is_null($fuel_amount) ? 0 : $fuel_amount;
    }


    public static function getArrears($empCode,$month)
    {
        $arrear_amount =  \DB::table('emp_arrears')->where('emp_id',$empCode)->where('month',$month)->orderBy('id','desc')->value(\DB::raw('SUM(arrear_amount)'));
        return $response = is_null($arrear_amount) ? 0 : $arrear_amount;
    }

    public static function getLoanDeductionAmt($empCode,$month)
    {
        $amt  =  \DB::table('loan_ledger as ll')
                        ->join('loan as l','l.id','=','ll.loan_id')
                        ->join('employees as e','e.emp_no','=','l.emp_id')
                        ->where('l.emp_id',$empCode)
                        ->where('ll.month',$month)
                        ->where('ll.deduction_type','system')
                        ->orderBy('ll.month','desc')
                        ->value('ll.deduction_amount');

        //     if($amt->balance == 0)
        //     {
        //         $amount = $amt->balance;
        //     }else{
        //         $amount = $amt->deduction_amount;
        //     }

        // }else{
        //     $amount = 0;
        // }                
                        //
        return $response   =  is_null($amt) ? 0 : $amt;
    } 

    public static function deductLeavesFromSalary($empCode, $month)
    {
//        $empCode = 423;
        $leaves_from_salary  =  \DB::table('leave_deduction as ld')
                        ->where('emp_id',$empCode)
                        ->where('month',$month)
                        ->where('leave_type_id',0)
                        ->value(\DB::raw('IFNULL(SUM(deducted_leave),0)'));
        // deducted amount
        $payroll_date = self::getSubCompanyAppParamValue('payroll_date',self::getCompanyId($empCode),$region = NULL); 
        $from = date('Y-m-'.$payroll_date.'', strtotime('last month', strtotime($month)));
        $to =  date('Y-m-'.$payroll_date.'', strtotime($month));   
        $date_of_hiring =  Employee::where('emp_no', $empCode)->value('date_of_hiring');
        //if(date('F', strtotime($date_of_hiring)) == $month && date('Y', strtotime($date_of_hiring)) == date('Y'))
        if($date_of_hiring <= $to && $date_of_hiring >= $from)
        {    
            $leaves_from_salary  = self::getDaysBetweenTwoDates($from, $to) -  (self::getDaysBetweenTwoDates($date_of_hiring, $to) + 1);
        }

        $no_of_days_for_salary = self::getDaysBetweenTwoDates($from, $to);       
        $deducted_amount = round($leaves_from_salary * self::getSalary($empCode)/$no_of_days_for_salary);
        return $deducted_amount;
    }

    public static function getItemDeduction($empCode, $month)
    {
        // get number of leave to be deducted from salary
        $emp_item_id = EmployeeItem::where('emp_id',$empCode)->pluck('id');
        $dedAmt = EmpItemLedger::whereIn('emp_item_id',$emp_item_id)->where('month', $month)->sum('deduction_amt');
        return is_null($dedAmt) ? 0 : $dedAmt;              
    }

    public static function payroll($empCode, $month)
    {
                   

        $payroll = self::getSalary($empCode) - self::deductLeavesFromSalary($empCode, $month) -self::getPfValue($empCode) - self::getAdvanceSalAmount($empCode, $month) - self::getTax($empCode)  - self::getLoanDeductionAmt($empCode, $month) - self::getItemDeduction($empCode, $month) - self::getEobi($empCode) + self::getFuelAmount($empCode) + self::getParkingAmount($empCode) + self::getArrears($empCode, $month);

        return sprintf("%.2f",$payroll);
    }


    public static function getEmpTypeKey($empCode)
    {
        $type = EmployeeType::where('e.emp_no', $empCode)
                              ->join('employees as e','e.emp_type_id','=','emp_type.id')
                              ->value('key');
        return $type;                       
    }

    public static function leaveDeduction($employees, $from, $to, $month)
    {
        $leaveids = self::getLeaveIdByName();
        foreach($employees as $employee){

            $attendanceLog=  \DB::table('attendance_log')->select('emp_no',\DB::raw("SUM(late) as late"),\DB::raw("SUM(absent) as absent"),\DB::raw("SUM(f_halfday) as fhalfday"),\DB::raw("SUM(s_halfday) as shalfday"),\DB::raw("SUM(early_going) as eg"))
            ->where('emp_no',$employee->emp_no)
            ->whereBetween('date', [$from,$to])
            ->get();

            $late_limit = self::getSubCompanyAppParamValue('late_deduction_limit',$employee->company_id, $employee->region_id);
            $hday_limit = self::getSubCompanyAppParamValue('halfday_deduction_limit',$employee->company_id, $employee->region_id);

//   late / halfday / absent values
            $late =  floor(($attendanceLog[0]->late + $attendanceLog[0]->eg)/$late_limit);
            $halfday =  floor(($attendanceLog[0]->fhalfday + $attendanceLog[0]->shalfday)/$hday_limit);
            $absent = $attendanceLog[0]->absent;
            $total =  $late + $halfday + $absent;
            //$total = 14;
// total number of leave of leave type wrt company
            $casual_total_leaves =  \DB::table('sub_company_leave_type')
                                        ->where('sub_company_id',$employee->company_id)
                                        ->where('leave_type_id',$leaveids['casual_leaves'])
                                        ->value('no_of_leaves');
            $annual_total_leaves =  \DB::table('sub_company_leave_type')
                                        ->where('sub_company_id',$employee->company_id)
                                        ->where('leave_type_id',$leaveids['annual_leaves'])
                                        ->value('no_of_leaves');
            $medical_total_leaves =  \DB::table('sub_company_leave_type')
                                        ->where('sub_company_id',$employee->company_id)
                                        ->where('leave_type_id',$leaveids['medical_leaves'])
                                        ->value('no_of_leaves');

 // leaves balances
            $casual_balance = \DB::table('employee_leave')
                                ->where('sub_company_leave_type_id',$leaveids['casual_leaves'])
                                ->where('emp_id', $employee->emp_no) 
                                ->where('is_approved',1)
                                ->where(\DB::raw('MONTHNAME(application_date)'), $month)
                                //->value(\DB::raw('(SELECT IF(IFNULL(SUM(DATEDIFF(employee_leave.to,employee_leave.from)),0),0,SUM(DATEDIFF(employee_leave.to,employee_leave.from)+1)))'));
                                ->value(\DB::raw('IFNULL(SUM(DATEDIFF(employee_leave.to, employee_leave.from)+1),0)'));

            $annual_balance = \DB::table('employee_leave')
                                ->where('sub_company_leave_type_id',$leaveids['annual_leaves'])
                                ->where('emp_id', $employee->emp_no) 
                                ->where('is_approved',1)
                                ->where(\DB::raw('MONTHNAME(application_date)'), $month)
                                //->value(\DB::raw('(SELECT IF(IFNULL(SUM(DATEDIFF(employee_leave.to,employee_leave.from)),0),0,SUM(DATEDIFF(employee_leave.to,employee_leave.from)+1)))'));
                                ->value(\DB::raw('IFNULL(SUM(DATEDIFF(employee_leave.to, employee_leave.from)+1),0)'));

            $medical_balance = \DB::table('employee_leave')
                                ->where('sub_company_leave_type_id',$leaveids['medical_leaves'])
                                ->where('emp_id', $employee->emp_no) 
                                ->where('is_approved',1)
                                ->where(\DB::raw('MONTHNAME(application_date)'), $month)
                                //->value(\DB::raw('(SELECT IF(IFNULL(SUM(DATEDIFF(employee_leave.to,employee_leave.from)),0),0,SUM(DATEDIFF(employee_leave.to,employee_leave.from)+1)))'));
                                ->value(\DB::raw('IFNULL(SUM(DATEDIFF(employee_leave.to, employee_leave.from)+1),0)'));

            $payback_leave_bal =  \DB::table('payback_leaves')
                                    ->where('emp_id',$employee->emp_no)
                                    ->where('month',$month)
                                    ->value(\DB::raw('(SELECT IFNULL(SUM(no_of_leaves),0))'));

            // last deducted leaves
            $casual_ded_leave = \DB::table('leave_deduction')
                ->where('emp_id', $employee->emp_no)
                ->where('leave_type_id',$leaveids['casual_leaves'])
                ->where(\DB::raw('YEAR(date)'), \Carbon\Carbon::now()->format('Y'))
                ->sum('deducted_leave');
            $annual_ded_leave = \DB::table('leave_deduction')
                ->where('emp_id', $employee->emp_no)
                ->where('leave_type_id',$leaveids['annual_leaves'])
                ->where(\DB::raw('YEAR(date)'), \Carbon\Carbon::now()->format('Y'))
                ->sum('deducted_leave');
            $medical_ded_leave = \DB::table('leave_deduction')
                ->where('emp_id', $employee->emp_no)
                ->where('leave_type_id',$leaveids['medical_leaves'])
                ->where(\DB::raw('YEAR(date)'), \Carbon\Carbon::now()->format('Y'))
                ->sum('deducted_leave');
            // format balance

            $casual_balance = $casual_total_leaves - ($casual_balance + $casual_ded_leave);
            $annual_balance = $annual_total_leaves - ($annual_balance + $annual_ded_leave);
            $medical_balance = $medical_total_leaves - ($medical_balance + $medical_ded_leave);
            $direct_deduction = self::getSubCompanyAppParamValue('direct_deduction',$employee->company_id, $employee->region_id);
            if(!$direct_deduction){
// check if employee permanent or probation
            if(self::getEmpTypeKey($employee->emp_no)  == 'perm'){
            // check casual bal less either zero
            if($payback_leave_bal < $total){
                $total = $total - $payback_leave_bal;
                    $array[] =[
                        'emp'=>$employee->emp_no,
                        'deduction'=>$payback_leave_bal,
                        'leavetype'=>$leaveids['payback_leaves'],
                        'month' => $month,
                        'date' => date('Y-m-d'),
                        'company_id'=>$employee->company_id
                    ];

            if($casual_balance < $total)
                {
                    $newcasual = $total - ($total - $casual_balance);
                    $total = $total - $casual_balance;
                    $array[] =[
                        'emp'=>$employee->emp_no,
                        'deduction'=>$newcasual,
                        'leavetype'=>$leaveids['casual_leaves'],
                        'month' => $month,
                        'date' => date('Y-m-d'),
                        'company_id'=>$employee->company_id
                    ];
                    if($annual_balance < abs($total))
                    {
                        $newannual = $total  - ($total - $annual_balance);
                        $total = $total - $annual_balance;

                        $array[] = [
                        'emp'=>$employee->emp_no,
                        'deduction'=>$newannual,
                        'leavetype'=>$leaveids['annual_leaves'],
                        'month' => $month,
                        'date' => date('Y-m-d'),
                        'company_id'=>$employee->company_id
                    ];
                        if($medical_balance < abs($total))
                        {
                            $medicalannual = $total - ($total - $medical_balance);
                            $total = $total - $medical_balance;
                            $array[] = [
                                'emp'=>$employee->emp_no,
                                'deduction'=>$medicalannual,
                                'leavetype'=>$leaveids['medical_leaves'],
                                'month' => $month,
                                'date' => date('Y-m-d'),
                                'company_id'=>$employee->company_id
                        ];

                        // if leaves left after deduction from all leaves
                        if($total){
                            $array[] = [
                                'emp'=>$employee->emp_no,
                                'deduction'=>$total,
                                'leavetype'=>0,
                                'month' => $month,
                                'date' => date('Y-m-d'),
                                'company_id'=>$employee->company_id
                                ];


                        }

                        }else{
                            $total = $medical_balance - ($medical_balance - $total);
                            $array[] = [
                                'emp'=>$employee->emp_no,
                                'deduction'=>$total,
                                'leavetype'=>$leaveids['medical_leaves'],
                                'month' => $month,
                            'date' => date('Y-m-d'),
                            'company_id'=>$employee->company_id
                            ];
                        }

                    }else{
                        $total =$annual_balance - ($annual_balance - $total);
                        $array[] = [
                            'emp'=>$employee->emp_no,
                            'deduction'=>$total,
                            'leavetype'=>$leaveids['annual_leaves'],
                            'month' => $month,
                            'date' => date('Y-m-d'),
                            'company_id'=>$employee->company_id
                        ];
                    }
                }else{
                        $total = $casual_balance - ($casual_balance - $total);
                        $array[] = [
                            'emp'=>$employee->emp_no,'deduction'=>$total,
                            'leavetype'=>$leaveids['casual_leaves'],
                            'month' => $month,
                            'date' => date('Y-m-d'),
                            'company_id'=>$employee->company_id
                        ];
                }
            }else{
                $total = $payback_leave_bal - ($payback_leave_bal - $total);
                        $array[] = [
                            'emp'=>$employee->emp_no,
                            'deduction'=>$total,
                            'leavetype'=>$leaveids['payback_leaves'],
                            'month' => $month,
                            'date' => date('Y-m-d'),
                            'company_id'=>$employee->company_id
                        ];
            }
        } else { 
            $array[] = [

                         'emp'=>$employee->emp_no,
                         'deduction'=>$total,
                         'leavetype'=>0,
                         'month' => $month,
                         'date' => date('Y-m-d'),
                         'company_id'=>$employee->company_id

                        ];
        }
} else {

                $array[] = [

                     'emp'=>$employee->emp_no,
                     'deduction'=>$total,
                     'leavetype'=>0,
                     'month' => $month,
                     'date' => date('Y-m-d'),
                     'company_id'=>$employee->company_id

                    ];
}


        }
    
        foreach ($array as $key => $value) {
            # code...
            if($value['deduction'] > 0)
            LeaveDeduction::updateOrCreate(['emp_id'=>$value['emp'],'leave_type_id'=>$value['leavetype'],'month'=>$value['month']],
            ['company_id'=>$value['company_id'],
            'emp_id'=>$value['emp'],
            'leave_type_id'=>$value['leavetype'],
            'deducted_leave'=>$value['deduction'],
            'date'=>$value['date'],
            'month'=>$value['month']

            ]);
        }

        return response()->json(["msg" => "Leave deduction process has been done successfully."],200);
    }


// get pay back leave
    public static function getPayBackLeaves($empCode, $month)
    {
        
        // get company id
        $count = [];
        $leaveCount = 0;
        $company_id =  Employee::where('emp_no',$empCode)->value('company_id');
        if($company_id != 3 )
        {
            $from = date('Y-m-26', strtotime('last month', strtotime($month)));
            $to =  date('Y-m-25', strtotime($month));
        }else{
            $from = date('Y-m-21', strtotime('last month',strtotime($month)));
            $to =  date('Y-m-20', strtotime($month));
        }


        // get holidays of company
        $holidays = Holidays::where('sub_company_id',$company_id)->pluck('date')->toArray();
        // get off times from attendance log
        $offDates = \DB::table('attendance_log')
                        ->where('hours','>=',6)
                        ->where('emp_no',$empCode)
                        ->whereBetween('date',[$from,$to])
                        ->get(['date']);

        foreach ($offDates as $key => $value) {
                            # code...
           // holiday check  
           if(in_array($value->date, $holidays))
           {
                $count[] = ($leaveCount) + 1;
           }else{
                $count[] = 0;
           }
           // weekday off check 
          if(date('l', strtotime($value->date)) == self::getSubCompanyAppParamValue('weekday_off',$company_id,self::getRegionId($empCode)))
           {
                $count[] = ($leaveCount) + 1;
           }else{
                $count[] = 0;
           }
        }                

        $newcount = array_sum($count);
        return $newcount;

    }

    public static  function getMonthList()
    {
        $array = [
            '01'=>'January',
            '02'=>'February',
            '03'=>'March',
            '04'=>'April',
            '05'=>'May',
            '06'=>'June',
            '07'=>'July',
            '08'=>'August',
            '09'=>'September',
            '10'=>'October',
            '11'=>'November',
            '12'=>'December',
        ];

        return $array;
    }

    public static  function getYearList()
    {
        $first = (int)date('Y') - 9;
        $lastYear = (int)date('Y') ;
        for($i=$lastYear;$i>=$first;$i--)
        {
            $array[] = $i;
        }

        return $array;
    }

    public static function employeePayBackLeaves($empCode, $month = false)
    {
        if($month ===  false){
        $payback_leaves =  PayBackLeaves::where('emp_id', $empCode)
            ->where(\DB::raw("YEAR(paid_at)"), \Carbon\Carbon::now()->format('Y'))
            ->sum('no_of_leaves');
        }else {
            $payback_leaves =  PayBackLeaves::where('emp_id', $empCode)
            ->where(\DB::raw("YEAR(paid_at)"), \Carbon\Carbon::now()->format('Y'))
            ->where('month', \Carbon\Carbon::now()->subMonth()->format('F'))
            ->value('no_of_leaves');

        }
        return $total = $payback_leaves;
    }
    public static  function employeePayBackDeductedLeaves($empCode)
    {
        $paybk_deducted_leave = LeaveDeduction::where('emp_id', $empCode)
            //->where('month', \Carbon\Carbon::now()->subMonth()->format('F'))
            ->where('leave_type_id',7)
            ->where(\DB::raw("YEAR(created_at)"), \Carbon\Carbon::now()->format('Y'))
            ->sum('deducted_leave');
        return $paybk_deducted_leave;
    }

    public static function getPfTotalValue($empCode)
    {
        $total = self::getEmployeeDurationInCompany(Employee::where('emp_no', $empCode)->value('pf_effective_date')) * self::getPfValue($empCode);
        return $total;
    }

    public static function getReportingManager($empCode)
    {
        $reporting_manager = \DB::table('employees')->join('employees as e2','e2.emp_no','=','employees.report_manager')
                              ->where('employees.emp_no',$empCode)
                              ->value('e2.name');
        return $reporting_manager;                      
    }

    public static function getSubCompanyLeaveTypeId($empCode, $leave_type_id)
    {
        $company_id = self::getCompanyId($empCode);
        $sub_company_leave_type_id =  SubCompanyLeaveType::where('sub_company_id', $company_id)->where('leave_type_id', $leave_type_id)->value('id');
        return $sub_company_leave_type_id;
    }



    public static function getLeaveCounts($empCode)
    {

        $company_id = self::getCompanyId($empCode);
        $permanent_year =  Employee::where('emp_no', $empCode)->value(\DB::raw('YEAR(permanent_at)'));
        $get_company_leaves =  SubCompanyLeaveType::where('sub_company_id',$company_id)->join('leave_type','leave_type.id','=','sub_company_leave_type.leave_type_id')->get(['no_of_leaves','leave_type.name','sub_company_leave_type.id']);
        if(self::getEmpTypeKey($empCode)  == 'perm'){
            foreach($get_company_leaves as $key => $value)
            {
                $data[str_replace(" ","_",strtolower($value->name))] = $value->no_of_leaves;

            }
            foreach($get_company_leaves as $key => $value)
            {
                $data2[str_replace(" ","_",strtolower($value->name))] =$value->id;
            }
        $perm_date = explode("-", Employee::where('emp_no',$empCode)->value('permanent_at'));
        if($permanent_year == \Carbon\Carbon::now()->format('Y'))
        {
                if($perm_date[2] > 15)
                {
                    $cut_off_point = 1;
                }else{
                    $cut_off_point = 0;
                }
            $new_perm_date =  $perm_date[0].'-'.$perm_date[1].'-'.$perm_date[2];
            self::$first_date_of_the_year =  Carbon::now()->format('Y-01-01');
            $month_diff = self::getNoOfMonths(self::$first_date_of_the_year,$new_perm_date);
            $casual_leaves = round( ($data['casual_leaves'] / 12) * ($month_diff  + $cut_off_point));
            $annual_leaves = round( ($data['annual_leaves'] / 12) * ($month_diff  + $cut_off_point));
            $medical_leaves = round( ($data['medical_leaves'] / 12) * ($month_diff + $cut_off_point));
            $response['casual_leaves'] = ($data['casual_leaves'] - $casual_leaves);
            $response['annual_leaves'] = ($data['annual_leaves'] - $annual_leaves);
            $response['medical_leaves']= ($data['medical_leaves'] - $medical_leaves);

        }else{
            $response['casual_leaves']  = $data['casual_leaves'];
            $response['annual_leaves']  = $data['annual_leaves'];
            $response['medical_leaves'] = $data['medical_leaves'];
        }


        }else{
            foreach($get_company_leaves as $key => $value)
            {
                $response[str_replace(" ","_",strtolower($value->name))] = 0.00;
            }
        }


        return $response;
    }

    public static function  getAppliedLeaves($empCode, $leaveId)
    {
        $data = \DB::table('employee_leave')
                 ->where('emp_id',$empCode)
                ->where('sub_company_leave_type_id', $leaveId)
                ->where('is_approved',1)
                ->where(\DB::raw('YEAR(application_date)'), \Carbon\Carbon::now()->format('Y'))
                ->value(\DB::raw('SUM(DATEDIFF(employee_leave.to, employee_leave.from)+1)'));
        return $data;
    }

    public static function  getLeaveIdByName()
    {
        $data =  \DB::table('leave_type')->get(['name','id']);
        foreach($data as $key => $value)
        {
            $response[str_replace(" ","_",strtolower($value->name))] =  $value->id;
        }
        return $response;
    }

    public static function getSubCompanyLeaveCount($id)
    {
        $count =  SubCompanyLeaveType::where('id',$id)->value('no_of_leaves');
        return $count;
    }

    public static function getAwards($empCode)
    {
        $awards =  LoyalityAward::where('emp_no', $empCode)->get();
        return response()->json($awards, 200);
    }


    public static function getLeaveRequestCount($mangerId)
    {
        $count = Employee::join('employee_leave as el','employees.emp_no','=','el.emp_id')
                           ->where('el.is_approved','=',0)
                           ->where('employees.is_active','=',1)
                           ->where('employees.report_manager','=',$mangerId)
                           ->get();

        if($count->count()){
            $count = $count->count();
        }else{
            $count = 0;
        }                  
        return $count; 
    }

    public static function getLoanAdvSalRequestCount($role , $adv_sal = false)
    {
        if($adv_sal){

        if(in_array('hr-manager', $role))
        {
            $count = AdvancedSalary::where('is_approved',0)->count();
        } else if(in_array('finance', $role)) {

            $count = AdvancedSalary::whereIsPaid(0)->count();
        }else{
            $count = 0;
        }   

        }else{

        if(in_array('hr-manager', $role))
        {
            $count = Loan::where('is_approved',0)->count();
        } else if(in_array('finance', $role)) {

            $count = Loan::whereIsPaid(0)->count();
        }else{
            $count = 0;
        }   

        }

        return $count;
    } 

    public static function getAllApprovalCount($managerId, $role , $adv_sal = false)
    {
        $count = self::getLoanAdvSalRequestCount($role) + self::getLoanAdvSalRequestCount($role, true)+ self::getLeaveRequestCount($managerId);

        if($count)
        {
            return '<span class="count bg-danger">'.$count."</span>";
        }else{
            $count = '';
        }

        return $count;
    }


    public static function getLoyalityAwards($emp_id)
    {
        $employee = \DB::table('loyality_award')->where('emp_no', $emp_id)->get();
        return $employee;
    }

    public static function lastLoanPaymentDuration($empCode)
    {
        $last_payment_date = \DB::table('loan_ledger')->join('loan','loan_ledger.loan_id','=','loan.id')
                                ->where('loan.emp_id',$empCode)
                                ->orderBy('loan_ledger.id','desc')
                                ->value('loan_ledger.date');
        $duration  = self::getEmployeeDurationInCompany($last_payment_date);
        return $duration;                         
    }

   
    public static function adminCheckForAll($callback, $query = null){

        return $callback($query);
    }

    public static function queryCallback($query){
        if(auth()->user()->roles->first()->name != 'admin')
            {
              return   $query->where('employees.company_id',self::getCompanyId(auth()->user()->emp_code));
            }
        
    }

    public static function getManagerEmail($empCode)
    {
        $managerId = Employee::where('emp_no', $empCode)->value('report_manager');
        if($managerId)
        {
            $user = \App\Model\User::where('emp_code',$managerId)->first();
        }else{
            $user = NULL;
        }

        return $user;

    } 

}




