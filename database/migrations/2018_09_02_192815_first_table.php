<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FirstTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // company table
        Schema::create('company',function(Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->boolean('is_active')->default(1);
            $table->timestamps();
        });
        // sub company table
        Schema::create('sub_company',function(Blueprint $table){
            $table->increments('id');
            $table->integer('company_id');
            $table->string('name');
            $table->boolean('is_active')->default(1);
            $table->timestamps();
        });    
        //  department table
        Schema::create('department',function(Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->boolean('is_active')->default(1);
            $table->timestamps();
        });
        //sub department table
        Schema::create('sub_department',function(Blueprint $table){
            $table->increments('id');
            $table->integer('depart_id');
            $table->string('name');
            $table->boolean('is_active')->default(1);
            $table->timestamps();
        });
        // designation table
        Schema::create('designation',function(Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->boolean('is_active')->default(1);
            $table->timestamps();
        });    
        // employee type table
        Schema::create('emp_type',function(Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->boolean('is_active')->default(1);
            $table->timestamps();
        });            
        // employee table
        Schema::create('employee',function(Blueprint $table){
            $table->increments('id');
            $table->integer('emp_type_id')->nullable(true);
            $table->integer('depart_id')->nullable(true);
            $table->integer('schedule_id')->nullable(true);
            $table->string('emp_id_no')->nullable(true);
            $table->string('first_name')->nullable(true);
            $table->string('last_name')->nullable(true);
            $table->string('email')->nullable(true);
            $table->string('phone')->nullable(true);
            $table->string('alter_phone')->nullable(true);
            $table->string('address');
            $table->string('city');
            $table->string('religion');
            $table->enum('sex',['male','female','other'])->default('other');
            $table->enum('marital_status',['yes','no'])->default('no');
            $table->string('next_to_kin_name');
            $table->string('next_to_kin_phone');
            $table->string('next_to_kin_address');
            $table->string('next_to_kin_email');
            $table->date('joining_date');
            $table->string('bank_account_no');
            $table->date('salary');
            $table->date('leaving_date');
            $table->boolean('is_active')->default(1);
            $table->timestamps();
        });
        // schedule table            
        Schema::create('schedule', function(Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->boolean('is_active')->default(1);
            $table->timestamps();
        });
        // leave_type table
        Schema::create('leave_type', function(Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->boolean('is_active')->default(1);
            $table->timestamps();
        });
        // leave_assign table
        Schema::create('leave_assign', function(Blueprint $table){
            $table->increments('id');
            $table->integer('emp_id');
            $table->integer('leave_type_id');
            $table->integer('quantity');
            $table->boolean('is_active')->default(1);
            $table->timestamps();
        });
        // holidays table 
        Schema::create('holidays', function(Blueprint $table){
            $table->increments('id');
            $table->integer('sub_company_id');
            $table->boolean('is_active')->default(1);
            $table->timestamps();
        });
        // attendance table
        Schema::create('attendance', function(Blueprint $table){
            $table->increments('id');
            $table->integer('emp_id');
            $table->time('time_in');
            $table->time('time_out');
            $table->date('date');
            $table->string('month');
            $table->boolean('is_active')->default(1);
            $table->timestamps();
        });
        

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
