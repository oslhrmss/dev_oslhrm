<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Oslhrm extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('employees', function(Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->string('nationality')->nullable();
            $table->enum('gender',['male','female','other'])->default('other');
            $table->enum('religion',['islam','christian','hindu'])->default('islam');
            $table->enum('marital_status',['marreid','single','divorced','widow'])->default('single');
            $table->date('dob');
            $table->string('place_of_birth')->nullable();
            $table->string('eobi_no')->nullable();
            $table->string('cnic_no')->nullable();
            $table->string('cnic_place_of_issue')->nullable();
            $table->string('cnic_date_of_issue')->nullable();
            $table->string('cnic_date_of_expiry')->nullable();
            $table->string('passport_no')->nullable();
            $table->string('passport_place_of_issue')->nullable();
            $table->string('passport_date_of_issue')->nullable();
            $table->string('passport_date_of_expiry')->nullable();
            $table->string('visa_no')->nullable();
            $table->string('visa_place_of_issue')->nullable();
            $table->string('visa_date_of_issue')->nullable();
            $table->string('visa_date_of_expiry')->nullable();
            $table->string('iqama_no')->nullable();
            $table->string('iqama_place_of_issue')->nullable();
            $table->string('iqama_date_of_issue')->nullable();
            $table->string('iqama_date_of_expiry')->nullable();
            $table->string('vehicle_no')->nullable();
            $table->string('vehicle_name');
            $table->string('vehicle_model')->nullable();
            $table->string('vehicle_date_issue')->nullable();
            $table->string('liscence_no')->nullable();
            $table->string('liscence_place_of_issue')->nullable();
            $table->string('liscence_date_of_issue')->nullable();
            $table->string('liscence_date_of_expiry')->nullable();
            $table->string('position_title')->nullable();
            $table->string('position_no')->nullable();
            $table->string('dept_name')->nullable();
            $table->string('dept_no')->nullable();
            $table->string('date_of_hiring')->nullable();
            $table->string('currenty_salary')->nullable();
            $table->string('other_benifits')->nullable();
            $table->string('house_no')->nullable();
            $table->string('building')->nullable();
            $table->string('street_no')->nullable();
            $table->string('area')->nullable();
            $table->string('po_box')->nullable();
            $table->string('city')->nullable();
            $table->string('landline')->nullable();
            $table->string('mobile_no')->nullable();
            $table->string('email')->nullable();
            $table->string('p_house')->nullable();
            $table->string('p_building')->nullable();
            $table->string('p_street')->nullable();
            $table->string('p_area')->nullable();
            $table->string('p_landline')->nullable();
            $table->string('p_mobile_no')->nullable();
            $table->string('p_email')->nullable();
            $table->string('degree_title')->nullable();
            $table->string('institute')->nullable();
            $table->string('passing_date')->nullable();
            $table->string('degree_country')->nullable();
            $table->string('degree_city')->nullable();
            $table->string('last_designation')->nullable();
            $table->string('last_employer')->nullable();
            $table->string('last_emp_dept')->nullable();
            $table->date('last_emp_from')->nullable();
            $table->date('last_to')->nullable();
            $table->string('last_emp_contact')->nullable();
            $table->string('dependents_spouse_name')->nullable();
            $table->enum('d_gender',['male','female','other'])->default('male');
            $table->string('d_cnic_no')->nullable();
            $table->string('children_name')->nullable();
            $table->string('children_cnic')->nullable();
            $table->date('children_dob')->nullable();
            $table->string('children_contact')->nullable();
            $table->enum('children_gender',['male','female','other'])->default('other');
            $table->string('primary_emrgency_name')->nullable();
            $table->string('primary_emergency_relation')->nullable();
            $table->string('primary_emergency_telephone')->nullable();
            $table->string('primary_emergency_address')->nullable();
            $table->string('secondry_emrgency_name')->nullable();
            $table->string('secondry_emergency_relation')->nullable();
            $table->string('secondry_emergency_telephone')->nullable();
            $table->string('secondry_emergency_address')->nullable();
            $table->string('primary_benificiary_name')->nullable();
            $table->string('primary_benificiary_relation')->nullable();
            $table->string('primary_benificiary_telephone')->nullable();
            $table->string('primary_benificiary_address')->nullable();
            $table->string('primary_benificiary_cnic')->nullable();
            $table->string('primary_benificiary_email')->nullable();
            $table->string('secondry_benificiary_name')->nullable();
            $table->string('secondry_benificiary_relation')->nullable();
            $table->string('secondry_benificiary_telephone')->nullable();
            $table->string('secondry_benificiary_address')->nullable();
            $table->string('secondry_benificiary_cnic')->nullable();
            $table->string('secondry_benificiary_email')->nullable();
            $table->timestamps();



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('employees');
    }
}
