<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InstallmetnsAndDuedutionAmoun extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
 public function up()
    {
        //
        Schema::table('loan', function(Blueprint $table){
            $table->decimal('monthly_deduction_amount',8,2)->after('amount')->default(0);
            $table->integer('no_of_installments')->after('monthly_deduction_amount')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('loan', function(Blueprint $table){
            $table->dropColumn('monthly_deduction_amount');
            $table->dropColumn('no_of_installments');
        });

    }
}
