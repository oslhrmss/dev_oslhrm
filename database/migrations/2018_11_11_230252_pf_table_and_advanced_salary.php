<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PfTableAndAdvancedSalary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('advanced_salary', function(Blueprint $table){
            $table->increments('id');
            $table->integer('emp_id')->nullable();
            $table->decimal('amount',8,2);
            $table->foreign('emp_id')->references('emp_no')->on('employees')->onDelete('cascade')->onUpdate('cascade');
            $table->boolean('is_active')->default(1);
            $table->timestamps();
        });

        Schema::create('pf_slabs', function(Blueprint $table){
            $table->increments('id');
            $table->string('slab');
            $table->decimal('amount',8,2);
            $table->boolean('is_active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExist('advanced_salary');
        Schema::dropIfExist('pf_slabs');
    }
}
