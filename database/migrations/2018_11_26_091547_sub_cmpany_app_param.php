<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SubCmpanyAppParam extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('sub_company_app_param', function(Blueprint $table){
            $table->increments('id');
            $table->integer('sub_company_id');
            $table->string('config_key');
            $table->string('config_value');
            $table->string('config_detail');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('sub_company_app_param');
    }
}
