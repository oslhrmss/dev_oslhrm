<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LoanLedger2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('loan_ledger', function(Blueprint $table){
            $table->increments('id');
            $table->integer('loan_id')->unsigned()->nullable();
            $table->integer('deduction_amount');
            $table->string('month');
            $table->date('date');
            $table->integer('balance');
            $table->timestamps();
            // $table->foreign('loan_id')->references('id')->on('loan')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('loan_ledger');
    }
}
