<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatAttendanceLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('attendance_log', function(Blueprint $table){
            $table->increments('id');
            $table->integer('attendance_id')->unsigned()->nullable();
            $table->integer('emp_no')->nullable(1);
            $table->string('emp_name')->nullable(1);
            $table->date('date')->nullable(1);
            $table->string('day')->nullable(1);
            $table->string('month')->nullable(1);
            $table->string('time_in')->nullable(1);
            $table->string('time_out')->nullable(1);
            $table->string('m_time_in')->nullable(1);
            $table->string('m_time_out')->nullable(1);
            $table->integer('late')->nullable(1);
            $table->integer('absent')->nullable(1);
            $table->integer('f_halfday')->nullable(1);
            $table->integer('s_halfday')->nullable(1);
            $table->integer('early_going')->nullable(1);
            $table->integer('hours')->nullable(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('attendance_log');
    }
}
