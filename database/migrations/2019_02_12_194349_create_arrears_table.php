<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArrearsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('emp_arrears', function(Blueprint $table){
            $table->increments('id');
            $table->integer('emp_id')->nullable();
            $table->decimal('arrear_amount')->nullable();
            $table->string('month')->nullable();
            $table->string('year')->nullable();
            $table->text('reason')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('emp_arrears');
    }
}
