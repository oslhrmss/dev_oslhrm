<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttendAdjustmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('attendance_adjustment',function(Blueprint $table){
            $table->increments('id');
            $table->integer('atten_id')->nullable();
            $table->enum('adjustment_type',['system','manual'])->default('system');
            $table->time('time_in');
            $table->time('time_out');
            $table->date('date');
            $table->string('month')->nullable();
            $table->integer('adjusted_by')->nullable();
            $table->text('reason')->nullable();
            $table->timestamp('adjusted_at')->useCurrent();
            $table->boolean('is_active')->default(1);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('attendance_adjustment');
    }
}
