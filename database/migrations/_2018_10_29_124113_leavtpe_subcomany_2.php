<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LeavtpeSubcomany2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('sub_company_leave_type', function(Blueprint $table){
            $table->increments('id');    
            $table->integer('sub_company_id')->unsigned();    
            $table->integer('leave_type_id')->unsigned();    
            $table->integer('no_of_leaves');    
            $table->boolean('is_active')->default(1);    
            $table->timestamps();    
            $table->foreign('sub_company_id')->references('id')->on('sub_company')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('leave_type_id')->references('id')->on('leave_type')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('sub_company_leave_type');
    }
}
