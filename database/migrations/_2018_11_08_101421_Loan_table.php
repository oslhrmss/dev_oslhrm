<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LoanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('loan', function(Blueprint $table){
            $table->increments('id');
            $table->integer('emp_id')->nullable();
            $table->decimal('amount',8,2)->default(0);
            $table->text('reason')->nullable();
            $table->date('apply_at')->nullable();
            $table->boolean('is_approved')->default(0);
            $table->date('approved_at')->nullable();
            $table->integer('approved_by')->nullable();
            $table->timestamps();
            $table->foreign('emp_id')->references('emp_no')->on('employees')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('position', function(Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->boolean('is_active')->default(1);
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('loan');
        Schema::dropIfExists('position');
    }
}
