<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubCompanyIdFkApParamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('sub_company_app_param', function(Blueprint $table){
            $table->dropColumn('sub_company_id');
            // $table->integer('sub_company_id')->nullable()->unsigned();
            // $table->foreign('sub_company_id')->references('id')->on('sub_company')->onDelete('cascade')->onUpdate('cascade');
        });
        Schema::table('sub_company_app_param', function(Blueprint $table){
//            $table->dropColumn('sub_company_id');
            $table->integer('sub_company_id')->nullable()->unsigned();
            $table->foreign('sub_company_id')->references('id')->on('sub_company')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('sub_company_app_param', function(Blueprint $table){
            $table->dropForeign('sub_company_id');
            $table->integer('sub_company_id');
        });

    }
}
