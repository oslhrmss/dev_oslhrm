<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFkInasubcompamyApprpram extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('sub_company_app_param', function(Blueprint $table){
            $table->foreign('sub_company_id')->references('id')->on('sub_company')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('sub_company_app_param', function(Blueprint $table){
            $table->dropForeign('sub_company_id');
        });
    }
}
