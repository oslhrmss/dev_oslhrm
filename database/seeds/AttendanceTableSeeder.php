<?php

use Illuminate\Database\Seeder;
use App\Model\Attendance;

class AttendanceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        for($i=1; $i <=31; $i++)
        {
            Attendance::create([
                'emp_id' => 1,
                'time_in' => '09:00',
                'time_out' => '06:00',
                'date' => $i.'08-2018',
                'month'=>'August',

            ]);
        }

    }
}
