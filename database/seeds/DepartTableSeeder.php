<?php

use Illuminate\Database\Seeder;
use App\Model\Department;
class DepartTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $departments = [
            [
            'name' => 'Local Marketing',
            ],
            [
                'name' => 'Operations',
            ],
            [
                'name' => 'Tradeg',
            ],
            [
                'name' => 'Inventory',
            ],
            [
                'name' => 'Complaince and Legal',
            ],
            [
                'name' => 'M & R',
            ],
            [
                'name' => 'Financing',
            ],
            [
                'name' => 'Audit',
            ],
            [
                'name' => 'Administration',
            ],
            [
                'name' => 'HR',
            ],
            [
                'name' => 'MIS',
            ],
            [
                'name' => 'IT',
            ],
        
        ];

        foreach($departments as $key => $value)
        {
            Department::create($value);
        }
    }
}
