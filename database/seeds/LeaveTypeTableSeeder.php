<?php

use Illuminate\Database\Seeder;
use App\Model\LeaveType;
class LeaveTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $leave_type = [
            [
                'name'=>'Annual Leave Pakistan'
            ],
            [
                'name'=>'Annual Leave UAE'
            ],
            [
                'name'=>'Annual Leave Malaysia'
            ],
            [
                'name'=>'Annual Leave Farm'
            ],
            [
                'name'=>'Annual Leave'
            ],
            [
                'name'=>'Casual Leave Pakistan'
            ],
            [
                'name'=>'Sick Leave Pakistan'
            ],
            [
                'name'=>'Sick Leave UAE'
            ],
            [
                'name'=>'Sick Leave Malaysia'
            ],
            [
                'name'=>'Grievance Leaves'
            ],
            [
                'name'=>'Maternity Leaves'
            ],
            [
                'name'=>'Special Leaves'
            ],

        ];

        foreach($leave_type as $key => $value)
        {
            LeaveType::create($value);
        }
    }
}
