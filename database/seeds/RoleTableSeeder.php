<?php

use Illuminate\Database\Seeder;
use App\Model\Role;
class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $roles = [
            [
            'name' => 'admin',
            'display_name' => 'Admin',
            'description' => 'admin role'
            ],
            [
                'name' => 'hr-manager',
                'display_name' => 'HR Manager',
                'description' => 'this is HR manager'
            ],
            [
                'name' => 'employee',
                'display_name' => 'Employee',
                'description' => 'thsi is employee'
            ],
            [
                'name' => 'finance-manager',
                'display_name' => 'Finance Manager',
                'description' => 'finance-manager'
            ],
            
        ];
        foreach ($roles as $key => $value) {
        	Role::create($value);
        }
    }
}
