<?php

use Illuminate\Database\Seeder;
use App\Model\SubCompany;
class SubCompanyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $sub_company = [
            ['company_id'=> 1, 'name'=>'OSL Pakistan Head Office'],
            ['company_id'=>1,'name' => 'Pakistan Yards'],
            ['company_id'=>1,'name'=>'UAE Office'],
            ['company_id'=>2,'name'=>'Malaysia Office'],
            ['company_id'=>3,'name'=>'Farm'],
            ['company_id'=>3,'name'=>'Distribution Center'],
        ];
        foreach($sub_company as $key=>$value){ SubCompany::create($value); }
    }
}
