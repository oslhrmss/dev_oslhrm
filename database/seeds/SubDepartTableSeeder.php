<?php

use Illuminate\Database\Seeder;
use App\Model\SubDepartment;

class SubDepartTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $sub_depart = [

            [
                'depart_id' => 2,
                'name' =>'Import'
            ],
            [
                'depart_id' => 2,
                'name' =>'Export'
            ],
            [
                'depart_id' => 4,
                'name' =>'ISO Tank'
            ],
            [
                'depart_id' => 4,
                'name' =>'Dry'
            ],
            [
                'depart_id' => 4,
                'name' =>'OOG'
            ],
            [
                'depart_id' => 4,
                'name' =>'Int. Marketing'
            ],
            [
                'depart_id' => 4,
                'name' =>'Container Trading'
            ],
            [
                'depart_id' => 4,
                'name' =>'R & D'
            ],
            [
                'depart_id' => 6,
                'name' =>'Local Office Yard'
            ],
            [
                'depart_id' => 6,
                'name' =>'International Yard'
            ],
            [
                'depart_id' => 7,
                'name' =>'Agency'
            ],
            [
                'depart_id' => 12,
                'name' =>'Software Development'
            ],
            [
                'depart_id' => 12,
                'name' =>'Infrastructure'
            ],
        ];

        foreach($sub_depart as $key => $value)
        {
            SubDepartment::create($value);
        }
    }
}
