
var html ;
var data ;
var sr = 1;
var APP_URL = window.location.hostname;
function getEmployeeList(url)
{
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url : url,
        data : data
    }).done(function(data){
        html  = '<option value=" ">--Select Employee--</option>';
        $.each(data, function(key,value){
            html  += '<option value="'+value.id+'">'+value.first_name+' '+value.last_name+'</option>';
        });

        $("#employee").html(html);
    });
}



function getPayrollDetails(url)
{
    var form  =  $("#employeeFrm").serialize();
    $.ajax({
        type : 'POST',
        dataType: 'JSON',
        url: url,
        data : form
    }).done(function(data){
        html = '<h3>Salary breakup of September</h3><table class="table table-bordered"><tr><td>Salary</td><td>'+data.salary+'</td></tr><tr><td>Total Days</td><td>'+data.total_days+'</td></tr><tr><td>Working Days</td><td>'+data.working_days+'</td></tr><tr><td>Off Days</td><td>'+data.off_days+'</td></tr><tr><td>Present Days</td><td>'+data.present_days+'</td></tr><tr><td>Leaves</td><td>'+data.leaves+'</td></tr><tr><td><b>Gross Salary</b></td><td><b>'+data.gross_salary+'</b></td></tr></table>';
        $("#payroll_detail").html(html);
    })


}



function login(url)
{
    var form = $("#loginForm").serialize();
    $.ajax({
        type: "POST",
        dataType : "JSON",
        url :  url,
        data : form ,
        cache: false
    }).done(function(data){
        console.log(data.status);

        alert('sdasd');
    }).fail(function(data){
        if(data.status == 422){
            
            var errors = data.responseJSON;
            if(errors.password){
                $("#errors").html(errors.password).addClass("alert alert-danger");
            }
            if(errors.email){
                $("#errors").html(errors.email).addClass("alert alert-danger");
            }
        }
    });
}


function company(url)
{
   var html; 
   jQuery.ajax({
    dataType: 'JSON',
    type: 'GET',
    url : url,
    data: data
}).done(function(data){
    jQuery.each(data, function(key,value){
        html +='<option value="'+value.id+'">'+value.name+'</option>';

    });

    jQuery("#company").html(html);
});
}

function department(url)
{
    var html;
    jQuery.ajax({
        dataType: 'JSON',
        type: 'GET',
        url : url,
        data: data
    }).done(function(data){
        html ='<option value="">Select Department</option>';
        jQuery.each(data, function(key,value){
            html +='<option value="'+value.id+'">'+value.name+'</option>';

        });

        jQuery("#department").html(html);
    });
}


function subDepartment(url)
{
    var html;
    jQuery.ajax({
        dataType: 'JSON',
        type: 'GET',
        url : url,
        data: data
    }).done(function(data){
       // html ='<option value="">Select Sub Department</option>';
        if(data.length > 0){
        jQuery.each(data, function(key,value){
                html +='<option value="'+value.id+'">'+value.name+'</option>';
        });
        }else{
            html +='<option value=""></option>';
        }

        jQuery("#sub_department").html(html);
    });
}

function designation(url)
{
    var html;
    jQuery.ajax({
        dataType: 'JSON',
        type: 'GET',
        url : url,
        data: data
    }).done(function(data){
        jQuery.each(data, function(key,value){
            html +='<option value="'+value.id+'">'+value.name+'</option>';

        });

        jQuery("#designation").html(html);
    });
}


function getEmployees(url)
{
    var html;
    var sr = 1;
    jQuery.ajax({
        dataType: 'JSON',
        type: 'GET',
        url : url,
        data: data
    }).done(function(data){
        html ='<thead><tr><th>S #.</th><th>Employee ID</th><th>Name</th><th>Reporting Manager</th><th>Designation</th><th>Department</th><th>Company</th><th>Joining Date</th><th>Email</th><th>Phone</th><th>Status</th><th>Action</th></tr></thead><tbody>';
        jQuery.each(data, function(key,value){
            if(value.is_active == '1'){
                html +='<tr><td>'+ sr ++ +'</td><td>'+ value.emp_no +'</td><td>'+ value.name +'&nbsp;<span class="badge badge-info">('+value.emp_status+')</span></td><td>'+ value.reportmanager +'</td><td>'+ value.desig +'</td><td>'+value.dept+'</td><td>'+value.company+'</td><td>'+ value.date_of_hiring +'</td><td>'+ value.p_email +'</td><td>'+ value.mobile_no +'</td><td><div class="dropdown"><span class="badge badge-'+ statusBackground(value.is_active) +'">'+ statusValue(value.is_active) +'</span><div class="dropdown-content" id="'+ value.id +'"><a class="lins" href="javascript:void(0);">Inactive</a></div></div></td><td data-id="'+value.id+'"><a href="edit-employee?id='+value.id+'"><i class="fa fa-edit"></i></a></td></tr>';
            }
        });
        html += '</tbody>';
        jQuery("#example").html(html);
        jQuery('#example').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print','pageLength'
            ],
            destroy:true,
        });
    //     var table = jQuery('#bootstrap-data-table').DataTable({
    //     lengthChange: true,
    //     buttons: [ 'copy', 'excel', 'pdf'],
    //     destroy:true
    
    
    // });
   // jQuery('#bootstrap-data-table').DataTable({});
        // table.buttons().container()
        // .appendTo( '#dataTable_wrapper .col-md-6:eq(0)' );
    });

}

// ex employees 

function getExEmployees(url)
{
    var html;
    var sr = 1;
    jQuery.ajax({
        dataType: 'JSON',
        type: 'GET',
        url : url,
        data: data
    }).done(function(data){
        html ='<thead><tr><th>S #.</th><th>Employee ID</th><th>Name</th><th>Reporting Manager</th><th>Designation</th><th>Department</th><th>Company</th><th>Joining Date</th><th>Email</th><th>Phone</th><th>Status</th><th>Action</th></tr></thead><tbody>';
        jQuery.each(data, function(key,value){
            if(value.is_active == '0'){
                html +='<tr><td>'+ sr ++ +'</td><td>'+ value.emp_no +'</td><td>'+ value.name +'</td><td>'+ value.reportmanager +'</td><td>'+ value.desig +'</td><td>'+value.dept+'</td><td>'+value.company+'</td><td>'+ value.date_of_hiring +'</td><td>'+ value.p_email +'</td><td>'+ value.mobile_no +'</td><td><div class="dropdown"><span class="badge badge-'+ statusBackground(value.is_active) +'">'+ statusValue(value.is_active) +'</span><div class="dropdown-content" id="'+ value.id +'"><a class="lins" href="javascript:void(0);">Active</a></div></div></td><td id="'+value.id+'"><a href="#"><i class="fa fa-edit"></i></a></td></tr>';
            }
        });
        html += '</tbody>';
        jQuery("#example").html(html);
        jQuery('#example').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print','pageLength'
            ],
            destroy:true,
        });
    //     var table = jQuery('#bootstrap-data-table').DataTable({
    //     lengthChange: true,
    //     buttons: [ 'copy', 'excel', 'pdf'],
    //     destroy:true
    
    
    // });
   // jQuery('#bootstrap-data-table').DataTable({});
        // table.buttons().container()
        // .appendTo( '#dataTable_wrapper .col-md-6:eq(0)' );
    });

}



function employeeDropDown(url)
{
    var html;
    $.ajax({
        type : 'GET',
        dataType : 'JSON',
        url  : url,
        data : data
    }).done(function(data){
        $.each(data, function(key,value){
         if(value.is_active == 1){
            html += '<option value="'+value.emp_no+'">'+value.name+' ('+value.emp_no+')</option>';
        }
    });

        $(".empdrop").html(html);
    });
}


function employeeChosenDropDown(url)
{
   var html;
   $.ajax({
    type : 'GET',
    dataType : 'JSON',
    url  : url,
    data : data
}).done(function(data){
    $.each(data, function(key,value){
       if(value.is_active == 1){
            // html += '<option value="'+value.emp_no+'">'+value.name+' ('+value.emp_no+')</option>';
            $(".empdrop").append("<option value="+value.emp_no+">"+value.name+" ("+value.emp_no+") </option>");
        }
    });

    $(".empdrop").chosen({
       disable_search_threshold: 10,
       no_results_text: "Oops, nothing found!",
       width: "100%"
   });
});
}

function employeeAttendance(url)
{
      total_working_hrs =0;
      total_late = 0;
      total_absent = 0;
      total_eg = 0;
      fhd = 0;
      shd = 0;
    var form  =  $("#atten_form").serialize();
    //$("#view").html('<i class="fa fa-eye"></i>&nbsp;loading...');
    $(".loading").show();
    $.ajax({
        type : 'POST',
        dataType :'JSON',
        url: url,
        data : form
    }).done(function(data){

        
        $(".loading").hide();    

        html = '<thead><tr><th>Employee ID</th><th>Employee Name</th><th>Date </th><th>Day</th><th>Time In</th><th>Time Out</th><th>Modified Time In</th><th>Modified Time Out</th><th>Modified Type</th><th>Modified By</th><th>Hours</th><th>Reason</th><th class="noexport">Action</th></tr></thead><tbody>';
        $.each(data, function(keys, values){
            $.each(values, function($key, value){

//          var late =  (value.late == 1) ? 'style="color:red;font-weight:bold;"' : '';
            var late_text =  (value.late == 1) ? '<span style="color:red;"><small><strong> (Late)</strong></small></span>' : '';
            var firsthd = (value.f_halfday == 1) ? '<span style="color:red;"><small><strong> (Half Day)</strong></small></span>' : '';
            var secondhd = (value.s_halfday == 1) ? '<span style="color:red;"><small><strong> (Half Day)</strong></small></span>' : '';
            var earlygoing = (value.early_going == 1) ? '<span style="color:red;"><small><strong> (Early Going)</strong></small>' : '';
            var absent =  (value.time_in == "ABSENT" && value.time_out == "ABSENT") ? 'style="color:red;font-weight:bold;"' : '';
            var leave =  (value.time_in == "Casual Leave" && value.time_in == "Casual Leave") ? 'style="color:rgb(80, 130, 26);font-weight:bold;"' : (value.time_in == "Annual Leave" && value.time_in == "Annual Leave") ? 'style="color:rgba(236, 98, 0, 0.94);font-weight:bold;"' : (value.time_in == "Sick Leave" && value.time_in == "Sick Leave") ? 'style="color:#995cd0;font-weight:bold;"' : (value.time_in == "Payback Leave" && value.time_in == "Payback Leave") ? 'style="color:#2132f3;font-weight:bold;"' : '';
            var time_in  = (value.time_in  == '00:00:00') ? '--:--:--' : value.time_in;
            var time_out  = (value.time_out  == '00:00:00') ? '--:--:--' : value.time_out;
            total_working_hrs+= value.hours;
            total_late += value.late;
            total_eg += value.early_going;
            fhd += value.f_halfday;
            shd += value.s_halfday;
            total_absent =  value.total_absent;
           // modifyDisabel = (value.id == 0) ? 'disabled=disabled' : '';

            modifyBtn = (value.role_id == 3) ? '' : '<button type="button" id="modifyBtn" class="btn btn-primary btn-sm mb-1" data-toggle="modal" data-target="#modifyPopup" >Modify</button>';
            html += '<tr><td>'+value.empcode+'</td><td>'+value.empname+'</td><td>'+value.date+'</td><td>'+value.day+'</td><td '+ absent + leave +'>'+(time_in + late_text + firsthd)+'</td><td '+ absent + leave +'>'+(time_out + secondhd + earlygoing)+'</td><td>'+(value.mtimein )+'</td><td>'+(value.mtimeout)+'</td><td>'+(value.atype)+'</td><td>'+(value.adjustedby)+'</td><td>'+value.hours+'</td><td><a id="reasonspn" data-toggle="modal" data-target="#reasonPopup" style="cursor:pointer" title="'+ value.reason +'">Reason</a></td><td data-id='+value.id+' data-date='+value.date+' data-empcode='+value.empcode+'>'+modifyBtn+'</td></tr>';

        });
        
        });

        console.log(total_working_hrs);
        html += '<tr><th>Total Late</th><th>'+total_late+'</th><th>Total Absent</th><th>'+total_absent+'</th><th>Total First HD</th><th>'+fhd+'</th><th>Total Second HD</th><th>'+shd+'</th><th>Total Early Going</th><th>'+total_eg+'</th><th>Total Hours</th><th>'+total_working_hrs.toFixed()+'</th><th></th></tr>' ;

        html += '</tbody>';
        
        
        jQuery("#example").html(html);
        jQuery('#example').DataTable({
            dom: 'Bfrtip',
            pageLength:100,
            lengthMenu: [[50, 100, 150, -1], [50, 100, 150, "All"]],
            buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print','pageLength',
            {
                extend: 'excel',
                text: 'Export Search Results',
        exportOptions: {
            columns: 'th:not(:last-child)'
        }
            }

            ],
            destroy: true,

        });
        $("#view").html('<i class="fa fa-eye"></i>&nbsp;View');
    });
}


function statusBackground(status)
{
    var badge;
    switch(status)
    {
        case 1:
        badge = "success";
        break;

        case 0:
        badge = "danger";     
        break;
    }


    return badge;
}

function paidStatusValue(status)
{
    switch(status)
    {
        case 1:
        var badge = "Paid";
        break;

        case 0:
        var badge = "Not Paid";
        break;
    }

    return badge;
}

function leaveStatusBackground(status)
{
    var badge;
    switch(status)
    {
        case 1:
        badge = "success";
        break;

        case 2:
        badge = "danger";     
        break;

        case 0:
        badge = "warning";     
        break;

        case 3:
        badge = "danger";     
        break;
    }


    return badge;
}

function statusValue(status)
{
    switch(status)
    {
        case 1:
        var badge = "Active";
        break;

        case 0:
        var badge = "Inactive";
        break;
    }


    return badge;
}

function leaveStatusValue(status)
{
    switch(status)
    {
        case 1:
        var badge = "Approved";
        break;

        case 2:
        var badge = "Rejected";
        break;

        case 0:
        var badge = "Pending";
        break;

        case 3:
        var badge = "Cancelled";
        break;
    }


    return badge;
}

function employeeActiveInactive(url, status,url2,active)
{
    $.ajax({
        type : "GET",
        dataType : 'JSON',
        url : url+'?status='+status,
        data : data
    }).done(function(data){
        //$("#message").html('Status Updated').css('color','#0d6f0d','float','left').delay(3000).fadeOut(300);
        if(active == true){
            getEmployees(url2);
        }else{
            getExEmployees(url2);
        }

        // var table =  $("#dataTable").DataTable();
        //  table.draw(false);
    });
}




function getTimeInTimeOutValues(url, id, date, empcode)
{
    $.ajax({
        type : 'GET',
        dataType: 'JSON',
        url : url+'?id='+ id,
        data : data
    }).done(function(data){
     $("input[name=atten_id]").val(data.id);
     $("input[name=date]").val(date);
     $("input[name=empcode]").val(empcode);
     $("#time_in").val(data.time_in); 
     $("#time_out").val(data.time_out); 
 });
}


function modifyManualAttendance(url)
{
    var form = $("#modifyForm").serialize();
    $.ajax({
        type : 'POST',
        dataType: 'JSON',
        url : url,
        data : form
    }).done(function(data){
        alert('Modfied');
        $("#modifyForm")[0].reset();
        $("#modifyPopup").hide('modal');
        $("body").removeClass('modal-open');
        $(".modal-backdrop").remove();
        $("#view").click();
    }).fail(function(data){
        if(data.status == 422){
           var errors = data.responseJSON;
           if(errors.time_in){
            $("#time_in").css('border','1px solid #e62020').focus();
        }
        if(errors.time_out){
            $("#time_out").css('border','1px solid #e62020').focus();
        }
        if(errors.reason){
            $("#reason").css('border','1px solid #e62020').focus();
        }
    }
})
}

function getSubCompany(url){
    $.ajax({
        type: "GET",
        dataType : "JSON",
        url : url,
        data : data 
    }).done(function(data){
        html = '<option value="">--Select Company--</option>';
        $.each(data, function(key,value){
            html += '<option value="'+value.id+'">'+value.name+'</option>';
        })
        $(".subcomp").html(html);
    });
}

function getLeaveType(url){
    $.ajax({
        type: "GET",
        dataType : "JSON",
        url : url,
        data : data 
    }).done(function(data){
        html = '<option value="">--Select Type--</option>';
        $.each(data, function(key,value){
            html += '<option value="'+value.id+'">'+value.name+'</option>';
        })
        $(".leave_type").html(html);
    });
}


function getSubCompanyLeaveType(url){
    $.ajax({
        type: "GET",
        dataType : "JSON",
        url : url,
        data : data 
    }).done(function(data){
        html = '<option value="">--Select Type--</option>';
        $.each(data, function(key,value){
            html += '<option value="'+value.id+'">'+value.name+'</option>';
        })
        $(".sub_comp_leave_type").html(html);
    });
}

function getAllCompanyLeaves(url){
    var sr = 1;
    $.ajax({
        type: "GET",
        dataType : "JSON",
        url : url,
        data : data 
    }).done(function(data){
        html = '<thead><tr><th>S #.</th><th>Company</th><th>Leave Type</th><th>No Of Leaves</th><th>Action</th></tr></thead><tbody>';
        $.each(data, function(key,value){
            html += '<tr><td>'+sr ++ +'</td><td>'+value.company_name+'</td><td>'+value.leave_name+'</td><td>'+value.no_of_leaves+'</td><td data-id= '+value.rowid+'><a href="#" data-toggle="modal" data-target="#amountEditModal" id="amountEditBtn"><i class="fa fa-edit"></i></a></td></tr>';
        })
        html += '</tbody>';
        jQuery("#example").html(html);
        jQuery('#example').DataTable({
            destroy:true,
        });
    });
}


function assignCompanyLeave(url,url2)
{
    var  form = $("#company_leave").serialize();
    $.ajax({
        type: "POST",
        dataType : "JSON",
        url : url,
        data : form, 
    }).done(function(data){
        
        $("#messages").html('<strong>Succesfully Added</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>').addClass('alert alert-success');
        $("#company_leave")[0].reset();
        getAllCompanyLeaves(url2)
    }).fail(function(data){
        if(data.status == 422){
         $("#place_order").text('PLACE ORDER');	   
         var errors = data.responseJSON;
         errorsHtml = '<div class="alert alert-danger"><ul>';
         $.each( errors, function( key, value ) {
            errorsHtml += '<li>' + value + '</li>'; //showing only the first error.
        });
         errorsHtml += '</ul></div>';
         $( '#messages' ).html( errorsHtml );
         
     }else{
         return false;
     }
 });
}


function getLeaveBalance(url)
{

    $.ajax({
        type: "GET",
        dataType : "JSON",
        url : url,
        data : data, 
    }).done(function(data){
        html = '<thead><tr><th>Leave Type</th><th>Total Leaves</th><th>Availed Leaves</th><th>Deducted Leaves</th><th>Balance Leave</th></tr></thead><tbody>';
        $.each(data, function(key,value){
            html += '<tr><td>'+value.leavetype+'</td><td>'+value.total_leaves+'</td><td>'+value.availed+'</td><td>'+value.deducted_leave+'</td><td>'+value.balance+'</td></tr>';   
        });
        html += '</tbody>';
        $("#examples").html(html);
        jQuery('#examples').DataTable();
    });
}

function getLeaveBalanceH(url)
{

    $.ajax({
        type: "GET",
        dataType : "JSON",
        url : url,
        data : data, 
    }).done(function(data){
        html = '<thead><tr><th>Leave Type</th><th>Total Leaves</th><th>Availed Leaves</th><th>Deducted Leaves</th><th>Balance Leave</th></tr></thead><tbody>';
        $.each(data, function(key,value){
            html += '<tr><td>'+value.leavetype+'</td><td>'+value.total_leaves+'</td><td>'+value.availed+'</td><td>'+value.deducted_leave+'</td><td>'+value.balance+'</td></tr>';   
        });
        html += '</tbody>';
        $("#examples").html(html);
        jQuery('#examples').DataTable();
    });
}

// submit leave application
function leaveApplication(url)
{
    $(".loading").show();
    var  form = $("#leave_app_form").serialize();
    $.ajax({
        type: "POST",
        dataType : "JSON",
        url : url,
        data : form, 
    }).done(function(data){
        $("#messages").html('<strong>You have succesfully applied for leaves, please wait for approval.</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>').addClass('alert alert-success');
        $("#leave_app_form")[0].reset();
        $(".loading").hide();
        setTimeout(function(){
            location.reload();
        },1000);  
    }).fail(function(data){
        if(data.status == 422){
//            $("#place_order").text('PLACE ORDER');     
        $(".loading").hide();
            var errors = data.responseJSON;
            errorsHtml = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><ul>';
            $.each( errors, function( key, value ) {
            errorsHtml += '<li>' + value + '</li>'; //showing only the first error.
        });
            errorsHtml += '</ul></div>';
//           $( '#messages' ).html( errorsHtml );

}else if(data.status == 400)
{
    $(".loading").hide();
    var errors = data.responseJSON;
    errorsHtml = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><ul><li>'+errors+'</li></ul></div>';
}else{
    return false;
}

$( '#messages' ).html( errorsHtml );
$("#leave_app_form")[0].reset();
});

}

function myLeaves(url)
{
    var  form = $("#leave_balance").serialize();
    $.ajax({
        type: "GET",
        dataType : "JSON",
        url : url,
        data : data, 
    }).done(function(data){
        html = '<thead><tr><th>Employee Name</th><th>Emp Code</th><th>Annual Leaves</th><th>Casual Leaves</th><th>Medical Leaves</th></tr></thead><tbody>';
        $.each(data, function(key,value){
            html += '<tr><td>'+value.EmpName+'</td><td>'+value.EmpNo+'</td><td>'+value.annual_leaves+'</td><td>'+value.casual_leaves+'</td><td>'+value.medical_leaves+'</td></tr>';   
        });
        html += '</tbody>';
        $("#example").html(html);

    });       
}



function managerDropDown(url)
{
    $.ajax({
        type: "GET",
        dataType : "JSON",
        url : url,
        data : data 
    }).done(function(data){
        html = '<option value="">--Select Manager--</option>';
        $.each(data, function(key,value){
            html += '<option value="'+value.emp_no+'">'+value.name+'</option>';
        })
        $(".managersdrop").html(html);
    });
}




function getLeaveApprovals(url)
{

    $.ajax({

        type: "GET",
        dataType : "JSON",
        url : url,
        data : data, 
    }).done(function(data){
        html = '<thead><tr><th>Resource Name</th><th>Emp Code</th><th>Type</th><th>From</th><th>To</th><th>Days</th><th>Applied At</th><th>Status</th></tr></thead><tbody>';
        $.each(data, function(key,value){
            if(value.status == 1)
            {
//                var statusDrop = '<span class="badge badge-'+ leaveStatusBackground(value.status) +'">'+leaveStatusValue(value.status)+'</span>';     
                var statusDrop = '<div class="dropdown"><span class="badge badge-'+ leaveStatusBackground(value.status) +'">'+ leaveStatusValue(value.status) +'</span><div class="dropdown-content" id="'+value.rowid+'"><a href="javascript:void(0);" class="lins">Cancel</a></div></div>';

            }else if(value.status == 3)
            {
                var statusDrop = '<span class="badge badge-'+ leaveStatusBackground(value.status) +'">'+leaveStatusValue(value.status)+'</span>';
            }
            else{
                var statusDrop = '<div class="dropdown"><span class="badge badge-'+ leaveStatusBackground(value.status) +'">'+ leaveStatusValue(value.status) +'</span><div class="dropdown-content" id="'+value.rowid+'"><a class="lins" href="javascript:void(0);">Approve</a><a class="lins" href="javascript:void(0);">Reject</a><a class="lins" href="javascript:void(0);">Pending</a><a href="javascript:void(0);" class="lins">Cancel</a></div></div>';
            }              
            html += '<tr><td>'+value.empname+'</td><td>'+value.empcode+'</td><td>'+value.leavetype+'</td><td>'+value.from+'</td><td>'+value.to+'</td><td>'+value.days+'</td><td>'+value.appliedat+'</td><td>'+ statusDrop +'</td></tr>';   
        });
        html += '</tbody>';
        $("#example").html(html);
        jQuery('#example').DataTable({
            "order": [[ 7, 'desc' ]],
            destroy: true,

        } );
 
    });       
}




// is mei status text jayega or ye apni value change krelga is ko hum view mein use krenge
function approvalStatusValueReplace(statusText)
{
    switch(statusText)
    {
        case "Approve":
        var value = 1;
        break;

        case "Reject":
        var value = 2;
        break;

        case "Pending":
        var value = 0;
        break;

        case "Cancel":
        var value = 3;
        break;
    }


    return value;

}

// is mei status text jayega or ye apni value change krelga is ko hum view mein use krenge
function paidStatusValueReplace(statusText)
{
    switch(statusText)
    {
        case "Paid":
        var value = 1;
        break;

        case "Not Paid":
        var value = 0;
        break;
    }

    return value;
}



    function leaveApprovalAction(url,status)
    {
      $.ajax({
        type : "GET",
        dataType : 'JSON',
        url : url+'?status='+status,
        data : data
    }).done(function(data){
            //$("#message").html('Status Updated').css('color','#0d6f0d','float','left').delay(3000).fadeOut(300);
            //getEmployees(url2);
            alert(data);
            location.reload();
    });
}


// position drop down 


function positionDropDown(url)
{
    html = '';
    $.ajax({
        type: "GET",
        dataType : "JSON",
        url : url,
        data : data 
    }).done(function(data){
        html = '<option value="">--Select Position--</option>';
        $.each(data, function(key,value){
            html += '<option value="'+value.value+'">'+value.name+'</option>';
        })
        $(".positionsdrop").html(html);
    });
}   


function empTypeDropDown(url)
{
    html = '';
    $.ajax({
        type: "GET",
        dataType : "JSON",
        url : url,
        data : data 
    }).done(function(data){
        html = '<option value="">--Select Emp Type--</option>';
        $.each(data, function(key,value){
            html += '<option value="'+value.id+'">'+value.value+'</option>';
        })
        $(".emptypesdrop").html(html);
    });
}   


function editLeaveDuration(url)
{
        //$("#modifyForm").append('<input type="hidden" name="id" value="'+id+'" /> ');
        var  form = $("#modifyForm").serialize();
        $.ajax({
            type: "POST",
            dataType : "JSON",
            url : url,
            data : form, 
        }).done(function(data){
            $('#amountEditModal').find("#messages").html('<strong>Done.</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>').addClass('alert alert-success');
            $("#modifyForm")[0].reset();
            setTimeout(function(){
                location.reload();
            },2000);  
        }).fail(function(data){
            if(data.status == 422){
            // $("#place_order").text('PLACE ORDER');     
            var errors = data.responseJSON;
            
            errorsHtml = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><ul>';
            $.each( errors, function( key, value ) {
            errorsHtml += '<li>' + value + '</li>'; //showing only the first error.
        });
            errorsHtml += '</ul></div>';
            $('#amountEditModal').find( '#messages' ).html( errorsHtml );
            
        }else if(data.status == 400){
            var errors = data.responseJSON;
            errorsHtml = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><ul><li>'+errors+'</li></ul></div>';
            $('#amountEditModal').find( '#messages' ).html( errorsHtml );
            $("#modifyForm")[0].reset();
        }else{
            return false;
        }
    });

}

    function loanApplication(url)
    {
        var  form = $("#loan_application_form").serialize();
        $.ajax({
            type: "POST",
            dataType : "JSON",
            url : url,
            data : form, 
        }).done(function(data){
            $("#messages").html('<strong>You have succesfully applied for loan.</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>').addClass('alert alert-success');
            $("#loan_application_form")[0].reset();
            location.reload();    
        }).fail(function(data){
            if(data.status == 422){
            // $("#place_order").text('PLACE ORDER');     
            var errors = data.responseJSON;
            errorsHtml = '<div class="alert alert-danger"><ul>';
            $.each( errors, function( key, value ) {
            errorsHtml += '<li>' + value + '</li>'; //showing only the first error.
        });
            errorsHtml += '</ul></div>';
            $( '#messages' ).html( errorsHtml );
            
        }else if(data.status == 400){
            var errors = data.responseJSON;
            alert(errors);
            $("#loan_application_form")[0].reset();
        }else{
            return false;
        }
    });
        
    }



    function getLoans(url)
    {
        // var  form = $("#leave_balance").serialize();
        $.ajax({
            type: "GET",
            dataType : "JSON",
            url : url,
            data : data, 
        }).done(function(data){
            html = '<thead><tr><th>Employee Name</th><th>Loan Amount</th><th>Monthly Deduction Amount</th><th>Installments</th><th>Applied At</th><th>Approved By</th><th>Status</th><th>Paid</th><th>Paid At</th><th>Paid By</th><th>Reason</th><th>Action</th></tr></thead><tbody>';
            $.each(data, function(key,value){
                if(value.status == 0){ //|| value.status == 2 [edited]
                    var editBtn = '<a href="javascript:void(0)" id="amountEditBtn" data-toggle="modal" data-target="#amountEditModal"><i class="fa fa-edit"></i></a>';
                }else{
                    var editBtn = '';
                } 
                html += '<tr><td>'+value.EmpName+'</td><td>'+value.amount+'</td><td>'+value.monthlyamount+'</td><td>'+value.installments+'</td><td>'+value.apply_at+'</td><td>'+value.approvedby+'</td><td><span class="badge badge-'+ leaveStatusBackground(value.status) +'">'+ leaveStatusValue(value.status) +'</span></td><td><span class="badge badge-'+ statusBackground(value.paid) +'">'+ paidStatusValue(value.paid) +'</span></td><td>'+value.paid_at+'</td><td>'+value.paid_by+'</td><td>'+value.reason+'</td><td data-id="'+value.loan_id+'">'+editBtn+'</td></tr>';   
            });
            html += '</tbody>';
            $("#example").html(html);
            jQuery('#example').DataTable({
                dom: 'Bfrtip',
                buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print','pageLength'
                ],
                destroy: true,

            });
        });       
    }


    function getPfSlabs(url)
    {
        html = '';
        $.ajax({
            type: "GET",
            dataType : "JSON",
            url : url,
            data : data 
        }).done(function(data){
            html = '<option value="">--Select Slabs--</option>';
            $.each(data, function(key,value){
                html += '<option value="'+value.id+'">'+value.slab+' ('+value.amount+')</option>';
            })
            $(".pfslabs").html(html);
        });
    }

    function getRegions(url)
    {
        html = '';
        $.ajax({
            type: "GET",
            dataType : "JSON",
            url : url,
            data : data 
        }).done(function(data){
            html = '<option value="">--Select Regions--</option>';
            $.each(data, function(key,value){
                html += '<option value="'+value.id+'">'+value.name+'</option>';
            })
            $(".regiondrop").html(html);
        });
    }

    // applying advance salary
    function advanceSalary(url)
    {
        var  form = $("#adv_sal_form").serialize();
        $.ajax({
            type: "POST",
            dataType : "JSON",
            url : url,
            data : form, 
        }).done(function(data){
            $("#messages").html('<strong>You have succesfully applied for advance salary.</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>').addClass('alert alert-success');
            $("#adv_sal_form")[0].reset();
            setTimeout(function(){
                location.reload();
            },2000);  
        }).fail(function(data){
            if(data.status == 422){
            // $("#place_order").text('PLACE ORDER');     
            var errors = data.responseJSON;
            
            errorsHtml = '<div class="alert alert-danger"><ul>';
            $.each( errors, function( key, value ) {
            errorsHtml += '<li>' + value + '</li>'; //showing only the first error.
        });
            errorsHtml += '</ul></div>';
            $( '#messages' ).html( errorsHtml );
            
        }else if(data.status == 400){
            var errors = data.responseJSON;
            alert(errors);
            $("#adv_sal_form")[0].reset();
        }else{
            return false;
        }
    });
        
    }


    function getAdvSalary(url)
    {
        // var  form = $("#leave_balance").serialize();
        $.ajax({
            type: "GET",
            dataType : "JSON",
            url : url,
            data : data, 
        }).done(function(data){
            html = '<thead><tr><th>Employee Name</th><th>Adv. Amount</th><th>Applied At</th><th>Month</th><th>Approved By</th><th>Status</th><th>Paid Status</th><th>Paid At</th><th>Paid By</th><th>Reason</th><th>Action</th></tr></thead><tbody>';
            $.each(data, function(key,value){
             if(value.status == 0 || value.status == 2){
                var editBtn = '<a href="javascript:void(0)" id="amountEditBtn" data-toggle="modal" data-target="#amountEditModal"><i class="fa fa-edit"></i></a>';
            }else{
                var editBtn = '';
            }
            html += '<tr><td>'+value.EmpName+'</td><td>'+value.amount+'</td><td>'+value.apply_at+'</td><td>'+value.month+'</td><td>'+value.approvedby+'</td><td><span class="badge badge-'+ leaveStatusBackground(value.status) +'">'+ leaveStatusValue(value.status) +'</span></td><td><span class="badge badge-'+ statusBackground(value.is_paid) +'">'+ paidStatusValue(value.is_paid) +'</span></td><td>'+value.paid_at+'</td><td>'+value.paid_by+'</td><td>'+value.reason+'</td><td data-id="'+value.rowid+'">'+  editBtn +'</td></tr>';   
        });
            html += '</tbody>';
            $("#example").html(html);
            jQuery('#example').DataTable({
                dom: 'Bfrtip',
                buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print','pageLength'
                ],
                destroy: true,

            });
        });       
    }



    function getEmployeesResetPass(url)
    {
        var html;
        var sr = 1;
        jQuery.ajax({
            dataType: 'JSON',
            type: 'GET',
            url : url,
            data: data
        }).done(function(data){
            html ='<thead><tr><th>S #.</th><th>Employee ID</th><th>Name</th><th>Hiring Date</th><th>Reset Password</th></tr></thead><tbody>';
            jQuery.each(data, function(key,value){
                if(value.is_active == '1'){
                    html +='<tr><td>'+ sr ++ +'</td><td>'+ value.emp_no +'</td><td>'+ value.name +'</td><td>'+ value.date_of_hiring +'</td><td data-id="'+value.emp_no+'"><button id="reset-pass" class="btn btn-primary btn-sm">Reset Default</button></td></tr>';
                }
            });
            html += '</tbody>';
            jQuery("#example").html(html);
            jQuery('#example').DataTable({
                dom: 'Bfrtip',
                buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print','pageLength'
                ],
                destroy: true,
            });
    //     var table = jQuery('#bootstrap-data-table').DataTable({
    //     lengthChange: true,
    //     buttons: [ 'copy', 'excel', 'pdf'],
    //     destroy:true
    
    
    // });
   // jQuery('#bootstrap-data-table').DataTable({});
        // table.buttons().container()
        // .appendTo( '#dataTable_wrapper .col-md-6:eq(0)' );
    });

    }

    function resetDefaultPasswordPost(url,empCode)
    {
      $.ajax({
        type : "GET",
        dataType : 'JSON',
        url : url+'?emp_code='+empCode,
        data : data
    }).done(function(data){
        $("#messages").html('Password Reset Successfully!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>').addClass('alert alert-success');
            //getEmployees(url2);
            $('html, body').animate({
                scrollTop: 0
            }, 1000);
            setTimeout(function(){
                location.reload();
            },3000);  
        });
}

function getLoanApprovals(url)
{
    $.ajax({
        type: "GET",
        dataType : "JSON",
        url : url,
        data : data, 
    }).done(function(data){
        
        html = '<thead><tr><th>Resource Name</th><th>Emp Code</th><th>Loan Amount</th><th>Installments</th><th>Applied At</th><th>Reason</th><th>Status</th><th>Approved By</th><th>Paid Status</th></tr></thead><tbody>';
        $.each(data, function(key,value){
            if(value.is_manager == 1)
            {   
                if(value.paid_status == 0 || value.paid_status == 2)
                {
                    var statusDrop = '<div class="dropdown"><span class="badge badge-'+ leaveStatusBackground(value.status) +'">'+ leaveStatusValue(value.status) +'</span><div class="dropdown-content" id="'+value.rowid+'"><a class="lins" href="javascript:void(0);">Approve</a><a class="lins" href="javascript:void(0);">Reject</a></div></div>';                  
                }else{
                    var statusDrop = '<span class="badge badge-'+ leaveStatusBackground(value.status) +'">'+leaveStatusValue(value.status)+'</span>';                 
                }            
            }else{
                var statusDrop = '<span class="badge badge-'+ leaveStatusBackground(value.status) +'">'+leaveStatusValue(value.status)+'</span>';
            }
            if(value.is_finance == 1)
            {
                if(value.paid_status == 0)
                {
                    
                    var paidDrop = '<div class="dropdown"><span class="badge badge-'+ statusBackground(value.paid_status) +'">'+ paidStatusValue(value.paid_status) +'</span><div class="dropdown-content" id="'+value.rowid+'"><a class="lins-paid" href="javascript:void(0);">Paid</a><a class="lins-paid" href="javascript:void(0);">Not Paid</a></div></div>';
                }else{
                    var paidDrop = '<span class="badge badge-'+ statusBackground(value.paid_status) +'">'+paidStatusValue(value.paid_status)+'</span>';
                    
                }

            }
            else{
                var paidDrop = '<span class="badge badge-'+ statusBackground(value.paid_status) +'">'+paidStatusValue(value.paid_status)+'</span>';
            }


            if((value.is_finance == 1 && value.status == 1) || value.is_manager == 1){       
               html += '<tr><td>'+value.empname+'</td><td>'+value.empno+'</td><td>'+value.loan_amount+'</td><td>'+value.installments+'</td><td>'+value.applied_at+'</td><td>'+value.reason+'</td><td>'+statusDrop+'</td><td>'+value.approvedby+'</td><td>'+paidDrop+'</td></tr>';   
           }

//        html += '<tr><td>'+value.empname+'</td><td>'+value.empno+'</td><td>'+value.adv_amount+'</td><td>'+value.actual_salary+'</td><td>'+value.reason+'</td><td>' +(value.roleid == 4) ? '<div class="dropdown"><span class="badge badge-'+ leaveStatusBackground(value.status) +'">'+ leaveStatusValue(value.status) +'</span><div class="dropdown-content" id="'+value.rowid+'"><a class="lins" href="javascript:void(0);">Approve</a><a class="lins" href="javascript:void(0);">Reject</a><a class="lins" href="javascript:void(0);">Pending</a></div></div>': '<span class="badge badge-'+leaveStatusBackground(value.status)+'">'+leaveStatusValue(value.status)+'</span>' + '</td><td><span class="badge badge-'+ statusBackground(value.paid_status) +'">'+paidStatusValue(value.paid_status)+'</span></td><t></tr>';   
});
        html += '</tbody>';
        $("#example").html(html);
        jQuery('#example').DataTable({
            "order": [[ 6, 'desc' ]]
        });

    });
}
function loanApprovalAction(url,status)
{
  $.ajax({
    type : "GET",
    dataType : 'JSON',
    url : url+'?status='+status,
    data : data
}).done(function(data){
            //$("#message").html('Status Updated').css('color','#0d6f0d','float','left').delay(3000).fadeOut(300);
            //getEmployees(url2);
            alert("Success! ");
            location.reload();
        }).fail(function(data){
         if(data.status == 400){
            var errors = data.responseJSON;
            alert(errors);
            
        }else{
            return false;
        }
    });
    }

    function loanPaidAction(url,status)
    {
        $.ajax({
            type : "GET",
            dataType : 'JSON',
            url : url+'?status='+status,
            data : data
        }).done(function(data){
            //$("#message").html('Status Updated').css('color','#0d6f0d','float','left').delay(3000).fadeOut(300);
            //getEmployees(url2);
            alert("Success!");
            location.reload();
        }).fail(function(data){
         if(data.status == 400){
            var errors = data.responseJSON;
            alert(errors);
            
        }else{
            return false;
        }
    });
    }

    function editLoanAmount(url)
    {
        //$("#modifyForm").append('<input type="hidden" name="id" value="'+id+'" /> ');
        var  form = $("#modifyForm").serialize();
        $.ajax({
            type: "POST",
            dataType : "JSON",
            url : url,
            data : form, 
        }).done(function(data){
            $('#amountEditModal').find("#messages").html('<strong>Done.</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>').addClass('alert alert-success');
            $("#modifyForm")[0].reset();
            setTimeout(function(){
                location.reload();
            },2000);  
        }).fail(function(data){
            if(data.status == 422){
            // $("#place_order").text('PLACE ORDER');     
            var errors = data.responseJSON;
            
            errorsHtml = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><ul>';
            $.each( errors, function( key, value ) {
            errorsHtml += '<li>' + value + '</li>'; //showing only the first error.
        });
            errorsHtml += '</ul></div>';
            $('#amountEditModal').find( '#messages' ).html( errorsHtml );
            
        }else if(data.status == 400){
            var errors = data.responseJSON;
            alert(errors);
            $("#modifyForm")[0].reset();
        }else{
            return false;
        }
    });

    }

    function getAdvSalApprovals(url)
    {

        $.ajax({
            type: "GET",
            dataType : "JSON",
            url : url,
            data : data, 
        }).done(function(data){
            
            html = '<thead><tr><th>Resource Name</th><th>Emp Code</th><th>Adv. Amount</th><th>Applied At</th><th>Reason</th><th>Status</th><th>Approved By</th><th>Paid Status</th></tr></thead><tbody>';
            $.each(data, function(key,value){
                if(value.is_manager == 1)
                {   
                    if(value.paid_status == 0 || value.paid_status == 2)
                    {
                        var statusDrop = '<div class="dropdown"><span class="badge badge-'+ leaveStatusBackground(value.status) +'">'+ leaveStatusValue(value.status) +'</span><div class="dropdown-content" id="'+value.rowid+'"><a class="lins" href="javascript:void(0);">Approve</a><a class="lins" href="javascript:void(0);">Reject</a></div></div>';                  
                    }else{
                        var statusDrop = '<span class="badge badge-'+ leaveStatusBackground(value.status) +'">'+leaveStatusValue(value.status)+'</span>';                 
                    }            
                }else{
                    var statusDrop = '<span class="badge badge-'+ leaveStatusBackground(value.status) +'">'+leaveStatusValue(value.status)+'</span>';
                }
                if(value.is_finance == 1)
                {
                    if(value.paid_status == 0)
                    {
                        
                        var paidDrop = '<div class="dropdown"><span class="badge badge-'+ statusBackground(value.paid_status) +'">'+ paidStatusValue(value.paid_status) +'</span><div class="dropdown-content" id="'+value.rowid+'"><a class="lins-paid" href="javascript:void(0);">Paid</a><a class="lins-paid" href="javascript:void(0);">Not Paid</a></div></div>';
                    }else{
                        var paidDrop = '<span class="badge badge-'+ statusBackground(value.paid_status) +'">'+paidStatusValue(value.paid_status)+'</span>';
                        
                    }

                }else{
                    var paidDrop = '<span class="badge badge-'+ statusBackground(value.paid_status) +'">'+paidStatusValue(value.paid_status)+'</span>';
                }

                if((value.is_finance == 1 && value.status == 1) || value.is_manager == 1){               
                    html += '<tr><td>'+value.empname+'</td><td>'+value.empno+'</td><td>'+value.adv_amount+'</td><td>'+value.applied_at+'</td><td>'+value.reason+'</td><td>' + statusDrop +'</td><td>'+value.approvedby+'</td><td>'+paidDrop+'</td></tr>';   
                }


//        html += '<tr><td>'+value.empname+'</td><td>'+value.adv_amount+'</td><td>'+value.actual_salary+'</td><td>'+value.reason+'</td><td>' +(value.roleid == 4) ? '<div class="dropdown"><span class="badge badge-'+ leaveStatusBackground(value.status) +'">'+ leaveStatusValue(value.status) +'</span><div class="dropdown-content" id="'+value.rowid+'"><a class="lins" href="javascript:void(0);">Approve</a><a class="lins" href="javascript:void(0);">Reject</a><a class="lins" href="javascript:void(0);">Pending</a></div></div>': '<span class="badge badge-'+leaveStatusBackground(value.status)+'">'+leaveStatusValue(value.status)+'</span>' + '</td><td><span class="badge badge-'+ statusBackground(value.paid_status) +'">'+paidStatusValue(value.paid_status)+'</span></td><t></tr>';   
});
            html += '</tbody>';
            $("#example").html(html);
            jQuery('#example').DataTable({
                "order": [[ 5, 'desc' ]]
            });

        });       
    }


    function advSalApprovalAction(url,status)
    {
      $.ajax({
        type : "GET",
        dataType : 'JSON',
        url : url+'?status='+status,
        data : data
    }).done(function(data){
            //$("#message").html('Status Updated').css('color','#0d6f0d','float','left').delay(3000).fadeOut(300);
            //getEmployees(url2);
            alert("Success! ");
            location.reload();
        }).fail(function(data){
           if(data.status == 400){
            var errors = data.responseJSON;
            alert(errors);
            
        }else{
            return false;
        }
    });
    }

    function advSalPaidAction(url,status)
    {
        $.ajax({
            type : "GET",
            dataType : 'JSON',
            url : url+'?status='+status,
            data : data
        }).done(function(data){
            //$("#message").html('Status Updated').css('color','#0d6f0d','float','left').delay(3000).fadeOut(300);
            //getEmployees(url2);
            alert("Success!");
            location.reload();
        }).fail(function(data){
           if(data.status == 400){
            var errors = data.responseJSON;
            alert(errors);
            
        }else{
            return false;
        }
    });
    }


    function getAdvSalId(id)
    {

        $("#modifyForm").append('<input type="hidden" name="id" value="'+id+'" /> ');
        
    }

    function getleaveAppId(id,fr,t,rea)
    {

        $("#modifyForm").append('<input type="hidden" name="id" value="'+id+'" /> ');
        $("#modifyForm").append('<input type="hidden" class="date" name="fr" value="'+fr+'" /> ');
        $("#modifyForm").append('<input type="hidden" class="date" name="t" value="'+t+'" /> ');
        $("#modifyForm").append('<input type="hidden" name="rea" value="'+rea+'" /> ');
    }

    function editAdvSalAmount(url)
    {
        //$("#modifyForm").append('<input type="hidden" name="id" value="'+id+'" /> ');
        var  form = $("#modifyForm").serialize();
        $.ajax({
            type: "POST",
            dataType : "JSON",
            url : url,
            data : form, 
        }).done(function(data){
            $('#amountEditModal').find("#messages").html('<strong>Done.</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>').addClass('alert alert-success');
            $("#modifyForm")[0].reset();
            setTimeout(function(){
                location.reload();
            },2000);  
        }).fail(function(data){
            if(data.status == 422){
            // $("#place_order").text('PLACE ORDER');     
            var errors = data.responseJSON;
            
            errorsHtml = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><ul>';
            $.each( errors, function( key, value ) {
            errorsHtml += '<li>' + value + '</li>'; //showing only the first error.
        });
            errorsHtml += '</ul></div>';
            $('#amountEditModal').find( '#messages' ).html( errorsHtml );
            
        }else if(data.status == 400){
            var errors = data.responseJSON;
            errorsHtml = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><ul><li>'+errors+'</li></ul></div>';
            $('#amountEditModal').find( '#messages' ).html( errorsHtml );            
//            alert(errors);
$("#modifyForm")[0].reset();
}else{
    return false;
}
});

    }

    function getAdvSalaryList(url)
    {
        // var  form = $("#leave_balance").serialize();
        $.ajax({
        type: "GET",
        dataType : "JSON",
        url : url,
        data : data, 
    }).done(function(data){
        html = '<thead><tr><th>Employee Name</th><th>Employee Code</th><th>Adv. Amount</th><th>Applied At</th><th>Month</th><th>Status</th><th>Approved By</><th>Paid Status</th><th>Paid By</th><th>Reason</th></tr></thead><tbody>';
        $.each(data, function(key,value){
        html += '<tr><td>'+value.empname+'</td><td>'+value.emp_code+'</td><td>'+value.amount+'</td><td>'+value.apply_at+'</td><td>'+value.month+'</td><td><span class="badge badge-'+ leaveStatusBackground(value.status) +'">'+ leaveStatusValue(value.status) +'</span></td><td>'+value.approved_by+'</td><td><span class="badge badge-'+ statusBackground(value.paid) +'">'+ paidStatusValue(value.paid) +'</span></td><td>'+value.paid_by+'</td><td>'+value.reason+'</td></tr>';   
        });
        html += '</tbody>';
        $("#example").html(html);
        jQuery('#example').DataTable({
            dom: 'Bfrtip',
            buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print','pageLength'
            ],
            destroy: true,

        });
    });       
    }

    function getLoansList(url)
    {
        // loanlist
        // var  form = $("#leave_balance").serialize();
        $.ajax({
        type: "GET",
        dataType : "JSON",
        url : url,
        data : data, 
        }).done(function(data){
            html = '<thead><tr><th>Employee Name</th><th>Employee Code</th><th>Adv. Amount</th><th>Applied At</th><th>Month</th><th>Status</th><th>Paid Status</th><th>Reason</th></tr></thead><tbody>';
            $.each(data, function(key,value){
                html += '<tr><td>'+value.empname+'</td><td>'+value.emp_code+'</td><td>'+value.amount+'</td><td>'+value.apply_at+'</td><td>'+value.month+'</td><td><span class="badge badge-'+ leaveStatusBackground(value.status) +'">'+ leaveStatusValue(value.status) +'</span></td><td><span class="badge badge-'+ statusBackground(value.paid) +'">'+ paidStatusValue(value.paid) +'</span></td><td>'+value.reason+'</td></tr>';   
            });
            html += '</tbody>';
            $("#example").html(html);
            jQuery('#example').DataTable({
                dom: 'Bfrtip',
                buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print','pageLength'
                ],
                destroy: true,


            });
        });       
    }



    function getLoansList(url)
    {
        // loanlist
        // var  form = $("#leave_balance").serialize();
        $.ajax({
            type: "GET",
            dataType : "JSON",
            url : url,
            data : data, 
        }).done(function(data){
            html = '<thead><tr><th>Employee Name</th><th>Employee Code</th><th>Loan Amount</th><th>Installments</th><th>Applied At</th><th>Month</th><th>Status</th><th>Paid Status</th><th>Reason</th></tr></thead><tbody>';
            $.each(data, function(key,value){
                html += '<tr><td>'+value.empname+'</td><td>'+value.emp_code+'</td><td>'+value.amount+'</td><td>'+value.installments+'</td><td>'+value.apply_at+'</td><td>'+value.month+'</td><td><span class="badge badge-'+ leaveStatusBackground(value.status) +'">'+ leaveStatusValue(value.status) +'</span></td><td><span class="badge badge-'+ statusBackground(value.paid) +'">'+ paidStatusValue(value.paid) +'</span></td><td>'+value.reason+'</td></tr>';   
            });
            html += '</tbody>';
            $("#example").html(html);
            jQuery('#example').DataTable({
                dom: 'Bfrtip',
                buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print','pageLength'
                ],
                destroy: true,

            });
        });       
    }

    function  employeeListWhoseBalancNonZero(url)
    {
        html = '';
        var form = $("#loan_deduction").serialize();
        $.ajax({
            type: "POST",
            dataType : "JSON",
            url : url,
            data : form 
        }).done(function(data){
            html = '<thead><tr><th>Employee Name</th><th>Emp Code</th><th>Amount</th><th>Monthly Deduction Amt</th><th>Installments</th><th>Balance</th><th>Apply At</th><th>Month</th><th>Year</th><th>Relax</th><th>Action</th></tr></thead><tbody>';
            $.each(data, function(key,value){
                var editBtn = '<a href="javascript:void(0)" id="amountEditBtn" data-toggle="modal" data-target="#amountEditModal"><i class="fa fa-edit"></i></a>';
                var relaxUnrelax = (value.is_relaxed == 1) ? '<a href="javascript:void(0)" class="btn btn-danger btn-sm" id="remove_relax"><i class="fa fa-remove"></i>&nbsp;Remove</a>' : '<a href="javascript:void(0)" class="btn btn-primary btn-sm" id="relax"><i class="fa fa-plus"></i>&nbsp;Relax</a>';
                html += '<tr><td>'+value.name+'</td><td>'+value.emp_no+'</td><td>'+value.amount+'</td><td>'+value.monthly_deduction_amount+'</td><td>'+value.installments+'</td><td>'+value.balance+'</td><td>'+value.apply_at+'</td><td>'+value.month+'</td><td>'+value.year+'</td><td data-id="'+value.loan_id+'">'+relaxUnrelax+'</td><td data-id="'+value.loan_id+'">'+ editBtn +'</td></tr>';

            });
            $("#example").html(html);
            $('#example').DataTable({
                dom: 'Bfrtip',
                buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                paging: false,  
                destroy: true,


        });


  

            $("#deduction_btn").show();

        });
    }

    function loanDeduct(url)
    {
        $("#deduction_btn").html('<i class="fa fa-dot-circle-o"></i>&nbsp;loading...').prop('disabled',true);
        $.ajax({
            type: "GET",
            dataType : "JSON",
            url : url,
            data : data, 
        }).done(function(data){
            $("#messages").html('<strong>Deduction has been applied for this month.</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>').addClass('alert alert-success');
            $("#deduction_btn").html('<i class="fa fa-dot-circle-o"></i>&nbsp; Generate Deductions').prop('disabled',false);
            setTimeout(function(){
                location.reload();
            },2000); 

        }).fail(function(data){
           if(data.status == 400){
            var errors = data.responseJSON;
            alert(errors);
            location.reload();         
        }else{
            return false;
        }
    });
    }


    function loanRelaxation(url,url2)
    {
        $.ajax({
            type: "GET",
            dataType : "JSON",
            url : url,
            data : data, 
        }).done(function(data){
            employeeListWhoseBalancNonZero(url2);
        // alert('Relaxation has been saved.');
        // setTimeout(function(){
        //     location.reload();
        // },2000); 
    });   
    }

    function removeLoanRelaxation(url,url2)
    {
        $.ajax({
            type: "GET",
            dataType : "JSON",
            url : url,
            data : data, 
        }).done(function(data){
            employeeListWhoseBalancNonZero(url2);
        // alert('Relaxation has been removed.');
        // setTimeout(function(){
        //     location.reload();
        // },2000); 
    });   
    }



function hideModal(){
  $(".modal").removeData('modal');
  $(".modal").removeClass("in");
  $(".modal-backdrop").remove();
  $('body').removeClass('modal-open');
  $('body').css('padding-right', '');
  $(".modal").hide();
}


  function setDeductionAmountByFinance(url)
  {
    var  form = $("#modifyForm").serialize();

    $.ajax({
        type: "POST",
        dataType : "JSON",
        url : url,
        data : form, 
    }).done(function(data){
        $('#amountEditModal').find("#messages").html('<strong>Done.</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>').addClass('alert alert-success');
        $("#modifyForm")[0].reset();
        setTimeout(function(){
            location.reload();
            hideModal();
        },1000);  
    }).fail(function(data){
        if(data.status == 422){
            // $("#place_order").text('PLACE ORDER');     
            var errors = data.responseJSON;
            
            errorsHtml = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><ul>';
            $.each( errors, function( key, value ) {
            errorsHtml += '<li>' + value + '</li>'; //showing only the first error.
        });
            errorsHtml += '</ul></div>';
            $('#amountEditModal').find( '#messages' ).html( errorsHtml );
            
        }else if(data.status == 400){
            var errors = data.responseJSON;
            alert(errors);
            $("#modifyForm")[0].reset();
        }else{
            return false;
        }
    });

}


function employeeLoanLedger(url)
{
    html = '';
    $.ajax({
        type: "GET",
        dataType : "JSON",
        url : url,
        data : data 
    }).done(function(data){
        html = '<thead><tr><th>Name</th><th>Month</th><th>Date</th><th>Loan Amount</th><th>Deduction</th><th>Paid Amount</th><th>Balance</th></tr></thead><tbody>';
        $.each(data, function(key,value){
                //var editBtn = '<a href="javascript:void(0)" id="amountEditBtn" data-toggle="modal" data-target="#amountEditModal"><i class="fa fa-edit"></i></a>';
                //var relaxUnrelax = (value.is_relaxed == 1) ? '<a href="javascript:void(0)" class="btn btn-danger btn-sm" id="remove_relax"><i class="fa fa-remove"></i>&nbsp;Remove</a>' : '<a href="javascript:void(0)" class="btn btn-primary btn-sm" id="relax"><i class="fa fa-plus"></i>&nbsp;Relax</a>';
                html += '<tr><td>'+value.name+'</td><td>'+value.month+'</td><td>'+value.date+'</td><td>'+value.amount+'</td><td>'+value.deduction+'</td><td>'+value.paid_amount+'</td><td>'+value.balance+'</td></tr>';
            });
        $("#example").html(html);
        $('#example').DataTable({
            dom: 'Bfrtip',
            buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print','pageLength'
            ],
            destroy: true,

        });
    });
}

    function getPayrollAttendanceData(url)
    {
        $(".loading").show();
        var form  =  $("#atten_form").serialize();
        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            url: url,
            data: form, 
        }).done(function(data){
            $(".loading").hide();
            $("#messages").html('<strong>'+data.msg+'</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>').addClass('alert alert-success');
        });
    }




function employeeAllLeaves(url)
{

    $.ajax({
        type: "GET",
        dataType : "JSON",
        url : url,
        data : data, 
    }).done(function(data){
        html = '<thead><tr><th>Employee Name</th><th>Leave Type</th><th>Month</th><th>Date</th><th>From</th><th>To</th><th>Days</th><th>Status</th><th>Approved By</th><th>Approved At</th><th>Reason</th><th>Action</th></tr></thead><tbody>';
        $.each(data, function(key,value){
            if(value.status == 0 || value.status == 2){
                var editBtn = '<a href="#" id="amountEditBtn" data-toggle="modal" data-target="#amountEditModal"><i class="fa fa-edit"></i></a>';
            }else{
                var editBtn = '';
            }
            html += '<tr><td>'+value.name+'</td><td>'+value.leavetype+'</td><td>'+value.month+'</td><td>'+value.date+'</td><td>'+value.from+'</td><td>'+value.to+'</td><td>'+value.days+'</td><td><span class="badge badge-'+ leaveStatusBackground(value.status) +'">'+ leaveStatusValue(value.status) +'</span></td><td>'+value.approved_by+'</td><td>'+value.approved_at+'</td><td>'+value.reason+'</td><td data-id="'+value.rowid+'" data-leave-id="'+value.leave_id+'" data-leave-from="'+value.from+'" data-leave-to="'+value.to+'" data-leave-rea="'+value.reason+'">'+ editBtn +'</td></tr>';   
        });
        html += '</tbody>';
        $("#example").html(html);
        jQuery('#example').DataTable({
            dom: 'Bfrtip',
            buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print','pageLength'
            ],
            destroy: true,

        });
    });
}

    function leaveDeduction(url)
    {
         $(".loading").show();
        var form  =  $("#leave_deduction").serialize();
        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            url: url,
            data: form, 
        }).done(function(data){
            $(".loading").hide();
            $("#messages").html('<strong>'+data.msg+'</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>').addClass('alert alert-success');
            $("#leave_deduction")[0].reset();
            setTimeout(function(){
                location.reload();
            },1000);  
        });   
    }





function leaveDeduction(url)
{
   $(".loading").show();
   var form  =  $("#leave_deduction").serialize();
   $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: url,
    data: form, 
}).done(function(data){
    $(".loading").hide();
    $("#messages").html('<strong>'+data.msg+'</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>').addClass('alert alert-success');
    $("#leave_deduction")[0].reset();
    setTimeout(function(){
        location.reload();
    },1000);  
});   
}



function employeeLeaveLedger(url)
{
    html = '';
    $.ajax({
        type: "GET",
        dataType : "JSON",
        url : url,
        data : data 
    }).done(function(data){
        html = '<thead><tr><th>Name</th><th>Emp Code</th><th>Month</th><th>Date</th><th>Total Deduction</th></tr></thead><tbody>';
        $.each(data, function(key,value){
            html += '<tr><td>'+value.name+'</td><td>'+value.emp_code+'</td><td>'+value.month+'</td><td>'+value.date+'</td><td>'+value.total_deduction+'</td></tr>';
        });
        $("#example").html(html);
        $('#example').DataTable({

            dom: 'Bfrtip',
            buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print','pageLength'
            ],
            destroy: true,

        });
    });
}



    function editSubCompanyNoOfLeaves(url)
    {
    var  form = $("#modifyForm").serialize();
    $.ajax({
        type: "POST",
        dataType : "JSON",
        url : url,
        data : form, 
    }).done(function(data){
        $('#amountEditModal').find("#messages").html('<strong>Done.</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>').addClass('alert alert-success');
        $("#modifyForm")[0].reset();
        setTimeout(function(){
            location.reload();
        },2000);  
    }).fail(function(data){
        if(data.status == 422){
            // $("#place_order").text('PLACE ORDER');     
            var errors = data.responseJSON;
            
         errorsHtml = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><ul>';
         $.each( errors, function( key, value ) {
            errorsHtml += '<li>' + value + '</li>'; //showing only the first error.
        });
          errorsHtml += '</ul></div>';
           $('#amountEditModal').find( '#messages' ).html( errorsHtml );
           
        }else if(data.status == 400){
            var errors = data.responseJSON;
            errorsHtml = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><ul><li>'+errors+'</li></ul></div>';
            $('#amountEditModal').find( '#messages' ).html( errorsHtml );
            $("#modifyForm")[0].reset();
        }else{
            return false;
        }
    });
 
    }


function  generatePayroll(url)
{
    html = '';
    var form  =  $("#payrollform").serialize();
    $(".loading").show();
    $.ajax({
        type: "POST",
        dataType : "JSON",
        url : url,
        data : form 
    }).done(function(data){
        $(".loading").hide();
        html = '<thead><tr><th>Employee Name</th><th>Emp Code</th><th>Gross Sal.</th><th>Leaves Ded.</th><th>PF</th><th>Adv. Salary</th><th>Tax</th><th>EOBI</th><th>Loan Ded.</th><th>Item Deduction.</th><th>Fuel Amount.</th><th>Parking Amount.</th><th>Arrears</th><th>Net Sal.</th><th>Month</th><th>Date</th><th>Year</th></tr></thead><tbody>';
        $.each(data, function(key,value){
           var editBtn = '<a href="javascript:void(0)" id="amountEditBtn" data-toggle="modal" data-target="#amountEditModal"><i class="fa fa-edit"></i></a>';
                // var relaxUnrelax = (value.is_relaxed == 1) ? '<a href="javascript:void(0)" class="btn btn-danger btn-sm" id="remove_relax"><i class="fa fa-remove"></i>&nbsp;Remove</a>' : '<a href="javascript:void(0)" class="btn btn-primary btn-sm" id="relax"><i class="fa fa-plus"></i>&nbsp;Relax</a>';
                html += '<tr><td>'+value.emp_name+'</td><td>'+value.emp_id+'</td><td>'+value.gross_sal+'</td><td>'+value.leave_ded_amt+'</td><td>'+value.pf+'</td><td>'+value.adv_sal+'</td><td>'+value.tax+'</td><td>'+value.eobi+'</td><td>'+value.loan_ded+'</td><td>'+value.item_deduction+'</td><td>'+value.fuel_amount+'</td><td>'+value.parking_amount+'</td><td>'+value.arrear_amount+'</td><td>'+value.net_sal+'</td><td>'+value.month+'</td><td>'+value.payroll_date+'</td><td>'+value.year+'</td></tr>';
            });
        $("#example").html(html);
        $('#example').DataTable({
            dom: 'Bfrtip',
            pageLength:100,
            lengthMenu: [[50, 100, 150, -1], [50, 100, 150, "All"]],            
            buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print','pageLength'
            ],
            destroy: true,

        });
    });
}

function  getLoanSetOffHistory(url)
{
    html = '';
    var form  =  $("#set_off_form").serialize();
    $(".loading").show();
    $.ajax({
        type: "POST",
        dataType : "JSON",
        url : url,
        data : form 
    }).done(function(data){
        $(".loading").hide();
        $(".set-off-detail").show();
        $(".set-off-amt-frm").show();
        $("#emp_name").text(data.name);
        $("#emp_code").text(data.emp_no);
        $("#loan_amt").text(data.amount);
        $("#ded_amt").text(data.deduction_amount);
        $("#balance").text(data.balance);
        $("#applied_date").text(data.apply_at);
        $("#month").text(data.month);
        $(".set-off-amt-frm").find('input[name=loan_id]').val(data.id);
        $("#set_off_form")[0].reset();
    }).fail(function(data){
       $(".loading").hide();
       if(data.status == 422){
            // $("#place_order").text('PLACE ORDER');     
            var errors = data.responseJSON;
            errorsHtml = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><ul>';
            $.each( errors, function( key, value ) {
            errorsHtml += '<li>' + value + '</li>'; //showing only the first error.
        });
            errorsHtml += '</ul></div>';
            $(document).find( '#messages' ).html( errorsHtml );
            
        }else if(data.status == 400){

            var errors = data.responseJSON;
            $(document).find( '#messages' ).addClass('alert alert-danger').html(errors.msg);
            $("#set_off_form")[0].reset();
        }else{
            return false;
        }
    });

    }  

function  setSetOffLoanAmount(url)
{
    html = '';
    var form  =  $("#set_off_amt").serialize();
    $(".loading").show();
    $.ajax({
        type: "POST",
        dataType : "JSON",
        url : url,
        data : form 
    }).done(function(data){
        $(".loading").hide();
        $(".set-off-amt-frm").find('#messages').addClass("alert alert-success").html("Success.");
        $("#set_off_amt")[0].reset();
        setTimeout(function(){
            location.reload();
        },1000); 
    }).fail(function(data){
       $(".loading").hide();
       if(data.status == 422){

            // $("#place_order").text('PLACE ORDER');     
            var errors = data.responseJSON;
            errorsHtml = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><ul>';
            $.each( errors, function( key, value ) {
            errorsHtml += '<li>' + value + '</li>'; //showing only the first error.
        });
            errorsHtml += '</ul></div>';
            $('.set-off-amt-frm').find( '#messages' ).html( errorsHtml );
            
        }else if(data.status == 400){

            var errors = data.responseJSON;
            errorsHtml = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><ul><li>' + errors.msg + '</li></ul></div>';

            $('.set-off-amt-frm').find( '#messages' ).html(errorsHtml);
            $("#set_off_amt")[0].reset();
        }else{
            return false;
        }
    });
}


    function allEmployeeLeaveList(url)
    {
 
    $.ajax({
        type: "GET",
        dataType : "JSON",
        url : url,
        data : data, 
    }).done(function(data){
        html = '<thead><tr><th>Employee Name</th><th>Employee No.</th><th>Leave Type</th><th>Applied At</th><th>From</th><th>To</th><th>Days</th><th>Status</th><th>Approved By</th><th>Approved At</th></tr></thead><tbody>';
        $.each(data, function(key,value){
        html += '<tr><td>'+value.name+'</td><td>'+value.emp_no+'</td><td>'+value.leavetype+'</td><td>'+value.applied_at+'</td><td>'+value.from+'</td><td>'+value.to+'</td><td>'+value.days+'</td><td><span class="badge badge-'+ leaveStatusBackground(value.status) +'">'+ leaveStatusValue(value.status) +'</span></td><td>'+value.approved_by+'</td><td>'+value.approved_at+'</td></tr>';   
        });
        html += '</tbody>';
        $("#example").html(html);
        jQuery('#example').DataTable({});

    });
    
    }
///////////////
function ItemStatusBackground(status)
{
    var badge;
    switch(status)
    {
        case 'Assigned':
        badge = "success";
        break;

        case 'Recovered':
        badge = "primary";     
        break;

        case 'New':
        badge = "warning";     
        break;

        case 'Snatched':
        badge = "danger";     
        break;

        case 'Dead':
        badge = "danger";     
        break;

        case 'Sold':
        badge = "secondary";     
        break;
    }


    return badge;
}

function ItemStatusValue(eis)
{
    switch(eis)
    {
        case 'Assigned':
        var badge = "Assigned";
        break;

        case 'Recovered':
        var badge = "Recovered";
        break;

        case 'New':
        var badge = "New";
        break;

        case 'Snatched':
        var badge = "Snatched";
        break;

        case 'Dead':
        var badge = "Dead";
        break;

        case 'Sold':
        var badge = "Sold";
        break;
    }


    return badge;
}

// is mei status text jayega or ye apni value change krelga is ko hum view mein use krenge
function ItemStatusValueReplace(statusText)
{
    switch(statusText)
    {
        case 'Assigned':
        var value = "Assigned";
        break;

        case 'Recovered':
        var value = "Recovered";
        break;

        case 'New':
        var value = "New";
        break;

        case 'Snatched':
        var value = "Snatched";
        break;

        case 'Dead':
        var value = "Dead";
        break;

        case 'Sold':
        var value = "Sold";
        break;
    }


    return value;

}

function ItemAction(url,status)
    {
      $.ajax({
        type : "GET",
        dataType : 'JSON',
        url : url+'?status='+status,
        data : data
    }).done(function(data){
            //$("#message").html('Status Updated').css('color','#0d6f0d','float','left').delay(3000).fadeOut(300);
            //getEmployees(url2);
            alert("Success!");
            location.reload();
        }).fail(function(data){
           if(data.status == 400){
            var errors = data.responseJSON;
            alert(errors);
            
        }else{
            return false;
        }
    });
    }



    function getItemId(id,itemid)
    {

        $("#modifyItemForm").append('<input type="hidden" name="id" value="'+id+'" /> ');
        $("#modifyItemForm").append('<input type="hidden" name="itemid" value="'+itemid+'" /> ');
        
    }

    function getItemRemarkId(id,itemid)
    {

        $("#modifyRemarkItemForm").append('<input type="hidden" name="id" value="'+id+'" /> ');
        $("#modifyRemarkItemForm").append('<input type="hidden" name="itemid" value="'+itemid+'" /> ');
        
    }


// function getItems(url)
// {
//     html = '';
//     $.ajax({
//         type: "GET",
//         dataType : "JSON",
//         url : url,
//         data : data 
//     }).done(function(data){
//         html = '<thead><tr><th>Item Name</th><th>Type</th><th>Brand</th><th>Model</th><th>Serial#</th><th>Purchase Date</th><th>Warranty Date</th><th>Cost</th><th>Vendor</th><th>Deduction Amount</th><th>Monthly Deduction</th><th>Status</th></tr></thead><tbody>';
//         $.each(data, function(key,value){

//             if(value.status == 'Assigned')
//             {
// //                var statusDrop = '<span class="badge badge-'+ ItemStatusBackground(value.status) +'">'+ItemStatusValue(value.status)+'</span>';     
//                 var statusDrop = '<div class="dropdown"><span class="badge badge-'+ ItemStatusBackground(value.status) +'">'+ ItemStatusValue(value.status) +'</span><div class="dropdown-content" id="'+value.id+'"><a href="javascript:void(0);" class="lins">Recovered</a><a href="javascript:void(0);" class="lins">Snatched</a><a href="javascript:void(0);" class="lins">Dead</a></div></div>';

//             }else if(value.status == 'Snatched')
//             {
//                 var statusDrop = '<span class="badge badge-'+ ItemStatusBackground(value.status) +'">'+ItemStatusValue(value.status)+'</span>';
            
//             }else if(value.status == 'Dead')
//             {
//                 var statusDrop = '<span class="badge badge-'+ ItemStatusBackground(value.status) +'">'+ItemStatusValue(value.status)+'</span>';
            
//             }else if(value.status == 'Recovered')
//             {
//                 var statusDrop = '<span class="badge badge-'+ ItemStatusBackground(value.status) +'">'+ItemStatusValue(value.status)+'</span>';
//             }
//             else{
//                 // var statusDrop = '<div class="dropdown"><span class="badge badge-'+ ItemStatusBackground(value.status) +'">'+ ItemStatusValue(value.status) +'</span><div class="dropdown-content" id="'+value.rowid+'"><a class="lins" href="javascript:void(0);">Approve</a><a class="lins" href="javascript:void(0);">Reject</a><a class="lins" href="javascript:void(0);">Pending</a><a href="javascript:void(0);" class="lins">Cancel</a></div></div>';
//                 var statusDrop = '<span class="badge badge-'+ ItemStatusBackground(value.status) +'">'+ItemStatusValue(value.status)+'</span>';
//             } 

//             html += '<tr><td>'+value.name+'</td><td>'+value.in+'</td><td>'+value.brand+'</td><td>'+value.model+'</td><td>'+value.imei_no+'</td><td>'+value.purchase_date+'</td><td>'+value.warranty_date+'</td><td>'+value.cost+'</td><td>'+value.vn+'</td><td>'+value.deduction_amt+'</td><td>'+value.monthly_deduction_amt+'</td><td>'+statusDrop+'</td></tr>';
//         });
//         $("#example").html(html);
//         $('#example').DataTable({
//             dom: 'Bfrtip',
//             buttons: [
//             'copy', 'csv', 'excel', 'pdf', 'print','pageLength'
//             ],
//             destroy: true,

//         });
//     });
// }
function VendorDropDown(url)
{
    var html;
   $.ajax({
    type : 'GET',
    dataType : 'JSON',
    url  : url,
    data : data
}).done(function(data){
    $.each(data, function(key,value){
       
        $(".empdrop").append("<option value="+value.vendor_name+">"+value.vendor_name+"</option>");
    });

    $(".vendrop").chosen({
       disable_search_threshold: 10,
       no_results_text: "Oops, nothing found!",
       width: "100%"
   });
});
}

function getVendors(url)
 {
        $.ajax({
            type: "GET",
            dataType : "JSON",
            url : url,
            data : data, 
        }).done(function(data){
            html = '<thead><tr><th>Vendor Name</th><th>Contact Person</th><th>Contact #</th></tr></thead><tbody>';
            $.each(data, function(key,value){
                html += '<tr><td>'+value.vendor_name+'</td><td>'+value.contact_person+'</td><td>'+value.vendor_phone+'</td></tr>';   
            });
            html += '</tbody>';
            $("#example").html(html);
            jQuery('#example').DataTable({
            dom: 'Bfrtip',
            buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print','pageLength'
            ],
            destroy: true,
            });

        });
 }

function getItems(url)
{
    html = '';
    $.ajax({
        type: "GET",
        dataType : "JSON",
        url : url,
        data : data 
    }).done(function(data){
        html = '<thead><tr><th>Item Id</th><th>Item Name</th><th>Assign To</th><th>Type</th><th>Brand</th><th>Model</th><th>IME/SR/REG</th><th>Purchase Date</th><th>Warranty Date</th><th>Cost</th><th>Vendor</th><th>Deduction Amount</th><th>Monthly Deduction</th><th>Status</th><th>Action</th></tr></thead><tbody>';
        $.each(data, function(key,value){

            if(value.eis == 'Assigned')
            {
//                var statusDrop = '<span class="badge badge-'+ ItemStatusBackground(value.status) +'">'+ItemStatusValue(value.status)+'</span>';     
                var updateBtn = '&nbsp;&nbsp;<a href="edit-item?id='+value.item_id+'"><i class="fa fa-edit"></i></a>';
                var statusDrop = '<div class="dropdown"><span class="badge badge-'+ ItemStatusBackground(value.eis) +'">'+ ItemStatusValue(value.eis) +'</span><div class="dropdown-content" id="'+value.item_id+'"><a href="javascript:void(0);" class="lins">Recovered</a><a href="javascript:void(0);" class="lins">Snatched</a><a href="javascript:void(0);" class="lins">Dead</a></div></div>';
                var assignBtn = '';
                var remarkBtn = '';
            }else if(value.eis == 'Snatched')
            {
                var statusDrop = '<span class="badge badge-'+ ItemStatusBackground(value.eis) +'">'+ItemStatusValue(value.eis)+'</span>';
                var assignBtn = '';
                var updateBtn ='';
                var remarkBtn = '';
            }else if(value.eis == 'Dead')
            {
                var statusDrop = '<span class="badge badge-'+ ItemStatusBackground(value.eis) +'">'+ItemStatusValue(value.eis)+'</span>';
                var assignBtn = '';
                var updateBtn ='';
                var remarkBtn = '';
            }else if(value.eis == 'Sold')
            {
                var statusDrop = '<span class="badge badge-'+ ItemStatusBackground(value.eis) +'">'+ItemStatusValue(value.eis)+'</span>';
                var assignBtn = '';
                var updateBtn ='';
                var remarkBtn = '';
            }
            else if(value.eis == 'Recovered')
            {   
                var statusDrop = '<div class="dropdown"><span class="badge badge-'+ ItemStatusBackground(value.eis) +'">'+ItemStatusValue(value.eis)+'</span><div class="dropdown-content" id="'+value.item_id+'"><a href="javascript:void(0);" class="lins">Sold</a></div>';
                var assignBtn = '<a href="#" id="itemEditBtn" data-toggle="modal" data-target="#itemEditModal"><i class="fa fa-user-o" title="Assign Item"></i></a>&nbsp;';
                var updateBtn ='';
                var remarkBtn = '&nbsp;&nbsp;<a href="#" id="itemremarkBtn" data-toggle="modal" data-target="#itemremarkModal"><i class="fa fa-sticky-note-o" title="Add Remarks"></i></a>';
            }
            else{
                // var statusDrop = '<div class="dropdown"><span class="badge badge-'+ ItemStatusBackground(value.status) +'">'+ ItemStatusValue(value.status) +'</span><div class="dropdown-content" id="'+value.rowid+'"><a class="lins" href="javascript:void(0);">Approve</a><a class="lins" href="javascript:void(0);">Reject</a><a class="lins" href="javascript:void(0);">Pending</a><a href="javascript:void(0);" class="lins">Cancel</a></div></div>';
                var assignBtn = '<a href="#" id="itemEditBtn" data-toggle="modal" data-target="#itemEditModal"><i class="fa fa-user-o"></i></a>&nbsp;';
                var updateBtn = '&nbsp;&nbsp;<a href="edit-item?id='+value.item_id+'" title="Edit Item"><i class="fa fa-edit"></i></a>';
                var statusDrop = '<span class="badge badge-'+ ItemStatusBackground(value.eis) +'">'+ItemStatusValue(value.eis)+'</span>';
                var remarkBtn = '';
            } 

            html += '<tr><td>'+value.item_id+'</td><td>'+value.name+'</td><td>'+value.empname+'</td><td>'+value.in+'</td><td>'+value.brand+'</td><td>'+value.model+'</td><td>'+value.imei_no+'</td><td>'+value.purchase_date+'</td><td>'+value.warranty_date+'</td><td>'+value.cost+'</td><td>'+value.vn+'</td><td>'+value.deductamt+'</td><td>'+value.m_deduct_amt+'</td><td>'+statusDrop+'</td><td data-id="'+value.employee_item_id+'" data-itemid="'+value.item_id+'" >'+  assignBtn + updateBtn + remarkBtn +'</td></tr>'; //<td data-id="'+value.id+'">'+ updateBtn +'</td>
        });
        $("#example").html(html);
        $('#example').DataTable({
            dom: 'Bfrtip',
            buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print','pageLength'
            ],
            destroy: true,

        });
    });
}

function assignItemToEmp (url)
{
        //$("#modifyForm").append('<input type="hidden" name="id" value="'+id+'" /> ');
        var  form = $("#modifyItemForm").serialize();
        $.ajax({
            type: "POST",
            dataType : "JSON",
            url : url,
            data : form, 
        }).done(function(data){
            $('#itemEditModal').find("#messages").html('<strong>&#10004;&nbsp;Assigned successfully!</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>').addClass('alert alert-success');
            $("#modifyItemForm")[0].reset();
            setTimeout(function(){
                location.reload();
            },2000);  
        }).fail(function(data){
            if(data.status == 422){
            // $("#place_order").text('PLACE ORDER');     
            var errors = data.responseJSON;
            
            errorsHtml = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><ul>';
            $.each( errors, function( key, value ) {
            errorsHtml += '<li>' + value + '</li>'; //showing only the first error.
        });
            errorsHtml += '</ul></div>';
            $('#itemEditModal').find( '#messages' ).html( errorsHtml );
            
        }else if(data.status == 400){
            var errors = data.responseJSON;
            errorsHtml = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><ul><li>'+errors+'</li></ul></div>';
            $('#itemEditModal').find( '#messages' ).html( errorsHtml );
            $("#modifyItemForm")[0].reset();
        }else{
            return false;
        }
    });

}

function storeItem(url)
{
    $(".loading").show();
    var form  =  $("#add_item_form").serialize();
    $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: url,
        data: form, 
    }).done(function(data){
        $(".loading").hide();
        $("#messages").html('<strong>Success</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>').addClass('alert alert-success');
        $("#add_item_form")[0].reset();
        setTimeout(function(){
            location.reload();
        },1000);  
    }).fail(function(data){
       $(".loading").hide();
       if(data.status == 422){
            // $("#place_order").text('PLACE ORDER');     
            var errors = data.responseJSON;
            errorsHtml = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><ul>';
            $.each( errors, function( key, value ) {
            errorsHtml += '<li>' + value + '</li>'; //showing only the first error.
        });
            errorsHtml += '</ul></div>';
            $( '#messages' ).html( errorsHtml );
            
        }else if(data.status == 400){

            var errors = data.responseJSON;
            errorsHtml = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><ul><li>' + errors.msg + '</li></ul></div>';

            $('.set-off-amt-frm').find( '#messages' ).html(errorsHtml);
            $("#set_off_amt")[0].reset();
        }else{
            return false;
        }
    });   
}

function storeVendor(url)
{
    $(".loading").show();
    var  form = $("#add_vendor_form").serialize();
    $.ajax({
        type: "POST",
        dataType : "JSON",
        url : url,
        data : form, 
    }).done(function(data){
        $("#messages").html('<strong>&#10004;&nbsp; Vendor has been added successfully!</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>').addClass('alert alert-success');
        $("#add_vendor_form")[0].reset();
        $(".loading").hide();
        setTimeout(function(){
            location.reload();
        },1000);  
    }).fail(function(data){
        if(data.status == 422){
        $(".loading").hide();
            var errors = data.responseJSON;
            errorsHtml = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><ul>';
            $.each( errors, function( key, value ) {
            errorsHtml += '<li>' + value + '</li>'; //showing only the first error.
        });
            errorsHtml += '</ul></div>';
//           $( '#messages' ).html( errorsHtml );

}else if(data.status == 400)
{
    $(".loading").hide();
    var errors = data.responseJSON;
    errorsHtml = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><ul><li>'+errors+'</li></ul></div>';
}else{
    return false;
}

$( '#messages' ).html( errorsHtml );
$("#add_vendor_form")[0].reset();
});

}

function getListOfReport_1(url)
 {
        $.ajax({
            type: "GET",
            dataType : "JSON",
            url : url,
            data : data, 
        }).done(function(data){
            html = '<thead><tr><th>Item Id</th><th>Item Name</th><th>Assign To</th><th>Type</th><th>Brand</th><th>Model</th><th>IME/SR/REG</th><th>Purchase Date</th><th>Warranty Date</th><th>Cost</th><th>Vendor</th><th>Deduction Amount</th><th>Monthly Deduction</th><th>Status</th></tr></thead><tbody>';
            $.each(data, function(key,value){
                if(value.eis == 'Assigned')
            {
//                var statusDrop = '<span class="badge badge-'+ ItemStatusBackground(value.status) +'">'+ItemStatusValue(value.status)+'</span>';     
                var statusDrop = '<div class="dropdown"><span class="badge badge-'+ ItemStatusBackground(value.eis) +'">'+ ItemStatusValue(value.eis) +'</span><div class="dropdown-content" id="'+value.id+'"><a href="javascript:void(0);" class="lins">Recovered</a><a href="javascript:void(0);" class="lins">Snatched</a><a href="javascript:void(0);" class="lins">Dead</a></div></div>';
                var editBtn = '';
            }else if(value.eis == 'Snatched')
            {
                var statusDrop = '<span class="badge badge-'+ ItemStatusBackground(value.eis) +'">'+ItemStatusValue(value.eis)+'</span>';
                var editBtn = '';
            }else if(value.eis == 'Dead')
            {
                var statusDrop = '<span class="badge badge-'+ ItemStatusBackground(value.eis) +'">'+ItemStatusValue(value.eis)+'</span>';
                var editBtn = '';
            }else if(value.eis == 'Recovered')
            {   
                var editBtn = '<a href="#" id="itemEditBtn" data-toggle="modal" data-target="#itemEditModal"><i class="fa fa-edit"></i></a>';
                var statusDrop = '<span class="badge badge-'+ ItemStatusBackground(value.eis) +'">'+ItemStatusValue(value.eis)+'</span>';
            }
            else{
                var editBtn = '<a href="#" id="itemEditBtn" data-toggle="modal" data-target="#itemEditModal"><i class="fa fa-edit"></i></a>';
                var statusDrop = '<span class="badge badge-'+ ItemStatusBackground(value.eis) +'">'+ItemStatusValue(value.eis)+'</span>';
            } 
                html += '<tr><td>'+value.id+'</td><td>'+value.name+'</td><td>'+value.empname+'</td><td>'+value.in+'</td><td>'+value.brand+'</td><td>'+value.model+'</td><td>'+value.imei_no+'</td><td>'+value.purchase_date+'</td><td>'+value.warranty_date+'</td><td>'+value.cost+'</td><td>'+value.vn+'</td><td>'+value.deduction_amt+'</td><td>'+value.monthly_deduction_amt+'</td><td>'+statusDrop+'</td></tr>';   
            });
            html += '</tbody>';
            $("#example").html(html);
            jQuery('#example').DataTable({
            dom: 'Bfrtip',
            buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print','pageLength'
            ],
            destroy: true,
            });

        });
 }


function getListOfReport_2(url)
 {
        $.ajax({
            type: "GET",
            dataType : "JSON",
            url : url,
            data : data, 
        }).done(function(data){
            html = '<thead><tr><th>Item Id</th><th>Item Name</th><th>Assign To</th><th>Type</th><th>Brand</th><th>Model</th><th>IME/SR/REG</th><th>Purchase Date</th><th>Warranty Date</th><th>Cost</th><th>Vendor</th><th>Deduction Amount</th><th>Monthly Deduction</th><th>Status</th><th>Status Date</th><th>Remarks</th></tr></thead><tbody>';
            $.each(data, function(key,value){
                if(value.eis == 'Assigned')
            {
//                var statusDrop = '<span class="badge badge-'+ ItemStatusBackground(value.status) +'">'+ItemStatusValue(value.status)+'</span>';     
                var statusDrop = '<div class="dropdown"><span class="badge badge-'+ ItemStatusBackground(value.eis) +'">'+ ItemStatusValue(value.eis) +'</span></div>';
                var editBtn = '';
            }else if(value.eis == 'Snatched')
            {
                var statusDrop = '<span class="badge badge-'+ ItemStatusBackground(value.eis) +'">'+ItemStatusValue(value.eis)+'</span>';
                var editBtn = '';
            }else if(value.eis == 'Dead')
            {
                var statusDrop = '<span class="badge badge-'+ ItemStatusBackground(value.eis) +'">'+ItemStatusValue(value.eis)+'</span>';
                var editBtn = '';
            }else if(value.eis == 'Recovered')
            {   
                var editBtn = '<a href="#" id="itemEditBtn" data-toggle="modal" data-target="#itemEditModal"><i class="fa fa-edit"></i></a>';
                var statusDrop = '<span class="badge badge-'+ ItemStatusBackground(value.eis) +'">'+ItemStatusValue(value.eis)+'</span>';
            }
            else{
                var editBtn = '<a href="#" id="itemEditBtn" data-toggle="modal" data-target="#itemEditModal"><i class="fa fa-edit"></i></a>';
                var statusDrop = '<span class="badge badge-'+ ItemStatusBackground(value.eis) +'">'+ItemStatusValue(value.eis)+'</span>';
            } 
                html += '<tr><td>'+value.id+'</td><td>'+value.name+'</td><td>'+value.empname+'</td><td>'+value.in+'</td><td>'+value.brand+'</td><td>'+value.model+'</td><td>'+value.imei_no+'</td><td>'+value.purchase_date+'</td><td>'+value.warranty_date+'</td><td>'+value.cost+'</td><td>'+value.vn+'</td><td>'+value.deduction_amt+'</td><td>'+value.monthly_deduction_amt+'</td><td>'+statusDrop+'</td><td>'+value.eisd+'</td><td>'+value.eir+'</td></tr>';   
            });
            html += '</tbody>';
            $("#example").html(html);
            jQuery('#example').DataTable({
            dom: 'Bfrtip',
            buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print','pageLength'
            ],
            destroy: true,
            });

        });
 }

 function addRemarks (url)
{
        //$("#modifyForm").append('<input type="hidden" name="id" value="'+id+'" /> ');
        var  form = $("#modifyRemarkItemForm").serialize();
        $.ajax({
            type: "POST",
            dataType : "JSON",
            url : url,
            data : form, 
        }).done(function(data){
            $('#itemremarkModal').find("#messages").html('<strong>&#10004;&nbsp;Remark added successfully!</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>').addClass('alert alert-success');
            $("#modifyRemarkItemForm")[0].reset();
            setTimeout(function(){
                location.reload();
            },2000);  
        }).fail(function(data){
            if(data.status == 422){
            // $("#place_order").text('PLACE ORDER');     
            var errors = data.responseJSON;
            
            errorsHtml = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><ul>';
            $.each( errors, function( key, value ) {
            errorsHtml += '<li>' + value + '</li>'; //showing only the first error.
        });
            errorsHtml += '</ul></div>';
            $('#itemremarkModal').find( '#messages' ).html( errorsHtml );
            
        }else if(data.status == 400){
            var errors = data.responseJSON;
            errorsHtml = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><ul><li>'+errors+'</li></ul></div>';
            $('#itemremarkModal').find( '#messages' ).html( errorsHtml );
            $("#modifyRemarkItemForm")[0].reset();
        }else{
            return false;
        }
    });

}


function getMyItems(url)
 {
        $.ajax({
            type: "GET",
            dataType : "JSON",
            url : url,
            data : data, 
        }).done(function(data){
            html = '<thead><tr><th>Item Name</th><th>Type</th><th>Brand</th><th>Model</th><th>IME/SR/REG</th><th>Deduction Amount</th><th>Monthly Deduction Amount</th></tr></thead><tbody>';
            $.each(data, function(key,value){
                html += '<tr><td>'+value.name+'</td><td>'+value.type+'</td><td>'+value.brand+'</td><td>'+value.model+'</td><td>'+value.ime+'</td><td>'+value.da+'</td><td>'+value.mda+'</td></tr>';   
            });
            html += '</tbody>';
            $("#example").html(html);
            jQuery('#example').DataTable({
            dom: 'Bfrtip',
            buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print','pageLength'
            ],
            destroy: true,
            });

        });
 }





function getAllEmployeeLeaveBalance(url)
{
    var form =  $("#leave_balance").serialize();
    $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url : url,
        data : form
    }).done(function(data){
        html = '<thead><tr><th>Emp Name</th><th>Emp Code</th><th>Leave Type</th><th>Total Leaves</th><th>Availed Leaves</th><th>Deducted Leaves</th><th>Balance Leave</th></tr></thead><tbody>';
        $.each(data, function(keys, values){
            $.each(values, function(key,value){
                html += '<tr><td>'+value.emp_name+'</td><td>'+value.emp_no+'</td><td>'+value.leavetype+'</td><td>'+value.total_leaves+'</td><td>'+value.availed+'</td><td>'+value.deducted_leave+'</td><td>'+value.balance+'</td></tr>';   
            });
        });
        html += '</tbody>';
        $("#example").html(html);
        jQuery('#example').DataTable({
            dom: 'Bfrtip',
            buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print','pageLength'
            ],
            destroy: true,
        });


    });
//        .fail(function(xhr, status, error){
//        if (xhr.status == 401) {
//            confirm("You session has been expire, please login again.");
//            window.location = "login" ;
//        }
//    });
}

    



function generatePayBackLeave(url)
{
    var form  =  $("#payback_leave_form").serialize();
    $(".loading").show();
    $.ajax({
        type: "POST",
        dataType : "JSON",
        url : url,
        data : form 
    }).done(function(data){
        $(".loading").hide();
        $("#messages").html('<strong>'+data.msg+'</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>').addClass('alert alert-success');
        $("#payback_leave_form")[0].reset();
            // setTimeout(function(){
            //     location.reload();
            // },1000);
        }).fail(function(data){
           $(".loading").hide();
           if(data.status == 422){
            // $("#place_order").text('PLACE ORDER');     
            var errors = data.responseJSON;
            errorsHtml = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><ul>';
            $.each( errors, function( key, value ) {
            errorsHtml += '<li>' + value + '</li>'; //showing only the first error.
        });
            errorsHtml += '</ul></div>';
            $(document).find( '#messages' ).html( errorsHtml );
            
        }else if(data.status == 400){

            var errors = data.responseJSON;
            $(document).find( '#messages' ).addClass('alert alert-danger').html(errors.msg);
            $("#payback_leave_form")[0].reset();
        }else{
            return false;
        }
    });
    }

    function assignItemToEmployee(url)
    {

        var form  =  $("#assign_item_form").serialize();
        $(".loading").show();
        $.ajax({
            type: "POST",
            dataType : "JSON",
            url : url,
            data : form 
        }).done(function(data){
            $(".loading").hide();
            $("#messages").html('<strong>'+data.msg+'</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>').addClass('alert alert-success');
            $("#assign_item_form")[0].reset();
            setTimeout(function(){
                location.reload();
            },1000);
        }).fail(function(data){
           $(".loading").hide();
           if(data.status == 422){
            // $("#place_order").text('PLACE ORDER');     
            var errors = data.responseJSON;
            errorsHtml = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><ul>';
            $.each( errors, function( key, value ) {
            errorsHtml += '<li>' + value + '</li>'; //showing only the first error.
        });
            errorsHtml += '</ul></div>';
            $(document).find( '#messages' ).html( errorsHtml );
            
        }else if(data.status == 400){

            var errors = data.responseJSON;
            $(document).find( '#messages' ).addClass('alert alert-danger').html(errors.msg);
            $("#assign_item_form")[0].reset();
        }else{
            return false;
        }
    });
    }

    function generateEmpItemLedger(url)
    {

        var form  =  $("#item_deduction_form").serialize();
        $(".loading").show();
        $.ajax({
            type: "POST",
            dataType : "JSON",
            url : url,
            data : form 
        }).done(function(data){
            $(".loading").hide();
            $("#messages").html('<strong>'+data.msg+'</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>').addClass('alert alert-success');
            $("#item_deduction_form")[0].reset();
            setTimeout(function(){
                location.reload();
            },1000);
        }).fail(function(data){
           $(".loading").hide();
           if(data.status == 422){
            // $("#place_order").text('PLACE ORDER');     
            var errors = data.responseJSON;
            errorsHtml = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><ul>';
            $.each( errors, function( key, value ) {
            errorsHtml += '<li>' + value + '</li>'; //showing only the first error.
        });
            errorsHtml += '</ul></div>';
            $(document).find( '#messages' ).html( errorsHtml );
            
        }else if(data.status == 400){

            var errors = data.responseJSON;
            $(document).find( '#messages' ).addClass('alert alert-danger').html(errors.msg);
            $("#item_deduction_form")[0].reset();
        }else{
            return false;
        }
    });

}




    function employeePayBackLeaves(url)
    {
        $.ajax({

            type: "GET",
            dataType : "JSON",
            url : url,
            data : data 
        }).done(function(data){
            $("#pay_back_count").html(data);

        });
   }     

    function getPfTotalValue(url)
    {

        $.ajax({
            type: "GET",
            dataType : "JSON",
            url : url,
            data : data 
        }).done(function(data){
            $("#pf_total_val").html(data+"/- PKR");
        });

   }

    function getReportingManager(url)
    {

        $.ajax({
            type: "GET",
            dataType : "JSON",
            url : url,
            data : data 
        }).done(function(data){
            $("#reporting_manager").html(data).css('font-weight','bold');
        });

   }
   

    

    function allEmployeeLeaveList(url)
    {
       
        $.ajax({

            type: "GET",
            dataType : "JSON",
            url : url,
            data : data, 
        }).done(function(data){


            html = '<thead><tr><th>Employee Name</th><th>Employee No.</th><th>Leave Type</th><th>Applied At</th><th>From</th><th>To</th><th>Days</th><th>Status</th><th>Approved By</th><th>Approved At</th></tr></thead><tbody>';
            $.each(data, function(key,value){
                html += '<tr><td>'+value.name+'</td><td>'+value.emp_no+'</td><td>'+value.leavetype+'</td><td>'+value.applied_at+'</td><td>'+value.from+'</td><td>'+value.to+'</td><td>'+value.days+'</td><td><span class="badge badge-'+ leaveStatusBackground(value.status) +'">'+ leaveStatusValue(value.status) +'</span></td><td>'+value.approved_by+'</td><td>'+value.approved_at+'</td></tr>';   

            });
            html += '</tbody>';
            $("#example").html(html);
            jQuery('#example').DataTable({});

        });


}

function assignArrears(url)
    {
        var form  =  $("#arrear_form").serialize();
        $(".loading").show();
        $.ajax({
            type: "POST",
            dataType : "JSON",
            url : url,
            data : form 
        }).done(function(data){
            $(".loading").hide();
            $("#messages").html('<strong>'+data.msg+'</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>').addClass('alert alert-success');
            $("#arrear_form")[0].reset();
            setTimeout(function(){
                location.reload();
            },1000);
        }).fail(function(data){
           $(".loading").hide();
           if(data.status == 422){
            // $("#place_order").text('PLACE ORDER');     
            var errors = data.responseJSON;
            errorsHtml = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><ul>';
            $.each( errors, function( key, value ) {
            errorsHtml += '<li>' + value + '</li>'; //showing only the first error.
        });
            errorsHtml += '</ul></div>';
            $(document).find( '#messages' ).html( errorsHtml );
            
        }else if(data.status == 400){

            var errors = data.responseJSON;
            $(document).find( '#messages' ).addClass('alert alert-danger').html(errors.msg);
            $("#arrear_form")[0].reset();
        }else{
            return false;
        }
    });
    }


function  getPayrollHistory(url)
{
    html = '';
    //var form  =  $("#payrollform").serialize();
    $(".loading").show();
    $.ajax({
        type: "GET",
        dataType : "JSON",
        url : url,
        data : data 
    }).done(function(data){
        $(".loading").hide();
        html = '<thead><tr><th>Employee Name</th><th>Emp Code</th><th>Company</th><th>Gross Sal.</th><th>Leaves Ded.</th><th>PF</th><th>Adv. Salary</th><th>Tax</th><th>EOBI</th><th>Loan Ded.</th><th>Item Deduction.</th><th>Fuel Amount.</th><th>Parking Amount.</th><th>Arrears</th><th>Net Sal.</th><th>Month</th><th>Date</th><th>Year</th></tr></thead><tbody>';
        $.each(data, function(key,value){
           var editBtn = '<a href="javascript:void(0)" id="amountEditBtn" data-toggle="modal" data-target="#amountEditModal"><i class="fa fa-edit"></i></a>';
                // var relaxUnrelax = (value.is_relaxed == 1) ? '<a href="javascript:void(0)" class="btn btn-danger btn-sm" id="remove_relax"><i class="fa fa-remove"></i>&nbsp;Remove</a>' : '<a href="javascript:void(0)" class="btn btn-primary btn-sm" id="relax"><i class="fa fa-plus"></i>&nbsp;Relax</a>';
                html += '<tr><td>'+value.emp_name+'</td><td>'+value.emp_id+'</td><td>'+value.company_name+'</td><td>'+value.gross_sal+'</td><td>'+value.leave_ded_amt+'</td><td>'+value.pf+'</td><td>'+value.adv_sal+'</td><td>'+value.tax+'</td><td>'+value.eobi+'</td><td>'+value.loan_ded+'</td><td>'+value.item_deduction+'</td><td>'+value.fuel_amount+'</td><td>'+value.parking_amount+'</td><td>'+value.arrear_amount+'</td><td>'+value.net_sal+'</td><td>'+value.month+'</td><td>'+value.payroll_date+'</td><td>'+value.year+'</td></tr>';
            });
        $("#example").html(html);


       var table = $('#example').DataTable({
            dom: 'Bfrtip',
            pageLength:100,
            lengthMenu: [[50, 100, 150, -1], [50, 100, 150, "All"]],            
            buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print','pageLength'
            ],
            destroy: true,

        });

       $.fn.dataTable.ext.search.push(
        function( settings, data, dataIndex ) {
            var month = $('#month').val();
            var sub_company = $('#sub_company option:selected').text();
            var year = $('#year').val();
           // var max = parseInt( $('#max').val(), 10 );
            var sub_compan_col = data[2]; // use data for the age column
            var month_col = data[15]; // use data for the age column
            var year_col = data[17]; // use data for the age column
                //alert(sub_company);

        if (sub_compan_col == sub_company && month_col == month && year_col ==  year)
        {
            return true;
        }
        return false;
    }
    );

    $('#sub_company, #month, #year').change( function() {
        table.draw();
        //alert('asdasd');
        });

    });
}


function getPayBackLeavesList(url)
{
    $.ajax({
            type: "GET",
            dataType : "JSON",
            url : url,
            data : data, 
        }).done(function(data){
            html = '<thead><tr><th>Employee Name</th><th>Employee Code</th><th>No. of Leaves</th><th>Leave Date</th><th>Paid At</th><th>Paid By</th><th>Month</th><th>Year</th><th>Action</th></tr></thead><tbody>';
            $.each(data, function(key,value){
                html += '<tr><td>'+value.name+'</td><td>'+value.emp_no+'</td><td>'+value.no_of_leaves+'</td><td>'+value.leave_date+'</td><td>'+value.paid_at+'</td><td>'+value.paid_by+'</td><td>'+value.month+'</td><td>'+value.year+'</td><td data-id="'+value.id+'"><a href="void:javascript(0)" id="modifyBtn" class="fa fa-edit" data-toggle="modal" data-target="#amountEditModal"></a> &nbsp; <a href="void:javascript(0)" id="delBtn" class="fa fa-trash"></a></td></tr>';   
            });
            html += '</tbody>';
            $("#example").html(html);
            jQuery('#example').DataTable({});

        });
}

 function getArrears(url)
 {
        $.ajax({
            type: "GET",
            dataType : "JSON",
            url : url,
            data : data, 
        }).done(function(data){
            html = '<thead><tr><th>Employee Name</th><th>Employee Code</th><th>Arrear Amt.</th><th>Month</th><th>Year</th><th>Reason</th></tr></thead><tbody>';
            $.each(data, function(key,value){
                html += '<tr><td>'+value.name+'</td><td>'+value.emp_no+'</td><td>'+value.arrear_amount+'</td><td>'+value.month+'</td><td>'+value.year+'</td><td>'+value.reason+'</td></tr>';   
            });
            html += '</tbody>';
            $("#example").html(html);
            jQuery('#example').DataTable({});

        });
 }


function getLeaveDeductionReport(url)
 {
        $.ajax({
            type: "GET",
            dataType : "JSON",
            url : url,
            data : data, 
        }).done(function(data){
            html = '<thead><tr><th>Employee Name</th><th>Employee Code</th><th>Company</th><th>Leave Type</th><th>Deducted Leaves</th><th>Date</th><th>Month</th></tr></thead><tbody>';
            $.each(data, function(key,value){
                if(value.leave_type_id == 7 )
                {
                    value.leavename = 'Payback Leave';
                }else if(value.leave_type_id == 0)
                {
                    value.leavename =  'Direct Deduction';
                }
                html += '<tr><td>'+value.name+'</td><td>'+value.emp_no+'</td><td>'+value.company+'</td><td>'+value.leavename+'</td><td>'+value.deducted_leave+'</td><td>'+value.date+'</td><td>'+value.month+'</td></tr>';   
            });
            html += '</tbody>';
            $("#example").html(html);
            jQuery('#example').DataTable({
                dom: 'Bfrtip',
            pageLength:100,
            lengthMenu: [[50, 100, 150, -1], [50, 100, 150, "All"]],            
            buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print','pageLength'
            ],
            destroy: true,

            });

        });
 }


 function getLoanDeductionReport(url)
 {
        $.ajax({
            type: "GET",
            dataType : "JSON",
            url : url,
            data : data, 
        }).done(function(data){
            html = '<thead><tr><th>Employee Name</th><th>Employee Code</th><th>Amount</th><th>Monthly Inst</th><th>Deducted Amt.</th><th>Balance</th><th>Date</th><th>Month</th></tr></thead><tbody>';
            $.each(data, function(key,value){

                html += '<tr><td>'+value.name+'</td><td>'+value.emp_no+'</td><td>'+value.amount+'</td><td>'+value.monthly_deduction_amount+'</td><td>'+value.deduction_amount+'</td><td>'+value.balance+'</td><td>'+value.date+'</td><td>'+value.month+'</td></tr>';   
            });
            html += '</tbody>';
            $("#example").html(html);
            jQuery('#example').DataTable({
                dom: 'Bfrtip',
            pageLength:100,
            lengthMenu: [[50, 100, 150, -1], [50, 100, 150, "All"]],            
            buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print','pageLength'
            ],
            destroy: true,

            });

        });
 }


 function getItemDeductionReport(url)
 {
        $.ajax({
            type: "GET",
            dataType : "JSON",
            url : url,
            data : data, 
        }).done(function(data){
            html = '<thead><tr><th>Employee Name</th><th>Employee Code</th><th>Item</th><th>Deduction Amt.</th><th>Date</th><th>Month</th></tr></thead><tbody>';
            $.each(data, function(key,value){

                html += '<tr><td>'+value.name+'</td><td>'+value.emp_no+'</td><td>'+value.item+'</td><td>'+value.deduction_amt+'</td><td>'+value.date+'</td><td>'+value.month+'</td></tr>';   
            });
            html += '</tbody>';
            $("#example").html(html);
            jQuery('#example').DataTable({
                dom: 'Bfrtip',
            pageLength:100,
            lengthMenu: [[50, 100, 150, -1], [50, 100, 150, "All"]],            
            buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print','pageLength'
            ],
            destroy: true,

            });

        });
 }




function payrollSummary(url)
{
    $.ajax({
        type: "GET",
        dataType : "JSON",
        url : url,
        data : data,
    }).done(function(data){
        html = '<table class="table" id="payroll"><thead><tr><th>Company</th><th>Total</th></tr></thead><tbody>';
        $.each(data, function(key, value){
        html += '<tr class="clickable-row" data-href="" style="cursor:pointer"><td>'+value.name+'</td><td>'+value.Total+'</td></tr>';
        });
        html += '</tbody></table>';
        $("#payroll_summary").html(html);
        $('#payroll').on('click','.clickable-row',function () {
            window.location = 'payroll-history';
        });
    });
}


function itemDeductionSummary(url)
{
    $.ajax({
        type: "GET",
        dataType : "JSON",
        url : url,
        data : data,
    }).done(function(data){
        html = '<table class="table" id="item"><thead><tr><th>Company</th><th>Total</th></tr></thead><tbody>';
        $.each(data, function(key, value){
            html += '<tr style="cursor:pointer"><td>'+value.name+'</td><td>'+value.total+'</td></tr>';
        });
        html += '</tbody></table>';
        $("#item_ded_summary").html(html);
        $('#item').on('click','tbody > tr',function () {
            window.location = 'item-deduction-report';
        });
    });
}


function loanDeductionSummary(url)
{
    $.ajax({
        type: "GET",
        dataType : "JSON",
        url : url,
        data : data,
    }).done(function(data){
        html = '<table class="table" id="loan"><thead><tr><th>Company</th><th>Total</th></tr></thead><tbody>';
        $.each(data, function(key, value){
            html += '<tr style="cursor:pointer"><td>'+value.name+'</td><td>'+value.total+'</td></tr>';
        });
        html += '</tbody></table>';
        $("#loan_ded_summary").html(html);
        $('#loan').on('click','tbody > tr',function () {
            window.location = 'loan-deduction-report';
        });
    });
}


function getItemLedger(url)
{
    $.ajax({
        type: "GET",
        dataType : "JSON",
        url : url,
        data : data,
    }).done(function(data){
        html = '<table class="table" ><thead><tr><th>Emp No</th><th>Emp Name</th><th>Item</th><th>Deduction Amt</th><th>Month</th><th>Date</th></tr></thead><tbody>';
        $.each(data, function(key, value){
            html += '<tr><td>'+value.emp_no+'</td><td>'+value.emp_name+'</td><td>'+value.item_name+'</td><td>'+value.deduction_amt+'</td><td>'+value.month+'</td><td>'+value.date+'</td></tr>';
        });
        html += '</tbody></table>';
        $("#example").html(html);
        jQuery('#example').DataTable({
                dom: 'Bfrtip',
            pageLength:100,
            lengthMenu: [[50, 100, 150, -1], [50, 100, 150, "All"]],            
            buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print','pageLength'
            ],
            destroy: true,

            });
        // $('#loan').on('click','tbody > tr',function () {
        //     window.location = 'loan-deduction-report';
        // });
    });
}

function getModifiedAttendance(url)
{

    var form  =  $("#mod_atten_form").serialize();
    //$("#view").html('<i class="fa fa-eye"></i>&nbsp;loading...');
    $(".loading").show();
    $.ajax({
        type : 'POST',
        dataType :'JSON',
        url: url,
        data : form
    }).done(function(data){

        
        $(".loading").hide();    

        html = '<thead><tr><th>Employee ID</th><th>Employee Name</th><th>Date </th><th>Month</th><th>Time In</th><th>Time Out</th><th>Reason</th><th class="noexport">Action</th></tr></thead><tbody>';
        $.each(data, function(key, value){
            html += '<tr><td>'+value.emp_no+'</td><td>'+value.name+'</td><td>'+value.date+'</td><td>'+value.month+'</td><td>'+value.time_in +'</td><td>'+value.time_out+'</td><td>'+value.reason+'</td><td data-id="'+value.id+'"><button id="modifyBtn"><i class="fa fa-trash"></button></td></tr>';
        });
        html += '</tbody>';
        $("#example").html(html);
        jQuery('#example').DataTable();
    });    
}


function deleteModifiedAttendance(url, id)
{
    if(confirm('Are you sure you want to delete this?'))
    {
//       alert(url);
        $.ajax({
        type : 'GET',
   //     dataType :'JSON',
        url: url,
        data: {
             "id":id,
//            "_method":"DELETE"
        }
        }).done(function(data){
           
           $("#messages").html('<strong>Successfully Deleted.</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>').addClass('alert alert-success');
        setTimeout(function(){
            location.reload();
        },2000);
        });
    }
}


function getPackageDetail(url)
{
    $.ajax({
        type: "GET",
        dataType : "JSON",
        url : url,
        data : data,
    }).done(function(data){
        html = '<table class="table" ><thead><tr><th>Emp No</th><th>Emp Name</th><th>Gross Salary</th><th>Fuel Amount</th><th>Parking Amount</th><th>Eobi Amount</th><th>PF Amount</th><th>Year</th></tr></thead><tbody>';
        $.each(data, function(key, value){
            html += '<tr><td>'+value.emp_id+'</td><td>'+value.emp_name+'</td><td>'+value.gross_salary+'</td><td>'+value.fuel_amount+'</td><td>'+value.parking_amount+'</td><td>'+value.eobi_amount+'</td><td>'+value.pf_amount+'</td><td>'+value.year+'</td></tr>';
        });
        html += '</tbody></table>';
        $("#example").html(html);
        //$("#example").DataTable({});
        $('#example').DataTable({
                dom: 'Bfrtip',
                order: [],
            pageLength:100,
            lengthMenu: [[50, 100, 150, -1], [50, 100, 150, "All"]],            
            buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print','pageLength'
            ],
            destroy: true,

            });
        // $('#loan').on('click','tbody > tr',function () {
        //     window.location = 'loan-deduction-report';
        // });
    });
}


function leaveCancel(url, url2)
{
//    alert($url);
      $.ajax({
        type : "GET",
        dataType : 'JSON',
        url : url,
        data : data
    }).done(function(data){
            alert('Cancel Successfully.');
            getLeaveApprovals(url2)

    });
}

function allowanceRequest(url)
{
    $(".loading").show();
    var  form = $("#dinnerAllowanceForm").serialize();
    $.ajax({
        type: "POST",
        dataType : "JSON",
        url : url,
        data : form, 
    }).done(function(data){
        $("#messages").html('<strong>&#10004;&nbsp;You have succesfully applied for Dinner Allowance.</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>').addClass('alert alert-success');
        $("#dinnerAllowanceForm")[0].reset();
        $(".loading").hide();
        setTimeout(function(){
            location.reload();
        },1000);  
    }).fail(function(data){
        if(data.status == 422){
//            $("#place_order").text('PLACE ORDER');     
        $(".loading").hide();
            var errors = data.responseJSON;
            errorsHtml = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><ul>';
            $.each( errors, function( key, value ) {
            errorsHtml += '<li>' + value + '</li>'; //showing only the first error.
        });
            errorsHtml += '</ul></div>';
//           $( '#messages' ).html( errorsHtml );

}else if(data.status == 400)
{
    $(".loading").hide();
    var errors = data.responseJSON;
    errorsHtml = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><ul><li>'+errors+'</li></ul></div>';
}else{
    return false;
}

$( '#messages' ).html( errorsHtml );
$("#dinnerAllowanceForm")[0].reset();
});

}


function getMyAllowanceList(url)
 {
        $.ajax({
            type: "GET",
            dataType : "JSON",
            url : url,
            data : data, 
        }).done(function(data){
            html = '<thead><tr><th>Self</th><th>Allowance Date</th><th>Reason</th><th>Status</th><th>Paid Status</th><th>Applied At</th></tr></thead><tbody>';
            $.each(data, function(key,value){
                if(value.Status == 1)
            {
                var statusDrop = '<div class="dropdown"><span class="badge badge-'+ DinnerAllowanceStatusBackground(value.Status) +'">'+ DinnerAllowanceStatusValue(value.Status) +'</span></div>';
                var paidStatusDrop = '<div class="dropdown"><span class="badge badge-'+ statusBackground(value.paidStatus) +'">'+ paidStatusValue(value.paidStatus) +'</span></div>';
            }else if(value.Status == 3)
            {
                var statusDrop = '<span class="badge badge-'+ DinnerAllowanceStatusBackground(value.Status) +'">'+DinnerAllowanceStatusValue(value.Status)+'</span>';
                var paidStatusDrop = '<div class="dropdown"><span class="badge badge-'+ statusBackground(value.paidStatus) +'">'+ paidStatusValue(value.paidStatus) +'</span></div>';
            }
            else{
                var statusDrop = '<div class="dropdown"><span class="badge badge-'+ DinnerAllowanceStatusBackground(value.Status) +'">'+ DinnerAllowanceStatusValue(value.Status) +'</span></div>';
                var paidStatusDrop = '<div class="dropdown"><span class="badge badge-'+ statusBackground(value.paidStatus) +'">'+ paidStatusValue(value.paidStatus) +'</span></div>';
            }  
                html += '<tr><td>'+value.self+'</td><td>'+value.ad+'</td><td>'+value.reasons+'</td><td>'+statusDrop+'</td><td>'+paidStatusDrop+'</td><td>'+value.apply_at+'</td></tr>';   
            });
            html += '</tbody>';
            $("#example").html(html);
            jQuery('#example').DataTable({
            dom: 'Bfrtip',
            buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print','pageLength'
            ],
            destroy: true,
            });

        });
 }


function getAllowanceApprovals(url)
{

    $.ajax({

        type: "GET",
        dataType : "JSON",
        url : url,
        data : data, 
    }).done(function(data){
        html = '<thead><tr><th>Sr #</th><th>Emp Name</th><th>Emp Code</th><th>Request Date</th><th>Reason</th><th>Status</th></tr></thead><tbody>';
        $.each(data, function(key,value){
            if(value.status == 1)
            {
                var statusDrop = '<div class="dropdown"><span class="badge badge-'+ DinnerAllowanceStatusBackground(value.status) +'">'+ DinnerAllowanceStatusValue(value.status) +'</span><div class="dropdown-content" id="'+value.rowid+'"><a href="javascript:void(0);" class="lins">Cancel</a></div></div>';

            }else if(value.status == 3)
            {
                var statusDrop = '<span class="badge badge-'+ DinnerAllowanceStatusBackground(value.status) +'">'+DinnerAllowanceStatusValue(value.status)+'</span>';
            }
            else{
                var statusDrop = '<div class="dropdown"><span class="badge badge-'+ DinnerAllowanceStatusBackground(value.status) +'">'+ DinnerAllowanceStatusValue(value.status) +'</span><div class="dropdown-content" id="'+value.rowid+'"><a class="lins" href="javascript:void(0);">Approve</a><a class="lins" href="javascript:void(0);">Reject</a><a class="lins" href="javascript:void(0);">Pending</a><a href="javascript:void(0);" class="lins">Cancel</a></div></div>';
            }              
            html += '<tr><td>'+value.rowid+'</td><td>'+value.empname+'</td><td>'+value.empcode+'</td><td>'+value.allowancedate+'</td><td>'+value.reason+'</td><td>'+ statusDrop +'</td></tr>';   
        });
        html += '</tbody>';
        $("#example").html(html);
        $('#example').DataTable({
                dom: 'Bfrtip',
                order: [],
            pageLength:100,
            lengthMenu: [[50, 100, 150, -1], [50, 100, 150, "All"]],            
            buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print','pageLength'
            ],
            destroy: true,

            });
 
    });       
}

function DinnerAllowanceStatusBackground(status)
{
    var badge;
    switch(status)
    {
        case 1:
        badge = "success";
        break;

        case 2:
        badge = "danger";     
        break;

        case 0:
        badge = "warning";     
        break;

        case 3:
        badge = "danger";     
        break;
    }


    return badge;
}

function DinnerAllowanceStatusValue(status)
{
    switch(status)
    {
        case 1:
        var badge = "Approved";
        break;

        case 2:
        var badge = "Rejected";
        break;

        case 0:
        var badge = "Pending";
        break;

        case 3:
        var badge = "Cancelled";
        break;
    }


    return badge;
}

function AllowanceApprovalStatusValueReplace(statusText)
{
    switch(statusText)
    {
        case "Approve":
        var value = 1;
        break;

        case "Reject":
        var value = 2;
        break;

        case "Pending":
        var value = 0;
        break;

        case "Cancel":
        var value = 3;
        break;
    }


    return value;

}

function AllowanceApprovalAction(url,status)
    {
      $.ajax({
        type : "GET",
        dataType : 'JSON',
        url : url+'?status='+status,
        data : data
    }).done(function(data){
            alert(data);
            location.reload();
    });
}


function getApprovedAllowances(url)
{

    $.ajax({

        type: "GET",
        dataType : "JSON",
        url : url,
        data : data, 
    }).done(function(data){
        html = '<thead><tr><th>Sr #</th><th>Emp Name</th><th>Emp Code</th><th>Request Date</th><th>Status</th><th>Approved By</th><th>Paid Status</th></tr></thead><tbody>';
        $.each(data, function(key,value){
            if(value.status == 1 && value.paidStatus == 0)
            {
//                var statusDrop = '<span class="badge badge-'+ leaveStatusBackground(value.status) +'">'+leaveStatusValue(value.status)+'</span>';     
                var statusDrop = '<div class="dropdown"><span class="badge badge-'+ DinnerAllowanceStatusBackground(value.status) +'">'+ DinnerAllowanceStatusValue(value.status) +'</span></div>'; //<div class="dropdown-content" id="'+value.rowid+'"><a href="javascript:void(0);" class="lins">Cancel</a></div>
                var paidStatusDrop = '<div class="dropdown"><span class="badge badge-'+ statusBackground(value.paidStatus) +'">'+ paidStatusValue(value.paidStatus) +'</span><div class="dropdown-content" id="'+value.rowid+'"><a href="javascript:void(0);" class="lins-paid">Paid</a></div></div>';
            }else if(value.status == 3)
            {
                var statusDrop = '<span class="badge badge-'+ DinnerAllowanceStatusBackground(value.status) +'">'+DinnerAllowanceStatusValue(value.status)+'</span>';
            }
            else{
                var statusDrop = '<div class="dropdown"><span class="badge badge-'+ DinnerAllowanceStatusBackground(value.status) +'">'+ DinnerAllowanceStatusValue(value.status) +'</span><div class="dropdown-content" id="'+value.rowid+'"><a class="lins" href="javascript:void(0);">Approve</a><a class="lins" href="javascript:void(0);">Reject</a><a class="lins" href="javascript:void(0);">Pending</a><a href="javascript:void(0);" class="lins">Cancel</a></div></div>';
                var paidStatusDrop = '<div class="dropdown"><span class="badge badge-'+ statusBackground(value.paidStatus) +'">'+ paidStatusValue(value.paidStatus) +'</span></div>';
            }              
            html += '<tr><td>'+value.rowid+'</td><td>'+value.empname+'</td><td>'+value.empcode+'</td><td>'+value.allowancedate+'</td><td>'+ statusDrop +'</td><td>'+value.approveby+'</td><td>'+paidStatusDrop+'</td></tr>';   
        });
        html += '</tbody>';
        $("#example").html(html);
        $('#example').DataTable({
                dom: 'Bfrtip',
                order: [],
            pageLength:100,
            lengthMenu: [[50, 100, 150, -1], [50, 100, 150, "All"]],            
            buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print','pageLength'
            ],
            destroy: true,

            });
 
    });       
}


function dinnerAllowancePaidAction(url,status)
    {
      $.ajax({
        type : "GET",
        dataType : 'JSON',
        url : url+'?status='+status,
        data : data
    }).done(function(data){
            alert(data);
            location.reload();
    });
}



function approveMedLeave(url)
{
    $(".loading").show();
    var  form = $("#hr-medical-approval-form").serialize();
    $.ajax({
        type: "POST",
        dataType : "JSON",
        url : url,
        data : form, 
    }).done(function(data){
        $("#messages").html('<strong>&#10004;&nbsp;You have succesfully applied for Dinner Allowance.</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>').addClass('alert alert-success');
        $("#hr-medical-approval-form")[0].reset();
        $(".loading").hide();
        setTimeout(function(){
            location.reload();
        },1000);  
    }).fail(function(data){
        if(data.status == 422){
//            $("#place_order").text('PLACE ORDER');     
        $(".loading").hide();
            var errors = data.responseJSON;
            errorsHtml = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><ul>';
            $.each( errors, function( key, value ) {
            errorsHtml += '<li>' + value + '</li>'; //showing only the first error.
        });
            errorsHtml += '</ul></div>';
//           $( '#messages' ).html( errorsHtml );

}else if(data.status == 400)
{
    $(".loading").hide();
    var errors = data.responseJSON;
    errorsHtml = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><ul><li>'+errors+'</li></ul></div>';
}else{
    return false;
}

$( '#messages' ).html( errorsHtml );
$("#hr-medical-approval-form")[0].reset();
});

}