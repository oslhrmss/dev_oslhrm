/**
 * Created by azizkhanm on 3/8/19.
 */
/**
 * This monitors all AJAX calls that have an error response. If a user's
 * session has expired, then the system will return a 401 status,
 * "Unauthorized", which will trigger this listener and so prompt the user if
 * they'd like to be redirected to the login page.
 */

$(function () {
    //setup ajax error handling
    $.ajaxSetup({
        error: function (x, status, error) {
            if (x.status == 401) {
                alert("Sorry, your session has expired. Please login again to continue");
                window.location ="login";
            }else if(x.status == 403)
            {
                console.log('Access Denied !');
            }
            else {
                console.log("An error occurred: " + status + "nError: " + error);
            }
        }
    });
});