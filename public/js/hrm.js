var html ='';
var data = '';


function getEmployeeList(url)
{
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url : url,
        data : data
    }).done(function(data){
        html  = '<option value=" ">--Select Employee--</option>';
        $.each(data, function(key,value){
            html  += '<option value="'+value.id+'">'+value.first_name+' '+value.last_name+'</option>';
        });

        $("#employee").html(html);
    });
}



function getPayrollDetails(url)
{
    var form  =  $("#employeeFrm").serialize();
    $.ajax({
        type : 'POST',
        dataType: 'JSON',
        url: url,
        data : form
    }).done(function(data){
        html = '<h3>Salary breakup of September</h3><table class="table table-bordered"><tr><td>Salary</td><td>'+data.salary+'</td></tr><tr><td>Total Days</td><td>'+data.total_days+'</td></tr><tr><td>Working Days</td><td>'+data.working_days+'</td></tr><tr><td>Off Days</td><td>'+data.off_days+'</td></tr><tr><td>Present Days</td><td>'+data.present_days+'</td></tr><tr><td>Leaves</td><td>'+data.leaves+'</td></tr><tr><td><b>Gross Salary</b></td><td><b>'+data.gross_salary+'</b></td></tr></table>';
        $("#payroll_detail").html(html);
    })


}



function login(url)
{
    var form = $("#loginForm").serialize();
    $.ajax({
        type: "POST",
        dataType : "JSON",
        url :  url,
        data : form 
    }).done(function(data){
        
    });
}