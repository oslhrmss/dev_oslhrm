@extends('layouts.app')
@section('title','Adv. Salary Approval')
@section('content')

        <div class="content mt-3">
            <div class="animated">
                <div class="row">
                    <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Adv. Salary Approvals</strong>
                            <div class="row">

                               <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <table id="example" class="table table-striped table-bordered"></table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>    
                </div>
            </div>
        </div>
     

<script src="{{ asset('assets/js/hrm.js') }}" type="text/javascript"></script>
        <script>
        jQuery(document).ready(function(){
            var url = "{{route('get.adv.sal.approvals')}}";
            getAdvSalApprovals(url);

        });
        $(document).on('click','.lins',function(e){
        e.preventDefault();
      
        var id = $(this).parent().attr('id');
        var edit_url =  "{{ route('adv.sal.approval.action',':id') }}";
        url1 = edit_url.replace(':id', id)
        var statusValue = approvalStatusValueReplace($(this).text());
        
        advSalApprovalAction(url1,statusValue);
     });

    $(document).on('click','.lins-paid',function(e){
        e.preventDefault();
      
        var id = $(this).parent().attr('id');
        var edit_url =  "{{ route('adv.sal.paid.action',':id') }}";
        url1 = edit_url.replace(':id', id)
        var statusValue = paidStatusValueReplace($(this).text());
        
        advSalPaidAction(url1,statusValue);
     });   
</script>

 @endsection           