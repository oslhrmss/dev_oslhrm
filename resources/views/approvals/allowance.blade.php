@extends('layouts.app')
@section('title','Dinner Allowance Approvals')
@section('content')

        <div class="content mt-3">
            <div class="animated">
                <div class="row">
                    <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Dinner Allowance Approvals</strong>
                            <div class="row">

                               <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <table id="example" class="table table-striped table-bordered"></table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>    
                </div>

                </div>
            </div>


<script src="{{ asset('assets/js/hrm.js') }}" type="text/javascript"></script>
    
    <script>
        jQuery(document).ready(function(){
            var url = "{{ route('get.allowance.approvals') }}";
            getAllowanceApprovals(url);
        });

        $(document).on('click','.lins',function(e){
        e.preventDefault();
      
        var id = $(this).parent().attr('id');
        var edit_url =  "{{ route('allowance.approval.action',':id') }}";
        url1 = edit_url.replace(':id', id)
        var statusValue = AllowanceApprovalStatusValueReplace($(this).text());
        
        AllowanceApprovalAction(url1,statusValue);
        });
        
</script>

 @endsection           