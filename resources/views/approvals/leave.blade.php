@extends('layouts.app')
@section('title','Leave Approval')
@section('content')

        <div class="content mt-3">
            <div class="animated">
                <div class="row">
                    <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Leaves Approvals</strong>
                            <div class="row">

                               <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <table id="example" class="table table-striped table-bordered"></table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>    
                </div>

                </div>
            </div>


<script src="{{ asset('assets/js/hrm.js') }}" type="text/javascript"></script>
        <script>
        jQuery(document).ready(function(){
            var url = "{{route('get.leave.approvals')}}";
            getLeaveApprovals(url);

        });
        $(document).on('click','.lins',function(e){
        e.preventDefault();
      
        var id = $(this).parent().attr('id');
        var edit_url =  "{{ route('leave.approval.action',':id') }}";
        url1 = edit_url.replace(':id', id)
        var statusValue = approvalStatusValueReplace($(this).text());
        
        leaveApprovalAction(url1,statusValue);
        });

        $(document).on('click','#cancel_leave', function(e){
            alert('sd');
            if(confirm("Are you sure to cancel?")){
            e.preventDefault();
            var id = $(this).parent().attr('id');
            var edit_url =  "{{ route('leave.cancel',':id') }}";
            url1 = edit_url.replace(':id', id)
            var url2 = "{{route('get.leave.approvals')}}";

            leaveCancel(url1, url2);
            } else {
                return false;
            }

        });
</script>

 @endsection           