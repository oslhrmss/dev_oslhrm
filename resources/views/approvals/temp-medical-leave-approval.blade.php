@extends('layouts.app')
@section('title','Medical Leaves Approval')
@section('content')

        <div class="content mt-3">
            <div class="animated">
            <div class="row">

                <div class="col-md-12">
                    <div class="card">
                    <div class="card-header"><strong class="card-title">Medical Leaves Approval</strong></div>	
                                        <div class="card-body">
                                        <div id="messages"></div> 
                                                <form id="hr-medical-approval-form" method="post" action="">
                                                {{csrf_field()}}
                                                <div class="form-group col-md-6">
                                                    <label for="employee" class=" form-control-label"><strong>Emp Name</strong> <span id="req">*</span></label>
                                                    <select name="emp_id" id="emp_id" class="empdrop form-control"></select>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <!--code-->
                                                </div>
                                                </form>
                                        </div>
                    
                    <div class="card-header">
                        <button onclick="window.history.go(-1); return false;" class="btn btn-danger"><i class="fa fa-ban"></i>&nbsp;&nbsp;Cancel</button>
                        <button type="button" id="submit" class="btn btn-primary">Approve</button>
                    </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
        


            <script src="{{ asset('assets/js/hrm.js') }}" type="text/javascript"></script>

            <script>
            	// $(document).ready(function(){
            	// 	//jQuery(".date").datetimepicker({format: 'YYYY-MM-DD'});
            	//  	var url  = "{{route('get.items')}}";
            	//  	getItems(url);
            	// });

                $(document).ready(function(){
                 var url = "{{route('emp.dropdown')}}";
                 employeeChosenDropDown(url);

                 });

                jQuery(document).on("click","#submit",function(e){
                   e.preventDefault();
                   var url = "{{ route('approve.med.leave') }}";
                   approveMedLeave(url);            
                });


             //    $(document).on('click','.lins',function(e){
             //        e.preventDefault();
                  
             //        var id = $(this).parent().attr('id');
             //        var edit_url =  "{{ route('item.action',':id') }}";
             //        url1 = edit_url.replace(':id', id)
             //        var statusValue = ItemStatusValueReplace($(this).text());
                    
             //        ItemAction(url1,statusValue);
             //     });

             //    jQuery(document).on("click","#itemEditBtn",function(e){
             //        e.preventDefault();
             //        getItemId($(this).parent("td").data('id'));
             //        $("input[name=item_name]").val($(this).parent("td").data('item-name'));
             //        // $("input[name=item_item_type_id]").val($(this).parent("td").data('item-item_type_id'));
             //    });

            </script>
@endsection