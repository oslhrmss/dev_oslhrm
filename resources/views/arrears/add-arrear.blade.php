@extends('layouts.app')
@section('title','Arrears')
@section('content')



        <div class="content mt-3">
            <div class="animated">
                <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Arrears</strong>
                            
                        </div>
                        <div class="card-body">
                        <div id="messages"></div>
                        <form name="arrear_form" id="arrear_form" method="POST" >
                            {{csrf_field()}}
                        <div class="form-group col-md-3">
                        <label for="employee" class=" form-control-label"><strong>Employee</strong> <span id="req">*</span></label>
                        <select name="emp_id" class="empdrop form-control"></select>
                        </div>
                        <div class="form-group col-md-3">
                          <label for="employee" class=" form-control-label"><strong> Amount</strong> <span id="req">*</span></label>
                          <input type="text" name="amount" class="form-control" />
                        </div>
                        <div class="form-group col-md-3">
                                <label for="employee" class=" form-control-label"><strong>Month</strong> <span id="req">*</span></label>
                                <select name="month" class="form-control">
                                    @foreach(App\Utilities\Utils::getMonthList() as $key => $value)
                                    <option value="{{$value}}">{{ $value }}</option>
                                    @endforeach
                                </select>
                        </div>
                        <div class="form-group col-md-3">
                                <label for="employee" class=" form-control-label"><strong>Reason</strong><span id="req">*</span></label>
                                <textarea name="reason" class="form-control"></textarea>
                        </div>
                        <div class="card-footer">
                        <label for="employee" class="form-control-label"></label>    
                        <button class="btn btn-primary btn-md" id="item_deduction">
                          <i class="fa fa-plus"></i> Add Arrears
                        </button>
                        <button type="reset" class="btn btn-danger btn-md">
                          <i class="fa fa-ban"></i> Reset
                        </button>
                        </div>
                        </form>

                        </div>
                    </div>

                </div>
                

                </div>
                <div class="row">

                 <div class="col-md-12">
                    <div class="card">
                    <div class="card-body">
                    <table id="example" class="table table-striped table-bordered"></table>
                    </div>
                    </div>
                 </div>

                </div>
</div>
</div>
<script src="{{ asset('assets/js/hrm.js') }}" type="text/javascript"></script>
        <script>
        jQuery(document).ready(function(){
            // var url = "{{route('sub.company')}}";
            // getSubCompany(url);
            var url = "{{route('emp.dropdown')}}";
            employeeChosenDropDown(url);
            var url  =  "{{route('get.arrears')}}";
            getArrears(url);
        });

        $(document).on('click','#item_deduction', function(e){

          e.preventDefault();
          var url1  = "{{ route('assign.arrear') }}";

          assignArrears(url1);
        });
        </script>
@endsection
