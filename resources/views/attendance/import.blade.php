@extends('layouts.app')
@section('title','Import Attendance')
@section('content')


<div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Import Attendance</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="#">Dashboard</a></li>
                            <li><a href="#">Attendance</a></li>
                            <li class="active">Import Attendance</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
          @if(session()->has('import'))
        <div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
                                            <span class="badge badge-pill badge-success">Success</span>
                                                {{session()->get('import')}}
                                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
        @endif
<div class="content mt-3">
            <div class="animated fadeIn">
            
            <div class="row">
    				<div class="col-lg-12">
    				<div class="card">
        			<div class="card-header"><strong>Personal </strong><small> Data</small></div>
        			<div class="card-body">
        			<form name="import-atten" method="post" enctype="multipart/form-data" action="{{route('import.post')}}">
        				{{csrf_field()}}
        			<div class="form-group col-md-12"><label for="company" class=" form-control-label">Import Attendance File <span id="req">*</span></label><input name ="atten_file" type="file" id="atten_file" placeholder="Profile Picture" class="form-control"></div>
        			<div class="card-footer">
                        <button type="submit" class="btn btn-primary btn-md">
                          <i class="fa fa-dot-circle-o"></i> Submit
                        </button>
                        <button type="reset" class="btn btn-danger btn-md">
                          <i class="fa fa-ban"></i> Reset
                        </button>
                    </div>	
        			</form>
        			</div>
    				</div>
    				</div>
	

			</div>


            </div>
        </div>
@endsection
