@extends('layouts.app')
@section('title','Payroll Attendance')
@section('content')

<div class="content mt-3">
            <div class="animated">
                <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Payroll Attendance</strong>
                            
                        </div>
                        <div class="card-body">
                        <form name="empAttendanceForm" id="atten_form" method="POST" autocompelete="off">
                            {{csrf_field()}}
                        <div class="form-group col-md-4">
                        <label for="employee" class=" form-control-label"><strong>Sub Company</strong> <span id="req">*</span></label>
                        <select name="sub_company_id" class="subcomp form-control"></select>
                        </div>    
                        <div class="form-group col-md-4">
                        <label for="employee" class=" form-control-label"><strong>From </strong><span id="req">*</span></label>
                        <input type="text" class="date form-control" name="from" autocomplete="off" value="{{ date('Y-m-26',strtotime('last month')) }}">
                        </div>
                        <div class="form-group col-md-4">
                        <label for="employee" class=" form-control-label"><strong>To </strong><span id="req">*</span></label>
                        <input type="text" class="date form-control" name="to" autocomplete="off" value="{{ date('Y-m-25') }}">
                        </div>
                        <div class="card-footer">
                        <div class="loading" style="display: none;">Loading&#8230;</div>    
                        <button class="btn btn-primary btn-md" id="view">
                          <i class="fa fa-dot-circle-o"></i> Generate 
                        </button>
                        </div>
                        </form>

                        </div>
                    </div>

                </div>
            </div>
                </div>
            </div>
            <script src="{{ asset('assets/js/hrm.js') }}" type="text/javascript"></script>

            <script>
            	$(document).ready(function(){
            		jQuery(".date").datetimepicker({format: 'YYYY-MM-DD'});
                    var url = "{{route('sub.company')}}";
                    getSubCompany(url);

            	});

            	 $(document).on('click','#view',function(e){
            	 	e.preventDefault();
            	 	var url  = "{{route('get.payroll.attendance')}}";
            	 	getPayrollAttendanceData(url);
            	 });

            </script>
@endsection            