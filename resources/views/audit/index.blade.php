@extends('layouts.app')
@section('title','Employee Attendance - Audit')
@section('content')
<!-- <div class="row">
<div class="col-md-6">
<a type="button" class="btn btn-primary" href="{{ route('add.employee') }}"><i class="fa fa-star"></i>&nbsp; Add new Employee</a>
</div>
</div> -->
        <div class="content mt-3">
            <div class="animated">
                <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Attendance</strong>
                            
                        </div>
                        <div class="card-body">
                        <form name="empAttendanceForm" id="atten_form" method="POST" action="" autocompelete="off">
                            {{csrf_field()}}

                        <div class="form-group col-md-4">
                        <label for="employee" class=" form-control-label"><strong>Employees</strong> <span id="req">*</span></label>
                        <select name="employee" class="empdrop form-control"></select>
                        </div>

                        <div class="form-group col-md-4">
                        <label for="employee" class=" form-control-label"><strong>From </strong><span id="req">*</span></label>
                        <input type="text" class="date form-control" name="from" autocomplete="off" value="{{ date('Y').'-'.date('m', strtotime('-1 months')).'-26' }}">
                        </div>
                        <div class="form-group col-md-4">
                        <label for="employee" class=" form-control-label"><strong>To </strong><span id="req">*</span></label>
                        <input type="text" class="date form-control" name="to" autocomplete="off" value="{{ date('Y').'-'.date('m').'-25' }}">
                        </div>
                        <div class="card-footer">
                        <button class="btn btn-primary btn-md" id="view">
                          <i class="fa fa-eye"></i> View
                        </button>
                        <button type="reset" class="btn btn-danger btn-md">
                          <i class="fa fa-ban"></i> Reset
                        </button>
                        </div>
                        </form>

                        </div>
                    </div>

                </div>
                

                </div>
                <div class="row">

                 <div class="col-md-12">
                    <div class="card">
                    <div class="card-body">
                    <table id="example" class="table table-striped table-bordered"></table>
                    </div>
                    </div>
                 <div>
                <div>

        <div class="modal fade" id="modifyPopup" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">

                            <div class="modal-header">
                                <h5 class="modal-title" id="smallmodalLabel">Modify Time</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            

                            <div class="modal-body">
                                <div class="sufee-alert alert with-close alert-success alert-dismissible fade show" id="message" style="display: none;">
                                            <span class="badge badge-pill badge-success">Success</span>
                                               Successfully Modiefied.
                                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                            <form id="modifyForm" method="post" action="">
                            {{csrf_field()}}
                            <div class="form-group col-md-4">
                            <label for="employee" class=" form-control-label"><strong>Time In</strong> <span id="req">*</span></label>
                            <input type="text" name="time_in" class="time form-control" id="time_in" placeholder="00:00:00" required>
                            </div>

                            <div class="form-group col-md-4">
                            <label for="employee" class=" form-control-label"><strong>Time out</strong> <span id="req">*</span></label>
                            <input type="text" name="time_out" class="time form-control" id="time_out" placeholder="00:00:00" required>
                            <input type="hidden" name="atten_id">
                            <input type="hidden" name="date">
                            <input type="hidden" name="empcode">
                            </div>
                            
                            <div class="form-group col-md-12">
                            <label for="employee" class=" form-control-label"><strong>Reason</strong> <span id="req">*</span></label>
                            <textarea name="reason" id="reason" class="form-control"></textarea>
                            </div>
                            
                            
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="button" id="submit" class="btn btn-primary">Confirm</button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>



        <div class="modal fade" id="reasonPopup" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">

                            <div class="modal-header">
                                <h5 class="modal-title" id="smallmodalLabel">Reason</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            

                            <div class="modal-body">

                                <h5 id="reasonText"></h5>
                            
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">OK</button>
                                
                            </div>

                        </div>
                    </div>
                </div>


            </div><!-- .animated -->

        </div><!-- .content -->

        <script src="{{ asset('assets/js/hrm.js') }}" type="text/javascript"></script>
		
        <script>
        $(document).ready(function(){
            jQuery(".date").datetimepicker({format: 'YYYY-MM-DD'});
            jQuery("#time_out").datetimepicker({format: 'HH:mm:ss '});
            jQuery("#time_in").datetimepicker({format: 'hh:mm:ss '});

        var url = "{{route('emp.dropdown')}}";
        employeeChosenDropDown(url);
        });

        $(document).on('click','#view',function(e){
            e.preventDefault();

            var url = "{{route('emp.attendance.audit')}}";
            employeeAttendance(url);
        });

        $(document).on('click','#modifyBtn', function(e){
            var url = "{{route('get.time')}}";
            getTimeInTimeOutValues(url,$(this).parent("td").data('id'),$(this).parent("td").data('date'),$(this).parent("td").data('empcode'));
        });
        

        $(document).on('click','#submit', function(e){
            //e.preventDefault();
            var url = "{{route('modify.time')}}";
            modifyManualAttendance(url);
        });
        $(document).on('click','#reasonspn', function(e){
            $("#reasonText").html(this.title);
        });
        </script>

@endsection