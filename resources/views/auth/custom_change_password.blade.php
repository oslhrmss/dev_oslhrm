<!DOCTYPE html>
<html>
<head>
	<title>Change Password</title>
	@include("include.header_inc")
</head>
<body>


	<div class="container">
<div class="row">
<div class="col-sm-12">
<h1>Change Password</h1>
</div>
</div>
<div class="row">
<div class="col-sm-6 col-sm-offset-3">
<!-- <p class="text-center">Use the form below to change your password. Your password cannot be the same as your username.</p>
 -->
@if($errors->any())
    @foreach ($errors->all() as $error)
        <div class="alert alert-danger">{{ $error }}</div>
    @endforeach
@endif
<form method="post" id="passwordChangeForm" action="{{route('change.password.custom')}}">
	{{ csrf_field()}}
<input type="password" class="input-lg form-control" name="password" id="password1" placeholder="New Password" autocomplete="off">

<input type="password" class="input-lg form-control" name="password_confirmation" id="password2" placeholder="Repeat Password" autocomplete="off">

<input type="submit" class="col-xs-12 btn btn-primary btn-load btn-lg" data-loading-text="Changing Password..." value="Change Password">
</form>
</div><!--/col-sm-6-->
</div><!--/row-->
</div>




</body>
</html>