@extends('layouts.app')
@section('title','Loyality Award')
@section('content')
<div class="content mt-3">
	<div class="animated">
		<div class="row">
			                <div class="col-md-12">
                	@if(session()->has('success'))
                	<div class="alert alert-success">
                		{{ session()->get('success') }}
                	</div>
                	@endif
                	@if( count( $errors ) > 0 )
                	@foreach($errors->all() as $error)
                	<div class="alert alert-danger">
                		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                		{{ $error }}
                	</div>
                	@endforeach
                	@endif
                    <div class="card">
                    <div class="card-header">
                            <strong class="card-title">Add Awards </strong>

                    </div>	
                    <div class="card-body">
                    <div id="messages"></div>	
                    <form name="assign_item_form" id="assign_item_form" method="PUT" action="">
                        {{ csrf_field() }}
                    <div class="form-group col-md-4">
                            <label for="employee" class=" form-control-label"><strong>Name</strong> <span id="req">*</span></label>
                   <select name="emp_no" id="emp_no" class="empdrop form-control"></select>
                    </div>
                    <div class="form-group col-md-4">
                            <label for="employee" class=" form-control-label"><strong>Type</strong> <span id="req">*</span></label>   
                            <select name="type" id="type" class="form-control">

                            	<option value="3">3 years</option>
                            	<option value="5">5 years</option>
                            </select>  
                    </div>  
                    <div class="form-group col-md-4">
                            <label for="employee" class=" form-control-label"><strong>Amount</strong> <span id="req">*</span></label>
                            <input type="text" name="amount" class="form-control" value="{{old('amount', $awards->amount)}}">    
                    </div>
                        	<input type="submit" name="submit" value="Submit" class="btn btn-primary">
 	                      	<button onclick="window.history.go(-1); return false;" class="btn btn-danger btn-md">
                          	<i class="fa fa-ban"></i> Back
                        	</button>
                    </form>
                    </div>
                    </div>
                </div>

		</div>

	</div>
</div>

@endsection