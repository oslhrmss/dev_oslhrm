@extends('layouts.app')
@section('title','Loyality Award')
@section('content')

<div class="content mt-3">
            <div class="animated">
            <div class="row">

                <div class="col-md-12">
                	@if(session()->has('success'))
                	<div class="alert alert-success">
                		{{ session()->get('success') }}
                	</div>
                	@endif
                	@if( count( $errors ) > 0 )
                	@foreach($errors->all() as $error)
                	<div class="alert alert-danger">
                		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                		{{ $error }}
                	</div>
                	@endforeach
                	@endif
                    <div class="card">
                    <div class="card-header">
                            <strong class="card-title">Add Awards </strong>

                    </div>	
                    <div class="card-body">
                    <div id="messages"></div>	
                    <form name="assign_item_form" id="assign_item_form" method="POST" action="{{ route('add.loyality.awards')}}">
                        {{ csrf_field() }}
                    <div class="form-group col-md-4">
                            <label for="employee" class=" form-control-label"><strong>Name</strong> <span id="req">*</span></label>
                   <select name="emp_no" id="emp_no" class="empdrop form-control"></select>
                    </div>
                    <div class="form-group col-md-4">
                            <label for="employee" class=" form-control-label"><strong>Type</strong> <span id="req">*</span></label>   
                            <select name="type" id="type" class="form-control">
                            	<option value="3">3 years</option>
                            	<option value="5">5 years</option>
                            </select>  
                    </div>  
                    <div class="form-group col-md-4">
                            <label for="employee" class=" form-control-label"><strong>Amount</strong> <span id="req">*</span></label>
                            <input type="text" name="amount" class="form-control" >    
                    </div>
                        	<input type="submit" name="submit" value="Submit" class="btn btn-primary">
 	                      	<button onclick="window.history.go(-1); return false;" class="btn btn-danger btn-md">
                          	<i class="fa fa-ban"></i> Back
                        	</button>
                    </form>
                    </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Loyality Awards</strong>
                        </div>
                        <div class="card-body">
                            <div id="messages"></div>
                            <table id="example" class="table table-striped table-bordered">
                            	<thead><tr><th>Employee ID</th><th>Name</th><th>Award</th><th>Amount</th><th>Awarded At</th><th>Year</th><th>Action</th></tr></thead><tbody>
                            	@foreach($awards  as $award)
                            	<tr>
                            		<td>{{$award->emp_no}}</td>
                            		<td>{{$award->employee->name}}</td>
                            		<td>{{$award->type}} Year Loyality Award</td>
                            		<td>{{$award->amount}}</td>
                            		<td>{{$award->date}}</td>
                            		<td>{{$award->year}}</td>
                            		<td><a href="#" ><i class="fa fa-pencil"></i></a> &nbsp; <a href="#" ><i class="fa fa-trash"></i></a></td>
                            	</tr>		
                            	@endforeach
                            	</tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>

            </div>
        </div>
<script src="{{ asset('assets/js/hrm.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/lib/data-table/datatables.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/data-table/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/data-table/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/data-table/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/data-table/jszip.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/data-table/pdfmake.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/data-table/vfs_fonts.js') }}"></script>
<script src="{{ asset('assets/js/lib/data-table/buttons.html5.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/data-table/buttons.print.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/data-table/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/data-table/datatables-init.js') }}"></script>
<script>
        $(document).ready(function(){
        var url = "{{route('emp.dropdown')}}";
        employeeChosenDropDown(url);
        });
        jQuery('#example').DataTable({
        	dom: 'Bfrtip',
        	buttons: [
        	'copy', 'csv', 'excel', 'pdf', 'print','pageLength'
        	],
        	destroy:true,
        });


</script>        
@endsection


