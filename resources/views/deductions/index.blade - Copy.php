@extends('layouts.app')
@section('title','Loan Deductions')
@section('content')



        <div class="content mt-3">
            <div class="animated">
                <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Loan Deduction hogai hai ab kia karain.....</strong>
                            <p>Samajh agya hain k kesy krna hai...</p>
                            
                        </div>
                        <div class="card-body">
                        <div id="messages"></div>
                        <form name="loan_deduction_form" id="loan_deduction" method="POST">
                            {{csrf_field()}}
                        <div class="form-group col-md-6">
                        <label for="employee" class=" form-control-label"><strong>Sub Company</strong> <span id="req">*</span></label>
                        <select name="company_id" class="subcomp form-control"></select>
                        </div>
                        <div class="form-group col-md-6">
                                <label for="employee" class=" form-control-label"><strong>Month</strong> <span id="req">*</span></label>
                                <select name="month" class="form-control">
                                    @foreach(App\Utilities\Utils::getMonthList() as $key => $value)
                                    <option value="{{$value}}">{{ $value }}</option>
                                    @endforeach
                                </select>
                        </div>
                        <div class="card-footer">
                        <label for="employee" class="form-control-label"></label>    
                        <button class="btn btn-primary btn-md" id="get_leave_deduction">
                          <i class="fa fa-plus"></i> Generate Loan List
                        </button>
                        <button type="reset" class="btn btn-danger btn-md">
                          <i class="fa fa-ban"></i> Reset
                        </button>
                        </div>
                        </form>

                        </div>
                    </div>
                    <div class="card">

                        <div class="card-body">
                            <div id="messages"></div>
                            <div class="row">

                               <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <table id="example" class="table table-striped table-bordered"></table>
                                    </div>
                                </div>
                            </div>
                        </div>    
                        <div class="card-footer">
                        <button class="btn btn-primary btn-md"  id="deduction_btn">
                          <i class="fa fa-dot-circle-o"></i> Generate Deductions
                        </button>
                        </div>
                        

                        </div>
                    </div>

                </div>
            </div>
               
                                <div class="modal fade" id="amountEditModal" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">

                            <div class="modal-header">
                                <h5 class="modal-title" id="smallmodalLabel">Edit Deduction Amount</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            

                            <div class="modal-body">
                            <div id="messages"></div>    
                            <form id="modifyForm" method="post" action="">
                            {{csrf_field()}}
                            <div class="form-group col-md-12">
                                <label for="employee" class=" form-control-label"><strong>Amount</strong> <span id="req">*</span></label>
                                <input type="text" name="deduction_amount" class="time form-control" id="deduction_amount" placeholder="" required>
                            </div>                         
                            </form>
                            
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="button" id="submit" class="btn btn-primary">Confirm</button>
                            </div>
                        </div>
                    </div>
                </div>    
</div>
</div>
<script src="{{ asset('assets/js/hrm.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#deduction_btn").hide();
        var url = "{{route('sub.company')}}";
        getSubCompany(url);
    });

    jQuery(document).on("click","#get_leave_deduction",function(e){
            e.preventDefault();
        var url =  "{{route('deduction.emp.list')}}"
        employeeListWhoseBalancNonZero(url);
    });

    $(document).on('click','#relax',function(e){
        //e.preventDefault();

    var id = $(this).parent().data('id');
    var edit_url = "{{route('relax',':loanid')}}";
    var url1 = edit_url.replace(':loanid', id);
    var url2 =  "{{route('deduction.emp.list')}}";
    loanRelaxation(url1,url2); 
    });
    
    $(document).on('click','#remove_relax',function(e){
        //e.preventDefault();

    var id = $(this).parent().data('id');
    var edit_url = "{{route('remove.relax',':loanid')}}";
    var url1 = edit_url.replace(':loanid', id);
    var url2 =  "{{route('deduction.emp.list')}}";
    removeLoanRelaxation(url1,url2); 
    });

    $('#deduction_btn').click(function(e){
        e.preventDefault();
        var url = "{{route('loan.deduct')}}";
        loanDeduct(url);
    });

    jQuery(document).on("click","#amountEditBtn",function(e){
            e.preventDefault();
            getAdvSalId($(this).parent("td").data('id'));
    });

    jQuery(document).on("click","#submit",function(e){
            e.preventDefault();
            var url = "{{ route('edit.deduct.amount') }}";
            setDeductionAmountByFinance(url);            
    });
    

    
</script>

@endsection