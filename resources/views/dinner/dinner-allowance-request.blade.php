@extends('layouts.app')
@section('title','Dinner Allowance')
@section('content')

        <div class="content mt-3">
            <div class="animated">
            <div class="row">
                
                <div class="col-md-12">
                    <div class="card">

                    <div class="card-header"><strong class="card-title">Dinner Allowance Request Form</strong></div>

                    <div class="card-body">
                    <!-- <table id="example" class="table table-striped table-bordered"></table> -->
                            <div id="messages"></div>    
                            <form id="dinnerAllowanceForm" method="post" action="">
                            {{csrf_field()}}
                            <div class="form-group col-md-4">
                                <label for="employee" class=" form-control-label"><strong>Request By:</strong></label>
                                <input name="emp_id" id="emp_id" class="form-control" disabled="disabled" placeholder="{{Auth::user()->name}}"/>
                            </div>

                            <div class="form-group col-md-4">
                                <label for="employee" class=" form-control-label"><strong>Request Date:</strong> <span id="req">*</span></label>
                                <input type="date" name="allowance_date" id="allowance_date" class="form-control" />
                            </div>

                            <div class="form-group col-md-4">
                                <label for="employee" class=" form-control-label"><strong>Reason:</strong> <span id="req">*</span></label>
                                <textarea type="text" rows="3" name="reason" id="reason" class="form-control"></textarea>
                            </div>
                                
                    </div>
                            <div class="card-header">
                                <button type="reset" class="btn btn-danger" data-dismiss="card"><i class="fa fa-ban"></i> Reset</button>
                                <button type="submit" id="allowance_submit" class="btn btn-primary"><i class="fa fa-send-o"></i> Submit</button>
                            </div>
                            </form>
                    </div>
                    </div>

                </div>
                <!---col end---->

                <!---item listings start--->
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header"><i class="fa fa-shopping-bag">&nbsp;&nbsp;</i><strong class="card-title">Requested Allowance List</strong></div>  
                        <div class="card-body">
                            <table id="example" class="table table-striped table-bordered"></table>
                        </div>
                    </div>
                </div>
                <!---item listings end--->

            </div>
            </div>
        </div>


        

                

         



<script src="{{ asset('assets/js/hrm.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    //
    $(document).on('click','#allowance_submit', function(e){
        e.preventDefault();
        var url  = "{{ route('dinner.allowance.request') }}";
        allowanceRequest(url);
    });

    $(document).ready(function(){
        var url  = "{{route('my.allowance.list')}}";
        getMyAllowanceList(url);
    });

</script>
@endsection