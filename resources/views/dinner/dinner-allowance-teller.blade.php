@extends('layouts.app')
@section('title','Dinner Allowance Teller')
@section('content')

        <div class="content mt-3">
            <div class="animated">
            <div class="row">
                
                <div class="col-md-12">
                    <div class="card">

                    <div class="card-header"><strong class="card-title">Dinner Allowance Teller</strong></div>

                    <div class="card-body">
                            <div id="messages"></div>    
                            <table id="example" class="table table-striped table-bordered"></table>
                    </div>
                    </div>
                </div>
            </div>
            </div>
        </div>




<script src="{{ asset('assets/js/hrm.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    
    jQuery(document).ready(function(){
            var url = "{{ route('get.approved.allowances') }}";
            getApprovedAllowances(url);
        });

    $(document).on('click','.lins-paid',function(e){
        e.preventDefault();
      
        var id = $(this).parent().attr('id');
        var edit_url =  "{{ route('dinner.allowance.paid.action',':id') }}";
        url1 = edit_url.replace(':id', id)
        var statusValue = paidStatusValueReplace($(this).text());
        
        dinnerAllowancePaidAction(url1,statusValue);
     }); 


</script>
@endsection