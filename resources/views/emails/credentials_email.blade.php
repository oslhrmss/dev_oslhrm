<h3>Welcom to Oceanic Star Liner HR Portal</h3>


<p>Dear {{ ucfirst($content['name']) }},</p>
&nbsp;
<p>Your official Email <strong>{{$content['email']}}</strong></p>
&nbsp;
<p>Kindly use these Credentials for HRM login</p>


<div>

	<p><strong>Portal Link </strong> = <strong>{{url('/')}}</strong></p>
	<p><strong>Login Id </strong> = <strong>{{$content['emp_code']}}</strong></p>
	<p><strong>Password </strong> = <strong>{{$content['password']}}</strong></p>
	
</div>

Regards,
<br>
Human Resource Department OSL.
<br>
<center><small>Star Liners &copy; {{date('Y')}} </small></center>	