@extends('layouts.app')
@section('title','Add Employee')
@section('content')

        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Dashboard</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="#">Dashboard</a></li>
                            <li><a href="#">Employee</a></li>
                            <li class="active">Add Employee</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        
<form name="employeeForm" id="employeeForm" method="POST" action="{{ route('add.employee.post') }}" enctype="multipart/form-data">
    {{ csrf_field()}}
        <div class="content mt-3">
            @if ($errors->any())
            <div class="alert alert-danger">
       @foreach ($errors->all() as $error)
        <strong>{{ $error }}</strong><br>
    @endforeach
    </div>
@endif
            <div class="animated fadeIn">
    <div class="row">
    <div class="col-lg-12">
        <div class="card">
        <div class="card-header"><strong>Personal </strong><small> Data</small></div>
        <div class="card-body">
        <div class="form-group col-md-12"><label for="company" class=" form-control-label">Profile Picture <span id="req">*</span></label><input name ="profile_pic" type="file" id="profile_pic" placeholder="Profile Picture" class="form-control" ></div>        
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">First name <span id="req">*</span></label><input name ="first" type="text" id="first_name" placeholder="First name" class="form-control" value="{{old('first')}}" ></div>
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Last name <span id="req">*</span></label><input name ="last" type="text" id="last_name" placeholder="Last name" class="form-control" value="{{old('last')}}" ></div>
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Father name <span id="req">*</span></label><input name ="father_name" type="text" id="father_name" placeholder="Father name" class="form-control" value="{{old('father_name')}}" ></div>
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Gender <span id="req">*</span></label><select class="form-control" name="gender"><option value="male" {{old('gender') == 'male' ? 'selected' : ''}}>Male</option><option value="female" {{old('gender') == 'female' ? 'selected' : ''}}>Female</option></select></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Nationality <span id="req">*</span></label><input name ="nationality" type="text" id="Nationality" placeholder="Nationality" class="form-control" value="{{old('father_name')}}" ></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Religion <span id="req">*</span></label><input name ="religion" type="text" id="religion" placeholder="Religion" class="form-control" value="{{old('father_name')}}" ></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Marital Status <span id="req">*</span></label><select class="form-control" name="m_status"><option value="single" {{old('m_status') == 'single' ? 'selected' : ''}}>Single</option><option value="marreid" {{old('m_status') == 'marreid' ? 'selected' : ''}}>Married</option><option value="divorced" {{old('m_status') == 'divorced' ? 'selected' : ''}}>Divorced</option><option value="widow" {{old('m_status') == 'widow' ? 'selected' : ''}}>Widow</option></select></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Date of birth<span id="req">*</span></label><input name ="dob" type="text" id="dob" placeholder="" class="date form-control" value="{{old('dob')}}"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Place of Birth</label><input name ="place_of_birth" type="text" id="place_of_birth" placeholder="" class="form-control" value="{{old('place_of_birth')}}"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">EOBI no</label><input name ="eobi" type="text" id="eobi" placeholder="" class="form-control" value="{{old('eobi')}}"></div>
        
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">CNIC no</label><input name ="cnic" type="text" id="cnic" placeholder="" class="form-control" value="{{old('cnic')}}"></div>
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Place of Issue</label><input name ="cnic_place" type="text" id="cnic_place" placeholder="" class=" form-control" value="{{old('cnic_place')}}"></div>
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Date of Issue</label><input name ="cnic_date" type="text" id="cnic_date" placeholder="" class="date form-control"  value="{{old('cnic_date')}}"></div>
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Expiry</label><input name ="cnic_expiry" type="text" id="cnic_expiry" placeholder="" class="date form-control" value="{{old('cnic_date')}}"></div>
        
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Passport no</label><input name ="passport_no" type="text" id="cnic" placeholder="" class="form-control" value="{{old('passport_no')}}"></div>
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Place of Issue</label><input name ="passport_place" type="text" id="cnic_place" placeholder="" class=" form-control" value="{{old('passport_place')}}"></div>
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Date of Issue</label><input name ="passport_date" type="text" id="cnic_date" placeholder="" class="date form-control" value="{{old('passport_date')}}"></div>
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Expiry</label><input name ="passport_expiry" type="text" id="cnic_expiry" placeholder="" class="date form-control" value="{{old('passport_expiry')}}"></div>
        
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Visa no</label><input name ="visa_no" type="text" id="cnic" placeholder="" class="form-control" value="{{old('visa_no')}}"></div>
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Place of Issue</label><input name ="visa_place" type="text" id="cnic_place" placeholder="" class=" form-control" value="{{old('visa_no')}}"></div>
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Date of Issue</label><input name ="visa_date" type="text" id="cnic_date" placeholder="" class="date form-control" value="{{old('visa_date')}}"></div>
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Expiry</label><input name ="visa_expiry" type="text" id="cnic_expiry" placeholder="" class="date form-control" value="{{old('visa_expiry')}}"></div>
        
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Iqama no</label><input name ="iqama_no" type="text" id="cnic" placeholder="" class="form-control" value="{{old('iqama_no')}}"></div>
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Place of Issue</label><input name ="iqama_place" type="text" id="cnic_place" placeholder="" class=" form-control" value="{{old('iqama_place')}}"></div>
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Date of Issue</label><input name ="iqama_date" type="text" id="cnic_date" placeholder="" class="date form-control" value="{{old('iqama_date')}}"></div>
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Expiry</label><input name ="iqama_expiry" type="text" id="cnic_expiry" placeholder="" class="date form-control" value="{{old('iqama_expiry')}}"></div>
 
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Vehicle no</label><input name ="vehicle_no" type="text" id="cnic" placeholder="" class="form-control" value="{{old('vehicle_no')}}"></div>
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Vehicle Name</label><input name ="vehicle_name" type="text" id="cnic_place" placeholder="" class=" form-control" value="{{old('vehicle_name')}}"></div>
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Vehicle Model</label><input name ="vehicle_model" type="text" id="cnic_date" placeholder="" class=" form-control" value="{{old('vehicle_model')}}"></div>
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Date issued</label><input name ="vehicle_date" type="text" id="cnic_expiry" placeholder="" class="date form-control" value="{{old('vehicle_date')}}"></div>
 
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Driving Liscence No</label><input name ="liscence_no" type="text" id="cnic" placeholder="" class="form-control" value="{{old('liscence_no')}}"></div>
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Place of Issue</label><input name ="liscence_place" type="text" id="cnic_place" placeholder="" class=" form-control" value="{{old('liscence_place')}}"></div>
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Date of Issue</label><input name ="liscence_date" type="text" id="cnic_date" placeholder="" class="date form-control" value="{{old('liscence_date')}}"></div>
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Expiry</label><input name ="liscence_expiry" type="text" id="cnic_expiry" placeholder="" class="date form-control" value="{{old('liscence_expiry')}}"></div>

        </div>
    </div>
    </div>
    </div>
            

    <div class="row">
    <div class="col-lg-12">
        <div class="card">
        <div class="card-header"><strong>Contract Data</strong><small> (Filled by HR department only.)</small></div>
        <div class="card-body">
        <!-- <div class="form-group col-md-4"><label for="company" class=" form-control-label">Position Title</label><input name ="position_title" type="text" id="cnic" placeholder="" class="form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Position No</label><input name ="position_no" type="text" id="cnic" placeholder="" class="form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Dept Name </label><input name ="dept_name" type="text" id="cnic" placeholder="" class="form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Dept No </label><input name ="dept_no" type="text" id="cnic" placeholder="" class="form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Date of Hiring </label><input name ="date_of_hiring" type="text" id="cnic" placeholder="" class="date form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Current Salary </label><input name ="current_salary" type="text" id="cnic" placeholder="" class="form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Other Benifits</label><input name ="other_benifits" type="text" id="cnic" placeholder="" class="form-control"></div> -->
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Company</label><select name="company" id="company" class="form-control">
        <option value="">Select Compnay</option>
        </select>
        </div>
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Department</label><select name="department" id="department" class="form-control">
        <option value="">Select Department</option>
        </select>
        </div>
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Sub Department</label><select name="subdepartment" id="sub_department" class="form-control">
        <option value="">Select Sub Department</option>
        </select>
        </div>
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Position</label><select name="designation" id="designation" class="form-control">
        <option value="">Select Position</option>
        </select>
        </div>
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Level</label><select name="role" id="role" class="positionsdrop form-control"></select>
        </div>
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Date of Hiring </label><input name ="date_of_hiring" type="text" id="cnic" placeholder="" class="date form-control"  value="{{old('date_of_hiring')}}"></div>
        <div class="form-group col-md-3">
            <label for="company" class=" form-control-label">Current Salary </label><input name ="current_salary" type="text" id="cnic" placeholder="" class="form-control" value="{{old('current_salary')}}">
        </div>
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Region</label><select name="region" id="region" class="regiondrop form-control"></select>
        </div>
        <div class="form-group col-md-3">
            
            <label for="company" class=" form-control-label">Employee Type</label>
            <select name="emp_type" id="emp_type" class="form-control emptypesdrop">
            </select>
            
        </div>
        <div class="form-group col-md-3 permanent_date"><label for="company" class=" form-control-label">Date of Permanent </label><input name ="permanent_at" type="text" id="permanent_at" placeholder="" class="date form-control"  value="{{old('permanent_at')}}"></div>
        <div class="form-group col-md-3"><label class="form-control-label"><input type="checkbox" name="skip_deduction" value="1"> Skip Deduction</label>
        </div>

<!--         <div class="form-group col-md-4">
            <label for="company" class=" form-control-label">Provident Fund Slab </label><select name ="pf_slabs"  class="pfslabs form-control"></select>
        </div> -->
        
        </div>        
    </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header"><strong>PF Slabs</strong><small></small></div>
            <div class="card-body">
                <div class="form-group col-md-6"> <label for="company" class=" form-control-label">Provident Fund Slab </label><select name ="pf_slabs"  class="pfslabs form-control"></select></div>
                <div class="form-group col-md-6"><label for="company" class=" form-control-label">PF Efective Date</label><input name ="pf_effective_date" type="text" id="cnic" placeholder="" class="date form-control"  value="{{old('pf_effective_date')}}"></div>
                <div class="form-group col-md-6"><label for="company" class=" form-control-label">EOBI Amount</label><input name ="eobi" type="text" id="eobi" placeholder="" class=" form-control"  value="{{old('eobi_amount')}}"></div>
                <div class="form-group col-md-6"><label for="company" class=" form-control-label">Fuel Amount </label><input name ="fuel_amount" type="text" id="fuel_amount" placeholder="" class=" form-control"  value="{{old('fuel_amount')}}"></div>
               <div class="form-group col-md-6"><label for="company" class=" form-control-label">Parking Amount </label><input name ="parking_amount" type="text" id="parking_amount" placeholder="" class=" form-control"  value="{{old('parking_amount')}}"></div>

            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-lg-6">
        <div class="card">
        <div class="card-header"><strong>Contact Address</strong><small> (Current contact address)</small></div>
        <div class="card-body">
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">House#</label><input name ="house_no" type="text" id="cnic" placeholder="" class="form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Builiding</label><input name ="building" type="text" id="cnic" placeholder="" class="form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Street </label><input name ="street" type="text" id="cnic" placeholder="" class="form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Area </label><input name ="area" type="text" id="cnic" placeholder="" class="form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">PO Box #</label><input name ="po_box" type="text" id="cnic" placeholder="" class="form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">City </label><input name ="city" type="text" id="cnic" placeholder="" class="form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Home Landline</label><input name ="landline" type="text" id="cnic" placeholder="" class="form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Mobile</label><input name ="mobile_no" type="text" id="cnic" placeholder="" class="form-control"></div>
       

        </div>

    </div>
    </div>  
    <div class="col-lg-6">
        <div class="card">
        <div class="card-header"><strong>Contact Address</strong><small> (Permanent contact address)</small></div>
        <div class="card-body">
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">House#</label><input name ="p_house_no" type="text" id="cnic" placeholder="" class="form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Builiding</label><input name ="p_building" type="text" id="cnic" placeholder="" class="form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Street </label><input name ="p_street" type="text" id="cnic" placeholder="" class="form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Area </label><input name ="p_area" type="text" id="cnic" placeholder="" class="form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">PO Box #</label><input name ="p_po_box" type="text" id="cnic" placeholder="" class="form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">City </label><input name ="p_city" type="text" id="cnic" placeholder="" class="form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Home Landline</label><input name ="p_landline" type="text" id="cnic" placeholder="" class="form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Mobile</label><input name ="p_mobile_no" type="text" id="cnic" placeholder="" class="form-control"></div>
       

        </div>

    </div>
    </div>  
    </div><!-- .row -->
    <div class="row">
    <div class="col-lg-12">
        <div class="card">
        <div class="card-header"><strong>Email & Mobile</strong><small></small></div>
        <div class="card-body">
                <div class="form-group col-md-6"><label for="company" class=" form-control-label">Personal Email <span id="req">*</span></label><input name ="c_email" type="text" id="c_email" placeholder="" class="form-control" required></div>
                <div class="form-group col-md-6"><label for="company" class=" form-control-label">OSL Email <span id="req">*</span></label><input name ="p_email" type="text" id="c_email" placeholder="" class="form-control" required></div>
                <div class="form-group col-md-6"><label for="company" class=" form-control-label">Mobile</label><input name ="mobile_no" type="text" id="cnic" placeholder="" class="form-control"></div> 
                <div class="form-group col-md-6"><label for="company" class=" form-control-label">OSL Mobile</label><input name ="p_mobile_no" type="text" id="cnic" placeholder="" class="form-control"></div>  

        </div>
    </div>
    </div>
    </div>

    <div class="row">
    <div class="col-lg-12">
        <div class="card">
        <div class="card-header"><strong>Concern Persons</strong><small></small></div>
        <div class="card-body">
                <div class="form-group col-md-6"><label for="company" class=" form-control-label">Report To <span id="req">*</span></label><select name="report_to" class="empdrop form-control"></select></div>
                <div class="form-group col-md-6"><label for="company" class=" form-control-label">Reporting Manager<span id="req">*</span></label><select name="report_manager" class="managersdrop form-control"></select></div>
  

        </div>
    </div>
    </div>
    </div>

    <div class="row">
    <div class="col-lg-12 educationRow">
    <div class="education">
        <div class="card">
        <div class="card-header"><strong>Education</strong><small> </small></div>
        <div class="card-body">
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Degree Title</label><input name ="degree_title[]" type="text" id="cnic" placeholder="" class="form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Institute Name</label><input name ="institute[]" type="text" id="cnic" placeholder="" class="form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">CGPA/Grade/Percentage </label><input name ="cgpa_grade[]" type="text" id="cnic" placeholder="" class="form-control"></div>
        <div class="form-group col-md-6"><label for="company" class=" form-control-label">Passing Date </label><!-- <input name ="passing_from[]" type="text" id="passing_from" placeholder="From" class="date form-control">
         --> 
           <select class="form-control" id="year" name="passing_from[]">
            <option value="">Select From</option>
            @foreach($yearFrom as $yf)
            <option value="{{$yf}}">{{$yf}}</option>
            @endforeach
        </select>
        </div>
        <div class="form-group col-md-6"><label for="company" class=" form-control-label"> &nbsp;</label><!-- <input name ="passing_to[]" type="text" id="cnic" placeholder="To" class="date form-control"> -->
        <select class="yearto form-control" name="passing_to[]">
            <option value="">Select To</option>
            @foreach($yearTo as $yt)
            <option value="{{$yt}}">{{$yt}}</option>
            @endforeach
        </select>
        </div>

        </div>
    </div>
    </div>
    </div>  
</div> 
<div class="row">
    <div class="col-lg-12">
        <div class="card">
        <span class="btn btn-primary fa fa-plus" id="add-education">  Add Education</span>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 employerRow">
        <div class="employer">
        <div class="card">
        <div class="card-header"><strong>Employer</strong><small> </small></div>
        <div class="card-body">
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Employer name</label><input name ="last_emp_name[]" type="text" id="cnic" placeholder="Last employer name" class="form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Last Designation</label><input name ="last_designation[]" type="text" id="cnic" placeholder="Last Designation" class="form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">From</label><input name ="last_emp_from[]" type="text" id="cnic" placeholder="" class="date form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">To </label><input name ="last_emp_to[]" type="text" id="cnic" placeholder="" class="date form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Ref #</label><input name ="last_emp_contact[]" type="text" id="cnic" placeholder="" class="form-control"></div>

        </div>
        </div>

    </div>
    </div>  
</div> 
 <!--/.row  -->
 <div class="row">
    <div class="col-lg-12">
        <div class="card">
        <span class="btn btn-primary fa fa-plus" id="add-employer">  Add Employer</span>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <div class="card">
        <div class="card-header"><strong>Primary Emergency Contact Detail</strong><small></small></div>
        <div class="card-body">
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Emergency Name</label><input name ="p_emergency_name" type="text" id="p_emergency_name" placeholder="" class="form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Relationship</label><input name ="relationship" type="text" id="relationship" placeholder="" class="form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Address </label><input name ="p_emergency_address" type="text" id="p_emergency_address" placeholder="" class="form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Telephone</label><input name ="telephone" type="text" id="telephone" placeholder="" class="form-control"></div>
     
        </div>

    </div>
    </div>  
    <div class="col-lg-6">
        <div class="card">
        <div class="card-header"><strong>Secondry Emergency Contact Detail</strong><small></small></div>
        <div class="card-body">
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Emergency Name</label><input name ="s_emergency_name" type="text" id="s_emergency_name" placeholder="" class="form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Relationship</label><input name ="s_relationship" type="text" id="s_relationship" placeholder="" class="form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Address </label><input name ="s_emergency_address" type="text" id="s_emergency_address" placeholder="" class="form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Telephone</label><input name ="s_telephone" type="text" id="s_telephone" placeholder="" class="form-control"></div>

        </div>

    </div>
    </div>  
    </div><!-- .row -->
    <div class="row">
    <div class="col-lg-6">
        <div class="card">
        <div class="card-header"><strong>Primary Benificiary Details</strong><small></small></div>
        <div class="card-body">
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Benificiary Name</label><input name ="p_benificiary_name" type="text" id="p_emergency_name" placeholder="" class="form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Relationship</label><input name ="p_ben_relationship" type="text" id="p_ben_relationship" placeholder="" class="form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Address </label><input name ="p_ben_address" type="text" id="p_ben_address" placeholder="" class="form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Telephone</label><input name ="p_ben_telephone" type="text" id="p_telephone" placeholder="" class="form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">NIC#</label><input name ="p_ben_nic" type="text" id="p_ben_nic" placeholder="" class="form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Email</label><input name ="p_ben_email" type="text" id="p_ben_email" placeholder="" class="form-control"></div>
     
        </div>

    </div>
    </div>  
    <div class="col-lg-6">
        <div class="card">
        <div class="card-header"><strong>Secondry Benificiary Details</strong><small></small></div>
        <div class="card-body">
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Benificiary Name</label><input name ="s_benificiary_name" type="text" id="s_emergency_name" placeholder="" class="form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Relationship</label><input name ="s_ben_relationship" type="text" id="s_ben_relationship" placeholder="" class="form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Address </label><input name ="s_ben_address" type="text" id="s_emergency_address" placeholder="" class="form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Telephone</label><input name ="s_ben_telephone" type="text" id="s_telephone" placeholder="" class="form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">NIC#</label><input name ="s_ben_nic" type="text" id="s_ben_nic" placeholder="" class="form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Email</label><input name ="s_ben_email" type="text" id="s_ben_email" placeholder="" class="form-control"></div>
        </div>

    </div>
    </div>  
    </div><!-- .row -->
    <div class="row">
    <div class="col-lg-12 dependentRow">
      <div class="dependent">
        <div class="card">
        <div class="card-header"><strong>Dependents</strong><small></small></div>
        <div class="card-body">
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Name</label><input name ="d_name[]" type="text" id="d_name" placeholder="" class="form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Type</label><select name="d_type[]" id="d_type" class="form-control"><option value="">Select Type</option><option value="spouse">Spouse</option><option value="children">Children</option></select></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Gender</label><select name="d_gender[]" id="d_gender" class="form-control"><option value="">Select Gender</option><option value="male">Male</option><option value="female">Female</option></select></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Date of Birth</label><input name ="d_dob[]" type="text" id="d_dob" placeholder="" class="date form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">CNIC/B Form</label><input name ="d_nic[]" type="text" id="d_nic" placeholder="" class="form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Contact #</label><input name ="d_contact[]" type="text" id="d_contact" placeholder="" class="form-control"></div>
      
        </div>
    </div>
    </div>
    </div>
    </div>
    <div class="row">
    <div class="col-lg-12">
        <div class="card">
        <span class="btn btn-primary fa fa-plus" id="add-dependent">  Add Dependent</span>
        </div>
    </div>
</div>

 <div class="card-footer">
                        <button type="submit" id="submit"  class="btn btn-primary btn-sm" >
                      <i class="fa fa-dot-circle-o"></i> Submit
                      </button> 
 <!--                        onclick="this.disabled=true;this.value='Loading...';this.form.submit(); -->
                        <button type="reset" class="btn btn-danger btn-sm">
                          <i class="fa fa-ban"></i> Reset
                        </button>
                      </div>
</form>                      
</div>
</div>
<script src="{{ asset('assets/js/hrm.js') }}" type="text/javascript"></script>
<script>
$(document).ready(function(){
    jQuery(".date").datetimepicker({format: 'YYYY-MM-DD'});
    var regionurl = "{{route('get.regions')}}";
    getRegions(regionurl);
    var companyurl = "{{route('company')}}";
    company(companyurl);
    var departmenturl = "{{route('department')}}";
        department(departmenturl);
    var designationurl = "{{route('designation')}}";
        designation(designationurl);
    var emp_url = "{{route('emp.dropdown')}}";
        employeeDropDown(emp_url);
    var managerUrl = "{{route('manager.dropdown')}}";
        managerDropDown(managerUrl);
    var positionUrl = "{{route('get.position')}}";
        positionDropDown(positionUrl);
    var pfslabsUrl = "{{route('pf.slabs')}}";
        getPfSlabs(pfslabsUrl);
    var empTypeUrl = "{{route('get.emp.type')}}";
        empTypeDropDown(empTypeUrl);

    // permanent date div hidden 
    $(".permanent_date").hide();    

    var  old_role_val =  "{{old('role')}}" ;
    if(old_role_val !== '')
    {
        $("#role").val(old_role_val);
    }   

    var  old_company_val =  "{{old('company')}}" ;
    if(old_company_val !== '')
    {
        $("#company").val(old_company_val);
    } 

    var  old_depart_val =  "{{old('department')}}" ;
    if(old_depart_val !== '')
    {
        $("#department").val(old_depart_val);
    } 

    var  old_designation_val =  "{{old('designation')}}" ;
    if(old_designation_val !== '')
    {
        $("#designation").val(old_designation_val);
    } 
    var  old_reportto_val =  "{{old('report_to')}}" ;
    if(old_reportto_val !== '')
    {
        $("#report_to").val(old_reportto_val);
    }
    var  old_report_manager_val =  "{{old('report_manager')}}" ;
    if(old_report_manager_val !== '')
    {
        $("#report_manager").val(old_report_manager_val);
    }

    $('form').submit(function(){
        $(this).find(':submit').html('<i class="fa fa-dot-circle-o"></i> loading...');
     $(this).find(':submit').attr('disabled','disabled');
    });
});

$("#add-education").click(function(){

    jQuery(".education").after('<div class="card"><span class="text-right fa fa-times fa-2x" id="remove-edu"></span><div class="card-header"><strong>Education</strong><small> </small></div><div class="card-body"><div class="form-group col-md-4"><label for="company" class=" form-control-label">Degree Title</label><input name ="degree_title[]" type="text" id="cnic" placeholder="" class="form-control"></div><div class="form-group col-md-4"><label for="company" class=" form-control-label">Institute Name</label><input name ="institute[]" type="text" id="cnic" placeholder="" class="form-control"></div><div class="form-group col-md-4"><label for="company" class=" form-control-label">CGPA/Grade/Percentage </label><input name ="cgpa_grade[]" type="text" id="cnic" placeholder="" class="form-control"></div><div class="form-group col-md-6"><label for="company" class=" form-control-label">Passing Date </label><select class="yearto form-control" name="passing_from[]"><option value="">Select From</option>@foreach($yearFrom as $yf)<option value="{{$yf}}">{{$yf}}</option>@endforeach</select></div><div class="form-group col-md-6"><label for="company" class=" form-control-label"> &nbsp;</label><select class="yearto form-control" name="passing_to[]"><option value="">Select To</option>           @foreach($yearTo as $yt)<option value="{{$yt}}">{{$yt}}</option>        @endforeach</select></div></div></div>');
    jQuery(".date").datetimepicker({format: 'YYYY-MM-DD'});

});
$('.educationRow').on('click','#remove-edu',function() {
     
    jQuery(this).parent().remove();
});
$("#add-employer").click(function(){

    jQuery(".employer").after('<div class="card"><span class="text-right fa fa-times fa-2x" id="remove-emp"></span><div class="card-header"><strong>Employer</strong><small> </small></div><div class="card-body"><div class="form-group col-md-4"><label for="company" class=" form-control-label">Employer name</label><input name ="last_emp_name[]" type="text" id="cnic" placeholder="Last employer name" class="form-control"></div><div class="form-group col-md-4"><label for="company" class=" form-control-label">Last Designation</label><input name ="last_designation[]" type="text" id="cnic" placeholder="Last Designation" class="form-control"></div><div class="form-group col-md-4"><label for="company" class=" form-control-label">From</label><input name ="last_emp_from[]" type="text" id="cnic" placeholder="" class="date form-control"></div><div class="form-group col-md-4"><label for="company" class=" form-control-label">To </label><input name ="last_emp_to[]" type="text" id="cnic" placeholder="" class="date form-control"></div><div class="form-group col-md-4"><label for="company" class=" form-control-label">Ref #</label><input name ="last_emp_contact[]" type="text" id="cnic" placeholder="" class="form-control"></div></div></div>');
    jQuery(".date").datetimepicker({format: 'YYYY-MM-DD'});

});

$('.employerRow').on('click','#remove-emp',function() {
    jQuery(this).parent().remove();
});

$("#add-dependent").click(function(){

jQuery(".dependent").after('<div class="card"><span class="text-right fa fa-times fa-2x" id="remove-dep"></span><div class="card-header"><strong>Dependents</strong><small></small></div><div class="card-body"><div class="form-group col-md-4"><label for="company" class=" form-control-label">Name</label><input name ="d_name[]" type="text" id="d_name" placeholder="" class="form-control"></div><div class="form-group col-md-4"><label for="company" class=" form-control-label">Type</label><select name="d_type[]" id="d_type" class="form-control"><option value="spouse">Spouse</option><option value="children">Children</option></select></div><div class="form-group col-md-4"><label for="company" class=" form-control-label">Gender</label><select name="d_gender[]" id="d_gender" class="form-control"><option value="male">Male</option><option value="female">Female</option></select></div><div class="form-group col-md-4"><label for="company" class=" form-control-label">Date of Birth</label><input name ="d_dob[]" type="text" id="d_dob[]" placeholder="" class="date form-control"></div><div class="form-group col-md-4"><label for="company" class=" form-control-label">CNIC/B Form</label><input name ="d_nic[]" type="text" id="d_nic" placeholder="" class="form-control"></div><div class="form-group col-md-4"><label for="company" class=" form-control-label">Contact #</label><input name ="d_contact[]" type="text" id="d_contact" placeholder="" class="form-control"></div></div>');

jQuery(".date").datetimepicker({format: 'YYYY-MM-DD'});

});
$('.dependentRow').on('click','#remove-dep',function() {
    jQuery(this).parent().remove();
});

$(document).on("change",".emptypesdrop", function(){
    var selectedVal =  $(".emptypesdrop option:selected").text();
    if(selectedVal == 'Permanent')
    {
        $(".permanent_date").show();
    }else{
        $(".permanent_date").hide();
        $("#permanent_at").val("");

    }
});

$(document).on("change","#department", function(){
    var selectedVal =  $(this).val();
    var url = "{{ route('sub.department',':id') }}";
    url = url.replace(':id', selectedVal);
    subDepartment(url);

});
</script>
@endsection