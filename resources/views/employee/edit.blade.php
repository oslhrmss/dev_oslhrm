@extends('layouts.app')
@section('title','Edit Employee')
@section('content')

        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Dashboard</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="#">Dashboard</a></li>
                            <li><a href="#">Employee</a></li>
                            <li class="active">Add Employee </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        @if(session()->has('flash-message'))
        <div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
                                            <span class="badge badge-pill badge-success">Success</span>
                                                {{session()->get('flash-message')}}
                                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
        @endif
<form name="editEmployeeForm" method="post" action="{{ route('edit.employee.post') }}" enctype="multipart/form-data">
    {{ csrf_field()}}
    <input type="hidden" name="id" value="{{ $employee->id }}">
        <div class="content mt-3">
            @if ($errors->any())
            <div class="alert alert-danger">
               @foreach ($errors->all() as $error)
               <strong>{{ $error }}</strong><br>
               @endforeach
           </div>
           @endif    
            <div class="animated fadeIn">
    <div class="row">
    <div class="col-lg-12">
        <div class="card">
        <div class="card-header"><strong>Personal </strong><small> Data</small></div>
        <div class="card-body">
        <div class="form-group col-md-4 col-md-offset-2"><img src="{{asset('images/avatar')}}/{{$image}}" width="160" height="200"></div>
        <div class="form-group col-md-4 col-md-offset-2"><label>Employee Code:</label>&nbsp; <span><strong>{{$employee->emp_no}}</strong></span></div>    
        <div class="form-group col-md-8"><label for="company" class=" form-control-label">Profile Picture <span id="req">*</span></label><input name ="profile_pic" type="file" id="profile_pic" placeholder="Profile Picture" class="form-control" value="{{$employee->profile_pic}}"></div>    
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">First name <span id="req">*</span></label><input name ="first" type="text" id="first_name" placeholder="First name" class="form-control" value="{{ $first }}"></div>
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Last name <span id="req">*</span></label><input name ="last" type="text" id="last_name" placeholder="Last name" class="form-control" value="{{ $last }}"></div>
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Father name <span id="req">*</span></label><input name ="father_name" type="text" id="father_name" placeholder="Father name" class="form-control" value="{{ $employee->father_name }}"></div>
        <?php 
        if($employee->gender == 'male')
        {
            $m_selected =  'selected';
        }else{
            $m_selected =  '';
        }
        if($employee->gender == 'female')
        {
            $f_selected =  'selected';
        }else{
            $f_selected =  '';
        }
        if($employee->gender == 'other')
        {
            $o_selected=  'selected';
        }else{
            $o_selected =  '';
        }
        ?>
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Gender <span id="req">*</span></label><select class="form-control" name="gender">
        <option value="male" <?php echo $m_selected;?> >Male</option>
        <option value="female" <?php echo $f_selected;?> >Female</option>
        <option value="other" <?php echo $o_selected;?> >Other</option>
        </select></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Nationality <span id="req">*</span></label><input name ="nationality"  type="text" id="Nationality" placeholder="Nationality" class="form-control" value="{{ $employee->nationality }}"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Religion <span id="req">*</span></label><input name ="religion" type="text" id="religion" placeholder="Religion" class="form-control" value="{{ $employee->religion }}"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Marital Status <span id="req">*</span></label><select class="form-control" name="m_status">
            @if($employee->marital_status == 'single')
            <option value="single" selected>Single</option>
             <option value="marreid">Married</option>
             <option value="divorced">Divorced</option>
             <option value="widow" >Widow</option>
            @elseif($employee->marital_status == 'marreid')
               <option value="single">Single</option>
            <option value="marreid" selected>Married</option>
            <option value="divorced">Divorced</option>
             <option value="widow" >Widow</option>
            @elseif($employee->marital_status == 'divorced')
             <option value="single">Single</option>
             <option value="marreid">Married</option>
            <option value="divorced" selected>Divorced</option>
             <option value="widow" >Widow</option>
            @elseif($employee->marital_status == 'widow')
            <option value="single">Single</option>
             <option value="marreid">Married</option>
            <option value="divorced">Divorced</option>
            <option value="widow" selected>Widow</option>
            @else
             <option value="single">Single</option>
             <option value="marreid">Married</option>
             <option value="divorced">Divorced</option>
             <option value="widow" >Widow</option>
            @endif 
        </select></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Date of birth<span id="req">*</span></label><input name ="dob" type="text" id="dob" placeholder="" class="date form-control" value="{{ $employee->dob }}"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Place of Birth</label><input name ="place_of_birth" type="text" id="place_of_birth" placeholder="" class="form-control" value="{{ $employee->place_of_birth }}"> </div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">EOBI no</label><input name ="eobi" type="text" id="eobi" placeholder="" class="form-control" value="{{ $employee->eobi_no }}"></div>
        
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">CNIC no</label><input name ="cnic" type="text" id="cnic" placeholder="" class="form-control" value="{{ $employee->cnic_no }}"></div>
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Place of Issue</label><input name ="cnic_place" type="text" id="cnic_place" placeholder="" class=" form-control" value="{{ $employee->cnic_place_of_issue }}"></div>
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Date of Issue</label><input name ="cnic_date" type="text" id="cnic_date" placeholder="" class="date form-control" value="{{ $employee->cnic_date_of_issue }}"></div>
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Expiry</label><input name ="cnic_expiry" type="text" id="cnic_expiry" placeholder="" class="date form-control" value="{{ $employee->cnic_date_of_expiry }}"></div>
        
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Passport no</label><input name ="passport_no" type="text" id="cnic" placeholder="" class="form-control" value="{{ $employee->passport_no }}"></div>
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Place of Issue</label><input name ="passport_place" type="text" id="cnic_place" placeholder="" class=" form-control" value="{{ $employee->passport_place_of_issue }}"></div>
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Date of Issue</label><input name ="passport_date" type="text" id="cnic_date" placeholder="" class="date form-control" value="{{$employee->passport_date_of_issue}}"></div>
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Expiry</label><input name ="passport_expiry" type="text" id="cnic_expiry" placeholder="" class="date form-control" value="{{$employee->passport_date_of_expiry}}"></div>
        
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Visa no</label><input name ="visa_no" type="text" id="cnic" placeholder="" class="form-control" value="{{$employee->visa_no}}"></div>
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Place of Issue</label><input name ="visa_place" type="text" id="cnic_place" placeholder="" class=" form-control" value="{{$employee->visa_place_of_issue}}"></div>
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Date of Issue</label><input name ="visa_date" type="text" id="cnic_date" placeholder="" class="date form-control" value="{{$employee->visa_date_of_issue}}"></div>
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Expiry</label><input name ="visa_expiry" type="text" id="cnic_expiry" placeholder="" class="date form-control" value="{{$employee->visa_date_of_expiry }}"></div>
        
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Iqama no</label><input name ="iqama_no" type="text" id="cnic" placeholder="" class="form-control" value="{{$employee->iqama_no }}"></div>
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Place of Issue</label><input name ="iqama_place" type="text" id="cnic_place" placeholder="" class=" form-control" value="{{$employee->iqama_place_of_issue }}"></div>
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Date of Issue</label><input name ="iqama_date" type="text" id="cnic_date" placeholder="" class="date form-control" value="{{$employee->iqama_date_of_issue }}"></div>
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Expiry</label><input name ="iqama_expiry" type="text" id="cnic_expiry" placeholder="" class="date form-control"  value="{{$employee->iqama_date_of_expiry }}"></div>
 
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Vehicle no</label><input name ="vehicle_no" type="text" id="cnic" placeholder="" class="form-control" value="{{$employee->vehicle_no }}"></div>
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Vehicle Name</label><input name ="vehicle_name" type="text" id="cnic_place" placeholder="" class=" form-control" value="{{$employee->vehicle_name }}"></div>
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Vehicle Model</label><input name ="vehicle_model" type="text" id="cnic_date" placeholder="" class=" form-control" value="{{$employee->vehicle_model }}"></div>
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Date issued</label><input name ="vehicle_date" type="text" id="cnic_expiry" placeholder="" class="date form-control" value="{{$employee->vehicle_date_issue }}"></div>
 
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Driving Liscence No</label><input name ="liscence_no" type="text" id="cnic" placeholder="" class="form-control" value="{{$employee->liscence_no }}"></div>
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Place of Issue</label><input name ="liscence_place" type="text" id="cnic_place" placeholder="" class=" form-control"  value="{{$employee->liscence_place_of_issue }}"></div>
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Date of Issue</label><input name ="liscence_date" type="text" id="cnic_date" placeholder="" class="date form-control"  value="{{$employee->liscence_date_of_issue }}"></div>
        <div class="form-group col-md-3"><label for="company" class=" form-control-label">Expiry</label><input name ="liscence_expiry" type="text" id="cnic_expiry" placeholder="" class="date form-control"  value="{{$employee->liscence_date_of_expiry }}"></div>

        </div>
    </div>
    </div>
    </div>
            

    <div class="row">
    <div class="col-lg-12">
        <div class="card">
        <div class="card-header"><strong>Contract Data</strong><small> (Filled by HR department only.)</small></div>
        <div class="card-body">
        <!-- <div class="form-group col-md-4"><label for="company" class=" form-control-label">Position Title</label><input name ="position_title" type="text" id="cnic" placeholder="" class="form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Position No</label><input name ="position_no" type="text" id="cnic" placeholder="" class="form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Dept Name </label><input name ="dept_name" type="text" id="cnic" placeholder="" class="form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Dept No </label><input name ="dept_no" type="text" id="cnic" placeholder="" class="form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Date of Hiring </label><input name ="date_of_hiring" type="text" id="cnic" placeholder="" class="date form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Current Salary </label><input name ="current_salary" type="text" id="cnic" placeholder="" class="form-control"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Other Benifits</label><input name ="other_benifits" type="text" id="cnic" placeholder="" class="form-control"></div> -->
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Company</label><select name="company" id="company" class="form-control">
        <option value="">Select Compnay</option>
        @foreach($company as $value)
        @if($value->id  == $employee->company_id)
        <option value="{{$value->id}}" selected="selected">{{ $value->name }}</option>
        @else
        <option value="{{$value->id}}" >{{ $value->name }}</option>
        @endif
        @endforeach
        </select>
        </div>
<!-- regions id  -->
            


        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Department</label>
        <select name="department" id="department" class="form-control">
        <option value="">Select Department</option>
        @foreach($department as $value)
        @if($value->id  == $employee->dept_name)
        <option value="{{$value->id}}" selected="selected">{{ $value->name }}</option>
        @else
        <option value="{{$value->id}}" >{{ $value->name }}</option>
        @endif
        @endforeach
        </select>
        </div>
            <div class="form-group col-md-4"><label for="company" class=" form-control-label">Sub Department</label>
                <select name="subdepartment" id="sub_department" class="form-control">
                    <option value="">Select Sub Department</option>
                    @foreach($sub_department as $value)
                        @if($value->id  == $employee->sub_depart_id)
                            <option value="{{$value->id}}" selected="selected">{{ $value->name }}</option>
                        @else
                            <option value="{{$value->id}}" >{{ $value->name }}</option>
                        @endif
                    @endforeach
                </select>
            </div>


            <div class="form-group col-md-4"><label for="company" class=" form-control-label">Position</label><select name="designation" id="designation" class="form-control">
        <option value="">Select Position</option>
        @foreach($designation as $value)
        @if($value->id  == $employee->position_title)
        <option value="{{$value->id}}" selected="selected">{{ $value->name }}</option>
        @else
        <option value="{{$value->id}}" >{{ $value->name }}</option>
        @endif
        @endforeach
        </select>
        </div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Level</label><select name="role" id="role" class="positionsdrop form-control">
            @foreach($position as $value)

            @if($value->id == $employee->position_no)

            <option value="{{$value->value}}" selected="selected">{{$value->name}}</option>
            @else
            <option value="{{$value->value}}">{{$value->name}}</option>
            @endif
            @endforeach
        </select>
        </div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Date of Hiring </label><input name ="date_of_hiring" type="text" id="cnic" placeholder="" class="date form-control" value="{{ $employee->date_of_hiring}}"></div>
            @role(['hr-manager'])
        <div class="form-group col-md-4">
            <label for="company" class=" form-control-label">Current Salary </label>
            <input name ="current_salary" type="text" id="cnic" placeholder="" class="form-control" value="{{$employee->current_salary }}">
        </div>
            @endrole
        <div class="form-group col-md-4">
            
            <label for="company" class=" form-control-label">Regions</label>
            <select name="region" id="region" class="form-control">
            <option value="">--Select Regions--</option>
            @foreach($regions as $value)
            @if($value->id  == $employee->region_id)
            <option value="{{$value->id}}" selected="selected">{{ $value->name }}</option>
            @else
            <option value="{{$value->id}}" >{{ $value->name }}</option>
            @endif
            @endforeach
            </select>
            
        </div>
        <div class="form-group col-md-4">
            
            <label for="company" class=" form-control-label">Employee Type</label>
            <select name="emp_type" id="emp_type" class="form-control emptypesdrop">
            <option value="">--Select Emp Type--</option>
            @foreach($empTypes as $value)
            @if($value->id  == $employee->emp_type_id)
            <option value="{{$value->id}}" selected="selected">{{ $value->value }}</option>
            @else
            <option value="{{$value->id}}" >{{ $value->value }}</option>
            @endif
            @endforeach
            </select>
            
        </div>

        <div class="form-group col-md-4 permanent_date"><label for="company" class=" form-control-label">Date of Permanent </label><input name ="permanent_at" type="text" id="permanent_at" placeholder="" class="date form-control"  value="{{$employee->permanent_at}}"></div>
        
        <div class="form-group col-md-4"><label class="form-control-label">
            @if($employee->skip_deduction  == 1 )
            <input type="checkbox" name="skip_deduction" value="1" checked="checked">
            @else 
            <input type="checkbox" name="skip_deduction" value="1">
            @endif
        Skip Deduction
        </label>
        </div>
        
        </div>        
    </div>
    </div>
</div>



<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header"><strong>PF Slabs</strong><small></small></div>
            <div class="card-body">
                <div class="form-group col-md-6">
                <label for="company" class=" form-control-label">Provident Fund Slab </label><select name ="pf_slabs"  class="pfslabs form-control">
                @foreach($pf_slabs as $value)
                @if($value->id  == $employee->pf_slab_id)
                <option value="{{$value->id}}" selected="selected">{{ $value->slab }}  ({{ $value->amount}})</option>
                @else
                <option value="{{$value->id}}" >{{ $value->slab }} ({{ $value->amount}})</option>
                @endif
                @endforeach
                </select>
                </div>
                <div class="form-group col-md-6"><label for="company" class=" form-control-label">PF Efective Date</label><input name ="pf_effective_date" type="text" id="cnic" placeholder="" class="date form-control"  value="{{$employee->pf_effective_date }}"></div>
                <div class="form-group col-md-6"><label for="company" class=" form-control-label">EOBI Amount</label><input name ="eobi" type="text" id="eobi" placeholder="" class="form-control"  value="{{$employee->eobi_amount }}"></div>
                <div class="form-group col-md-6"><label for="company" class=" form-control-label">Fuel Amount</label><input name ="fuel_amount" type="text" id="fuel_amount" placeholder="" class="form-control"  value="{{$employee->fuel_amount }}"></div>
                <div class="form-group col-md-6"><label for="company" class=" form-control-label">Parking Amount</label><input name ="parking_amount" type="text" id="parking_amount" placeholder="" class="form-control"  value="{{$employee->parking_amount }}"></div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <div class="card">
        <div class="card-header"><strong>Contact Address</strong><small> (Current contact address)</small></div>
        <div class="card-body">
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">House#</label><input name ="house_no" type="text" id="cnic" placeholder="" class="form-control" value="{{ $employee->house_no }}"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Builiding</label><input name ="building" type="text" id="cnic" placeholder="" class="form-control" value="{{ $employee->building }}"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Street </label><input name ="street" type="text" id="cnic" placeholder="" class="form-control" value="{{ $employee->street_no }}"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Area </label><input name ="area" type="text" id="cnic" placeholder="" class="form-control" value="{{ $employee->area }}"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">PO Box #</label><input name ="po_box" type="text" id="cnic" placeholder="" class="form-control" value="{{ $employee->po_box }}"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">City </label><input name ="city" type="text" id="cnic" placeholder="" class="form-control" value="{{ $employee->city }}"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Home Landline</label><input name ="landline" type="text" id="cnic" placeholder="" class="form-control" value="{{ $employee->landline }}"></div>
       
        

        </div>

    </div>
    </div>  
    <div class="col-lg-6">
        <div class="card">
        <div class="card-header"><strong>Contact Address</strong><small> (Permanent contact address)</small></div>
        <div class="card-body">
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">House#</label><input name ="p_house_no" type="text" id="cnic" placeholder="" class="form-control" value="{{$employee->p_house}}"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Builiding</label><input name ="p_building" type="text" id="cnic" placeholder="" class="form-control" value="{{ $employee->p_building }}"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Street </label><input name ="p_street" type="text" id="cnic" placeholder="" class="form-control" value="{{ $employee->p_street }}"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Area </label><input name ="p_area" type="text" id="cnic" placeholder="" class="form-control" value="{{ $employee->p_area }}"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">PO Box #</label><input name ="p_po_box" type="text" id="cnic" placeholder="" class="form-control" value="{{ $employee->p_po_box }}"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">City </label><input name ="p_city" type="text" id="cnic" placeholder="" class="form-control" value="{{ $employee->p_city }}"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Home Landline</label><input name ="p_landline" type="text" id="cnic" placeholder="" class="form-control" value="{{ $employee->p_landline }}"></div>
        


        </div>

    </div>
    </div>  
    </div><!-- .row -->
    <div class="row">
    <div class="col-lg-12">
        <div class="card">
        <div class="card-header"><strong>Email & Mobile</strong><small></small></div>
        <div class="card-body">
            <div class="form-group col-md-6"><label for="company" class=" form-control-label">Personal Email <span id="req">*</span></label><input name ="c_email" type="text" id="c_email" placeholder="" value="{{ $employee->email}}" class="form-control" required></div>
            <div class="form-group col-md-6"><label for="company" class=" form-control-label">OSL Email <span id="req">*</span></label><input name ="p_email" type="text" value="{{ $employee->p_email}}" id="c_email" placeholder="" class="form-control" required></div>
            <div class="form-group col-md-6"><label for="company" class=" form-control-label">Personal Mobile</label><input name ="mobile_no" type="text" id="cnic" placeholder="" class="form-control" value="{{ $employee->mobile_no}}"></div>
            <div class="form-group col-md-6"><label for="company" class=" form-control-label">OSL Mobile</label><input name ="p_mobile_no" type="text" id="cnic" placeholder="" class="form-control" value="{{ $employee->p_mobile_no}}"></div>
        </div>
    </div>
    </div>
    </div>
        <div class="row">
    <div class="col-lg-12">
        <div class="card">
        <div class="card-header"><strong>Concern Persons</strong><small></small></div>
        <div class="card-body">
                <div class="form-group col-md-6"><label for="company" class=" form-control-label">Report To <span id="req">*</span></label><select name="report_to" class="empdrop form-control">
                    @foreach($allEmps as $emps)
                    @if($emps->emp_no == $employee->report_to)
                    <option value="{{$emps->emp_no}}" selected="selected">{{$emps->name}}</option>
                    @else
                    <option value="{{$emps->emp_no}}">{{$emps->name}}</option>
                    @endif
                    @endforeach
                </select></div>
                <div class="form-group col-md-6"><label for="company" class=" form-control-label">Reporting Manager<span id="req">*</span></label><select name="report_manager" class="managersdrop form-control">
                    @foreach($managers as $man)
                    @if($man->emp_no == $employee->report_manager)
                    <option value="{{$man->emp_no}}" selected="selected">{{$man->name}}</option>
                    @else
                    <option value="{{$man->emp_no}}">{{$man->name}}</option>
                    @endif
                    @endforeach
                </select></div>
  

        </div>
    </div>
    </div>
    </div>
    <div class="row">
    <div class="col-lg-12 educationRow">
    <div class="education">
        @foreach($education as $value)
        <div class="card">
        <div class="card-header"><strong>Education</strong><small> </small></div>
        <div class="card-body">
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Degree Title</label><input name ="degree_title[]" type="text" id="cnic" placeholder="" class="form-control" value="{{$value->degree_title}}"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Institute Name</label><input name ="institute[]" type="text" id="cnic" placeholder="" class="form-control" value="{{$value->institute}}"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">CGPA/Grade/Percentage </label><input name ="cgpa_grade[]" type="text" id="cnic" placeholder="" class="form-control" value="{{$value->cgpa_percent}}"></div>
        
           <?php
           $to =  explode(" ",  $value->passing_date);
           ?>
        <div class="form-group col-md-6"><label for="company" class=" form-control-label">Passing Date </label><!-- <input name ="passing_from[]" type="text" id="passing_from" placeholder="From" class="date form-control">
         --> 
         <select class="form-control" id="year" name="passing_from[]">
           
            @foreach($yearFrom as $yf)
            @if($yf ==  $to[0] )
            <option value="{{$yf}}" selected>{{$yf}}</option>
            <option value="{{$yf}}">{{$yf}}</option>
            @else
            <option value="{{$yf}}">{{$yf}}</option>
            @endif
            @endforeach
        </select> 
        </div>
        <div class="form-group col-md-6"><label for="company" class=" form-control-label"> &nbsp;</label>
             <select class="form-control" id="year" name="passing_to[]">
            <option value="">Select From</option>
            @foreach($yearFrom as $yt)
            @if($yt ==  $to[2] )
            <option value="{{$yt}}" selected>{{$yt}}</option>
            <option value="{{$yt}}">{{$yt}}</option>
            @else
            <option value="{{$yt}}">{{$yt}}</option>
            @endif
            @endforeach
        </select> 
        </div>

        </div>
    </div>
    @endforeach
    </div>
    </div>  
</div> 
<div class="row">
    <div class="col-lg-12">
        <div class="card">
        <span class="btn btn-primary fa fa-plus" id="add-education">  Add Education</span>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 employerRow">
        <div class="employer">
            @foreach($employer as $value)
        <div class="card">
        <div class="card-header"><strong>Employer</strong><small> </small></div>
        <div class="card-body">
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Employer name</label><input name ="last_emp_name[]" type="text" id="cnic" placeholder="Last employer name" class="form-control" value="{{ $value->name }}"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Last Designation</label><input name ="last_designation[]" type="text" id="cnic" placeholder="Last Designation" class="form-control" value="{{ $value->last_designation }}"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">From</label><input name ="last_emp_from[]" type="text" id="cnic" placeholder="" class="date form-control" value="{{ $value->from }}"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">To </label><input name ="last_emp_to[]" type="text" id="cnic" placeholder="" class="date form-control" value="{{ $value->to }}"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Ref #</label><input name ="last_emp_contact[]" type="text" id="cnic" placeholder="" class="form-control" value="{{ $value->ref_no }}"></div>

        </div>
        </div>
        @endforeach
    </div>
    </div>  
</div> 
 <!--/.row  -->
 <div class="row">
    <div class="col-lg-12">
        <div class="card">
        <span class="btn btn-primary fa fa-plus" id="add-employer">  Add Employer</span>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <div class="card">
        <div class="card-header"><strong>Primary Emergency Contact Detail</strong><small></small></div>
        <div class="card-body">
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Emergency Name</label><input name ="p_emergency_name" type="text" id="p_emergency_name" placeholder="" class="form-control" value="{{$employee->primary_emergency_name}}"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Relationship</label><input name ="relationship" type="text" id="relationship" placeholder="" class="form-control" value="{{$employee->primary_emergency_relation}}"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Address </label><input name ="p_emergency_address" type="text" id="p_emergency_address" placeholder="" class="form-control" value="{{$employee->primary_emergency_address}}"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Telephone</label><input name ="telephone" type="text" id="telephone" placeholder="" class="form-control" value="{{$employee->primary_emergency_telephone}}"></div>
     
        </div>

    </div>
    </div>  
    <div class="col-lg-6">
        <div class="card">
        <div class="card-header"><strong>Secondry Emergency Contact Detail</strong><small></small></div>
        <div class="card-body">
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Emergency Name</label><input name ="s_emergency_name" type="text" id="s_emergency_name" placeholder="" class="form-control" value="{{$employee->secondry_emergency_name}}"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Relationship</label><input name ="s_relationship" type="text" id="s_relationship" placeholder="" class="form-control" value="{{$employee->secondry_emergency_relation}}"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Address </label><input name ="s_emergency_address" type="text" id="s_emergency_address" placeholder="" class="form-control" value="{{$employee->secondry_emergency_address}}"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Telephone</label><input name ="s_telephone" type="text" id="s_telephone" placeholder="" class="form-control" value="{{$employee->secondry_emergency_telephone}}"></div>

        </div>

    </div>
    </div>  
    </div><!-- .row -->
    <div class="row">
    <div class="col-lg-6">
        <div class="card">
        <div class="card-header"><strong>Primary Benificiary Details</strong><small></small></div>
        <div class="card-body">
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Benificiary Name</label><input name ="p_benificiary_name" type="text" id="p_emergency_name" placeholder="" class="form-control" value="{{$employee->primary_benificiary_name}}"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Relationship</label><input name ="p_ben_relationship" type="text" id="p_ben_relationship" placeholder="" class="form-control" value="{{$employee->primary_benificiary_relation}}"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Address </label><input name ="p_ben_address" type="text" id="p_ben_address" placeholder="" class="form-control" value="{{$employee->primary_benificiary_address}}"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Telephone</label><input name ="p_ben_telephone" type="text" id="p_telephone" placeholder="" class="form-control" value="{{$employee->primary_benificiary_telephone}}"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">NIC#</label><input name ="p_ben_nic" type="text" id="p_ben_nic" placeholder="" class="form-control" value="{{$employee->primary_benificiary_cnic}}"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Email</label><input name ="p_ben_email" type="text" id="p_ben_email" placeholder="" class="form-control" value="{{$employee->primary_benificiary_email}}"></div>
     
        </div>

    </div>
    </div>  
    <div class="col-lg-6">
        <div class="card">
        <div class="card-header"><strong>Secondry Benificiary Details</strong><small></small></div>
        <div class="card-body">
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Benificiary Name</label><input name ="s_benificiary_name" type="text" id="s_emergency_name" placeholder="" class="form-control" value="{{$employee->secondry_benificiary_name}}"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Relationship</label><input name ="s_ben_relationship" type="text" id="s_ben_relationship" placeholder="" class="form-control" value="{{$employee->secondry_benificiary_relation}}"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Address </label><input name ="s_ben_address" type="text" id="s_emergency_address" placeholder="" class="form-control" value="{{$employee->secondry_benificiary_address}}"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Telephone</label><input name ="s_ben_telephone" type="text" id="s_telephone" placeholder="" class="form-control" value="{{$employee->secondry_benificiary_telephone}}"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">NIC#</label><input name ="s_ben_nic" type="text" id="s_ben_nic" placeholder="" class="form-control" value="{{$employee->secondry_benificiary_cnic}}"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Email</label><input name ="s_ben_email" type="text" id="s_ben_email" placeholder="" class="form-control" value="{{$employee->secondry_benificiary_email}}"></div>
        </div>

    </div>
    </div>  
    </div><!-- .row -->
    <div class="row">
    <div class="col-lg-12 dependentRow">
      <div class="dependent">
        @foreach($dependent as $value)

        <div class="card">
        <div class="card-header"><strong>Dependents</strong><small></small></div>
        <div class="card-body">
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Name</label><input name ="d_name[]" type="text" id="d_name" placeholder="" class="form-control" value="{{$value->name}}"></div>

        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Type</label><select name="d_type[]" id="d_type" class="form-control">
            @if($value->type == "spouse")
            <option value="">Select Type</option>
            <option value="spouse" selected="selected">Spouse</option>
            <option value="children" >Children</option>
            @elseif($value->type == "children")
            <option value="">Select Type</option>
            <option value="spouse" >Spouse</option>
            <option value="children" selected="selected">Children</option>
            @else
            
            <option value="children" >Children</option>
            @endif
        </select></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Gender</label><select name="d_gender[]" id="d_gender" class="form-control">
            @if($value->gender == 'male')
            <option value="male" selected>Male</option>
            <option value="female">Female</option>
            @elseif($value->gender == 'female')
            <option value="male">Male</option>
            <option value="female" selected>Female</option>
            @endif
        </select></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Date of Birth</label><input name ="d_dob[]" type="text" id="d_dob" placeholder="" class="date form-control" value="{{$value->dob}}"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">CNIC/B Form</label><input name ="d_nic[]" type="text" id="d_nic" placeholder="" class="form-control" value="{{$value->cnic_bform}}"></div>
        <div class="form-group col-md-4"><label for="company" class=" form-control-label">Contact #</label><input name ="d_contact[]" type="text" id="d_contact" placeholder="" class="form-control" value="{{$value->contact_no}}"></div>
      
        </div>
    </div>
    @endforeach
    </div>
    </div>
    </div>
    <div class="row">
    <div class="col-lg-12">
        <div class="card">
        <span class="btn btn-primary fa fa-plus" id="add-dependent">  Add Dependent</span>
        </div>
    </div>
</div>

 <div class="card-footer">
                        <button type="submit" class="btn btn-primary btn-sm">
                          <i class="fa fa-dot-circle-o"></i> Submit
                        </button>
                        <button type="reset" class="btn btn-danger btn-sm">
                          <i class="fa fa-ban"></i> Reset
                        </button>
                      </div>
</form>                      
</div>
</div>
<script src="{{ asset('assets/js/hrm.js') }}" type="text/javascript"></script>
<script>
$(document).ready(function(){
    jQuery(".date").datetimepicker({format: 'YYYY-MM-DD'});
    // var emp_url = "{{route('emp.dropdown')}}";
    //     employeeDropDown(emp_url);
     $('form').submit(function(){
        $(this).find(':submit').html('<i class="fa fa-dot-circle-o"></i> loading...');
     $(this).find(':submit').attr('disabled','disabled');
    });
    var selectedVal =  $(".emptypesdrop option:selected").text();
    if(selectedVal == 'Permanent')
    {
        $(".permanent_date").show();
    }else{
        $(".permanent_date").hide();
    }

    var subdepart =  $("#department").val();
    var url = "{{ route('sub.department',':id') }}";
    sub_depart_url = url.replace(':id', subdepart);
    subDepartment(sub_depart_url);

});

$("#add-education").click(function(){

    jQuery(".education").after('<div class="card"><span class="text-right fa fa-times fa-2x" id="remove-edu"></span><div class="card-header"><strong>Education</strong><small> </small></div><div class="card-body"><div class="form-group col-md-4"><label for="company" class=" form-control-label">Degree Title</label><input name ="degree_title[]" type="text" id="cnic" placeholder="" class="form-control"></div><div class="form-group col-md-4"><label for="company" class=" form-control-label">Institute Name</label><input name ="institute[]" type="text" id="cnic" placeholder="" class="form-control"></div><div class="form-group col-md-4"><label for="company" class=" form-control-label">CGPA/Grade/Percentage </label><input name ="cgpa_grade[]" type="text" id="cnic" placeholder="" class="form-control"></div><div class="form-group col-md-6"><label for="company" class=" form-control-label">Passing Date </label> <select class="form-control" id="year" name="passing_from[]"><option value="">Select From</option>@foreach($yearFrom as $yf)<option value="{{$yf}}">{{$yf}}</option>@endforeach</select> </div><div class="form-group col-md-6"><label for="company" class=" form-control-label"> &nbsp;</label> <select class="form-control" id="year" name="passing_to[]"><option value="">Select To</option>  @foreach($yearTo as $yt)<option value="{{$yt}}">{{$yt}}</option>@endforeach</select> </div></div></div>');
    jQuery(".date").datetimepicker({format: 'YYYY-MM-DD'});

});
$('.educationRow').on('click','#remove-edu',function() {
     
    jQuery(this).parent().remove();
});
$("#add-employer").click(function(){

    jQuery(".employer").after('<div class="card"><span class="text-right fa fa-times fa-2x" id="remove-emp"></span><div class="card-header"><strong>Education</strong><small> </small></div><div class="card-body"><div class="form-group col-md-4"><label for="company" class=" form-control-label">Employer name</label><input name ="last_emp_name[]" type="text" id="cnic" placeholder="Last employer name" class="form-control"></div><div class="form-group col-md-4"><label for="company" class=" form-control-label">Last Designation</label><input name ="last_designation[]" type="text" id="cnic" placeholder="Last Designation" class="form-control"></div><div class="form-group col-md-4"><label for="company" class=" form-control-label">From</label><input name ="last_emp_from[]" type="text" id="cnic" placeholder="" class="date form-control"></div><div class="form-group col-md-4"><label for="company" class=" form-control-label">To </label><input name ="last_emp_to[]" type="text" id="cnic" placeholder="" class="date form-control"></div><div class="form-group col-md-4"><label for="company" class=" form-control-label">Ref #</label><input name ="last_emp_contact[]" type="text" id="cnic" placeholder="" class="form-control"></div></div></div>');
    jQuery(".date").datetimepicker({format: 'YYYY-MM-DD'});

});

$('.employerRow').on('click','#remove-emp',function() {
 	jQuery(this).parent().remove();
});

$("#add-dependent").click(function(){

jQuery(".dependent").after('<div class="card"><span class="text-right fa fa-times fa-2x" id="remove-dep"></span><div class="card-header"><strong>Dependents</strong><small></small></div><div class="card-body"><div class="form-group col-md-4"><label for="company" class=" form-control-label">Name</label><input name ="d_name[]" type="text" id="d_name" placeholder="" class="form-control"></div><div class="form-group col-md-4"><label for="company" class=" form-control-label">Type</label><select name="d_type[]" id="d_type" class="form-control"><option value="spouse">Spouse</option><option value="children">Children</option></select></div><div class="form-group col-md-4"><label for="company" class=" form-control-label">Gender</label><select name="d_gender[]" id="d_gender" class="form-control"><option value="male">Male</option><option value="female">Female</option></select></div><div class="form-group col-md-4"><label for="company" class=" form-control-label">Date of Birth</label><input name ="d_dob[]" type="text" id="d_dob" placeholder="" class="date form-control"></div><div class="form-group col-md-4"><label for="company" class=" form-control-label">CNIC/B Form</label><input name ="d_nic[]" type="text" id="d_nic" placeholder="" class="form-control"></div><div class="form-group col-md-4"><label for="company" class=" form-control-label">Contact #</label><input name ="d_contact[]" type="text" id="d_contact" placeholder="" class="form-control"></div></div>');

jQuery(".date").datetimepicker({format: 'YYYY-MM-DD'});

});
$('.dependentRow').on('click','#remove-dep',function() {
 	jQuery(this).parent().remove();
});

$(document).on("change",".emptypesdrop", function(){
    var selectedVal =  $(".emptypesdrop option:selected").text();
    if(selectedVal == 'Permanent')
    {
        $(".permanent_date").show();
    }else{
        $(".permanent_date").hide();
    }
});

$(document).on("change","#department", function(){
    var selectedVal =  $(this).val();
    var url = "{{ route('sub.department',':id') }}";
    url = url.replace(':id', selectedVal);
    subDepartment(url);

});
</script>
@endsection