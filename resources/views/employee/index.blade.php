@extends('layouts.app')
@section('title','Employee')
@section('content')
<!-- <div class="row">
<div class="col-md-6">
<a type="button" class="btn btn-primary" href="{{ route('add.employee') }}"><i class="fa fa-star"></i>&nbsp; Add new Employee</a>
</div>
</div> -->
@if(session()->has('flash-message'))
        <div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
                                            <span class="badge badge-pill badge-success">Success</span>
                                                {{session()->get('flash-message')}}.
                                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
        @endif
        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Employees</strong>
                            <a class="card-title pull-right btn btn-primary" href="{{route('add.employee')}}"> Add Employees</a>
                        </div>
                        <div class="card-body">
                  <table id="example" class="table table-striped table-bordered">
            
                  </table>
                        </div>
                    </div>
                </div>


                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
        <script src="{{ asset('assets/js/hrm.js') }}" type="text/javascript"></script>
 <script type="text/javascript">
        $(document).ready(function() {
        	var url =  "{{ route('employees') }}";
        	getEmployees(url);
          
        });

    $(document).on('click','.lins',function(e){
      	e.preventDefault();
      
        var id = $(this).parent().attr('id');
    	var edit_url =  "{{ route('emp.status.change',':id') }}";
     	url1 = edit_url.replace(':id', id)
    	var url2 = "{{ route('employees')}}";
        var active = true;
        employeeActiveInactive(url1, $(this).text().toLowerCase(), url2, active);
     });
    </script>
@endsection