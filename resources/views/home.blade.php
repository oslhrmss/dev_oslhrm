@extends('layouts.app')

@section('content')

<style>
#chartdiv {
  width: 100%;
  height: 500px;
}

#exportdiv {
  float: right;
}

#exportdiv:hover .amcharts-export-menu {
  opacity: 1;
}
</style>


<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/pie.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/dataloader/dataloader.min.js"></script>

<div class="container">
    <!-- <div class="row">
        <div class="col-md-12">
        @if (Session::has('bday-message'))
        <div class="alert alert-success text-center">
            <strong>{{ Session::get('bday-message') }}</strong>
        </div>
        @endif
    </div>
    </div> -->
<div class="row" style="margin: 0px; float: right;">
  <div class="dropdown">
  <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Download Payslip
  <span class="caret"></span></button>
  <div class="user-menu dropdown-menu">
    @php
    $payroll =  \App\Model\Payroll::where('emp_id',Auth::user()->emp_code)->get(['month','year']);
    @endphp
    @foreach($payroll as $pay)
    <a class="nav-link" href="{{route('get.payslip')}}?month={{$pay->month}}&year={{$pay->year}}"><i class="fa fa- user"></i>{{$pay->month}}-{{$pay->year}}</a>
    @endforeach
  </div>
</div>
</div>    
    <div class="row">
        <div class="col-md-12">
        @if (Session::has('bday-message'))
        <div class="alert alert-success text-center">
            <strong>{{ Session::get('bday-message') }}</strong>
        </div>
        @endif
    </div>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>
                <div class="panel-body">
                    @if (Session::has('status'))
                        <div class="alert alert-success">
                            {{-- Session::get('status') --}}
                        </div>
                    @endif

                    You are logged in! as {{ ucfirst(Auth::user()->roles->first()->name) }}
                </div>
                @if(session()->has('password-changed'))
                                        <div class="alert alert-success">
                            {{ session()->get('password-changed') }}
                        </div>
                @endif        
            </div>
        </div>
    </div>

 <div class="row">
    <div class="col-xl-4 col-lg-6">
                <div class="card">
                    <div class="card-body">
                        <div class="stat-widget-one">
                            <div class="stat-icon dib"><i class="ti-user text-warning border-warning"></i></div>
                            <div class="stat-content dib">
                                <div class="stat-text">Reporting Manager</div>
                                <div class="" id="reporting_manager"></div>
                            </div>
                        </div>
                    </div>
                </div>
    </div>
    <div class="col-xl-4 col-lg-6">
                <div class="card">
                    <div class="card-body">
                        <div class="stat-widget-one">
                            <div class="stat-icon dib"><i class="ti-money text-success border-success"></i></div>
                            <div class="stat-content dib">
                                <div class="stat-text">Total PF</div>
                                <div class="stat-digit" id="pf_total_val"></div>

                            </div>
                        </div>
                    </div>
                </div>
    </div>
    <div class="col-xl-4 col-lg-6">
                <div class="card">
                    <div class="card-body">
                        <div class="stat-widget-one">
                            <div class="stat-icon dib"><i class="ti-star text-primary border-primary"></i></div>

<!--                             <div class="stat-content dib">
                                <div class="stat-text">Payback Leaves ({{ date('F', strtotime('last month')) }})</div>
                                <div class="" id="pay_back_count"></div>
                            </div> -->
                            <div class="stat-content dib">
                                <div class="stat-text">LoyalityAwards<br>
                                  @foreach(\Utils::getLoyalityAwards(Auth::user()->emp_code) as $value)
                                  <span style="font-size: 14px;font-weight: bold;color:#000000 !important;"><i class="fa fa-trophy"></i> {{$value->type}} Years Loyality Award &nbsp;({{$value->year}}) </span><br>
                                 
                                  @endforeach               
<!--  -->
                            </div>
                        </div>
                    </div>
                </div>
    </div>
  </div>
<!--         <div class="col-lg-12">
                    <div id="chartdiv"></div>
                    <div id="exportdiv"></div>
        </div> -->
            </div>
            @if(Auth::user()->can('dashboard-summary'))
    <div class="row">
        <div class="col-lg-4">
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">Payroll Summary ({{ date('F', strtotime('last month')) }})</strong>
                </div>
                <div class="card-body" id="payroll_summary">
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">Item Summary ({{ date('F', strtotime('last month')) }})</strong>
                </div>
                <div class="card-body" id="item_ded_summary">
                </div>
            </div>
        </div>
        <div class="col-lg-4">
          <div class="card">
            <div class="card-header">
              <strong class="card-title">Loan  Summary ({{ date('F', strtotime('last month')) }})</strong>
            </div>
            <div class="card-body" id="loan_ded_summary">
            </div>
          </div>
        </div>
    </div>
    @endif

</div>
</div>
<script src="{{ asset('assets/js/hrm.js') }}" type="text/javascript"></script>
<script type="text/javascript">
jQuery(document).ready(function(){
  var url1  = "{{route('get.rep.manager')}}";
  getReportingManager(url1);
  var url  = "{{route('get.pf.value')}}";
  getPfTotalValue(url);
  var url  =  "{{route('get.payback.leaves')}}";
  employeePayBackLeaves(url);
  var url  =  "{{route('get.payroll.summary')}}";
  payrollSummary(url);
  var url1  =  "{{route('get.item.ded.summary')}}";
    itemDeductionSummary(url1);
  var url2  =  "{{route('get.loan.ded.summary')}}";
    loanDeductionSummary(url2);
  

});


const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];

const d = new Date();
 
var chart = AmCharts.makeChart("chartdiv", {
  "type": "pie",
  "hideCredits":true,
  "theme": "light",
  // "dataProvider" : [{"columne":"late","column2":"5"},{"columne":"absent","column2":"8"},{"columne":"first_halfday","column2":"7"},{"columne":"second_halfday","column2":"1"},{"columne":"early_going","column2":"0"}],
  "dataLoader":{
    "url" : "{{route('monthly.timing.report')}}",
    "format": "JSON"
  },
  // "dataProvider": [{
  //   "country": "Austria",
  //   "litres": 128.3
  // }, {
  //   "country": "UK",
  //   "litres": 99
  // }, {
  //   "country": "Belgium",
  //   "litres": 60
  // }, {
  //   "country": "The Netherlands",
  //   "litres": 50
  // }],
  "valueField": "column2",
  "titleField": "columne",
  "balloon": {
    "fixedPosition": true
  },
  "titles": [
        {
            "text": monthNames[d.getMonth()]+" Time Graph",
            "size": 15
        }
    ],
  "export": {
    "enabled": true,
    "divId": "exportdiv"
  }
});

</script>
@endsection
