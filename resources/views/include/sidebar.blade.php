<aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="./"><!-- <img src="images/logo.png" alt="Logo"> -->HRM</a>
                <a class="navbar-brand hidden" href="./"><!-- <img src="images/logo2.png" alt="Logo"> -->OSL</a>
            </div>

            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="{{url('/')}}"><i class="menu-icon fa fa-dashboard"></i>Dashboard </a>
                    </li>
                    <h3 class="menu-title">Organogram</h3><!-- /.menu-title -->
                     <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-users"></i>Employee</a>
                        <ul class="sub-menu children dropdown-menu">
                            @if(Auth::user()->can('all-employee'))
                            <li><i class="fa fa-puzzle-piece"></i><a href="javascript:void(0);">Employee Type</a></li>
                            <li><i class="fa fa-list-ul"></i><a href="{{route('employee')}}">Employee List</a></li>
                            <li><i class="fa fa-list-ul"></i><a href="{{route('ex.employee')}}">Ex Employee List</a></li>
                            @endif
                            @if(Auth::user()->can('view-package-detail'))
                            <li><i class="fa fa-list-ul"></i><a href="{{route('package.detail')}}">Emp. Package Detail</a></li>
                            @endif
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-clock-o"></i>Attendance</a>
                        <ul class="sub-menu children dropdown-menu">
                            @if(Auth::user()->can('view-attendance'))
                            @if(Auth::user()->roles->first()->name == 'employee')
                            <li><i class="fa fa-table "></i><a href="{{route('timesheet')}}">Time Sheet</a></li>
                            @else
                            <li><i class="fa fa-calendar"></i><a href="{{route('company.attendance')}}">Company TimeSheet</a></li>
                            <li><i class="fa fa-calendar"></i><a href="{{route('attendance')}}">Time Sheet</a></li>
                            @endif                           
                            @endif
                            @if(Auth::user()->can('import-attendance'))
                            <li><i class="fa fa-upload"></i><a href="{{route('import')}}">Import TimeSheet</a></li>
                            @if(Auth::user()->can('delete-modified-attendance'))
                            <li><i class="fa fa-calendar-check-o"></i><a href="{{route('modified.attendance')}}">Modified Time Sheet</a></li>
                            @endif
                            @endif
                            @if(Auth::user()->can('audit-view-attendance'))
                            <li><i class="fa fa-calendar"></i><a href="{{route('audit.attendance')}}">Time Sheet for Audit</a></li>
                            @endif
                        </ul>
                    </li>
                    @role(['admin', 'hr-manager','audit'])
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-bar-chart"></i>Reports</a>
                        <ul class="sub-menu children dropdown-menu">
                        <li><i class="fa fa-bar-chart"></i><a href="{{route('loan.deduction.report')}}">Loan Ded. Report</a></li>
                       <li><i class="fa fa-bar-chart"></i><a href="{{route('leave.deduction.report')}}">Leave Ded. Report</a></li>
                       <li><i class="fa fa-bar-chart"></i><a href="{{route('item.deduction.report')}}">Item Ded. Report</a></li>
                            <li><i class="fa fa-area-chart"></i><a href="{{route('all.employee.leave.list.view')}}">All Emp. Leaves</a></li>
                            <li><i class="fa fa-area-chart"></i><a href="{{route('all.employee.leave.balance')}}">All Emp. Leaves Bal</a></li>
                            <li><i class="fa fa-area-chart"></i><a href="{{route('payback.leave.list')}}">Payback Leave List</a></li>
                        @if(Auth::user()->can('advance-salary-list'))
                            <li><i class="fa fa-area-chart"></i><a href="{{route('adv.sal.list')}}">Adv. Salary List</a></li>
                            @endif    
                        </ul>
                    </li>
                    @endrole
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-calendar-o"></i>Leaves</a>
                        <ul class="sub-menu children dropdown-menu">
                            @if(Auth::user()->can('leave-type'))
                            <li><i class="fa fa-table"></i><a href="#">Leave Type</a></li>
                            @endif
                            @if(Auth::user()->can('company-leaves'))
                            <li><i class="fa fa-building"></i><a href="{{route('company.leave')}}">Company Leaves</a></li>
                            @endif

                            <!-- <li><i class="fa fa-building"></i><a href="{{route('leave.balance.view')}}">Leave Balance</a></li> -->

                            @if(Auth::user()->can('my-leaves'))
                            <li><i class="fa fa-building"></i><a href="{{route('my.leaves')}}">My Leaves</a></li>
                            @endif
                            @if(Auth::user()->can('leave-application'))
                            <li><i class="fa fa-building"></i><a href="{{route('leave.application')}}">Leave Application</a></li>
                            <li><i class="fa fa-book"></i><a href="{{route('leave.ledger')}}">Leave Ledger</a></li>
                            @endif
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-money"></i>Loan</a>
                        <ul class="sub-menu children dropdown-menu">
                            @if(Auth::user()->can('advance-salary-application'))
                            <li><i class="fa fa-dollar"></i><a href="{{route('advance.salary')}}">Adv. Salary Request</a></li>
                            @endif
                            @if(Auth::user()->can('loan-application'))
                            <li><i class="fa fa-table"></i><a href="{{route('loan.application.form')}}">Loan Application</a></li>
                            <li><i class="fa fa-book"></i><a href="{{route('loan.ledger')}}">Loan Ledger</a></li>
                            @endif
                            @if(Auth::user()->can('advance-salary-list'))
                            <li><i class="fa fa-table"></i><a href="{{route('adv.sal.list')}}">Adv. Salary List</a></li>
                            <li><i class="fa fa-table"></i><a href="{{route('loan.list')}}">Loans List</a></li>
                            @endif

                            @role(['finance'])
                            <li><i class="fa fa-book"></i><a href="{{route('loan.set.off')}}">Loan Set Off</a></li>
                            @endrole

                        </ul>
                    </li>   
                    @role(['admin','hr-manager'])
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-lock"></i>Security</a>
                        <ul class="sub-menu children dropdown-menu">
                            
                            <li><i class="fa fa-key"></i><a href="{{route('default.password')}}">Default Password</a></li>
                           
                        </ul>
                    </li>  
                    @endrole
                    @role(['hr-manager','finance','manager'])
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-check-square-o "></i>Approvals &nbsp;{!! \Utils::getAllApprovalCount(Auth::user()->emp_code, Auth::user()->roles->pluck('name')->toArray(),true) !!}
                        </a> 
                        <ul class="sub-menu children dropdown-menu">
                            @if(Auth::user()->can('leave-approval'))
                            <li><i class="fa fa-building"></i><a href="{{route('leave.approvals')}}">Leave&nbsp; <span class="count bg-danger">{{ \Utils::getLeaveRequestCount(Auth::user()->emp_code) }}</span></a>
                            </li>
                            @endif
                            @role(['hr-manager'])
                            @if(Auth::user()->can('temp-leave-approve'))
                            <li><i class="fa fa-building"></i><a href="{{route('temp.leave.approvals')}}">Medical Leaves&nbsp; <span class="count bg-danger">{{ \Utils::getTempLeaveRequestCount(Auth::user()->emp_code) }}</span></a>
                            </li>
                            @endif
                            @endrole
                            @if(Auth::user()->can('loan-approval'))
                            <li><i class="fa fa-building"></i><a href="{{route('loan.approval')}}">Loan &nbsp; <span class="count bg-danger">{{\Utils::getLoanAdvSalRequestCount(Auth::user()->roles->pluck('name')->toArray())}}</span></a>
                            </li>
                            @endif
                             @if(Auth::user()->can('adv-salary-approval'))
                            <li><i class="fa fa-check "></i><a href="{{route('adv.sal.approvals')}}">Adv.Salary &nbsp; <span class="count bg-danger">{{\Utils::getLoanAdvSalRequestCount(Auth::user()->roles->pluck('name')->toArray(),true)}}</span></a></li>
                            @endif
                            @if(Auth::user()->can('allowance-approval'))
                            <li><i class="fa fa-cutlery"></i><a href="{{route('allowance.approvals')}}">Dinner Allowance<span class="count bg-danger">{{ \Utils::getAllowanceRequestCount(Auth::user()->emp_code) }}</span></a>
                            </li>
                            @endif
                        </ul>
                    </li>  
                    @endrole
                    @if(Auth::user()->can('deduction-view'))
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-window-close"></i>Deduction</a>
                        <ul class="sub-menu children dropdown-menu">
                        <li><a href="{{route('deduction')}}"> <i class="fa fa-window-close"></i>Loan Deductions</a></li>
                        <li><a href="{{route('payback.leave')}}"><i class="fa fa-window-close"></i>Payback Leave</a></li>
                        <li><a href="{{route('leave.deduction')}}"> <i class="fa fa-window-close"></i>Leave Deductions</a></li>
                        <li><a href="{{route('item.deduction')}}"><i class="fa fa-window-close"></i>Item Deduction</a></li>
                        <li><a href="{{route('arrears')}}"><i class="fa fa-window-close"></i>Arrears</a></li>
                        </ul>
                    </li>
                    @endif
                    @role(['admin','hr-manager','finance','audit'])
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-calendar"></i>Payroll</a>
                        <ul class="sub-menu children dropdown-menu">
                        @if(Auth::user()->can('payroll-history-view'))
                        <li><i class="fa fa-eye"></i><a href="{{route('payroll.history')}}">Payroll History</a></li>
                        @endif
                        @if(Auth::user()->can('payroll-attendance'))
                        <li><i class="fa fa-file"></i><a href="{{route('payroll.attendance')}}">Payroll Attendance</a></li>
                        @endif
                        @if(Auth::user()->can('payroll-generate-view'))
                        <li><i class="fa fa-gears"></i><a href="{{route('payroll')}}">Generate Payroll</a></li>    
                        @endif                        

                        </ul>
                    </li>

                    @endrole
                    
                    
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-shopping-bag"></i>Items</a>
                        <ul class="sub-menu children dropdown-menu">
                        @role(['test','admin'])
                        <li><i class="fa fa-shopping-bag"></i><a href="{{route('items')}}">Items</a></li>
                        <!-- <li><i class="fa fa-file"></i><a href="{{route('assign.item')}}">Assign Items</a></li> -->
                        @endrole
                        @role(['employee'])
                        <li><i class="fa fa-file"></i><a href="{{route('item.ledger')}}">Item Ledger</a></li>
                        @if(Auth::user()->can('view-my-items-list'))
                        <li><i class="fa fa-file"></i><a href="{{route('my.items')}}">My Items</a></li>
                        @endif
                        @endrole
                        </ul>
                    </li>
                    
                    <!-- @role(['employee']) -->
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-cutlery"></i>Dinner Allowance</a>
                        <ul class="sub-menu children dropdown-menu">
                        @role(['employee'])
                        <li><i class="fa fa-file"></i><a href="{{route('dinner.allowance.request')}}">Allowance Request</a></li>
                        @endrole
                        @role(['asst-finance'])
                        @if(Auth::user()->can('view-approved-allowance-request'))
                        <li><i class="fa fa-file"></i><a href="{{route('dinner.allowance.teller')}}">Allowance Teller</a></li>
                        @endif                        
                        @endrole
                        </ul>
                    </li>
                    <!-- @endrole -->

                    @role(['admin','hr-manager'])
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-trophy"></i>Awards</a>
                        <ul class="sub-menu children dropdown-menu">
                            
                            <li><i class="fa fa-trophy"></i><a href="{{route('loyality.awards')}}">Loyality Awards</a></li>
                        </ul>
                    </li>
                    @endrole

                    <!-- <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-table"></i>Tables</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-table"></i><a href="tables-basic.html">Basic Table</a></li>
                            <li><i class="fa fa-table"></i><a href="tables-data.html">Data Table</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-th"></i>Forms</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-th"></i><a href="forms-basic.html">Basic Form</a></li>
                            <li><i class="menu-icon fa fa-th"></i><a href="forms-advanced.html">Advanced Form</a></li>
                        </ul>
                    </li> -->

                                        <!-- /.menu-title -->
                    <!--- <h3 class="menu-title">Icons</h3>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-tasks"></i>Icons</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-fort-awesome"></i><a href="font-fontawesome.html">Font Awesome</a></li>
                            <li><i class="menu-icon ti-themify-logo"></i><a href="font-themify.html">Themefy Icons</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="widgets.html"> <i class="menu-icon ti-email"></i>Widgets </a>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-bar-chart"></i>Charts</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-line-chart"></i><a href="charts-chartjs.html">Chart JS</a></li>
                            <li><i class="menu-icon fa fa-area-chart"></i><a href="charts-flot.html">Flot Chart</a></li>
                            <li><i class="menu-icon fa fa-pie-chart"></i><a href="charts-peity.html">Peity Chart</a></li>
                        </ul>
                    </li>

                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-area-chart"></i>Maps</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-map-o"></i><a href="maps-gmap.html">Google Maps</a></li>
                            <li><i class="menu-icon fa fa-street-view"></i><a href="maps-vector.html">Vector Maps</a></li>
                        </ul>
                    </li> -->
                    <!-- <h3 class="menu-title">Extras</h3>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-glass"></i>Pages</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-sign-in"></i><a href="page-login.html">Login</a></li>
                            <li><i class="menu-icon fa fa-sign-in"></i><a href="page-register.html">Register</a></li>
                            <li><i class="menu-icon fa fa-paper-plane"></i><a href="pages-forget.html">Forget Pass</a></li>
                        </ul>
                    </li>  -->
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside><!-- /#left-panel -->

    <!-- Left Panel -->