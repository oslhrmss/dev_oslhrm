@extends('layouts.app')
@section('title','Add Item')
@section('content')

        <div class="content mt-3">
            <div class="animated">
            <div class="row">

                <div class="col-md-12">
                    <div class="card">
                    <div class="card-header">
                            <strong class="card-title">Add Items</strong>

                    </div>  
                    <div class="card-body">
                    <div id="messages"></div>   
                    <form name="add_item_form" id="add_item_form" method="POST" action="">
                        {{ csrf_field() }}
                    <div class="form-group col-md-3">
                            <label for="employee" class=" form-control-label"><strong>Item Fullname</strong> <span id="req">*</span></label>
                    <input type="text" name="item_name" id="item_name" class="form-control">
                    </div>
                    <div class="form-group col-md-3">
                            <label for="employee" class=" form-control-label"><strong>Brand</strong> <span id="req">*</span></label>
                    <input type="text" name="item_brand" id="item_brand" class="form-control">
                    </div>
                    <div class="form-group col-md-3">
                            <label for="employee" class=" form-control-label"><strong>Model</strong> <span id="req">*</span></label>
                    <input type="text" name="item_model" id="item_model" class="form-control">
                    </div>
                    <div class="form-group col-md-3">
                            <label for="employee" class=" form-control-label"><strong>IME/SR/REG</strong> <span id="req">*</span></label>
                    <input type="text" name="imei_no" id="imei_no" class="form-control">
                    </div>
                    <div class="form-group col-md-3">
                            <label for="employee" class=" form-control-label"><strong>Type</strong> <span id="req">*</span></label>
                            <select name="type" data-placeholder="Select Type" class="standardSelects form-control">
                                <option value=""></option>
                            </select>
                    </div>  
                    <div class="form-group col-md-3">
                            <label for="employee" class=" form-control-label"><strong>Cost</strong> <!-- <span id="req">*</span> --></label>
                            <input type="text" name="cost" id="cost" class="form-control" placeholder="0">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="vendor" class=" form-control-label"><strong>Vendor</strong> <span id="req">*</span></label>
                    <select name="vendor" data-placeholder="Select Vendor" class="standardSelect form-control">
                        <option value=""></option>
                    </select>
                    </div>
                    <div class="form-group col-md-3">
                            <label for="employee" class=" form-control-label"><strong>Purchase Date</strong> <span id="req">*</span></label>
                    <input type="date" name="pur_date" id="pur_date" class="form-control">
                    </div>
                    <div class="form-group col-md-3">
                            <label for="employee" class=" form-control-label"><strong>Warranty Date</strong> <!-- <span id="req">*</span> --></label>
                    <input type="date" name="war_date" id="war_date" class="form-control">
                    </div>
                    <div class="form-group col-md-3">
                            <label for="employee" class=" form-control-label"><strong>Dedcution Amount</strong> <!-- <span id="req">*</span> --></label>
                            <input type="text" name="deduction_amt" id="deduction_amt" class="form-control" placeholder="0">
                    </div>
                    <div class="form-group col-md-3">
                            <label for="employee" class=" form-control-label"><strong>Mon. Deduction Amt</strong> <!-- <span id="req">*</span> --></label>
                            <input type="text" name="monthly_deduction" id="monthly_deduction" class="form-control" placeholder="0">
                    </div>
                    <!-- <div class="form-group col-md-4">
                            <label for="employee" class=" form-control-label"><strong>Specification</strong> <span id="req">*</span></label>
                            <textarea name="specification" class="form-control"></textarea>
                    </div> -->
                    </form>
                    </div>
                    <div class="card-footer">
                            <button class="btn btn-primary btn-md" id="add_item_btn">
                            <i class="fa fa-plus"></i> Submit
                            </button>

                            <button onclick="window.history.go(-1); return false;" class="btn btn-danger btn-md">
                            <i class="fa fa-ban"></i> Back
                            </button>
                    </div>

                    </div>
                </div>
            </div>

            </div>
        </div>


            <script src="{{ asset('assets/js/hrm.js') }}" type="text/javascript"></script>

            <script>
                $(document).ready(function(){
                    //jQuery(".date").datetimepicker({format: 'YYYY-MM-DD'});
                    var url  = "{{route('get.items')}}";
                    getItems(url);

                });

                $(document).on('click','#add_item_btn', function(e){
        
                    e.preventDefault();
                    var url1  = "{{ route('store.item') }}";
        
                    storeItem(url1);
                });

               $(document).ready(function(){
                
                 var url1 = "{{route('get.vendor.with.id')}}";

                 $.ajax({
                    type: "GET",
                    dataType: "JSON",
                    url : url1,
                    data : data

                 }).done(function(data){
                    $.each(data,function(key,value){
                        $(".standardSelect").append("<option value="+value.id+">"+value.vendor_name+"</option>");
                    });
                    $(".standardSelect").trigger("liszt:updated");

                    $(".standardSelect").chosen({
                          disable_search_threshold: 10,
                 no_results_text: "Oops, nothing found!",
                width: "100%"
                    });
                 });

                });


               $(document).ready(function(){
                
                 var url1 = "{{route('get.type.with.id')}}";

                 $.ajax({
                    type: "GET",
                    dataType: "JSON",
                    url : url1,
                    data : data

                 }).done(function(data){
                    $.each(data,function(key,value){
                        $(".standardSelects").append("<option value="+value.id+">"+value.name+"</option>");
                    });
                    $(".standardSelects").trigger("liszt:updated");

                    $(".standardSelects").chosen({
                          disable_search_threshold: 10,
                 no_results_text: "Oops, nothing found!",
                width: "100%"
                    });
                 });

                });


            </script>
@endsection