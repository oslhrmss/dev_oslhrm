@extends('layouts.app')
@section('title','Add Item')
@section('content')

        <div class="content mt-3">
            <div class="animated">
            <div class="row">

                <div class="col-md-12">
                    <div class="card">
                    <div class="card-header">
                            <strong class="card-title">Add Items</strong>

                    </div>	
                    <div class="card-body">
                    <div id="messages"></div>	
                    <form name="assign_item_form" id="assign_item_form" method="POST" action="">
                        {{ csrf_field() }}
                    <div class="form-group col-md-4">
                            <label for="employee" class=" form-control-label"><strong>Name</strong> <span id="req">*</span></label>
                   <select name="emp_name" id="emp_name" class="empdrop form-control"></select>
                    </div>
                    <div class="form-group col-md-4">
                            <label for="employee" class=" form-control-label"><strong>Type</strong> <span id="req">*</span></label>
<!--                             <select name="type" class="sub_comp_leave_type form-control">
                                <option value="">--Select--</option>
                                <option value="1">Electronic</option>
                                <option value="2">Vehical </option>
                                <option value="3">Stationary</option>
                            </select> -->
                            <select name="type[]" data-placeholder="Select Item" multiple class="standardSelect" tabindex="5">
                                    <option value=""></option>
<!--                                     <optgroup label="NFC EAST">
                                      <option>Dallas Cowboys</option>
                                      <option>New York Giants</option>
                                      <option>Philadelphia Eagles</option>
                                      <option>Washington Redskins</option>
                                    </optgroup>
                                    <optgroup label="NFC NORTH">
                                      <option>Chicago Bears</option>
                                      <option>Detroit Lions</option>
                                      <option>Green Bay Packers</option>
                                      <option>Minnesota Vikings</option>
                                    </optgroup>
                                    <optgroup label="NFC SOUTH">
                                      <option>Atlanta Falcons</option>
                                      <option>Carolina Panthers</option>
                                      <option>New Orleans Saints</option>
                                      <option>Tampa Bay Buccaneers</option>
                                    </optgroup>
                                    <optgroup label="NFC WEST">
                                      <option>Arizona Cardinals</option>
                                      <option>St. Louis Rams</option>
                                      <option>San Francisco 49ers</option>
                                      <option>Seattle Seahawks</option>
                                    </optgroup>
                                    <optgroup label="AFC EAST">
                                      <option>Buffalo Bills</option>
                                      <option>Miami Dolphins</option>
                                      <option>New England Patriots</option>
                                      <option>New York Jets</option>
                                    </optgroup>
                                    <optgroup label="AFC NORTH">
                                      <option>Baltimore Ravens</option>
                                      <option>Cincinnati Bengals</option>
                                      <option>Cleveland Browns</option>
                                      <option>Pittsburgh Steelers</option>
                                    </optgroup>
                                    <optgroup label="AFC SOUTH">
                                      <option>Houston Texans</option>
                                      <option>Indianapolis Colts</option>
                                      <option>Jacksonville Jaguars</option>
                                      <option>Tennessee Titans</option>
                                    </optgroup>
                                    <optgroup label="AFC WEST">
                                      <option>Denver Broncos</option>
                                      <option>Kansas City Chiefs</option>
                                      <option>Oakland Raiders</option>
                                      <option>San Diego Chargers</option>
                                    </optgroup> -->
                                  </select>        
                    </div>  

                    </form>
                    </div>
					<div class="card-footer">
                        	<button class="btn btn-primary btn-md" id="add_item_btn">
                          	<i class="fa fa-plus"></i> Submit
                        	</button>

                        	<button onclick="window.history.go(-1); return false;" class="btn btn-danger btn-md">
                          	<i class="fa fa-ban"></i> Back
                        	</button>
                    </div>

                    </div>
                </div>
            </div>

            </div>
        </div>


            <script src="{{ asset('assets/js/hrm.js') }}" type="text/javascript"></script>


            <script>
            	$(document).ready(function(){
                //  $(".empdrop").chosen({
                //  disable_search_threshold: 10,
                //  no_results_text: "Oops, nothing found!",
                // width: "100%"
                // });
                 var url = "{{route('emp.dropdown')}}";
                 employeeChosenDropDown(url);
                 var url1 = "{{route('get.item.with.type')}}";

                 $.ajax({
                    type: "GET",
                    dataType: "JSON",
                    url : url1,
                    data : data

                 }).done(function(data){
                    $.each(data,function(key,value){
                        $(".standardSelect").append("<option value="+value.item_id+">"+value.item_name+"</option>");
                    });
                    $(".standardSelect").trigger("liszt:updated");

                    $(".standardSelect").chosen({
                          disable_search_threshold: 10,
                 no_results_text: "Oops, nothing found!",
                width: "100%"
                    });
                 });

            	});

            	$(document).on('click','#add_item_btn', function(e){
		
					e.preventDefault();
					var url1  = "{{ route('assign.item.emp') }}";
		
					assignItemToEmployee(url1);
				});

            </script>
@endsection