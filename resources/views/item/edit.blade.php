@extends('layouts.app')
@section('title','Edit Item')
@section('content')

        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Dashboard</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="{{ route('items') }}">Item</a></li>
                            <li class="active">Edit Item &nbsp;{{ $item->id }}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        @if(session()->has('flash-message'))
        <div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
                                            <span class="badge badge-pill badge-success">Success</span>
                                                {{session()->get('flash-message')}}
                                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
        @endif
<form name="editItemForm" method="post" action="{{ route('edit.item.post') }}" enctype="multipart/form-data">
    {{ csrf_field()}}
    <input type="hidden" name="id" value="{{ $item->id }}">
        <div class="content mt-3">
            @if ($errors->any())
            <div class="alert alert-danger">
               @foreach ($errors->all() as $error)
               <strong>{{ $error }}</strong><br>
               @endforeach
           </div>
           @endif    
            <div class="animated fadeIn">
    <div class="row">
    <div class="col-lg-12">
        <div class="card">
        <div class="card-header"><strong><a href="{{route('items')}}"><i class="fa fa-reply-all"></i> Item</a></strong><small>  - Edit</small></div>
        <div class="card-body">
            <form name="add_item_form" id="add_item_form" action="{{ route('edit.item.post') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                    <div class="form-group col-md-3">
                            <label for="employee" class=" form-control-label"><strong>Item Fullname</strong> <span id="req">*</span></label>
                    <input type="text" name="item_name" id="item_name" class="form-control" value="{{$item->name}}">
                    </div>
                    <div class="form-group col-md-3">
                            <label for="employee" class=" form-control-label"><strong>Brand</strong> <span id="req">*</span></label>
                    <input type="text" name="item_brand" id="item_brand" class="form-control" value="{{$item->brand}}">
                    </div>
                    <div class="form-group col-md-3">
                            <label for="employee" class=" form-control-label"><strong>Model</strong> <span id="req">*</span></label>
                    <input type="text" name="item_model" id="item_model" class="form-control" value="{{$item->model}}">
                    </div>
                    <div class="form-group col-md-3">
                            <label for="employee" class=" form-control-label"><strong>IME/SR/REG</strong> <span id="req">*</span></label>
                    <input type="text" name="imei_no" id="imei_no" class="form-control" value="{{$item->imei_no}}">
                    </div>
                    <div class="form-group col-md-3">
                            <label for="employee" class=" form-control-label"><strong>Type</strong> <span id="req">*</span></label>
                            <select name="type" class="form-control standardSelects">
                                @foreach($type as $value)
                                <option value="{{$value->id}}">{{$value->name}}</option>
                                @endforeach
                            </select>
                    </div>  
                    <div class="form-group col-md-3">
                            <label for="employee" class=" form-control-label"><strong>Cost</strong></label>
                            <input type="text" name="cost" id="cost" class="form-control" value="{{$item->cost}}">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="vendor" class=" form-control-label"><strong>Vendor</strong> <span id="req">*</span></label>
                    <select name="vendor" class="form-control standardSelect">
                                @foreach($vendor as $value)
                                <option value="{{$value->id}}">{{$value->vendor_name}}</option>
                                @endforeach
                            </select>
                    </div>
                    <div class="form-group col-md-3">
                            <label for="employee" class=" form-control-label"><strong>Purchase Date</strong> <span id="req">*</span></label>
                    <input type="date" name="pur_date" id="pur_date" class="form-control" value="{{$item->purchase_date}}">
                    </div>
                    <div class="form-group col-md-3">
                            <label for="employee" class=" form-control-label"><strong>Warranty Date</strong> <!-- <span id="req">*</span> --></label>
                    <input type="date" name="war_date" id="war_date" class="form-control" value="{{$item->warranty_date}}">
                    </div>
                    <div class="form-group col-md-3">
                            <label for="employee" class=" form-control-label"><strong>Dedcution Amount</strong> <!-- <span id="req">*</span> --></label>
                            <input type="text" name="deduction_amt" id="deduction_amt" class="form-control" placeholder="0"  value="{{$item->deduction_amt}}">
                    </div>
                    <div class="form-group col-md-3">
                            <label for="employee" class=" form-control-label"><strong>Mon. Deduction Amt</strong> <!-- <span id="req">*</span> --></label>
                            <input type="text" name="monthly_deduction" id="monthly_deduction" class="form-control" placeholder="0" value="{{$item->monthly_deduction_amt}}">
                    </div>
                    
        </div>
        <div class="card-footer">
                        <button onclick="window.history.go(-1); return false;" class="btn btn-danger btn-md">
                          <i class="fa fa-reply"></i> Back
                        </button>
                        <button type="submit" class="btn btn-primary btn-md">
                          <i class="fa fa-upload"></i> Update
                        </button>
                      </div>
</form>  
    </div>
    </div>
    </div>
            

    
</div>

                     

<script src="{{ asset('assets/js/hrm.js') }}" type="text/javascript"></script>
<script>
$(document).ready(function(){
                
                 var url1 = "{{route('get.vendor.with.id')}}";

                 $.ajax({
                    type: "GET",
                    dataType: "JSON",
                    url : url1,
                    data : data

                 }).done(function(data){
                    $.each(data,function(key,value){
                        $(".standardSelect").append("<option value="+value.id+">"+value.vendor_name+"</option>");
                    });
                    $(".standardSelect").trigger("liszt:updated");

                    $(".standardSelect").chosen({
                          disable_search_threshold: 10,
                 no_results_text: "Oops, nothing found!",
                width: "100%"
                    });
                 });

                });

$(document).ready(function(){
                
                 var url1 = "{{route('get.type.with.id')}}";

                 $.ajax({
                    type: "GET",
                    dataType: "JSON",
                    url : url1,
                    data : data

                 }).done(function(data){
                    $.each(data,function(key,value){
                        $(".standardSelects").append("<option value="+value.id+">"+value.name+"</option>");
                    });
                    $(".standardSelects").trigger("liszt:updated");

                    $(".standardSelects").chosen({
                          disable_search_threshold: 10,
                 no_results_text: "Oops, nothing found!",
                width: "100%"
                    });
                 });

                });
</script>
@endsection