@extends('layouts.app')
@section('title','Items')
@section('content')

        <div class="content mt-3">
            @if(session()->has('flash-message'))
        <div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
                                            <span class="badge badge-pill badge-success">Success</span>
                                                {{session()->get('flash-message')}}
                                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        @endif
            <div class="animated">
            <div class="row">

                <div class="col-md-12">
                    <div class="card">
                    <div class="card-header">
                            <strong class="card-title">Items</strong>
                            
                            <a href="{{route('add.vendor')}}" id="add_vendor"><strong class="card-title" style="float:right;">&nbsp; &nbsp;<i class="fa fa-plus"></i>&nbsp;Add Vendors</strong></a>
                            

                            
                            <a href="{{route('add.item')}}" id="add_item"><strong class="card-title" style="float:right;">&nbsp; &nbsp;<i class="fa fa-plus"></i>&nbsp;Add Items</strong></a>
                            

                            
                            <a href="{{route('report.2')}}" id="item_report_2"><strong class="card-title" style="float:right;">&nbsp; &nbsp;<i class="fa fa-file" aria-hidden="true"></i>&nbsp;Report 2</strong></a>
                            
                            <a href="{{route('report.1')}}" id="item_report_1"><strong class="card-title" style="float:right;">&nbsp; &nbsp;<i class="fa fa-file" aria-hidden="true"></i>&nbsp;Report 1</strong></a>
                          
                    </div>	
                    <div class="card-body">
                    <table id="example" class="table table-striped table-bordered"></table>
                    </div>
                    </div>
                </div>
            </div>

            </div>
        </div>


        <!-- Item Assign modal start -->
                <div class="modal fade" id="itemEditModal" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">

                            <div class="modal-header" style="background-color: #f8f9fa;">
                                <h5 class="modal-title" id="smallmodalLabel">Assign Item</h5>
                                <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            

                            <div class="modal-body">
                            <div id="messages"></div>    
                            <form id="modifyItemForm" method="post" action="">
                            {{csrf_field()}}
                            
                            
                            <br/>
                            <div class="form-group col-md-12">
                                <label for="employee" class=" form-control-label"><strong>Emp Name</strong> <span id="req">*</span></label>
                                <select name="emp_name" id="emp_name" class="empdrop form-control"></select>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="employee" class=" form-control-label"><strong>Assign Date</strong> <span id="req">*</span></label>
                                <input type="date" name="assign_date" id="assign_date" class="form-control" />
                            </div>
                                <!-- <center><b style="font-size: 30px; color: green; padding: 0px; margin: 0px;">&#x21E9;</b></center>
                                <br/> -->
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="button" id="submit" class="btn btn-primary">Confirm</button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                </div>   
                <!-- Item Assign modal end -->

                <!-- Item Remark modal start -->
                <div class="modal fade" id="itemremarkModal" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">

                            <div class="modal-header" style="background-color: #f8f9fa;">
                                <h5 class="modal-title" id="smallmodalLabel"><i class="fa fa-sticky-note-o"> </i> Remarks</h5>
                                <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            

                            <div class="modal-body">
                            <div id="messages"></div>
                            <form id="modifyRemarkItemForm" method="post" action="">
                            {{csrf_field()}}
                            
                            
                            <br/>
                            <div class="form-group col-md-12">
                                <textarea rows="3" name="remarks" id="remarks" class="form-control" placeholder="Enter remarks here..."></textarea>
                            </div>
                            
                                <!-- <center><b style="font-size: 30px; color: green; padding: 0px; margin: 0px;">&#x21E9;</b></center>
                                <br/> -->
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="button" id="add_remarks" class="btn btn-primary">Add</button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                </div>

                <!-- Item Remark modal end -->


            <script src="{{ asset('assets/js/hrm.js') }}" type="text/javascript"></script>

            <script>
            	$(document).ready(function(){
            		//jQuery(".date").datetimepicker({format: 'YYYY-MM-DD'});
            	 	var url  = "{{route('get.items')}}";
            	 	getItems(url);
            	});

                $(document).ready(function(){
                 var url = "{{route('emp.dropdown')}}";
                 employeeChosenDropDown(url);

                 });


                $(document).on('click','.lins',function(e){
                    e.preventDefault();
                  
                    var id = $(this).parent().attr('id');
                    var edit_url =  "{{ route('item.action',':item_id') }}";
                    // url1 = edit_url.replace(':id', id)
                    url1 = edit_url.replace(':item_id', id)
                    var statusValue = ItemStatusValueReplace($(this).text());
                    ItemAction(url1,statusValue);
                 });

                jQuery(document).on("click","#itemEditBtn",function(e){
                    e.preventDefault();
                    getItemId($(this).parent("td").data('id'),$(this).parent("td").data('itemid'));
                    //$("input[name=itemid]").val($(this).parent("td").data('itemid'));
                });

                jQuery(document).on("click","#itemremarkBtn",function(e){
                    e.preventDefault();
                    getItemRemarkId($(this).parent("td").data('id'),$(this).parent("td").data('itemid'));
                });

                jQuery(document).on("click","#submit",function(e){
                    e.preventDefault();
                    var url = "{{ route('assign.items') }}";
                    assignItemToEmp(url);            
                });

                jQuery(document).on("click","#add_remarks",function(e){
                    e.preventDefault();
                    var url = "{{ route('add.remarks') }}";
                    addRemarks(url);            
                });

            </script>
@endsection