@extends('layouts.app')
@section('title','My Items')
@section('content')

        <div class="content mt-3">
            <div class="animated">
            <div class="row">

                <div class="col-md-12">
                    <?php 
                    echo " The date: " . date('Y-m-d', strtotime('-1 day')) . "  :this";
                     ?>
                    <div class="card">
                        <div class="card-header"><i class="fa fa-shopping-bag">&nbsp;&nbsp;</i><strong class="card-title">My Items</strong></div>  
                        <div class="card-body">
                            <table id="example" class="table table-striped table-bordered"></table>
                        </div>
                        <div class="card-header bg-light"><button onclick="window.history.go(-1); return false;" class="btn btn-md btn-danger"><i class="fa fa-reply"></i>&nbsp;&nbsp;Back</button> </div>
                    </div>
                </div>

            </div>
            </div>
        </div>


            <script src="{{ asset('assets/js/hrm.js') }}" type="text/javascript"></script>

            <script>
                $(document).ready(function(){
                    var url  = "{{route('my.items.list')}}";
                    getMyItems(url);
                });
            </script>
@endsection