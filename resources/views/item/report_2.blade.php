@extends('layouts.app')
@section('title','Report 2')
@section('content')

        <div class="content mt-3">
            <div class="animated">
            <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header"><i class="fa fa-file-text-o">&nbsp;&nbsp;</i><strong class="card-title">Report 2</strong></div>  
                        <div class="card-body">
                            <table id="example" class="table table-striped table-bordered"></table>
                        </div>
                        <div class="card-header bg-light"><button onclick="window.history.go(-1); return false;" class="btn btn-md btn-danger"><i class="fa fa-reply"></i>&nbsp;&nbsp;Back</button> </div>
                    </div>
                </div>

                    
            </div>
            </div>
        </div>


            <script src="{{ asset('assets/js/hrm.js') }}" type="text/javascript"></script>

            <script>
                $(document).ready(function(){
                    var url  = "{{route('get.list.of.report.2')}}";
                    getListOfReport_2(url);
                });
            </script>
@endsection