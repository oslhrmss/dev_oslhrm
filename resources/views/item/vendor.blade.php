@extends('layouts.app')
@section('title','Add Vendor')
@section('content')

        <div class="content mt-3">
            <div class="animated">
            <div class="row">

                <div class="col-md-12">
                    <div class="card">
                    <div class="card-header">
                            <strong class="card-title">Add Vendor</strong>

                    </div>  
                    <div class="card-body">
                    <div id="messages"></div>   
                    <form name="add_vendor_form" id="add_vendor_form" method="POST">
                        {{ csrf_field() }}
                    <div class="form-group col-md-4">
                            <label for="employee" class=" form-control-label"><strong>Vendor Name</strong> <span id="req">*</span></label>
                    <input type="text" name="vendor_name" id="vendor_name" class="form-control">
                    </div>

                    <div class="form-group col-md-4">
                            <label for="employee" class=" form-control-label"><strong>Contact Person</strong> <span id="req">*</span></label>
                    <input type="text" name="contact_person" id="contact_person" class="form-control">
                    </div>

                    <div class="form-group col-md-4">
                            <label for="employee" class=" form-control-label"><strong>Vendor Ph#</strong> <span id="req">*</span></label>
                    <input type="text" name="vendor_phone" id="vendor_phone" class="form-control">
                    </div>
                    
                    </form>
                    </div>
                    <div class="card-footer" style="background-color: #f7f7f7;">
                            <button class="btn btn-primary btn-md" id="add_vendor_btn">
                            <i class="fa fa-plus"></i> Submit
                            </button>

                            <button onclick="window.history.go(-1); return false;" class="btn btn-danger btn-md">
                            <i class="fa fa-ban"></i> Back
                            </button>
                    </div>

                    </div>
                </div>

                    <!---vendor table start---->
                    <div class="col-md-12">
                    <div class="card">
                    <div class="card-header">
                            <strong class="card-title">Vendors</strong>
                            <!-- <a href="{{route('add.vendor')}}" id="add_vendor"><strong class="card-title" style="float:right;">&nbsp; &nbsp;<i class="fa fa-plus"></i>&nbsp;Add Vendors</strong></a>

                            <a href="{{route('add.item')}}" id="add_item"><strong class="card-title" style="float:right;"><i class="fa fa-plus"></i>&nbsp;Add Items</strong></a> -->
                    </div>  
                    <div class="card-body">
                    <table id="example" class="table table-striped table-bordered"></table>
                    </div>
                    </div>
                    </div>
                    <!---vendor table end---->
            </div>

            </div>
        </div>


            <script src="{{ asset('assets/js/hrm.js') }}" type="text/javascript"></script>

            <script>
                $(document).ready(function(){
                    //jQuery(".date").datetimepicker({format: 'YYYY-MM-DD'});
                    var url  = "{{route('get.vendors')}}";
                    getVendors(url);
                });

                $(document).on('click','#add_vendor_btn', function(e){
                    e.preventDefault();
                    var url  = "{{ route('store.vendor') }}";
                    storeVendor(url);
                });
            </script>
@endsection