<!DOCTYPE html>
<html>
<head>
	<title>@yield('title') - {{ config('app.name')}}</title>
	@include('include.header_inc')

</head>
<body class="open">
<div class="loading" style="display: none;">Loading&#8230;</div>	

@include('include.sidebar')
<div id="right-panel" class="right-panel">
@include('include.header')
@yield('content')
@include('include.footer')
</div>
@include('include.footer_inc')
</body>
</html>