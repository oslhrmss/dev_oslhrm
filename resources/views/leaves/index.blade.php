@extends('layouts.app')
@section('title','Company Leaves')
@section('content')



        <div class="content mt-3">
            <div class="animated">
                <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Company Leaves</strong>
                            
                        </div>
                        <div class="card-body">
                            <div id="messages"></div>
                        <form name="company_leave_form" id="company_leave" method="POST">
                            {{csrf_field()}}
                        <div class="form-group col-md-4">
                        <label for="employee" class=" form-control-label"><strong>Sub Company</strong> <span id="req">*</span></label>
                        <select name="sub_company_id" class="subcomp form-control"></select>
                        </div>

                        <div class="form-group col-md-4">
                        <label for="employee" class=" form-control-label"><strong>Leave Type</strong><span id="req">*</span></label>
                        <select name="leave_type" class="leave_type form-control"></select>
                        </div>
                        <div class="form-group col-md-4">
                        <label for="employee" class=" form-control-label"><strong>No of Leave</strong><span id="req">*</span></label>
                        <input type="text" class="form-control" name="no_of_leave" autocomplete="off" value="" required>
                        </div>
                        <div class="card-footer">
                        <button class="btn btn-primary btn-md" type="submit" id="add_company_leaves">
                          <i class="fa fa-plus"></i> Add
                        </button>
                        <button type="reset" class="btn btn-danger btn-md">
                          <i class="fa fa-ban"></i> Reset
                        </button>
                        </div>
                        </form>

                        </div>
                    </div>

                </div>
                

                </div>
                <div class="row">

                 <div class="col-md-12">
                    <div class="card">
                    <div class="card-body">
                    <table id="example" class="table table-striped table-bordered"></table>
                    </div>
                    </div>
                 </div>
                </div>
                                <div class="modal fade" id="amountEditModal" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">

                            <div class="modal-header">
                                <h5 class="modal-title" id="smallmodalLabel">Leave Edit</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            

                            <div class="modal-body">
                            <div id="messages"></div>    
                            <form id="modifyForm" method="post" action="">
                            {{csrf_field()}}
                            <div class="form-group col-md-12">
                            <label for="employee" class=" form-control-label"><strong>No. of Leaves</strong> <span id="req">*</span></label>
                             <input type="text" class="form-control" name="no_of_leaves" autocomplete="off" value="">
                            </div>
                            
                            
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="button" id="submit" class="btn btn-primary">Confirm</button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
</div>
</div>
<script src="{{ asset('assets/js/hrm.js') }}" type="text/javascript"></script>
        <script>
        jQuery(document).ready(function(){
            var url = "{{route('sub.company')}}";
            getSubCompany(url);
            var url = "{{route('leave.type')}}";
            getLeaveType(url);
            var url = "{{route('company.all.leave')}}";
            getAllCompanyLeaves(url);
        });
        jQuery(document).on("click","#add_company_leaves",function(e){
            e.preventDefault();
            var url1 = "{{ route('company.leave.post') }}";
            var url2 = "{{ route('company.all.leave') }}";
            assignCompanyLeave(url1,url2);            
            
        });
        jQuery(document).on("click","#amountEditBtn",function(e){
            e.preventDefault();
            getAdvSalId($(this).parent("td").data('id'));
        });

        jQuery(document).on("click","#submit",function(e){
            e.preventDefault();
            var url = "{{ route('edit.no.of.leaves') }}";
            editSubCompanyNoOfLeaves(url);            
        });
        </script>
@endsection
