@extends('layouts.app')
@section('title','Leave Balance')
@section('content')



        <div class="content mt-3">
            <div class="animated">
                <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Leaves Balance</strong>
                            
                        </div>
                        <div class="card-body">
                            <div id="messages"></div>
                        <form name="leave_balance_form" id="leave_balance" method="POST" action="{{ route('leave.balance.post')}}">
                            {{csrf_field()}}
                        <div class="form-group col-md-12">
                        <label for="employee" class=" form-control-label"><strong>Sub Company</strong> <span id="req">*</span></label>
                        <select name="sub_company_id" class="subcomp form-control"></select>
                        </div>
                        <!-- <div class="form-group col-md-6">
                        <label for="employee" class=" form-control-label"><strong>Year</strong> <span id="req">*</span></label>
                        <select name="leave_year" class="form-control">
                            <option value="">-- Select Year --</option>
                            <option value="2020">2020</option>
                            <option value="2019">2019</option>
                            <option value="2018">2018</option>
                        </select>
                        </div> -->
                        
                        <div class="card-footer">
                        <label for="employee" class="form-control-label"></label>    
                        <button class="btn btn-primary btn-md" id="get_leave_balance">
                          <i class="fa fa-plus"></i> Search
                        </button>
                        <button type="reset" class="btn btn-danger btn-md">
                          <i class="fa fa-ban"></i> Reset
                        </button>
                        </div>
                        </form>

                        </div>
                    </div>

                </div>
                

                </div>
                <div class="row">

                 <div class="col-md-12">
                    <div class="card">
                    <div class="card-body">
                    <table id="example" class="table table-striped table-bordered"></table>
                    </div>
                    </div>
                 </div>
                </div>
</div>
</div>
<script src="{{ asset('assets/js/hrm.js') }}" type="text/javascript"></script>
        <script>
        jQuery(document).ready(function(){
            var url = "{{route('sub.company')}}";
            getSubCompany(url);

        });
        jQuery(document).on("click","#get_leave_balance",function(e){
            e.preventDefault();
            var url = "{{ route('get.all.employee.leave.balance') }}";
            getAllEmployeeLeaveBalance(url);            
            
        });
        </script>
@endsection
