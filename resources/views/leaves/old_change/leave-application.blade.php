@extends('layouts.app')
@section('title','Leave Application')
@section('content')

        <div class="content mt-3">
            <div class="animated">
                <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Leave Application </strong>
                            <a href="#" id="show_balance" data-toggle="modal" data-target="#balanceModel"><strong class="card-title" style="float:right;">Show Balance</strong></a>
                            
                        </div>
                        <div class="card-body">
                        	<div id="messages"></div>
                        	<form name="leaveAppForm" id="leave_app_form" method="POST" >
                        		{{csrf_field()}}
                        	<div class="form-group col-md-3">
                        	<label for="employee" class=" form-control-label"><strong>Leave Type</strong> <span id="req">*</span></label>
                        	<select name="leave_type_id" class="sub_comp_leave_type form-control"></select>
                        	</div>
                        	<div class="form-group col-md-3">
                        	<label for="employee" class=" form-control-label"><strong>From</strong> <span id="req">*</span></label>
                        	 <input type="text" class="date form-control" name="from" autocomplete="off" value="">
                        	</div>
                        	<div class="form-group col-md-3">
                        	<label for="employee" class=" form-control-label"><strong>To</strong> <span id="req">*</span></label>
                            <input type="text" class="date form-control" name="to" autocomplete="off" value="">
                        	</div>
                            <div class="form-group col-md-3">
                                <label for="employee" class=" form-control-label"><strong>Reason</strong><span id="req">*</span></label>
                                <textarea name="reason" class="form-control"></textarea>
                            </div> 
                        	<div class="card-footer">
                        	<button class="btn btn-primary btn-md" id="leave_submit">
                          	<i class="fa fa-plus"></i> Submit
                        	</button>

                        	<button type="reset" class="btn btn-danger btn-md">
                          	<i class="fa fa-ban"></i> Reset
                        	</button>
                        	</div>

                        	</form>

                        </div>	
					</div>
				</div>
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">All Leaves</strong>
                            <!-- <a class="card-title pull-right btn btn-primary" href="{{route('add.employee')}}"> Reset Default Password</a> -->
                        </div>
                        <div class="card-body">
                            <div id="messages"></div>
                            <table id="example" class="table table-striped table-bordered">

                            </table>
                        </div>
                    </div>
                </div>

			</div>
<!-- edit amount modal start -->
                <div class="modal fade" id="amountEditModal" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">

                            <div class="modal-header">
                                <h5 class="modal-title" id="smallmodalLabel">Leave Edit</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            

                            <div class="modal-body">
                            <div id="messages"></div>    
                            <form id="modifyForm" method="post" action="">
                            {{csrf_field()}}
                            <div class="form-group col-md-12">
                            <label for="employee" class=" form-control-label"><strong>From</strong> <span id="req">*</span></label>
                             <input type="text" class="date form-control" name="from" autocomplete="off" value="">
                            </div>
                            <div class="form-group col-md-12">
                            <label for="employee" class=" form-control-label"><strong>To</strong> <span id="req">*</span></label>
                            <input type="text" class="date form-control" name="to" autocomplete="off" value="">
                            <input type="hidden" class="" name="leave_id" autocomplete="off" value="">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="employee" class=" form-control-label"><strong>Reason</strong><span id="req">*</span></label>
                                <textarea name="reason" class="form-control"></textarea>
                            </div>  
                            
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="button" id="submit" class="btn btn-primary">Confirm</button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>    
                <!-- edit amount modal end -->


                <!--  balance modal start -->
                <div class="modal fade" id="balanceModel" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-md" role="document">
                        <div class="modal-content">

                            <div class="modal-header">
                                <h5 class="modal-title" id="smallmodalLabel">Leave Balance</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            

                            <div class="modal-body">
                             <table id="examples" class="table table-striped table-bordered">

                            </table>    
                            </div>
                            <div class="modal-footer">
                                
                            </div>
                            
                        </div>
                    </div>
                </div>    
                <!-- edit amount modal end -->
		</div>
	</div>
<script src="{{ asset('assets/js/hrm.js') }}" type="text/javascript"></script>	
<script type="text/javascript">
	 $(document).ready(function(){

            var url = "{{route('get.sub.company.leave')}}";
            getSubCompanyLeaveType(url);

            jQuery(".date").datetimepicker({format: 'YYYY-MM-DD'});
            var url = "{{route('emp.all.leaves')}}";
            employeeAllLeaves(url);


        });
	$(document).on('click','#leave_submit', function(e){
		
		e.preventDefault();
		var url1  = "{{ route('leave.app.post') }}";
		
		leaveApplication(url1);
	});

    jQuery(document).on("click","#amountEditBtn",function(e){
            e.preventDefault();
            getAdvSalId($(this).parent("td").data('id'));
            $("input[name=leave_id]").val($(this).parent("td").data('leave-id'));
    });

    jQuery(document).on("click","#submit",function(e){
        e.preventDefault();
        var url = "{{ route('edit.leave') }}";
        editLeaveDuration(url);            
    });
    jQuery(document).on("click","#show_balance",function(e){
            e.preventDefault();
           var url = "{{ route('my.leaves.balance') }}";
            getLeaveBalance(url);
    });
</script>
@endsection