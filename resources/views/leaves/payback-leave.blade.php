    @extends('layouts.app')
    @section('title','Payback Leave')
    @section('content')



            <div class="content mt-3">
                <div class="animated">
                    <div class="row">

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Payback Leaves</strong>

                            </div>
                            <div class="card-body">
                            <div id="messages"></div>
                            <form name="payback_leave_form" id="payback_leave_form" method="POST">
                                {{csrf_field()}}
<!--                             <div class="form-group col-md-6">
                            <label for="employee" class=" form-control-label"><strong>Sub Company</strong> <span id="req">*</span></label>
                            <select name="sub_company_id" class="subcomp form-control"></select>
                            </div> -->
                        <div class="form-group col-md-6">
                        <label for="employee" class=" form-control-label"><strong>Employees</strong> <span id="req">*</span></label>
                        <select name="employee" class="empdrop form-control"></select>
                        </div>
                        
<!--                         <div class="form-group col-md-4">
                        <label for="employee" class=" form-control-label"><strong>No of Leaves</strong> <span id="req">*</span></label>
                        <input type="text" class="form-control" name="no_of_leaves">
                        </div> -->
                        
<!--                         <div class="form-group col-md-4">
                                        <label for="employee" class=" form-control-label"><strong>Month</strong> <span id="req">*</span></label>
                                        <select name="month" class="form-control">
                                            @foreach(App\Utilities\Utils::getMonthList() as $key => $value)
                                            <option value="{{$value}}">{{ $value }}</option>
                                            @endforeach
                                        </select>
                                    </div> -->

                        <div class="form-group col-md-6">
                        <label for="employee" class=" form-control-label"><strong>Leave Date </strong><span id="req">*</span></label>

                        <input type="text" class="date form-control" name="date" autocomplete="off" value="{{-- date('Y-m-26', strtotime('last month')) --}}">

                        </div>            
                            <div class="card-footer">
                            <label for="employee" class="form-control-label"></label>
                            <button class="btn btn-primary btn-md" id="payback_btn">
                              <i class="fa fa-plus"></i> Submit
                            </button>
                            <button type="reset" class="btn btn-danger btn-md">
                              <i class="fa fa-ban"></i> Reset
                            </button>
                            </div>
                            </form>

                            </div>
                        </div>

                    </div>


                    </div>
                    <div class="row">

                     <div class="col-md-12">
                        <div class="card">
                        <div class="card-body">
                        <table id="example" class="table table-striped table-bordered"></table>
                        </div>
                        </div>
                     </div>
                    </div>
    </div>
    </div>
    <script src="{{ asset('assets/js/hrm.js') }}" type="text/javascript"></script>
            <script>
            jQuery(document).ready(function(){
                // var url = "{{route('sub.company')}}";
                // getSubCompany(url);
                jQuery(".date").datetimepicker({format: 'YYYY-MM-DD'});
                var url = "{{route('emp.dropdown')}}";
                employeeChosenDropDown(url);

            });

            jQuery(document).on("click","#payback_btn",function(e){
                e.preventDefault();
                var url = "{{ route('generate.payback.leave')}}";
                generatePayBackLeave(url);

            });
            </script>
    @endsection
