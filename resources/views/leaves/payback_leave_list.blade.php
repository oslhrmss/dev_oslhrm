@extends('layouts.app')
@section('title','Payback Leaves List')
@section('content')

<div class="content mt-3">
            <div class="animated">
            	<div class="row">

                    <div class="col-md-12">
                    <div id="messages"></div>
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Payback Leave List</strong>
                                            <div class="row">

                 <div class="col-md-12">
                    <div class="card">
                    <div class="card-body">
                    <table id="example" class="table table-striped table-bordered"></table>
                    </div>
                    </div>
                 </div>
                </div>
                        </div>
                    </div>
                    </div>    
            	</div>

            	</div>
            </div>
<!-- edit amount modal start -->
                <div class="modal fade" id="amountEditModal" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">

                            <div class="modal-header">
                                <h5 class="modal-title" id="smallmodalLabel">Leave Edit</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            
                            <div class="modal-body">
                            <div id="messages"></div>    
                            <form id="modifyForm" method="post" action="">
                            {{csrf_field()}}
                            <div class="form-group col-md-12">
                            <label for="employee" class=" form-control-label"><strong>From</strong> <span id="req">*</span></label>
                             <input type="text" class="date form-control" name="from" autocomplete="off" value="">
                            </div>                            
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="button" id="submit" class="btn btn-primary">Confirm</button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>    
                <!-- edit amount modal end -->


<script src="{{ asset('assets/js/hrm.js') }}" type="text/javascript"></script>
        <script>
        jQuery(document).ready(function(){
            // var url = "{{route('my.leaves.balance')}}";
            // myLeaves(url);
            jQuery(".date").datetimepicker({format: 'YYYY-MM-DD'});
            var url = "{{ route('get.payback.leave.list') }}";
            getPayBackLeavesList(url);

            jQuery(document).on("click","#modifyBtn",function(e){
            e.preventDefault();
            getAdvSalId($(this).parent("td").data('id'));
            $("input[name=id]").val($(this).parent("td").data('id'));
        });

        jQuery(document).on("click","#submit",function(e){
            e.preventDefault();
            var url = "{{ route('edit.payback.leave') }}";
            editLeaveDuration(url);            
        });

        $(document).on('click','#delBtn', function(e){
            var id = $(this).parent("td").data('id');
            var url = "{{route('delete.payback.leave')}}";
           // var url1 = url.replace(':id', id);
            deleteModifiedAttendance(url,$(this).parent("td").data('id'));
            // $(function(){
            //     setTimeout(function() {
            //      $("#view").trigger('click'); 
            //  },100);
            // })
        });

});
</script>

 @endsection  