@extends('layouts.app')
@section('title','Leave Ledger')
@section('content')
        <div class="content mt-3">
            <div class="animated">
            	<div class="row">
            		<div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Leave Ledger</strong>
                                            <div class="row">

                 <div class="col-md-12">
                    <div class="card">
                    <div class="card-body">
                    <table id="example" class="table table-striped table-bordered"></table>
                    </div>
                    </div>
                 </div>
                </div>
                        </div>
                    </div>
                    </div>    
            	</div>

            	</div>
            </div>
<script src="{{ asset('assets/js/hrm.js') }}" type="text/javascript"></script>
        <script>
        jQuery(document).ready(function(){
            var url = "{{route('leave.ledger.detail')}}";
            employeeLeaveLedger(url);

        });
</script>

 @endsection                       