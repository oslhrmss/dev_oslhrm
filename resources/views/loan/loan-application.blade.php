@extends('layouts.app')
@section('title','Loan Application')
@section('content')



        <div class="content mt-3">
            <div class="animated">
                <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Loan Application</strong>
                            
                        </div>
                        <div class="card-body">
                            <div id="messages"></div>
                        <form name="loanAppForm" id="loan_application_form" method="POST">
                            {{csrf_field()}}
  <!--                       <div class="form-group col-md-4">
                        <label for="employee" class=" form-control-label"><strong>Employee</strong> <span id="req">*</span></label>
                        <select name="employee" class="empdrop form-control"></select>
                        </div> -->

                        <div class="form-group col-md-4">
                        <label for="employee" class=" form-control-label"><strong>Amount</strong><span id="req">*</span></label>
                        <input type="text" name="amount" class="form-control">
                        </div>
                        <div class="form-group col-md-4">
                        <label for="employee" class=" form-control-label"><strong>No of Installment</strong><span id="req">*</span></label>
                        <input type="text" class="form-control" name="no_of_installments" autocomplete="off" value="" required>
                        </div>
                        <div class="form-group col-md-4">
                        <label for="employee" class=" form-control-label"><strong>Reason</strong><span id="req">*</span></label>
                        <textarea name="reason" class="form-control"></textarea>
                        </div>      
                        <div class="card-footer">
                        <button class="btn btn-primary btn-md"  id="loan_application_submit">
                          <i class="fa fa-plus"></i> Add
                        </button>
                        <button type="reset" class="btn btn-danger btn-md">
                          <i class="fa fa-ban"></i> Reset
                        </button>
                        </div>
                        </form>

                        </div>
                    </div>

                </div>
                

                </div>
                <div class="row">

                 <div class="col-md-12">
                    <div class="card">
                    <div class="card-body">
                    <table id="example" class="table table-striped table-bordered"></table>
                    </div>
                    </div>
                 </div>
                </div>
                    <div class="modal fade" id="amountEditModal" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">

                            <div class="modal-header">
                                <h5 class="modal-title" id="smallmodalLabel">Amount Edit</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            

                            <div class="modal-body">
                            <div id="messages"></div>    
                            <form id="modifyForm" method="post" action="">
                            {{csrf_field()}}
                            <div class="form-group col-md-12">
                                <label for="employee" class=" form-control-label"><strong>Amount</strong> <span id="req">*</span></label>
                                <input type="text" name="amount" class="time form-control" id="amount" placeholder="" required>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="employee" class=" form-control-label"><strong>No of Installment</strong><span id="req">*</span></label>
                                <input type="text" class="form-control" name="no_of_installments" autocomplete="off" value="" required>
                            </div>
                            
                            
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="button" id="submit" class="btn btn-primary">Confirm</button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>    
</div>
</div>
<script src="{{ asset('assets/js/hrm.js') }}" type="text/javascript"></script>
        <script>
        jQuery(document).ready(function(){
            // var url = "{{route('emp.dropdown')}}";
            // employeeDropDown(url);
            // var url = "{{route('leave.type')}}";
            // getLeaveType(url);
            // var url = "{{route('company.all.leave')}}";
            // getAllCompanyLeaves(url);
            var loan_url = "{{route('loans')}}";
            getLoans(loan_url)
        });
        jQuery(document).on("click","#loan_application_submit",function(e){
            e.preventDefault();
            var url1 = "{{ route('loan.application.post') }}";
//            var url2 = "{{ route('company.all.leave') }}";
            loanApplication(url1);            
            
        });
        jQuery(document).on("click","#amountEditBtn",function(e){
            e.preventDefault();
            getAdvSalId($(this).parent("td").data('id'));
        });
        jQuery(document).on("click","#submit",function(e){
            e.preventDefault();
            var url = "{{ route('edit.loan.amount') }}";
            editLoanAmount(url);            
        });
        </script>
@endsection
