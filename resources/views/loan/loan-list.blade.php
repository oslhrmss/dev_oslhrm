@extends('layouts.app')
@section('title','Loan List')
@section('content')

        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Loan List</strong>
                            <!-- <a class="card-title pull-right btn btn-primary" href="{{route('add.employee')}}"> Reset Default Password</a> lon llist -->
                        </div>
                        <div class="card-body">
                            <div id="messages"></div>
                  <table id="example" class="table table-striped table-bordered">
            
                  </table>
                        </div>
                    </div>
                </div>


                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
        <script src="{{ asset('assets/js/hrm.js') }}" type="text/javascript"></script>
 <script type="text/javascript">
        $(document).ready(function() {
        	var url =  "{{ route('get.loan.list') }}";
        	getLoansList(url);
          
        });


    </script>
@endsection