@extends('layouts.app')
@section('title','Loan Set Off')
@section('content')

<div class="content mt-3">
	<div class="animated fadeIn">
		<div class="row">

			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<strong class="card-title">Loan Sett Off</strong>
					</div>
					<div class="card-body">
						<div id="messages"></div>
						<form name="set-off-form" id="set_off_form" method="POST" action="">
							{{ csrf_field() }}
							<div class="form-group col-md-4">
								<label for="employee" class=" form-control-label"><strong>Employee ID </strong><span id="req">*</span></label>
								<input type="text" name="emp_id" class="form-control">
							</div>
							<div class="card-footer">
								<button class="btn btn-primary btn-md"  id="sett_off">
									<i class="fa fa-search"></i> Search
								</button>
								<button type="reset" class="btn btn-danger btn-md">
									<i class="fa fa-ban"></i> Reset
								</button>
							</div>	
						</form>
					</div>
				</div>
			</div>


		</div>

		<!-- Sett off history -->
		<div class="row set-off-detail">

			<div class="col-md-12">
				<div class="card">
					<div class="card-body">
						<div class="row">
						<div class="col-md-3">
							<label><strong>Employee Name:</strong></label><br><span id="emp_name">Syed Imran Ali Shah</span>
						</div>
						<div class="col-md-3">
							<label><strong>Employee Code:</strong></label><br><span id="emp_code">61</span>
						</div>
						<div class="col-md-3">
							<label><strong>Loan Amount:</strong></label><br><span id="loan_amt">610000</span>
						</div>
						<div class="col-md-3">
							<label><strong>Last Deduction Amount:</strong></label><br><span id="ded_amt">8000</span>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<label><strong>Balance:</strong></label><br><span id="balance">80000</span>
						</div>
						<div class="col-md-3">
							<label><strong>Applied Date:</strong></label><br><span id="applied_date">2018-10-03</span>
						</div>
						<div class="col-md-3">
							<label><strong>Month:</strong></label><br><span id="month">January</span>
						</div>
					</div>	

				</div>

			</div>
		</div>
	</div><!-- .animated -->
	<div class="row set-off-amt-frm">

		<div class="col-md-12">
			<div class="card">
				<div class="card-body">
					<div id="messages"></div>
					<form name="set_off_amt" method="POST" id="set_off_amt">
						{{csrf_field()}}
						<div class="form-group col-md-4">
							<label for="employee" class=" form-control-label"><strong>Set Off Amount </strong><span id="req">*</span></label>
							<input type="text" name="amount" class="form-control">
							<input type="hidden" name="loan_id" class="form-control">
						</div>
						<div class="card-footer">
							<button class="btn btn-primary btn-md"  id="sett_off_amount">
								<i class="fa fa-plus"></i> Submit
							</button>
							<button type="reset" class="btn btn-danger btn-md">
								<i class="fa fa-ban"></i> Reset
							</button>
						</div>	
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="{{ asset('assets/js/hrm.js') }}" type="text/javascript"></script>
<script>
	$(document).ready(function(e){
		$(".set-off-detail").hide();
		$(".set-off-amt-frm").hide();
	});
	$(document).on("click","#sett_off",function(e){
		e.preventDefault();
		var url =  "{{route('get.set.off.history')}}";
		getLoanSetOffHistory(url);

	});

	$(document).on("click","#sett_off_amount",function(e){
		e.preventDefault();
		var url =  "{{route('set.set.off.loan.amt')}}";
		setSetOffLoanAmount(url);

	});
</script>


@endsection