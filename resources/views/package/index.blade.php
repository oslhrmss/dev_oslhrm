@extends('layouts.app')
@section('title','Package Detail')
@section('content')

<div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">
                <div class="col-md-12">
                    @if(session()->has('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                    @endif

                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Package Details</strong>
                              @if(Auth::user()->can('generate-package'))
                              <a class="card-title pull-right btn btn-primary" href="{{route('package')}}"> Generate Package</a>
                              @endif
                        </div>
                        <div class="card-body">
                            <div id="messages"></div>
                  <table id="example" class="table table-striped table-bordered">
            
                  </table>
                        </div>
                    </div>
                </div>


                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
        <script src="{{ asset('assets/js/hrm.js') }}" type="text/javascript"></script>
 <script type="text/javascript">
        $(document).ready(function() {
        	var url =  "{{ route('get.package.detail') }}";
        	getPackageDetail(url);
          
        });


    </script>

@endsection