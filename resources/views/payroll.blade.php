<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>OSL Payroll</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

<style>
body {
  padding-top: 50px;
}   
.clear {
  margin-top:60px;
  margin-bottom:60px;  
}       
.block1 {
  height: 440px;
  margin-bottom:60px;  
} 
.block2 {
  height: 200px; 
}     
.bg-clr {
  background-color: #ffffff;  
}
.bg-clr-blk {
  background-color: black;  
} 
.section-max-width {
  max-width:800px; 
}
/* b {
  color:black;  
} */
</style>

</head>

<body class="bg-clr">

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container section-max-width">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">OSL Payroll</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-right">
            <li class="active"><a href="#">Home</a></li>
            <!-- <li><a href="#about">About</a></li>
            <li><a href="#contact">Contact</a></li> -->
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

<center>    
<section class="section-max-width">

    <div class="clear">

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  block1">
        <form method="POST" action="" id="employeeFrm">
        {{csrf_field()}}
        <div class="row">
        <div class="col-md-6 col-md-offset-3">
        <label>Select Employee</label>
        <select name="employee" id="employee" class="form-control">
        </select>
        </div>
        </div>
        <div class="row">
        <div class="col-md-6 col-md-offset-3">
        <label></label>
        <button class="btn btn-primary" id="search" style="margin-top:15px;">Generate Salary</button>
        </div>
        </div>
        </form>
        <div class="row" id="payroll_detail">
        
        </div>        
    </div>

    </div><!-- /.container -->


    <!-- <div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12 bg-clr-blk clear"><br><br></div> -->

</section>
</center>    

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="{{asset('js/hrm.js')}}"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script>
    $(document).ready(function(){
        var  url = "{{ route('employee') }}";
        getEmployeeList(url);
    });

    $(document).on('click','#search', function(e){
        e.preventDefault();
        var  url = "{{ route('salary') }}";  
        getPayrollDetails(url);
    });

</script>
</body>
</html>