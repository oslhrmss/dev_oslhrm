@extends('layouts.app')
@section('title','Generate Payroll')
@section('content')



        <div class="content mt-3">
            <div class="animated">
            <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Payroll</strong>
                            
                        </div>
                        <div class="card-body">
                        <div id="messages"></div>
                        <form name="leave_deduction_form" id="payrollform"  method="POST">
                            {{csrf_field()}}
                        <div class="form-group col-md-4">
                        <label for="employee" class=" form-control-label"><strong>Sub Company</strong> <span id="req">*</span></label>
                        <select name="sub_company_id" class="subcomp form-control"></select>
                        </div>
                        <div class="form-group col-md-4">
                                <label for="employee" class=" form-control-label"><strong>Month</strong> <span id="req">*</span></label>
                                <select name="month" class="form-control">
                                    @foreach(App\Utilities\Utils::getMonthList() as $key => $value)
                                    <option value="{{$value}}">{{ $value }}</option>
                                    @endforeach
                                </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="employee" class=" form-control-label"><strong>Year</strong> <span id="req">*</span></label>
                            <select name="year" id="year" class="form-control">
                                <option value="">-- Select Year --</option>
                                @foreach(App\Utilities\Utils::getYearList() as $key => $value)
                                <option value="{{$value}}">{{ $value }}</option>
                                @endforeach
                            </select>
                        </div>  
                        <div class="card-footer">
                        <label for="employee" class="form-control-label"></label>    
                        <button class="btn btn-primary btn-md"  id="get_leave_deduction">
                          <i class="fa fa-plus"></i> Generate Payroll
                        </button>
                        <button type="reset" class="btn btn-danger btn-md">
                          <i class="fa fa-ban"></i> Reset
                        </button>
                        </div>
                        </form>

                        </div>
                    </div>

                </div>
                

                </div>
                <div class="row">

                 <div class="col-md-12">
                    <div class="card">
                    <div class="card-body">
                    <table id="example" class="table table-striped table-bordered"></table>
                    </div>
                    </div>
                 </div>
                </div>
</div>
</div>
<script src="{{ asset('assets/js/hrm.js') }}" type="text/javascript"></script>
        <script>
        jQuery(document).ready(function(){
            var url = "{{route('sub.company')}}";
            getSubCompany(url);

        });

        jQuery(document).on("click","#get_leave_deduction",function(e){
            e.preventDefault();
            var url = "{{ route('generate.payroll')}}";
            generatePayroll(url);            
            
        });
        </script>
@endsection
