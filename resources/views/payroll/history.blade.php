@extends('layouts.app')
@section('title','Payroll History')
@section('content')



        <div class="content mt-3">
            <div class="animated">
                <div class="row">

                 <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Payroll History</strong>
                            
                        </div>
                    <div class="card-body">
                                              <div class="form-group col-md-4">
                        <label for="employee" class=" form-control-label"><strong>Sub Company</strong> <span id="req">*</span></label>
                        <select name="sub_company_id" id="sub_company" class="subcomp form-control"></select>
                        </div>
                        <div class="form-group col-md-4">
                                <label for="employee" class=" form-control-label"><strong>Month</strong> <span id="req">*</span></label>
                                <select name="month" id="month" class="form-control">
                                    <option value="">-- Select Month --</option>
                                    @foreach(App\Utilities\Utils::getMonthList() as $key => $value)
                                    <option value="{{$value}}">{{ $value }}</option>
                                    @endforeach
                                </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="employee" class=" form-control-label"><strong>Year</strong> <span id="req">*</span></label>
                            <select name="year" id="year" class="form-control">
                                <option value="">-- Select Year --</option>
                                @foreach(App\Utilities\Utils::getYearList() as $key => $value)
                                <option value="{{$value}}">{{ $value }}</option>
                                @endforeach
                            </select>
                        </div>  
                    <table id="example" class="table table-striped table-bordered"></table>
                    </div>
                    </div>
                 </div>
                </div>
</div>
</div>
<script src="{{ asset('assets/js/hrm.js') }}" type="text/javascript"></script>
        <script>
        jQuery(document).ready(function(){
            var url = "{{route('sub.company')}}";
            getSubCompany(url);
            var url1 = "{{ route('get.payroll.history')}}";
            getPayrollHistory(url1);

       });

        </script>
@endsection
