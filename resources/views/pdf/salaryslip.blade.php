
<!doctype html>
<html lang="{{ app()->getLocale() }}"><head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
 
        <title>Salaryslip</title>
        <style type="text/css">
        body{
        	font-family: sans-serif;
        }
 table {
  border-collapse: collapse;
  width: 100%;
}
table, th, td{
	/*border: 1px solid #000000 !important;*/
}
td {
  height: 15px;
  font-size: 12px !important;
  /*vertical-align: bottom;*/
  text-align: left;
  padding-bottom: 10px !important;
}
td.ping {
	width: 130px !important;
}
div {
	margin : 0 0 10px 0;
}

table.table2, th {
	border: 1px solid #4c609f !important;
}

.table2 th{
    background: #4c609f;
    color: #ffffff;
        text-align: center !important;
}
.add{
	font-size:8px !important;
}       
        </style>
</head><body>
	<div class="row">
		<p align="right" style="color:red !important"><strong>CONFIDENTIAL</strong></p>
	</div>
	<div class="row" align="center">
		<img src="{{asset('images/oceanic_group.jpeg')}}" width="500">
	</div>
	<div class="row" align="center">

		<h3 class="text-center" style="text-transform: uppercase">{{$data->company}}</h3>
		<p class="text-center" style="text-transform: uppercase; background-color: #4c609f !important; padding: 5px 5px 5px 5px; color:#ffffff !important"><strong>Salary slip for the month of {{$data->month}} {{$data->year}}</strong></p>
	</div>
        <div class="row"  style="border: 1px solid #4c609f !important; padding: 8px !important;">
            <table>

                <tr>
                    <td class="ping">Employee Code:</td>
                    <td>{{$data->emp_id}}</td>
                    <td class="ping">Department:</td>
                    <td>{{$data->department}}</td>
                </tr>
                <tr>
                    <td class="ping">Employee Name:</td>
                    <td>{{$data->emp_name}}</td>
                    <td class="ping">Employee Type:</td>
                    <td>{{$data->emptype}}</td>
                </tr>
                <tr>
                    <td class="ping">Designation:</td>
                    <td>{{$data->designation}}</td>
                    <td class="ping">Payroll Period:</td>
                    <td>26-25</td>
                </tr>
 
            </table>
        </div>
        	<table class="table2">

                <tr>
                    <th>Gross Salary &nbsp; (Amount in PKR)</th>
                    <th>Deduction &nbsp; (Amount in PKR)</th>

                </tr>
                <tr>
                	<td style="border-right: 1px solid #4c609f;">
                		<table>
                			<tr>
                                <td>Basic Salary:</td>
                                <?php $varrr = $data->gross_sal/100 * 66.667; ?>
                                <td>{{ $varrr }} </td>
                            </tr>
                            <tr>
                                <td>Medical Allowance:</td>
                                <?php $varr = ($data->gross_sal/100 * 66.667)*10/100; ?>
                                <td>{{ $varr }}</td>
                            </tr>
                            <tr>
                                <td>House Rent:</td>
                                <?php
                                    $var = ($data->gross_sal/100 * 66.667)*40/100;
                                    
                                ?>
                                <td>{{ $var }}</td>
                            </tr>
                            <!-- <tr>
                                <td>Transpotation Allowance:</td>
                                <td>0</td>
                            </tr> -->
                            <tr>
                                <td>Gross Salary:</td>
                                <td>{{$data->gross_sal}}</td>
                            </tr>
                		</table>

                	</td>
	               	<td style="border-right: 1px solid #4c609f;">
                		<table>
                			<tr>
                				<td>Provident Fund:</td>
 				                <td>{{$data->pf}}</td>
                			</tr>
                			<tr>
                				<td>EOBI:</td>
 				                <td>{{$data->eobi}}</td>
                			</tr>
                			<tr>
                				<td>Income Tax:</td>
 				                <td>{{$data->tax}}</td>
                			</tr>
                			<tr>
                				<td>Advance Salary:</td>
 				                <td>{{$data->adv_sal ?:0}}</td>
                			</tr>
                			<tr>
                				<td>Loan Deduction:</td>
 				                <td>{{$data->loan_ded ?:0}}</td>
                			</tr>
                			<tr>
                				<td>Item Deduction:</td>
 				                <td>{{ $data->item_deduction ?:0 }}</td>
                			</tr>
                			<tr>
                				<td>Fuel Amount:</td>
 				                <td>{{ $data->fuel_amount ?:0 }}</td>
                			</tr>
                			<tr>
                				<td>Parking Amount:</td>
 				                <td>{{ $data->parking_amount ?:0 }}</td>
                			</tr>
                			<tr>
                				<td>Arrears:</td>
 				                <td>{{ $data->arrear_amount ?:0 }}</td>
                			</tr>
                			<tr>
                				<td>Other Deductions:</td>
 				                <td>{{ $data->leave_ded_amt ?:0 }}</td>
                			</tr>            
                		</table>
                	</td>

                </tr>
                <tr>
                    <th></th>
                    <th>Net Salary: &nbsp; {{ $data->net_sal ?:0 }}</th>

                </tr>
            </table>
			<div class="row" style="font-size:5px !important">
				<table class="address">
					<tr>
						<td class="add"><p><strong>Pakistan Office:<strong></p>
							
Oceanic Star Line (Pvt.) Ltd.

Office # 501 & 502, 5th Floor, Progressive Center, PECHS, Block-6, Main Shahra-e-Faisal, Karachi – 74000, Sindh, Pakistan.

UAN: +92 (21) 111-786-675
						</td>
						<td class="add"><p><strong>U.A.E<strong></p>
							
OSL Shipping L.L.C.

Suite # 302, 3rd Floor, Shipping Tower,

Al Mina Road, Bur Dubai. P.O. Box – 56457

Dubai – U.A.E

Landline: + 971-4-3235595
						</td>
						<td class="add"><p><strong>MALAYSIA<strong></p>
							
Oceanic Projects Sdn. Bhd.

Suite # 22-01, Level 22, Centro Building, No. 8,

Jalan Batu Tiga Lama, 41300, Klang, Selangor, Malaysia.

Landline: +603-33626903-4
						</td>
						<td class="add"><p><strong>SINGAPORE<strong></p>
							
Oceanic Projects Pte. Ltd.

10 Bukit Batok, Crescent # 13-05,

The Spire, Singapore 658079

Landline: +65-65153205
						</td>

					</tr>            
				</table>

			</div>
            <div class="row"  style="margin-top:10px !important; padding: 8px !important; color:red; font-size: 15px !important">
            	<strong>Note: This is system generated Salaryslip.</strong>
	        </div>
</body></html>


