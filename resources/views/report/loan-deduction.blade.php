@extends('layouts.app')
@section('title','Loan Deduction Report')
@section('content')

<div class="content mt-3">
            <div class="animated">
            	<div class="row">
            		<div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Loan Deduction Report</strong>
                                            <div class="row">

                 <div class="col-md-12">
                    <div class="card">
                    <div class="card-body">
                    <table id="example" class="table table-striped table-bordered"></table>
                    </div>
                    </div>
                 </div>
                </div>
                        </div>
                    </div>
                    </div>    
            	</div>

            	</div>
            </div>


<script src="{{ asset('assets/js/hrm.js') }}" type="text/javascript"></script>
        <script>
        jQuery(document).ready(function(){
            // var url = "{{route('my.leaves.balance')}}";
            // myLeaves(url);
            var url = "{{ route('get.loan.deduction.report') }}";
            getLoanDeductionReport(url);
        });
</script>

 @endsection  