<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Notifications\TestEmails;
Route::get('get-payslip',['as'=>'get.payslip','uses'=>'DataController@getPDF']);
Route::get('migrate', function(){
  return Artisan::call('migrate');
});

Route::get('send', function(){
    $user  =   App\Model\User::find(2);
    //dd($user);
    $user->email = 'mkdevse@outlook.com';
    $user->notify(new TestEmails('Hello','Emailcheck'));
    return 'done';
});

Route::get('check_slabs', function(){

for($i = 2018; $i <= (2018 + 1); $i++)
{
    $package = \App\Model\EmployeePackageDetail::where('year', $i)->where('emp_id',367)->get();
    $final = $package->map(function($item, $key){
        $employee = \App\Model\Employee::where('pf_slab_id', $item->pf_slab_id)->where('emp_no', 367)->first();
       

        if($employee)
        {
            $no_of_month = \App\Utilities\Utils::getEmployeeDurationInCompany($employee->pf_effective_date);
            
            $pf_amount = \App\Model\PfSlab::where('id',$item->pf_slab_id)->value('amount');
            $final_amt = $no_of_month * $pf_amount; 
            
        }
        return  $final_amt;
    });

    $array[$i] = $final;

}

     return $array;
});

Route::get('slabs', function(){
	$response = [];
	$employees = \App\Model\Employee::whereIsActive(1)->where('company_id', 1)->get(['emp_no','name','current_salary']);
	foreach ($employees as $key => $value) {
		# code...
		$tax_slabs = DB::table('tax_slabs')->get();
		$result[] = $tax_slabs->map(function($item, $keys) use ($value, $response){
		$annual_gross_sal = $value->current_salary * 12;
        $basic_salary = (($annual_gross_sal / 100) * 66);
        $annual_medical = (($basic_salary/100) * 10);
        $final_annual_sal = $annual_gross_sal - $annual_medical;
			if($item->from <= $final_annual_sal   && $final_annual_sal <= $item->to )
			{

				$response['emp_id'] = $value->emp_no;
				$response['tax_slab_id'] = $item->id;
				$response['created_by'] = 1;
				$response['updated_by'] = 1;
			}
			if(count($response))
			{
				\DB::table('employee_tax_slab')->insert($response);
			}	
		});
		
	}

});


Route::get('/',function(){
  return redirect('/login');
});
Route::group(['middleware' => 'auth'], function(){
Route::get('admin-reset-pass',['as'=>'admin.reset.pass', 'uses'=>'UserController@adminResetDefaultPasswordPost']);

  Route::get('leave-type',['as'=>'leave.type','uses'=>'LeaveController@getLeaveType']);
  Route::get('emp-dropdown',['as'=>'emp.dropdown','uses'=>'EmployeeController@employeeDropDown']);
Route::get('monthly-timing-report',['as'=>'monthly.timing.report','uses'=>'KPIController@monthlyTimingReport']);


  Route::group(['middleware'=>['role:admin|hr-manager|manager']], function(){
    
    Route::get('employee',['as'=>'employee','uses'=>'EmployeeController@employeeList']);
    Route::get('ex-employees',['as'=>'ex.employee','uses'=>'EmployeeController@getExEmployees']);
    Route::get('bulk-employee-creds',['as'=>'bulk.employee.creds','uses'=>'DataController@bulkCredentialsGenerate']);
    Route::get('change-password',function(){

      return view('auth.custom_change_password');
      

    })->name('change.password');
    Route::post('change-password',['as'=>'change.password.custom','uses'=>'UserController@changePassword']);
    Route::post('salary',['as'=>'salary','uses'=>'EmployeeController@generateSalary']);
    Route::post('import',['as'=>'import.post','uses'=>'DataController@import2']);
    Route::get('company',['as'=>'company','uses'=>'EmployeeController@getCompany']);
    Route::get('department',['as'=>'department','uses'=>'EmployeeController@getDepartment']);
    Route::get('sub-department/{id}',['as'=>'sub.department','uses'=>'EmployeeController@getSubDepartment']);
    Route::get('designation',['as'=>'designation','uses'=>'EmployeeController@getDesignation']);
    Route::get('get-employees',['as'=>'employees','uses'=>'EmployeeController@getEmployees']);
    Route::get('add-employee',['as'=>'add.employee','uses'=>'EmployeeController@addEmployee']);
    Route::post('add-employee',['as'=>'add.employee.post','uses'=>'EmployeeController@storeEmployee']);
    Route::get('edit-employee',['as'=>'edit.employee','uses'=>'EmployeeController@editEmployee']);

    Route::post('edit-employee',['as'=>'edit.employee.post','uses'=>'EmployeeController@editEmployees']);
    Route::get('emp-status-change/{id}',['as'=>'emp.status.change','uses'=>'EmployeeController@employeeActiveInactive']);
    Route::get('manager-dropdown',['as'=>'manager.dropdown','uses'=>'EmployeeController@managerDropDown']);



    /* attendance routes */
    Route::get('import',['as'=>'import','uses'=>'AttendanceController@importView']);
    Route::get('view',['as'=>'view','uses'=>'AttendanceController@viewAttendanceOfEmployee']);
    Route::get('attendance',['as'=>'attendance','uses'=>'AttendanceController@index']);
    Route::get('company-attendance',['as'=>'company.attendance','uses'=>'AttendanceController@companyWiseAttendance']);
    Route::post('company-attendance',['as'=>'company.attendance','uses'=>'AttendanceController@companyAttendance']);
    
    Route::post('emp-attendance',['as'=>'emp.attendance','uses'=>'AttendanceController@employeeAttendance']);
    Route::get('get-time',['as'=>'get.time','uses'=>'AttendanceController@getTimeInTimeOutValues']);
    Route::post('modify-time',['as'=>'modify.time','uses'=>'AttendanceController@modifyManualAttendance']);
    Route::get('modified-attendance',['as'=>'modified.attendance','uses'=>'AttendanceController@modifiedAttendance']);
    Route::post('get-modified-attendance',['as'=>'get.modified.attendance','uses'=>'AttendanceController@getModifiedAttendance']);
    Route::get('delete-modified-attendance',['as'=>'delete.modified.attendance','uses'=>'AttendanceController@deleteModifiedAttendance']);

    /* attendance routes end */

    /*Leaves routes start*/

    Route::get('company-leave',['as'=>'company.leave','uses'=>'LeaveController@index']);
    Route::get('company-all-leave',['as'=>'company.all.leave','uses'=>'LeaveController@getAllCompanyLeaves']);
    Route::get('sub-company',['as'=>'sub.company','uses'=>'LeaveController@getSubCompany']);
    Route::post('company-leave-post',['as'=>'company.leave.post','uses'=>'LeaveController@assignCompanyLeave']);
    Route::post('edit-no-of-leaves',['as'=>'edit.no.of.leaves','uses'=>'LeaveController@editSubCompanyNoOfLeaves']);
    Route::get('leave-balance',['as'=>'leave.balance.view','uses'=>'LeaveController@leaveBalanceView']);
    Route::post('leave-balance-post',['as'=>'leave.balance.post','uses'=>'LeaveController@getLeaveBalance']);
    Route::post('leave-app-post',['as'=>'leave.app.post','uses'=>'LeaveController@leaveApplication']);
    Route::get('leave-application',['as'=>'leave.application','uses'=>'LeaveController@leaveApplicationForm']);
    Route::get('leave-approvals',['as'=>'leave.approvals','uses'=>'LeaveController@leaveApprovals']);
    Route::get('get-leave-approvals',['as'=>'get.leave.approvals','uses'=>'LeaveController@getLeaveApprovals']);
    Route::get('leave-approval-action/{id}',['as'=>'leave.approval.action','uses'=>'LeaveController@leaveApprovalAction']);
    Route::get('leave-cancel/{id}',['as'=>'leave.cancel','uses'=>'LeaveController@leaveCancel']);
    Route::get('position',['as'=>'get.position','uses'=>'EmployeeController@getPosition']);

    Route::get('pf-slabs',['as'=>'pf.slabs','uses'=>'EmployeeController@getPfSlabs']);
    Route::get('default-password',['as'=>'default.password','uses'=>'UserController@defaultPassword']);
    Route::get('reset-default-password',['as'=>'reset.default.password','uses'=>'UserController@resetDefaultPasswordPost']);
    Route::get('advance-salary-approval',['as'=>'adv.sal.approvals','uses'=>'LoanController@getAdvSalApprovalView']);
    Route::get('get-adv-sal-approvals',['as'=>'get.adv.sal.approvals','uses'=>'LoanController@getAdvSalApprovals']);
    Route::get('adv-sal-approval-action/{id}',['as'=>'adv.sal.approval.action','uses'=>'LoanController@advSalApprovalAction']);
    Route::get('adv-sal-list',['as'=>'adv.sal.list','uses'=>'LoanController@getAdvSalList']);
    Route::get('get-adv-sal-list',['as'=>'get.adv.sal.list','uses'=>'LoanController@getAdvSalaryList']);
    Route::get('get-loan-list',['as'=>'get.loan.list','uses'=>'LoanController@getLoansList']);
    Route::get('loan-list',['as'=>'loan.list','uses'=>'LoanController@getLoanList']);
    Route::get('loan-approval',['as'=>'loan.approval','uses'=>'LoanController@getLoanApprovalView']);
    // Route::get('get-loan-approvals',['as'=>'get.loan.approvals','uses'=>'LoanController@getLoanApprovals']);
    Route::get('deduction',['as'=>'deduction','uses'=>'DeductionController@index']);

    Route::post('deduction-emp-list',['as'=>'deduction.emp.list','uses'=>'DeductionController@employeeListWhoseBalancNonZero']);

    Route::get('relaxation-detail-emp-wise/{id}',['as'=>'relaxation.detail.emp.wise','uses'=>'DeductionController@relaxationDetail']);
    Route::get('relax/{loanid}',['as'=>'relax','uses'=>'DeductionController@loanRelaxation']);
    Route::get('remove-relax/{loanid}',['as'=>'remove.relax','uses'=>'DeductionController@removeLoanRelaxation']);
    Route::get('loan-deduct',['as'=>'loan.deduct','uses'=>'DeductionController@loanDeduct']);
    Route::post('edit-deduct-amount',['as'=>'edit.deduct.amount','uses'=>'DeductionController@setDeductionAmountByFinance']);
    Route::get('payroll-attendance',['as'=>'payroll.attendance','uses'=>'AttendanceController@payrollAttendanceData']);
    Route::post('get-payroll-attendance',['as'=>'get.payroll.attendance','uses'=>'AttendanceController@getPayrollAttendanceData']);
    Route::get('leave-deduction',['as'=>'leave.deduction','uses'=>'DeductionController@leaveDeductionView']);

    Route::get('item-deduction',['as'=>'item.deduction','uses'=>'DeductionController@itemDeduction']);

    Route::post('leave-deduction-post',['as'=>'leave.deduction.post','uses'=>'DeductionController@leaveDeduction']);
    Route::get('all-employee-leave-list',['as'=>'all.employee.leave.list.view','uses'=>'LeaveController@allEmployeeLeaveListView']);
    Route::get('get-all-employee-leave-list',['as'=>'all.employee.leave.list','uses'=>'LeaveController@allEmployeeLeaveList']);
    Route::get('all-employee-leave-balance',['as'=>'all.employee.leave.balance','uses'=>'LeaveController@allEmployeeLeaveBalance']);
    Route::post('get-all-employee-leave-balance',['as'=>'get.all.employee.leave.balance','uses'=>'LeaveController@getAllEmployeeLeaveBalance']);



    // Route::get('payroll', ['as'=>'payroll','uses'=>'EmployeeController@getPayroll']);
    // Route::post('generate-payroll', ['as'=>'generate.payroll','uses'=>'EmployeeController@generatePayroll']);
    Route::get('get-regions', ['as'=>'get.regions','uses'=>'EmployeeController@getRegions']);
    Route::get('get-emp-type', ['as'=>'get.emp.type','uses'=>'EmployeeController@getEmployeeType']);

    Route::get('payback-leave',['as'=>'payback.leave','uses'=>'LeaveController@payBackLeaves']);
    Route::post('generate-payback-leave',['as'=>'generate.payback.leave','uses'=>'LeaveController@generatePayBackLeave']);


    Route::get('payroll', ['as'=>'payroll','uses'=>'EmployeeController@getPayroll']);
    Route::post('generate-payroll', ['as'=>'generate.payroll','uses'=>'EmployeeController@generatePayroll']);
    Route::get('payroll-history', ['as'=>'payroll.history','uses'=>'EmployeeController@payrollHistory']);
    Route::get('get-regions', ['as'=>'get.regions','uses'=>'EmployeeController@getRegions']);
    Route::get('get-emp-type', ['as'=>'get.emp.type','uses'=>'EmployeeController@getEmployeeType']);
    Route::get('items', ['as'=>'items','uses'=>'ItemController@index']);
    Route::get('get-items', ['as'=>'get.items','uses'=>'ItemController@getItems']);
    Route::get('add-item', ['as'=>'add.item','uses'=>'ItemController@addItem']);
    Route::post('store-item', ['as'=>'store.item','uses'=>'ItemController@storeItem']);
    Route::get('assign-item', ['as'=>'assign.item','uses'=>'ItemController@getEmployeeAssignedItem']);
    Route::get('get-item-wit-type', ['as'=>'get.item.with.type','uses'=>'ItemController@getItemWithType']);
    Route::post('assign-item-employee', ['as'=>'assign.item.emp','uses'=>'ItemController@assignItemToEmployee']);
    Route::get('get-emp-item', ['as'=>'get.emp.item','uses'=>'ItemController@getEmployeeItem']);
    Route::post('gen-item-ledger', ['as'=>'gen.item.ledger','uses'=>'ItemController@generateEmpItemLedger']);


    Route::get('payback-leave-list',['as'=>'payback.leave.list','uses'=>'LeaveController@payBackLeavesList']);
    Route::get('get-payback-leave-list',['as'=>'get.payback.leave.list','uses'=>'LeaveController@getPayBackLeavesList']);
    Route::post('edit-payback-leave',['as'=>'edit.payback.leave','uses'=>'LeaveController@editPayBackLeave']);
    Route::get('delete-payback-leave',['as'=>'delete.payback.leave','uses'=>'LeaveController@deletePayBackLeave']);
    Route::get('arrears', ['as'=>'arrears','uses'=>'ArrearController@index']);
    Route::post('assign-arrear', ['as'=>'assign.arrear','uses'=>'ArrearController@assignArrears']);
    Route::get('get-arrears', ['as'=>'get.arrears','uses'=>'ArrearController@getArrears']);


    Route::get('arrears', ['as'=>'arrears','uses'=>'ArrearController@index']);
    Route::post('assign-arrear', ['as'=>'assign.arrear','uses'=>'ArrearController@assignArrears']);
    Route::get('year-list','EmployeeController@getYearList');
    Route::get('get-payroll-history',['as'=>'get.payroll.history','uses'=>'EmployeeController@getPayrollHistory']);
    Route::get('payback-leave-list',['as'=>'payback.leave.list','uses'=>'LeaveController@payBackLeavesList']);
    Route::get('get-payback-leave-list',['as'=>'get.payback.leave.list','uses'=>'LeaveController@getPayBackLeavesList']);
    Route::get('get-payback-leave-list',['as'=>'get.payback.leave.list','uses'=>'LeaveController@getPayBackLeavesList']);
    Route::get('get-payroll-summary', ['as'=>'get.payroll.summary', 'uses'=>'EmployeeController@payrollSummary']);
    Route::get('get-item-ded-summary', ['as'=>'get.item.ded.summary', 'uses'=>'ItemController@itemDeductionSummary']);
    Route::get('get-loan-ded-summary', ['as'=>'get.loan.ded.summary', 'uses'=>'DeductionController@loanDeductionSummary']);
    Route::get('package-detail', ['as'=>'package.detail', 'uses'=>'PackageController@index']);
    Route::get('get-package-detail', ['as'=>'get.package.detail', 'uses'=>'PackageController@getPackageDetail']);
	Route::get('package', ['as'=>'package', 'uses'=>'PackageController@generatePackage']);
    Route::get('loyality-awards', ['as'=>'loyality.awards', 'uses'=>'LoyalityAwardController@index']);
    Route::post('add-loyality-awards', ['as'=>'add.loyality.awards', 'uses'=>'LoyalityAwardController@addAwards']);
    Route::get('loyality-awards/edit/{id}/{year}', ['as'=>'edit.awards', 'uses'=>'LoyalityAwardController@edit']);

  });

Route::group(['middleware' => ['role:employee']], function(){
  Route::get('change-password',function(){

    return view('auth.custom_change_password');
  })->name('change.password');
  Route::post('change-password',['as'=>'change.password.custom','uses'=>'UserController@changePassword']);
  Route::get('timesheet',['as'=>'timesheet','uses'=>'AttendanceController@index']);
  Route::post('my-attendance',['as'=>'my.attendance','uses'=>'AttendanceController@employeeAttendance']);
    // Route::get('sub-company',['as'=>'sub.company','uses'=>'LeaveController@getSubCompany']);

  Route::get('my-leaves',['as'=>'my.leaves','uses'=>'LeaveController@myLeaves']);
  Route::get('my-leave-balance',['as'=>'my.leaves.balance','uses'=>'LeaveController@getLeaveBalance']);
  Route::get('get-sub-company-leave',['as'=>'get.sub.company.leave','uses'=>'LeaveController@getSubCompanyLeaveType']);
  Route::get('emp-all-leaves',['as'=>'emp.all.leaves','uses'=>'LeaveController@employeeAllLeaves']);
  
  Route::post('edit-leave',['as'=>'edit.leave','uses'=>'LeaveController@editLeaveDuration']);

  Route::get('get-payback-leave',['as'=>'get.payback.leaves','uses'=>'LeaveController@employeePayBackLeaves']);

    // Route::get('leave-type',['as'=>'leave.type','uses'=>'LeaveController@getLeaveType']);
    // Route::get('my-leaves',['as'=>'leave.type','uses'=>'LeaveController@getLeaveType']);
  Route::post('leave-app-post',['as'=>'leave.app.post','uses'=>'LeaveController@leaveApplication']);
  Route::get('leave-application',['as'=>'leave.application','uses'=>'LeaveController@leaveApplicationForm']);
  Route::get('advance-salary',['as'=>'advance.salary','uses'=>'LoanController@advanceSalaryForm']);
  Route::post('advance-salary-post',['as'=>'advance.salary.post','uses'=>'LoanController@advanceSalary']);
  Route::get('get-dv-sal',['as'=>'get.adv.sal','uses'=>'LoanController@getAdvSalary']);
  Route::post('edit-amount',['as'=>'edit.amount','uses'=>'LoanController@editAdvSalAmount']);
  Route::get('loan-application',['as'=>'loan.application.form','uses'=>'LoanController@getLoanApplicationForm']);
  Route::post('loan-application-post',['as'=>'loan.application.post','uses'=>'LoanController@loanApplication']);
  Route::get('loans',['as'=>'loans','uses'=>'LoanController@getLoans']);
  Route::post('edit-loan-amount',['as'=>'edit.loan.amount','uses'=>'LoanController@editLoanAmount']);
  Route::get('loan-ledger',['as'=>'loan.ledger','uses'=>'LoanController@loanLedger']);
  Route::get('loan-ledger-detail',['as'=>'loan.ledger.detail','uses'=>'LoanController@employeeLoanLedger']);
  Route::get('leave-ledger',['as'=>'leave.ledger','uses'=>'LeaveController@leaveLedger']);
  Route::get('leave-ledger-detail',['as'=>'leave.ledger.detail','uses'=>'LeaveController@employeeLeaveLedger']);
  Route::get('get-pf-value',['as'=>'get.pf.value','uses'=>'HomeController@getPfTotalValue']);
  Route::get('item-ledger',['as'=>'item.ledger','uses'=>'ItemController@itemLedger']);
  Route::get('get-item-ledger',['as'=>'get.item.ledger','uses'=>'ItemController@getItemLedger']);
  Route::get('get-rep-manager',['as'=>'get.rep.manager','uses'=>'HomeController@getReportingManager']);

});


Route::group(['middleware' => ['role:finance|hr-manager']], function(){
  Route::get('advance-salary-approval',['as'=>'adv.sal.approvals','uses'=>'LoanController@getAdvSalApprovalView']);
  Route::get('get-adv-sal-approvals',['as'=>'get.adv.sal.approvals','uses'=>'LoanController@getAdvSalApprovals']);
  Route::get('adv-sal-approval-action/{id}',['as'=>'adv.sal.approval.action','uses'=>'LoanController@advSalApprovalAction']);
  Route::get('adv-sal-paid-action/{id}',['as'=>'adv.sal.paid.action','uses'=>'LoanController@advSalPaidAction']);
  Route::get('loan-approval',['as'=>'loan.approval','uses'=>'LoanController@getLoanApprovalView']);
  Route::get('get-loan-approvals',['as'=>'get.loan.approvals','uses'=>'LoanController@getLoanApprovals']);
  Route::get('loan-approval-action/{id}',['as'=>'loan.approval.action','uses'=>'LoanController@loanApprovalAction']);
  Route::get('loan-paid-action/{id}',['as'=>'loan.paid.action','uses'=>'LoanController@loanPaidAction']);
  Route::get('loan-set-off',['as'=>'loan.set.off','uses'=>'LoanController@loanSetOff']);
  Route::post('get-set-off-history',['as'=>'get.set.off.history','uses'=>'LoanController@getLoanSetOffHistory']);
  Route::post('set-set-off-loan-amt',['as'=>'set.set.off.loan.amt','uses'=>'LoanController@setSetOffLoanAmount']);
  Route::get('payroll', ['as'=>'payroll','uses'=>'EmployeeController@getPayroll']);
  Route::post('generate-payroll', ['as'=>'generate.payroll','uses'=>'EmployeeController@generatePayroll']);
  Route::get('payroll-history', ['as'=>'payroll.history','uses'=>'EmployeeController@payrollHistory']);
});

// audit routes
  Route::group(['middleware'=>['role:hr-manager|admin']], function(){
    Route::get('adv-sal-list',['as'=>'adv.sal.list','uses'=>'LoanController@getAdvSalList']);
    Route::get('get-adv-sal-list',['as'=>'get.adv.sal.list','uses'=>'LoanController@getAdvSalaryList']);
    Route::get('loan-list',['as'=>'loan.list','uses'=>'LoanController@getLoanList']);
    Route::get('get-loan-list',['as'=>'get.loan.list','uses'=>'LoanController@getLoansList']);
    Route::get('audit-attendance',['as'=>'audit.attendance','uses'=>'AttendanceController@auditAttendance']);
    Route::post('emp-attendance-audit',['as'=>'emp.attendance.audit','uses'=>'AttendanceController@employeeAttendance']);
    Route::get('company-leave',['as'=>'company.leave','uses'=>'LeaveController@index']);
    Route::get('company-all-leave',['as'=>'company.all.leave','uses'=>'LeaveController@getAllCompanyLeaves']);
    Route::get('sub-company',['as'=>'sub.company','uses'=>'LeaveController@getSubCompany']);
    Route::post('company-leave-post',['as'=>'company.leave.post','uses'=>'LeaveController@assignCompanyLeave']);
    Route::get('all-employee-leave-list',['as'=>'all.employee.leave.list.view','uses'=>'LeaveController@allEmployeeLeaveListView']);
    Route::get('get-all-employee-leave-list',['as'=>'all.employee.leave.list','uses'=>'LeaveController@allEmployeeLeaveList']);
    Route::get('all-employee-leave-balance',['as'=>'all.employee.leave.balance','uses'=>'LeaveController@allEmployeeLeaveBalance']);
    Route::post('get-all-employee-leave-balance',['as'=>'get.all.employee.leave.balance','uses'=>'LeaveController@getAllEmployeeLeaveBalance']);
   Route::get('payback-leave-list',['as'=>'payback.leave.list','uses'=>'LeaveController@payBackLeavesList']);
   Route::get('get-payback-leave-list',['as'=>'get.payback.leave.list','uses'=>'LeaveController@getPayBackLeavesList']);
   Route::get('payroll', ['as'=>'payroll','uses'=>'EmployeeController@getPayroll']);
    Route::post('generate-payroll', ['as'=>'generate.payroll','uses'=>'EmployeeController@generatePayroll']);
    Route::get('payroll-history', ['as'=>'payroll.history','uses'=>'EmployeeController@payrollHistory']);
  });

   Route::group(['middleware'=>['role:audit|finance|admin|hr-manager']], function(){
    Route::get('payroll-history', ['as'=>'payroll.history','uses'=>'EmployeeController@payrollHistory']);
    Route::get('get-payroll-history',['as'=>'get.payroll.history','uses'=>'EmployeeController@getPayrollHistory']);
    Route::get('leave-deduction-report',['as'=>'leave.deduction.report','uses'=>'ReportController@leaveDeductionReport']);
    Route::get('get-leave-deduction-report',['as'=>'get.leave.deduction.report','uses'=>'ReportController@getLeaveDeductionReport']);
    Route::get('loan-deduction-report',['as'=>'loan.deduction.report','uses'=>'ReportController@loanDeductionReport']);
    Route::get('get-loan-deduction-report',['as'=>'get.loan.deduction.report','uses'=>'ReportController@getLoanDeductionReport']);
    Route::get('item-deduction-report',['as'=>'item.deduction.report','uses'=>'ReportController@itemDeductionReport']);
    Route::get('get-item-deduction-report',['as'=>'get.item.deduction.report','uses'=>'ReportController@getItemDeductionReport']);
    Route::get('all-employee-leave-list',['as'=>'all.employee.leave.list.view','uses'=>'LeaveController@allEmployeeLeaveListView']);
    Route::get('get-all-employee-leave-list',['as'=>'all.employee.leave.list','uses'=>'LeaveController@allEmployeeLeaveList']);
    Route::get('all-employee-leave-balance',['as'=>'all.employee.leave.balance','uses'=>'LeaveController@allEmployeeLeaveBalance']);
    Route::post('get-all-employee-leave-balance',['as'=>'get.all.employee.leave.balance','uses'=>'LeaveController@getAllEmployeeLeaveBalance']);
    Route::get('payback-leave-list',['as'=>'payback.leave.list','uses'=>'LeaveController@payBackLeavesList']);
    Route::get('get-payback-leave-list',['as'=>'get.payback.leave.list','uses'=>'LeaveController@getPayBackLeavesList']);
    Route::get('audit-attendance',['as'=>'audit.attendance','uses'=>'AttendanceController@auditAttendance']);
    Route::post('emp-attendance-audit',['as'=>'emp.attendance.audit','uses'=>'AttendanceController@employeeAttendance']);
    Route::get('company-leave',['as'=>'company.leave','uses'=>'LeaveController@index']);
    Route::get('company-all-leave',['as'=>'company.all.leave','uses'=>'LeaveController@getAllCompanyLeaves']);
    Route::get('sub-company',['as'=>'sub.company','uses'=>'LeaveController@getSubCompany']);
    Route::get('adv-sal-list',['as'=>'adv.sal.list','uses'=>'LoanController@getAdvSalList']);
    Route::get('get-adv-sal-list',['as'=>'get.adv.sal.list','uses'=>'LoanController@getAdvSalaryList']);
    Route::get('package-detail', ['as'=>'package.detail', 'uses'=>'PackageController@index']);
    Route::get('get-package-detail', ['as'=>'get.package.detail', 'uses'=>'PackageController@getPackageDetail']);
	Route::get('package', ['as'=>'package', 'uses'=>'PackageController@generatePackage']);
   });

});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');



